const bodyParser = require("body-parser");
const express = require("express")
const app = express();
const port = 3000;
// создаем парсер для данных application/x-www-form-urlencoded
const urlencodedParser = bodyParser.urlencoded({extended: false});
console.log("folder parsing")

app.get("/index", urlencodedParser, function (request, response) {
   
    response.sendFile(__dirname+"/index.html",);

});
app.post("/", urlencodedParser, function (request, response) {
    console.log(request.body)
    if(!request.body) return response.sendStatus(400);
    console.log(response.statusCode)
    response.sendFile(__dirname+"/index.html",);
    //response.send(`${request.body.userName} - ${request.body.userAge} - ${request.body.vin} - ${request.body.spare}- ${request.body.phone} - ${request.body.name}`);
    
});
  
app.get("/", function(request, response){
  
    response.send(`server is listening on ${port}`);

});
  
app.listen(port);