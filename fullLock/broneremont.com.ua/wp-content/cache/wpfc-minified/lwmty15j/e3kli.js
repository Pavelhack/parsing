jQuery(document).ready(function($){
$.datepicker.setDefaults({
closeText: 'Закрыть',
prevText: '<Пред',
nextText: 'След>',
currentText: 'Сегодня',
monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
weekHeader: 'Нед',
dateFormat: 'dd-mm-yy',
firstDay: 1,
showAnim: 'slideDown',
isRTL: false,
showMonthAfterYear: false,
yearSuffix: ''
});
$(".elementor-button-link").addClass("cms30_link");
function cms30_open_modal(){
$('.cms30_link').click(function(){
var form_id=$(this).attr("href");
$(form_id).fadeIn(300);
var cms30_phone_mask=$(form_id+" .cms30_phone_mask").val();
$(form_id+" .cms30_phone").mask(cms30_phone_mask);
$(form_id+" .cms30_time").mask('99:99');
$(form_id+" .cms30_date").datepicker();
return false;
});
}
function cms30_close_modal(){
$('.cms30_close_modal_min').click(function(){
$('.cms30_modal_wrapper').fadeOut(300);
return false;
});
}
cms30_open_modal();
cms30_close_modal();
function isValidEmailAddress(emailAddress){
var pattern=new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
return pattern.test(emailAddress);
}
$(".cms30_form_agreement").change(function(){
var form_id=$(this).prop("id");
if($(this).is(':checked')){
$(".cms30_callback_form#"+form_id+" button").prop('disabled', false);
}else{
$(".cms30_callback_form#"+form_id+" button").prop('disabled', true);
}});
$('.cms30_callback_form button').click(function(){
var form_id=$(this).prop("id");
var cms30_name=$("#cms30_"+form_id+" .cms30_name").val();
var cms30_email=$("#cms30_"+form_id+" .cms30_email").val();
var cms30_phone=$("#cms30_"+form_id+" .cms30_phone").val();
var cms30_date=$("#cms30_"+form_id+" .cms30_date").val();
var cms30_time=$("#cms30_"+form_id+" .cms30_time").val();
var cms30_comment=$("#cms30_"+form_id+" .cms30_comment").val();
var cms30_name_required=$("#cms30_"+form_id+" .cms30_name_required").val();
var cms30_comment_required=$("#cms30_"+form_id+" .cms30_comment_required").val();
var cms30_date_required=$("#cms30_"+form_id+" .cms30_date_required").val();
var cms30_time_required=$("#cms30_"+form_id+" .cms30_time_required").val();
var cms30_msg=$("#cms30_"+form_id+" .cms30_form_msg").val();
var cms30_page=$("#cms30_"+form_id+" .cms30_page").val();
var cms30_select=$("#cms30_"+form_id+" .cms30_select").val();
if(cms30_name_required=='y'){
if(cms30_name==''){
$("#cms30_"+form_id+" .cms30_name").css({'border':'#e86363 solid 1px'});
return false;
}else{
$("#cms30_"+form_id+" .cms30_name").css({'border':'#DDDDDD solid 1px'});
}}
if(typeof cms30_email!="undefined"){
if(cms30_email==''){
$("#cms30_"+form_id+" .cms30_email").css({'border':'#e86363 solid 1px'});
return false;
}else{
if(isValidEmailAddress(cms30_email)){
$("#cms30_"+form_id+" .cms30_email").css({'border':'#DDDDDD solid 1px'});
}else{
$("#cms30_"+form_id+" .cms30_email").css({'border':'#e86363 solid 1px'});
return false;
}}
}
if(cms30_phone==''){
$("#cms30_"+form_id+" .cms30_phone").css({'border':'#e86363 solid 1px'});
return false;
}else{
$("#cms30_"+form_id+" .cms30_phone").css({'border':'#DDDDDD solid 1px'});
}
if(cms30_date_required=='y'){
if(cms30_date==''){
$("#cms30_"+form_id+" .cms30_date").css({'border':'#e86363 solid 1px'});
return false;
}else{
$("#cms30_"+form_id+" .cms30_date").css({'border':'#DDDDDD solid 1px'});
}}
if(cms30_time_required=='y'){
if(cms30_time==''){
$("#cms30_"+form_id+" .cms30_time").css({'border':'#e86363 solid 1px'});
return false;
}else{
$("#cms30_"+form_id+" .cms30_time").css({'border':'#DDDDDD solid 1px'});
}}
if(cms30_comment_required=='y'){
if(cms30_comment==''){
$("#cms30_"+form_id+" .cms30_comment").css({'border':'#e86363 solid 1px'});
return false;
}else{
$("#cms30_"+form_id+" .cms30_comment").css({'border':'#DDDDDD solid 1px'});
}}
jQuery.getJSON(ajax_object.ajax_url,{
'action':'cms30_send',
'id': form_id,
'name': cms30_name,
'email': cms30_email,
'phone': cms30_phone,
'comment': cms30_comment,
'page': cms30_page,
'date': cms30_date,
'time': cms30_time,
'select': cms30_select,
},
function(data){
cms30_close_modal();
$("#cms30_"+form_id+" .cms30_callback_form").html('<p>'+cms30_msg+'</p>');
}
);
return false;
});
$('.cms30_callback_form').on('keydown', function (){
var form_id=$(this).prop("id");
$("#cms30_"+form_id+" .cms30_name").css({'border':'#DDDDDD solid 1px'});
$("#cms30_"+form_id+" .cms30_phone").css({'border':'#DDDDDD solid 1px'});
$("#cms30_"+form_id+" .cms30_email").css({'border':'#DDDDDD solid 1px'});
$("#cms30_"+form_id+" .cms30_comment").css({'border':'#DDDDDD solid 1px'});
});
});