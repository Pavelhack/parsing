jQuery(document).ready(function(){
    jQuery('.callback-modal-btn').click(function(){
        jQuery('#callback-modal').addClass('show-modal');
        jQuery('body').addClass('fixed-body');
    });
    jQuery('.callback-modal__close').click(function(){
        jQuery('#callback-modal').removeClass('show-modal');
        jQuery('body').removeClass('fixed-body');
    });
    jQuery('#callback-modal .sender-phone').mask('+38 (999) 999-99-99');
    jQuery('#callback-modal .sender-btn').on('click', senderValidator);
    jQuery('#callback-modal .sender-phone').keypress(function(){jQuery('#callback-modal .err-val-phone').hide();});
    function phoneErr() {jQuery('#callback-modal .err-val-phone').show();}
    function senderValidator() {
        jQuery("#callback-modal .sender-phone").val() != '' ? senderStart() : phoneErr();
    }

    function senderStart() {
        if(jQuery('#callback-modal .sender-name').val() == '') jQuery('#callback-modal .sender-name').val('Имя не заполнено');
        jQuery.post(
            "/sender/callback-modal/core/sender.php", {
            "name": jQuery('#callback-modal .sender-name').val(), 
            "phone": jQuery('#callback-modal .sender-phone').val()
        }, ifSuccess);
    };

    function ifSuccess(data) {
        if(data == 0) { jQuery('#callback-modal .sender-err').show();} 
        else {
            jQuery('#callback-modal .sender-phone, #callback-modal .sender-name').val('');
            jQuery('#callback-modal .sender-done').show();
        }
    }
});