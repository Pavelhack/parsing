jQuery(document).ready(function(){
    function openModal() {
        if(!localStorage.getItem("discount shown")) {
            jQuery('#callback-sale').addClass('show-modal');
            localStorage.setItem("discount shown", "1");
        }
    }
    setTimeout(openModal, 5000);
    jQuery('.callback-sale__close').click(function(){
        jQuery('#callback-sale').removeClass('show-modal');
        jQuery('body').removeClass('fixed-body');
    });
    jQuery('#callback-sale .sender-phone').mask('+38 (999) 99-99-999');
    jQuery('#callback-sale .sender-btn').on('click', senderValidator);
    jQuery('#callback-sale .sender-phone').keypress(function(){jQuery('#callback-sale .err-val-phone').hide();});
    function phoneErr() {jQuery('#callback-sale .err-val-phone').show();}
    function senderValidator() {
        jQuery("#callback-sale .sender-phone").val() != '' ? senderStart() : phoneErr();
    }

    function senderStart() {
        jQuery.post(
            "/sender/callback-sale/core/sender.php", {
            "phone": jQuery('#callback-sale .sender-phone').val()
        }, ifSuccess);
    };

    function ifSuccess(data) {
        if(data == 0) { jQuery('#callback-sale .sender-err').show();} 
        else {
            jQuery('#callback-sale .sender-phone, #callback-sale .sender-name').val('');
            jQuery('#callback-sale .sender-done').show();
        }
    }
});