jQuery(document).ready(function(){
        jQuery('.request-modal-btn').click(function(){
        jQuery('#request-modal').addClass('show-modal');
        jQuery('body').addClass('fixed-body');
    });
    jQuery('.request-modal__close').click(function(){
        jQuery('#request-modal').removeClass('show-modal');
        jQuery('body').removeClass('fixed-body');
    });
    jQuery('#request-modal .sender-phone').mask('+38 (999) 99-99-999');
    jQuery('#request-modal .sender-btn').on('click', senderValidator);
    jQuery('#request-modal .sender-phone').keypress(function(){jQuery('#request-modal .err-val-phone').hide();});
    jQuery('#request-modal .sender-name').keypress(function() {
        if(jQuery('#request-modal .sender-name').val().length >= 40) return false;
    });
    function phoneErr() {jQuery('#request-modal .err-val-phone').show();}
    function senderValidator() {
        jQuery("#request-modal .sender-phone").val() != '' ? senderStart() : phoneErr();
    }

    function senderStart() {
        if(jQuery('#request-modal .sender-name').val() == '') jQuery('#request-modal .sender-name').val('Имя не заполнено');
        jQuery.post(
            "/sender/request-modal/core/sender.php", {
            "name": jQuery('#request-modal .sender-name').val(), 
            "email": jQuery('#request-modal .sender-email').val(),
            "phone": jQuery('#request-modal .sender-phone').val(),
            "message": jQuery('#request-modal .sender-message').val()
        }, ifSuccess);
    };

    function ifSuccess(data) {
        if(data == 0) { jQuery('#request-modal .sender-err').show();} 
        else {
            jQuery('#request-modal .sender-name, #request-modal .sender-email, #request-modal .sender-phone, #request-modal .sender-message').val('');
            jQuery('#request-modal .sender-done').show();
        }
    }
});