jQuery(document).ready(function(){
    jQuery('#request-open .sender-phone').mask('+38 (999) 99-99-999');
    jQuery('#request-open .sender-btn').on('click', senderValidator);
    jQuery('#request-open .sender-phone').keypress(function(){jQuery('#request-open .err-val-phone').hide();});
    jQuery('#request-open .sender-name').keypress(function() {
        if(jQuery('#request-open .sender-name').val().length >= 40) return false;
    });
    function phoneErr() {jQuery('#request-open .err-val-phone').show();}
    function senderValidator() {
        jQuery("#request-open .sender-phone").val() != '' ? senderStart() : phoneErr();
    }

    function senderStart() {
        if(jQuery('#request-open .sender-name').val() == '') jQuery('#request-open .sender-name').val('Имя не заполнено');
        jQuery.post(
            "/sender/request-open/core/sender.php", {
            "name": jQuery('#request-open .sender-name').val(), 
            "email": jQuery('#request-open .sender-email').val(),
            "phone": jQuery('#request-open .sender-phone').val(),
            "message": jQuery('#request-open .sender-message').val()
        }, ifSuccess);
    };

    function ifSuccess(data) {
        if(data == 0) { jQuery('#request-open .sender-err').show();} 
        else {
            jQuery('#request-open .sender-name, #request-open .sender-email, #request-open .sender-phone, #request-open .sender-message').val('');
            jQuery('#request-open .sender-done').show();
        }
    }
});