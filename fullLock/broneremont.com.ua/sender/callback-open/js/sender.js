jQuery(document).ready(function(){
    jQuery('#callback-open .sender-phone').mask('+38 (999) 99-99-999');
    jQuery('#callback-open .sender-btn').on('click', senderValidator);
    jQuery('#callback-open .sender-phone').keypress(function(){jQuery('#callback-open .err-val-phone').hide();});
    function phoneErr() {jQuery('#callback-open .err-val-phone').show();}
    function senderValidator() {
        jQuery("#callback-open .sender-phone").val() != '' ? senderStart() : phoneErr();
    }

    function senderStart() {
        if(jQuery('#callback-open .sender-name').val() == '') jQuery('#callback-open .sender-name').val('Имя не заполнено');
        jQuery.post(
            "/sender/callback-open/core/sender.php", {
            "name": jQuery('#callback-open .sender-name').val(), 
            "phone": jQuery('#callback-open .sender-phone').val()
        }, ifSuccess);
    };

    function ifSuccess(data) {
        if(data == 0) { jQuery('#callback-open .sender-err').show();} 
        else {
            jQuery('#callback-open .sender-phone, #callback-open .sender-name').val('');
            jQuery('#callback-open .sender-done').show();
        }
    }
});