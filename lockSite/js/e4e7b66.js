! function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
    "use strict";

    function n(e, t) {
        t = t || ne;
        var n = t.createElement("script");
        n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
    }

    function i(e) {
        var t = !!e && "length" in e && e.length,
            n = ge.type(e);
        return "function" !== n && !ge.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function r(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    function o(e, t, n) {
        return ge.isFunction(t) ? ge.grep(e, function (e, i) {
            return !!t.call(e, i, e) !== n
        }) : t.nodeType ? ge.grep(e, function (e) {
            return e === t !== n
        }) : "string" != typeof t ? ge.grep(e, function (e) {
            return ae.call(t, e) > -1 !== n
        }) : Se.test(t) ? ge.filter(t, e, n) : (t = ge.filter(t, e), ge.grep(e, function (e) {
            return ae.call(t, e) > -1 !== n && 1 === e.nodeType
        }))
    }

    function s(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e
    }

    function a(e) {
        var t = {};
        return ge.each(e.match(je) || [], function (e, n) {
            t[n] = !0
        }), t
    }

    function l(e) {
        return e
    }

    function c(e) {
        throw e
    }

    function u(e, t, n, i) {
        var r;
        try {
            e && ge.isFunction(r = e.promise) ? r.call(e).done(t).fail(n) : e && ge.isFunction(r = e.then) ? r.call(e, t, n) : t.apply(void 0, [e].slice(i))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    function h() {
        ne.removeEventListener("DOMContentLoaded", h), e.removeEventListener("load", h), ge.ready()
    }

    function p() {
        this.expando = ge.expando + p.uid++
    }

    function d(e) {
        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : He.test(e) ? JSON.parse(e) : e)
    }

    function f(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType)
            if (i = "data-" + t.replace(ze, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(i))) {
                try {
                    n = d(n)
                } catch (e) {}
                Le.set(e, t, n)
            } else n = void 0;
        return n
    }

    function g(e, t, n, i) {
        var r, o = 1,
            s = 20,
            a = i ? function () {
                return i.cur()
            } : function () {
                return ge.css(e, t, "")
            },
            l = a(),
            c = n && n[3] || (ge.cssNumber[t] ? "" : "px"),
            u = (ge.cssNumber[t] || "px" !== c && +l) && qe.exec(ge.css(e, t));
        if (u && u[3] !== c) {
            c = c || u[3], n = n || [], u = +l || 1;
            do {
                o = o || ".5", u /= o, ge.style(e, t, u + c)
            } while (o !== (o = a() / l) && 1 !== o && --s)
        }
        return n && (u = +u || +l || 0, r = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = r)), r
    }

    function m(e) {
        var t, n = e.ownerDocument,
            i = e.nodeName,
            r = We[i];
        return r || (t = n.body.appendChild(n.createElement(i)), r = ge.css(t, "display"), t.parentNode.removeChild(t), "none" === r && (r = "block"), We[i] = r, r)
    }

    function v(e, t) {
        for (var n, i, r = [], o = 0, s = e.length; o < s; o++) i = e[o], i.style && (n = i.style.display, t ? ("none" === n && (r[o] = Ie.get(i, "display") || null, r[o] || (i.style.display = "")), "" === i.style.display && Re(i) && (r[o] = m(i))) : "none" !== n && (r[o] = "none", Ie.set(i, "display", n)));
        for (o = 0; o < s; o++) null != r[o] && (e[o].style.display = r[o]);
        return e
    }

    function y(e, t) {
        var n;
        return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && r(e, t) ? ge.merge([e], n) : n
    }

    function w(e, t) {
        for (var n = 0, i = e.length; n < i; n++) Ie.set(e[n], "globalEval", !t || Ie.get(t[n], "globalEval"))
    }

    function x(e, t, n, i, r) {
        for (var o, s, a, l, c, u, h = t.createDocumentFragment(), p = [], d = 0, f = e.length; d < f; d++)
            if ((o = e[d]) || 0 === o)
                if ("object" === ge.type(o)) ge.merge(p, o.nodeType ? [o] : o);
                else if (Ve.test(o)) {
            for (s = s || h.appendChild(t.createElement("div")), a = (Xe.exec(o) || ["", ""])[1].toLowerCase(), l = Ue[a] || Ue._default, s.innerHTML = l[1] + ge.htmlPrefilter(o) + l[2], u = l[0]; u--;) s = s.lastChild;
            ge.merge(p, s.childNodes), s = h.firstChild, s.textContent = ""
        } else p.push(t.createTextNode(o));
        for (h.textContent = "", d = 0; o = p[d++];)
            if (i && ge.inArray(o, i) > -1) r && r.push(o);
            else if (c = ge.contains(o.ownerDocument, o), s = y(h.appendChild(o), "script"), c && w(s), n)
            for (u = 0; o = s[u++];) Qe.test(o.type || "") && n.push(o);
        return h
    }

    function b() {
        return !0
    }

    function _() {
        return !1
    }

    function C() {
        try {
            return ne.activeElement
        } catch (e) {}
    }

    function T(e, t, n, i, r, o) {
        var s, a;
        if ("object" == typeof t) {
            "string" != typeof n && (i = i || n, n = void 0);
            for (a in t) T(e, a, n, i, t[a], o);
            return e
        }
        if (null == i && null == r ? (r = n, i = n = void 0) : null == r && ("string" == typeof n ? (r = i, i = void 0) : (r = i, i = n, n = void 0)), !1 === r) r = _;
        else if (!r) return e;
        return 1 === o && (s = r, r = function (e) {
            return ge().off(e), s.apply(this, arguments)
        }, r.guid = s.guid || (s.guid = ge.guid++)), e.each(function () {
            ge.event.add(this, t, r, i, n)
        })
    }

    function S(e, t) {
        return r(e, "table") && r(11 !== t.nodeType ? t : t.firstChild, "tr") ? ge(">tbody", e)[0] || e : e
    }

    function E(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function k(e) {
        var t = it.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function A(e, t) {
        var n, i, r, o, s, a, l, c;
        if (1 === t.nodeType) {
            if (Ie.hasData(e) && (o = Ie.access(e), s = Ie.set(t, o), c = o.events)) {
                delete s.handle, s.events = {};
                for (r in c)
                    for (n = 0, i = c[r].length; n < i; n++) ge.event.add(t, r, c[r][n])
            }
            Le.hasData(e) && (a = Le.access(e), l = ge.extend({}, a), Le.set(t, l))
        }
    }

    function D(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Ye.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }

    function j(e, t, i, r) {
        t = oe.apply([], t);
        var o, s, a, l, c, u, h = 0,
            p = e.length,
            d = p - 1,
            f = t[0],
            g = ge.isFunction(f);
        if (g || p > 1 && "string" == typeof f && !de.checkClone && nt.test(f)) return e.each(function (n) {
            var o = e.eq(n);
            g && (t[0] = f.call(this, n, o.html())), j(o, t, i, r)
        });
        if (p && (o = x(t, e[0].ownerDocument, !1, e, r), s = o.firstChild, 1 === o.childNodes.length && (o = s), s || r)) {
            for (a = ge.map(y(o, "script"), E), l = a.length; h < p; h++) c = o, h !== d && (c = ge.clone(c, !0, !0), l && ge.merge(a, y(c, "script"))), i.call(e[h], c, h);
            if (l)
                for (u = a[a.length - 1].ownerDocument, ge.map(a, k), h = 0; h < l; h++) c = a[h], Qe.test(c.type || "") && !Ie.access(c, "globalEval") && ge.contains(u, c) && (c.src ? ge._evalUrl && ge._evalUrl(c.src) : n(c.textContent.replace(rt, ""), u))
        }
        return e
    }

    function N(e, t, n) {
        for (var i, r = t ? ge.filter(t, e) : e, o = 0; null != (i = r[o]); o++) n || 1 !== i.nodeType || ge.cleanData(y(i)), i.parentNode && (n && ge.contains(i.ownerDocument, i) && w(y(i, "script")), i.parentNode.removeChild(i));
        return e
    }

    function $(e, t, n) {
        var i, r, o, s, a = e.style;
        return n = n || at(e), n && (s = n.getPropertyValue(t) || n[t], "" !== s || ge.contains(e.ownerDocument, e) || (s = ge.style(e, t)), !de.pixelMarginRight() && st.test(s) && ot.test(t) && (i = a.width, r = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = r, a.maxWidth = o)), void 0 !== s ? s + "" : s
    }

    function O(e, t) {
        return {
            get: function () {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function P(e) {
        if (e in dt) return e;
        for (var t = e[0].toUpperCase() + e.slice(1), n = pt.length; n--;)
            if ((e = pt[n] + t) in dt) return e
    }

    function I(e) {
        var t = ge.cssProps[e];
        return t || (t = ge.cssProps[e] = P(e) || e), t
    }

    function L(e, t, n) {
        var i = qe.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
    }

    function H(e, t, n, i, r) {
        var o, s = 0;
        for (o = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; o < 4; o += 2) "margin" === n && (s += ge.css(e, n + Fe[o], !0, r)), i ? ("content" === n && (s -= ge.css(e, "padding" + Fe[o], !0, r)), "margin" !== n && (s -= ge.css(e, "border" + Fe[o] + "Width", !0, r))) : (s += ge.css(e, "padding" + Fe[o], !0, r), "padding" !== n && (s += ge.css(e, "border" + Fe[o] + "Width", !0, r)));
        return s
    }

    function z(e, t, n) {
        var i, r = at(e),
            o = $(e, t, r),
            s = "border-box" === ge.css(e, "boxSizing", !1, r);
        return st.test(o) ? o : (i = s && (de.boxSizingReliable() || o === e.style[t]), "auto" === o && (o = e["offset" + t[0].toUpperCase() + t.slice(1)]), (o = parseFloat(o) || 0) + H(e, t, n || (s ? "border" : "content"), i, r) + "px")
    }

    function M(e, t, n, i, r) {
        return new M.prototype.init(e, t, n, i, r)
    }

    function q() {
        gt && (!1 === ne.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(q) : e.setTimeout(q, ge.fx.interval), ge.fx.tick())
    }

    function F() {
        return e.setTimeout(function () {
            ft = void 0
        }), ft = ge.now()
    }

    function R(e, t) {
        var n, i = 0,
            r = {
                height: e
            };
        for (t = t ? 1 : 0; i < 4; i += 2 - t) n = Fe[i], r["margin" + n] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }

    function B(e, t, n) {
        for (var i, r = (X.tweeners[t] || []).concat(X.tweeners["*"]), o = 0, s = r.length; o < s; o++)
            if (i = r[o].call(n, t, e)) return i
    }

    function W(e, t, n) {
        var i, r, o, s, a, l, c, u, h = "width" in t || "height" in t,
            p = this,
            d = {},
            f = e.style,
            g = e.nodeType && Re(e),
            m = Ie.get(e, "fxshow");
        n.queue || (s = ge._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
            s.unqueued || a()
        }), s.unqueued++, p.always(function () {
            p.always(function () {
                s.unqueued--, ge.queue(e, "fx").length || s.empty.fire()
            })
        }));
        for (i in t)
            if (r = t[i], mt.test(r)) {
                if (delete t[i], o = o || "toggle" === r, r === (g ? "hide" : "show")) {
                    if ("show" !== r || !m || void 0 === m[i]) continue;
                    g = !0
                }
                d[i] = m && m[i] || ge.style(e, i)
            } if ((l = !ge.isEmptyObject(t)) || !ge.isEmptyObject(d)) {
            h && 1 === e.nodeType && (n.overflow = [f.overflow, f.overflowX, f.overflowY], c = m && m.display, null == c && (c = Ie.get(e, "display")), u = ge.css(e, "display"), "none" === u && (c ? u = c : (v([e], !0), c = e.style.display || c, u = ge.css(e, "display"), v([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === ge.css(e, "float") && (l || (p.done(function () {
                f.display = c
            }), null == c && (u = f.display, c = "none" === u ? "" : u)), f.display = "inline-block")), n.overflow && (f.overflow = "hidden", p.always(function () {
                f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
            })), l = !1;
            for (i in d) l || (m ? "hidden" in m && (g = m.hidden) : m = Ie.access(e, "fxshow", {
                display: c
            }), o && (m.hidden = !g), g && v([e], !0), p.done(function () {
                g || v([e]), Ie.remove(e, "fxshow");
                for (i in d) ge.style(e, i, d[i])
            })), l = B(g ? m[i] : 0, i, p), i in m || (m[i] = l.start, g && (l.end = l.start, l.start = 0))
        }
    }

    function Y(e, t) {
        var n, i, r, o, s;
        for (n in e)
            if (i = ge.camelCase(n), r = t[i], o = e[n], Array.isArray(o) && (r = o[1], o = e[n] = o[0]), n !== i && (e[i] = o, delete e[n]), (s = ge.cssHooks[i]) && "expand" in s) {
                o = s.expand(o), delete e[i];
                for (n in o) n in e || (e[n] = o[n], t[n] = r)
            } else t[i] = r
    }

    function X(e, t, n) {
        var i, r, o = 0,
            s = X.prefilters.length,
            a = ge.Deferred().always(function () {
                delete l.elem
            }),
            l = function () {
                if (r) return !1;
                for (var t = ft || F(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, o = 1 - i, s = 0, l = c.tweens.length; s < l; s++) c.tweens[s].run(o);
                return a.notifyWith(e, [c, o, n]), o < 1 && l ? n : (l || a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c]), !1)
            },
            c = a.promise({
                elem: e,
                props: ge.extend({}, t),
                opts: ge.extend(!0, {
                    specialEasing: {},
                    easing: ge.easing._default
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: ft || F(),
                duration: n.duration,
                tweens: [],
                createTween: function (t, n) {
                    var i = ge.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(i), i
                },
                stop: function (t) {
                    var n = 0,
                        i = t ? c.tweens.length : 0;
                    if (r) return this;
                    for (r = !0; n < i; n++) c.tweens[n].run(1);
                    return t ? (a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c, t])) : a.rejectWith(e, [c, t]), this
                }
            }),
            u = c.props;
        for (Y(u, c.opts.specialEasing); o < s; o++)
            if (i = X.prefilters[o].call(c, e, u, c.opts)) return ge.isFunction(i.stop) && (ge._queueHooks(c.elem, c.opts.queue).stop = ge.proxy(i.stop, i)), i;
        return ge.map(u, B, c), ge.isFunction(c.opts.start) && c.opts.start.call(e, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), ge.fx.timer(ge.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c
    }

    function Q(e) {
        return (e.match(je) || []).join(" ")
    }

    function U(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function V(e, t, n, i) {
        var r;
        if (Array.isArray(t)) ge.each(t, function (t, r) {
            n || kt.test(e) ? i(e, r) : V(e + "[" + ("object" == typeof r && null != r ? t : "") + "]", r, n, i)
        });
        else if (n || "object" !== ge.type(t)) i(e, t);
        else
            for (r in t) V(e + "[" + r + "]", t[r], n, i)
    }

    function K(e) {
        return function (t, n) {
            "string" != typeof t && (n = t, t = "*");
            var i, r = 0,
                o = t.toLowerCase().match(je) || [];
            if (ge.isFunction(n))
                for (; i = o[r++];) "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
        }
    }

    function Z(e, t, n, i) {
        function r(a) {
            var l;
            return o[a] = !0, ge.each(e[a] || [], function (e, a) {
                var c = a(t, n, i);
                return "string" != typeof c || s || o[c] ? s ? !(l = c) : void 0 : (t.dataTypes.unshift(c), r(c), !1)
            }), l
        }
        var o = {},
            s = e === Mt;
        return r(t.dataTypes[0]) || !o["*"] && r("*")
    }

    function G(e, t) {
        var n, i, r = ge.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((r[n] ? e : i || (i = {}))[n] = t[n]);
        return i && ge.extend(!0, e, i), e
    }

    function J(e, t, n) {
        for (var i, r, o, s, a = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (r in a)
                if (a[r] && a[r].test(i)) {
                    l.unshift(r);
                    break
                } if (l[0] in n) o = l[0];
        else {
            for (r in n) {
                if (!l[0] || e.converters[r + " " + l[0]]) {
                    o = r;
                    break
                }
                s || (s = r)
            }
            o = o || s
        }
        if (o) return o !== l[0] && l.unshift(o), n[o]
    }

    function ee(e, t, n, i) {
        var r, o, s, a, l, c = {},
            u = e.dataTypes.slice();
        if (u[1])
            for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        for (o = u.shift(); o;)
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = o, o = u.shift())
                if ("*" === o) o = l;
                else if ("*" !== l && l !== o) {
            if (!(s = c[l + " " + o] || c["* " + o]))
                for (r in c)
                    if (a = r.split(" "), a[1] === o && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                        !0 === s ? s = c[r] : !0 !== c[r] && (o = a[0], u.unshift(a[1]));
                        break
                    } if (!0 !== s)
                if (s && e.throws) t = s(t);
                else try {
                    t = s(t)
                } catch (e) {
                    return {
                        state: "parsererror",
                        error: s ? e : "No conversion from " + l + " to " + o
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }
    var te = [],
        ne = e.document,
        ie = Object.getPrototypeOf,
        re = te.slice,
        oe = te.concat,
        se = te.push,
        ae = te.indexOf,
        le = {},
        ce = le.toString,
        ue = le.hasOwnProperty,
        he = ue.toString,
        pe = he.call(Object),
        de = {},
        fe = "3.2.1",
        ge = function (e, t) {
            return new ge.fn.init(e, t)
        },
        me = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ve = /^-ms-/,
        ye = /-([a-z])/g,
        we = function (e, t) {
            return t.toUpperCase()
        };
    ge.fn = ge.prototype = {
        jquery: fe,
        constructor: ge,
        length: 0,
        toArray: function () {
            return re.call(this)
        },
        get: function (e) {
            return null == e ? re.call(this) : e < 0 ? this[e + this.length] : this[e]
        },
        pushStack: function (e) {
            var t = ge.merge(this.constructor(), e);
            return t.prevObject = this, t
        },
        each: function (e) {
            return ge.each(this, e)
        },
        map: function (e) {
            return this.pushStack(ge.map(this, function (t, n) {
                return e.call(t, n, t)
            }))
        },
        slice: function () {
            return this.pushStack(re.apply(this, arguments))
        },
        first: function () {
            return this.eq(0)
        },
        last: function () {
            return this.eq(-1)
        },
        eq: function (e) {
            var t = this.length,
                n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
        },
        end: function () {
            return this.prevObject || this.constructor()
        },
        push: se,
        sort: te.sort,
        splice: te.splice
    }, ge.extend = ge.fn.extend = function () {
        var e, t, n, i, r, o, s = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || ge.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++)
            if (null != (e = arguments[a]))
                for (t in e) n = s[t], i = e[t], s !== i && (c && i && (ge.isPlainObject(i) || (r = Array.isArray(i))) ? (r ? (r = !1, o = n && Array.isArray(n) ? n : []) : o = n && ge.isPlainObject(n) ? n : {}, s[t] = ge.extend(c, o, i)) : void 0 !== i && (s[t] = i));
        return s
    }, ge.extend({
        expando: "jQuery" + (fe + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function (e) {
            throw new Error(e)
        },
        noop: function () {},
        isFunction: function (e) {
            return "function" === ge.type(e)
        },
        isWindow: function (e) {
            return null != e && e === e.window
        },
        isNumeric: function (e) {
            var t = ge.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        },
        isPlainObject: function (e) {
            var t, n;
            return !(!e || "[object Object]" !== ce.call(e) || (t = ie(e)) && ("function" != typeof (n = ue.call(t, "constructor") && t.constructor) || he.call(n) !== pe))
        },
        isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        type: function (e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? le[ce.call(e)] || "object" : typeof e
        },
        globalEval: function (e) {
            n(e)
        },
        camelCase: function (e) {
            return e.replace(ve, "ms-").replace(ye, we)
        },
        each: function (e, t) {
            var n, r = 0;
            if (i(e))
                for (n = e.length; r < n && !1 !== t.call(e[r], r, e[r]); r++);
            else
                for (r in e)
                    if (!1 === t.call(e[r], r, e[r])) break;
            return e
        },
        trim: function (e) {
            return null == e ? "" : (e + "").replace(me, "")
        },
        makeArray: function (e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? ge.merge(n, "string" == typeof e ? [e] : e) : se.call(n, e)), n
        },
        inArray: function (e, t, n) {
            return null == t ? -1 : ae.call(t, e, n)
        },
        merge: function (e, t) {
            for (var n = +t.length, i = 0, r = e.length; i < n; i++) e[r++] = t[i];
            return e.length = r, e
        },
        grep: function (e, t, n) {
            for (var i = [], r = 0, o = e.length, s = !n; r < o; r++) !t(e[r], r) !== s && i.push(e[r]);
            return i
        },
        map: function (e, t, n) {
            var r, o, s = 0,
                a = [];
            if (i(e))
                for (r = e.length; s < r; s++) null != (o = t(e[s], s, n)) && a.push(o);
            else
                for (s in e) null != (o = t(e[s], s, n)) && a.push(o);
            return oe.apply([], a)
        },
        guid: 1,
        proxy: function (e, t) {
            var n, i, r;
            if ("string" == typeof t && (n = e[t], t = e, e = n), ge.isFunction(e)) return i = re.call(arguments, 2), r = function () {
                return e.apply(t || this, i.concat(re.call(arguments)))
            }, r.guid = e.guid = e.guid || ge.guid++, r
        },
        now: Date.now,
        support: de
    }), "function" == typeof Symbol && (ge.fn[Symbol.iterator] = te[Symbol.iterator]), ge.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
        le["[object " + t + "]"] = t.toLowerCase()
    });
    var xe = function (e) {
        function t(e, t, n, i) {
            var r, o, s, a, l, c, u, p = t && t.ownerDocument,
                f = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== f && 9 !== f && 11 !== f) return n;
            if (!i && ((t ? t.ownerDocument || t : F) !== O && $(t), t = t || O, I)) {
                if (11 !== f && (l = ve.exec(e)))
                    if (r = l[1]) {
                        if (9 === f) {
                            if (!(s = t.getElementById(r))) return n;
                            if (s.id === r) return n.push(s), n
                        } else if (p && (s = p.getElementById(r)) && M(t, s) && s.id === r) return n.push(s), n
                    } else {
                        if (l[2]) return G.apply(n, t.getElementsByTagName(e)), n;
                        if ((r = l[3]) && _.getElementsByClassName && t.getElementsByClassName) return G.apply(n, t.getElementsByClassName(r)), n
                    } if (_.qsa && !X[e + " "] && (!L || !L.test(e))) {
                    if (1 !== f) p = t, u = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((a = t.getAttribute("id")) ? a = a.replace(be, _e) : t.setAttribute("id", a = q), c = E(e), o = c.length; o--;) c[o] = "#" + a + " " + d(c[o]);
                        u = c.join(","), p = ye.test(e) && h(t.parentNode) || t
                    }
                    if (u) try {
                        return G.apply(n, p.querySelectorAll(u)), n
                    } catch (e) {} finally {
                        a === q && t.removeAttribute("id")
                    }
                }
            }
            return A(e.replace(ae, "$1"), t, n, i)
        }

        function n() {
            function e(n, i) {
                return t.push(n + " ") > C.cacheLength && delete e[t.shift()], e[n + " "] = i
            }
            var t = [];
            return e
        }

        function i(e) {
            return e[q] = !0, e
        }

        function r(e) {
            var t = O.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function o(e, t) {
            for (var n = e.split("|"), i = n.length; i--;) C.attrHandle[n[i]] = t
        }

        function s(e, t) {
            var n = t && e,
                i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (i) return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function (t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function l(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function c(e) {
            return function (t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && Te(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function u(e) {
            return i(function (t) {
                return t = +t, i(function (n, i) {
                    for (var r, o = e([], n.length, t), s = o.length; s--;) n[r = o[s]] && (n[r] = !(i[r] = n[r]))
                })
            })
        }

        function h(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }

        function p() {}

        function d(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i
        }

        function f(e, t, n) {
            var i = t.dir,
                r = t.next,
                o = r || i,
                s = n && "parentNode" === o,
                a = B++;
            return t.first ? function (t, n, r) {
                for (; t = t[i];)
                    if (1 === t.nodeType || s) return e(t, n, r);
                return !1
            } : function (t, n, l) {
                var c, u, h, p = [R, a];
                if (l) {
                    for (; t = t[i];)
                        if ((1 === t.nodeType || s) && e(t, n, l)) return !0
                } else
                    for (; t = t[i];)
                        if (1 === t.nodeType || s)
                            if (h = t[q] || (t[q] = {}), u = h[t.uniqueID] || (h[t.uniqueID] = {}), r && r === t.nodeName.toLowerCase()) t = t[i] || t;
                            else {
                                if ((c = u[o]) && c[0] === R && c[1] === a) return p[2] = c[2];
                                if (u[o] = p, p[2] = e(t, n, l)) return !0
                            } return !1
            }
        }

        function g(e) {
            return e.length > 1 ? function (t, n, i) {
                for (var r = e.length; r--;)
                    if (!e[r](t, n, i)) return !1;
                return !0
            } : e[0]
        }

        function m(e, n, i) {
            for (var r = 0, o = n.length; r < o; r++) t(e, n[r], i);
            return i
        }

        function v(e, t, n, i, r) {
            for (var o, s = [], a = 0, l = e.length, c = null != t; a < l; a++)(o = e[a]) && (n && !n(o, i, r) || (s.push(o), c && t.push(a)));
            return s
        }

        function y(e, t, n, r, o, s) {
            return r && !r[q] && (r = y(r)), o && !o[q] && (o = y(o, s)), i(function (i, s, a, l) {
                var c, u, h, p = [],
                    d = [],
                    f = s.length,
                    g = i || m(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !i && t ? g : v(g, p, e, a, l),
                    w = n ? o || (i ? e : f || r) ? [] : s : y;
                if (n && n(y, w, a, l), r)
                    for (c = v(w, d), r(c, [], a, l), u = c.length; u--;)(h = c[u]) && (w[d[u]] = !(y[d[u]] = h));
                if (i) {
                    if (o || e) {
                        if (o) {
                            for (c = [], u = w.length; u--;)(h = w[u]) && c.push(y[u] = h);
                            o(null, w = [], c, l)
                        }
                        for (u = w.length; u--;)(h = w[u]) && (c = o ? ee(i, h) : p[u]) > -1 && (i[c] = !(s[c] = h))
                    }
                } else w = v(w === s ? w.splice(f, w.length) : w), o ? o(null, s, w, l) : G.apply(s, w)
            })
        }

        function w(e) {
            for (var t, n, i, r = e.length, o = C.relative[e[0].type], s = o || C.relative[" "], a = o ? 1 : 0, l = f(function (e) {
                    return e === t
                }, s, !0), c = f(function (e) {
                    return ee(t, e) > -1
                }, s, !0), u = [function (e, n, i) {
                    var r = !o && (i || n !== D) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i));
                    return t = null, r
                }]; a < r; a++)
                if (n = C.relative[e[a].type]) u = [f(g(u), n)];
                else {
                    if (n = C.filter[e[a].type].apply(null, e[a].matches), n[q]) {
                        for (i = ++a; i < r && !C.relative[e[i].type]; i++);
                        return y(a > 1 && g(u), a > 1 && d(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(ae, "$1"), n, a < i && w(e.slice(a, i)), i < r && w(e = e.slice(i)), i < r && d(e))
                    }
                    u.push(n)
                } return g(u)
        }

        function x(e, n) {
            var r = n.length > 0,
                o = e.length > 0,
                s = function (i, s, a, l, c) {
                    var u, h, p, d = 0,
                        f = "0",
                        g = i && [],
                        m = [],
                        y = D,
                        w = i || o && C.find.TAG("*", c),
                        x = R += null == y ? 1 : Math.random() || .1,
                        b = w.length;
                    for (c && (D = s === O || s || c); f !== b && null != (u = w[f]); f++) {
                        if (o && u) {
                            for (h = 0, s || u.ownerDocument === O || ($(u), a = !I); p = e[h++];)
                                if (p(u, s || O, a)) {
                                    l.push(u);
                                    break
                                } c && (R = x)
                        }
                        r && ((u = !p && u) && d--, i && g.push(u))
                    }
                    if (d += f, r && f !== d) {
                        for (h = 0; p = n[h++];) p(g, m, s, a);
                        if (i) {
                            if (d > 0)
                                for (; f--;) g[f] || m[f] || (m[f] = K.call(l));
                            m = v(m)
                        }
                        G.apply(l, m), c && !i && m.length > 0 && d + n.length > 1 && t.uniqueSort(l)
                    }
                    return c && (R = x, D = y), g
                };
            return r ? i(s) : s
        }
        var b, _, C, T, S, E, k, A, D, j, N, $, O, P, I, L, H, z, M, q = "sizzle" + 1 * new Date,
            F = e.document,
            R = 0,
            B = 0,
            W = n(),
            Y = n(),
            X = n(),
            Q = function (e, t) {
                return e === t && (N = !0), 0
            },
            U = {}.hasOwnProperty,
            V = [],
            K = V.pop,
            Z = V.push,
            G = V.push,
            J = V.slice,
            ee = function (e, t) {
                for (var n = 0, i = e.length; n < i; n++)
                    if (e[n] === t) return n;
                return -1
            },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            ie = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            re = "\\[" + ne + "*(" + ie + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ie + "))|)" + ne + "*\\]",
            oe = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + re + ")*)|.*)\\)|)",
            se = new RegExp(ne + "+", "g"),
            ae = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            le = new RegExp("^" + ne + "*," + ne + "*"),
            ce = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            ue = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            he = new RegExp(oe),
            pe = new RegExp("^" + ie + "$"),
            de = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie + "|[*])"),
                ATTR: new RegExp("^" + re),
                PSEUDO: new RegExp("^" + oe),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            },
            fe = /^(?:input|select|textarea|button)$/i,
            ge = /^h\d$/i,
            me = /^[^{]+\{\s*\[native \w/,
            ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ye = /[+~]/,
            we = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            xe = function (e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            },
            be = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            _e = function (e, t) {
                return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            Ce = function () {
                $()
            },
            Te = f(function (e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            G.apply(V = J.call(F.childNodes), F.childNodes), V[F.childNodes.length].nodeType
        } catch (e) {
            G = {
                apply: V.length ? function (e, t) {
                    Z.apply(e, J.call(t))
                } : function (e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];);
                    e.length = n - 1
                }
            }
        }
        _ = t.support = {}, S = t.isXML = function (e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, $ = t.setDocument = function (e) {
            var t, n, i = e ? e.ownerDocument || e : F;
            return i !== O && 9 === i.nodeType && i.documentElement ? (O = i, P = O.documentElement, I = !S(O), F !== O && (n = O.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), _.attributes = r(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), _.getElementsByTagName = r(function (e) {
                return e.appendChild(O.createComment("")), !e.getElementsByTagName("*").length
            }), _.getElementsByClassName = me.test(O.getElementsByClassName), _.getById = r(function (e) {
                return P.appendChild(e).id = q, !O.getElementsByName || !O.getElementsByName(q).length
            }), _.getById ? (C.filter.ID = function (e) {
                var t = e.replace(we, xe);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }, C.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && I) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (C.filter.ID = function (e) {
                var t = e.replace(we, xe);
                return function (e) {
                    var n = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }, C.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && I) {
                    var n, i, r, o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                        for (r = t.getElementsByName(e), i = 0; o = r[i++];)
                            if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
                    }
                    return []
                }
            }), C.find.TAG = _.getElementsByTagName ? function (e, t) {
                return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : _.qsa ? t.querySelectorAll(e) : void 0
            } : function (e, t) {
                var n, i = [],
                    r = 0,
                    o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = o[r++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return o
            }, C.find.CLASS = _.getElementsByClassName && function (e, t) {
                if (void 0 !== t.getElementsByClassName && I) return t.getElementsByClassName(e)
            }, H = [], L = [], (_.qsa = me.test(O.querySelectorAll)) && (r(function (e) {
                P.appendChild(e).innerHTML = "<a id='" + q + "'></a><select id='" + q + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || L.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + q + "-]").length || L.push("~="), e.querySelectorAll(":checked").length || L.push(":checked"), e.querySelectorAll("a#" + q + "+*").length || L.push(".#.+[+~]")
            }), r(function (e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = O.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && L.push("name" + ne + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && L.push(":enabled", ":disabled"), P.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && L.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), L.push(",.*:")
            })), (_.matchesSelector = me.test(z = P.matches || P.webkitMatchesSelector || P.mozMatchesSelector || P.oMatchesSelector || P.msMatchesSelector)) && r(function (e) {
                _.disconnectedMatch = z.call(e, "*"), z.call(e, "[s!='']:x"), H.push("!=", oe)
            }), L = L.length && new RegExp(L.join("|")), H = H.length && new RegExp(H.join("|")), t = me.test(P.compareDocumentPosition), M = t || me.test(P.contains) ? function (e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function (e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, Q = t ? function (e, t) {
                if (e === t) return N = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !_.sortDetached && t.compareDocumentPosition(e) === n ? e === O || e.ownerDocument === F && M(F, e) ? -1 : t === O || t.ownerDocument === F && M(F, t) ? 1 : j ? ee(j, e) - ee(j, t) : 0 : 4 & n ? -1 : 1)
            } : function (e, t) {
                if (e === t) return N = !0, 0;
                var n, i = 0,
                    r = e.parentNode,
                    o = t.parentNode,
                    a = [e],
                    l = [t];
                if (!r || !o) return e === O ? -1 : t === O ? 1 : r ? -1 : o ? 1 : j ? ee(j, e) - ee(j, t) : 0;
                if (r === o) return s(e, t);
                for (n = e; n = n.parentNode;) a.unshift(n);
                for (n = t; n = n.parentNode;) l.unshift(n);
                for (; a[i] === l[i];) i++;
                return i ? s(a[i], l[i]) : a[i] === F ? -1 : l[i] === F ? 1 : 0
            }, O) : O
        }, t.matches = function (e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function (e, n) {
            if ((e.ownerDocument || e) !== O && $(e), n = n.replace(ue, "='$1']"), _.matchesSelector && I && !X[n + " "] && (!H || !H.test(n)) && (!L || !L.test(n))) try {
                var i = z.call(e, n);
                if (i || _.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i
            } catch (e) {}
            return t(n, O, null, [e]).length > 0
        }, t.contains = function (e, t) {
            return (e.ownerDocument || e) !== O && $(e), M(e, t)
        }, t.attr = function (e, t) {
            (e.ownerDocument || e) !== O && $(e);
            var n = C.attrHandle[t.toLowerCase()],
                i = n && U.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !I) : void 0;
            return void 0 !== i ? i : _.attributes || !I ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, t.escape = function (e) {
            return (e + "").replace(be, _e)
        }, t.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function (e) {
            var t, n = [],
                i = 0,
                r = 0;
            if (N = !_.detectDuplicates, j = !_.sortStable && e.slice(0), e.sort(Q), N) {
                for (; t = e[r++];) t === e[r] && (i = n.push(r));
                for (; i--;) e.splice(n[i], 1)
            }
            return j = null, e
        }, T = t.getText = function (e) {
            var t, n = "",
                i = 0,
                r = e.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += T(e)
                } else if (3 === r || 4 === r) return e.nodeValue
            } else
                for (; t = e[i++];) n += T(t);
            return n
        }, C = t.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: de,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(we, xe), e[3] = (e[3] || e[4] || e[5] || "").replace(we, xe), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return de.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && he.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(we, xe).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function (e) {
                    var t = W[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && W(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                    })
                },
                ATTR: function (e, n, i) {
                    return function (r) {
                        var o = t.attr(r, e);
                        return null == o ? "!=" === n : !n || (o += "", "=" === n ? o === i : "!=" === n ? o !== i : "^=" === n ? i && 0 === o.indexOf(i) : "*=" === n ? i && o.indexOf(i) > -1 : "$=" === n ? i && o.slice(-i.length) === i : "~=" === n ? (" " + o.replace(se, " ") + " ").indexOf(i) > -1 : "|=" === n && (o === i || o.slice(0, i.length + 1) === i + "-"))
                    }
                },
                CHILD: function (e, t, n, i, r) {
                    var o = "nth" !== e.slice(0, 3),
                        s = "last" !== e.slice(-4),
                        a = "of-type" === t;
                    return 1 === i && 0 === r ? function (e) {
                        return !!e.parentNode
                    } : function (t, n, l) {
                        var c, u, h, p, d, f, g = o !== s ? "nextSibling" : "previousSibling",
                            m = t.parentNode,
                            v = a && t.nodeName.toLowerCase(),
                            y = !l && !a,
                            w = !1;
                        if (m) {
                            if (o) {
                                for (; g;) {
                                    for (p = t; p = p[g];)
                                        if (a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                    f = g = "only" === e && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [s ? m.firstChild : m.lastChild], s && y) {
                                for (p = m, h = p[q] || (p[q] = {}), u = h[p.uniqueID] || (h[p.uniqueID] = {}), c = u[e] || [], d = c[0] === R && c[1], w = d && c[2], p = d && m.childNodes[d]; p = ++d && p && p[g] || (w = d = 0) || f.pop();)
                                    if (1 === p.nodeType && ++w && p === t) {
                                        u[e] = [R, d, w];
                                        break
                                    }
                            } else if (y && (p = t, h = p[q] || (p[q] = {}), u = h[p.uniqueID] || (h[p.uniqueID] = {}), c = u[e] || [], d = c[0] === R && c[1], w = d), !1 === w)
                                for (;
                                    (p = ++d && p && p[g] || (w = d = 0) || f.pop()) && ((a ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++w || (y && (h = p[q] || (p[q] = {}), u = h[p.uniqueID] || (h[p.uniqueID] = {}), u[e] = [R, w]), p !== t)););
                            return (w -= r) === i || w % i == 0 && w / i >= 0
                        }
                    }
                },
                PSEUDO: function (e, n) {
                    var r, o = C.pseudos[e] || C.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[q] ? o(n) : o.length > 1 ? (r = [e, e, "", n], C.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function (e, t) {
                        for (var i, r = o(e, n), s = r.length; s--;) i = ee(e, r[s]), e[i] = !(t[i] = r[s])
                    }) : function (e) {
                        return o(e, 0, r)
                    }) : o
                }
            },
            pseudos: {
                not: i(function (e) {
                    var t = [],
                        n = [],
                        r = k(e.replace(ae, "$1"));
                    return r[q] ? i(function (e, t, n, i) {
                        for (var o, s = r(e, null, i, []), a = e.length; a--;)(o = s[a]) && (e[a] = !(t[a] = o))
                    }) : function (e, i, o) {
                        return t[0] = e, r(t, null, o, n), t[0] = null, !n.pop()
                    }
                }),
                has: i(function (e) {
                    return function (n) {
                        return t(e, n).length > 0
                    }
                }),
                contains: i(function (e) {
                    return e = e.replace(we, xe),
                        function (t) {
                            return (t.textContent || t.innerText || T(t)).indexOf(e) > -1
                        }
                }),
                lang: i(function (e) {
                    return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(we, xe).toLowerCase(),
                        function (t) {
                            var n;
                            do {
                                if (n = I ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                            } while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function (t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                },
                root: function (e) {
                    return e === P
                },
                focus: function (e) {
                    return e === O.activeElement && (!O.hasFocus || O.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: c(!1),
                disabled: c(!0),
                checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                },
                empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function (e) {
                    return !C.pseudos.empty(e)
                },
                header: function (e) {
                    return ge.test(e.nodeName)
                },
                input: function (e) {
                    return fe.test(e.nodeName)
                },
                button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: u(function () {
                    return [0]
                }),
                last: u(function (e, t) {
                    return [t - 1]
                }),
                eq: u(function (e, t, n) {
                    return [n < 0 ? n + t : n]
                }),
                even: u(function (e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }),
                odd: u(function (e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }),
                lt: u(function (e, t, n) {
                    for (var i = n < 0 ? n + t : n; --i >= 0;) e.push(i);
                    return e
                }),
                gt: u(function (e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
                    return e
                })
            }
        }, C.pseudos.nth = C.pseudos.eq;
        for (b in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) C.pseudos[b] = a(b);
        for (b in {
                submit: !0,
                reset: !0
            }) C.pseudos[b] = l(b);
        return p.prototype = C.filters = C.pseudos, C.setFilters = new p, E = t.tokenize = function (e, n) {
            var i, r, o, s, a, l, c, u = Y[e + " "];
            if (u) return n ? 0 : u.slice(0);
            for (a = e, l = [], c = C.preFilter; a;) {
                i && !(r = le.exec(a)) || (r && (a = a.slice(r[0].length) || a), l.push(o = [])), i = !1, (r = ce.exec(a)) && (i = r.shift(), o.push({
                    value: i,
                    type: r[0].replace(ae, " ")
                }), a = a.slice(i.length));
                for (s in C.filter) !(r = de[s].exec(a)) || c[s] && !(r = c[s](r)) || (i = r.shift(), o.push({
                    value: i,
                    type: s,
                    matches: r
                }), a = a.slice(i.length));
                if (!i) break
            }
            return n ? a.length : a ? t.error(e) : Y(e, l).slice(0)
        }, k = t.compile = function (e, t) {
            var n, i = [],
                r = [],
                o = X[e + " "];
            if (!o) {
                for (t || (t = E(e)), n = t.length; n--;) o = w(t[n]), o[q] ? i.push(o) : r.push(o);
                o = X(e, x(r, i)), o.selector = e
            }
            return o
        }, A = t.select = function (e, t, n, i) {
            var r, o, s, a, l, c = "function" == typeof e && e,
                u = !i && E(e = c.selector || e);
            if (n = n || [], 1 === u.length) {
                if (o = u[0] = u[0].slice(0), o.length > 2 && "ID" === (s = o[0]).type && 9 === t.nodeType && I && C.relative[o[1].type]) {
                    if (!(t = (C.find.ID(s.matches[0].replace(we, xe), t) || [])[0])) return n;
                    c && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (r = de.needsContext.test(e) ? 0 : o.length; r-- && (s = o[r], !C.relative[a = s.type]);)
                    if ((l = C.find[a]) && (i = l(s.matches[0].replace(we, xe), ye.test(o[0].type) && h(t.parentNode) || t))) {
                        if (o.splice(r, 1), !(e = i.length && d(o))) return G.apply(n, i), n;
                        break
                    }
            }
            return (c || k(e, u))(i, t, !I, n, !t || ye.test(e) && h(t.parentNode) || t), n
        }, _.sortStable = q.split("").sort(Q).join("") === q, _.detectDuplicates = !!N, $(), _.sortDetached = r(function (e) {
            return 1 & e.compareDocumentPosition(O.createElement("fieldset"))
        }), r(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function (e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), _.attributes && r(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || o("value", function (e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), r(function (e) {
            return null == e.getAttribute("disabled")
        }) || o(te, function (e, t, n) {
            var i;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), t
    }(e);
    ge.find = xe, ge.expr = xe.selectors, ge.expr[":"] = ge.expr.pseudos, ge.uniqueSort = ge.unique = xe.uniqueSort, ge.text = xe.getText, ge.isXMLDoc = xe.isXML, ge.contains = xe.contains, ge.escapeSelector = xe.escape;
    var be = function (e, t, n) {
            for (var i = [], r = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (r && ge(e).is(n)) break;
                    i.push(e)
                } return i
        },
        _e = function (e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        },
        Ce = ge.expr.match.needsContext,
        Te = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        Se = /^.[^:#\[\.,]*$/;
    ge.filter = function (e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? ge.find.matchesSelector(i, e) ? [i] : [] : ge.find.matches(e, ge.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, ge.fn.extend({
        find: function (e) {
            var t, n, i = this.length,
                r = this;
            if ("string" != typeof e) return this.pushStack(ge(e).filter(function () {
                for (t = 0; t < i; t++)
                    if (ge.contains(r[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < i; t++) ge.find(e, r[t], n);
            return i > 1 ? ge.uniqueSort(n) : n
        },
        filter: function (e) {
            return this.pushStack(o(this, e || [], !1))
        },
        not: function (e) {
            return this.pushStack(o(this, e || [], !0))
        },
        is: function (e) {
            return !!o(this, "string" == typeof e && Ce.test(e) ? ge(e) : e || [], !1).length
        }
    });
    var Ee, ke = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (ge.fn.init = function (e, t, n) {
        var i, r;
        if (!e) return this;
        if (n = n || Ee, "string" == typeof e) {
            if (!(i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : ke.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (i[1]) {
                if (t = t instanceof ge ? t[0] : t, ge.merge(this, ge.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : ne, !0)), Te.test(i[1]) && ge.isPlainObject(t))
                    for (i in t) ge.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                return this
            }
            return r = ne.getElementById(i[2]), r && (this[0] = r, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : ge.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(ge) : ge.makeArray(e, this)
    }).prototype = ge.fn, Ee = ge(ne);
    var Ae = /^(?:parents|prev(?:Until|All))/,
        De = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ge.fn.extend({
        has: function (e) {
            var t = ge(e, this),
                n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++)
                    if (ge.contains(this, t[e])) return !0
            })
        },
        closest: function (e, t) {
            var n, i = 0,
                r = this.length,
                o = [],
                s = "string" != typeof e && ge(e);
            if (!Ce.test(e))
                for (; i < r; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && ge.find.matchesSelector(n, e))) {
                            o.push(n);
                            break
                        } return this.pushStack(o.length > 1 ? ge.uniqueSort(o) : o)
        },
        index: function (e) {
            return e ? "string" == typeof e ? ae.call(ge(e), this[0]) : ae.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function (e, t) {
            return this.pushStack(ge.uniqueSort(ge.merge(this.get(), ge(e, t))))
        },
        addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ge.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function (e) {
            return be(e, "parentNode")
        },
        parentsUntil: function (e, t, n) {
            return be(e, "parentNode", n)
        },
        next: function (e) {
            return s(e, "nextSibling")
        },
        prev: function (e) {
            return s(e, "previousSibling")
        },
        nextAll: function (e) {
            return be(e, "nextSibling")
        },
        prevAll: function (e) {
            return be(e, "previousSibling")
        },
        nextUntil: function (e, t, n) {
            return be(e, "nextSibling", n)
        },
        prevUntil: function (e, t, n) {
            return be(e, "previousSibling", n)
        },
        siblings: function (e) {
            return _e((e.parentNode || {}).firstChild, e)
        },
        children: function (e) {
            return _e(e.firstChild)
        },
        contents: function (e) {
            return r(e, "iframe") ? e.contentDocument : (r(e, "template") && (e = e.content || e), ge.merge([], e.childNodes))
        }
    }, function (e, t) {
        ge.fn[e] = function (n, i) {
            var r = ge.map(this, t, n);
            return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (r = ge.filter(i, r)), this.length > 1 && (De[e] || ge.uniqueSort(r), Ae.test(e) && r.reverse()), this.pushStack(r)
        }
    });
    var je = /[^\x20\t\r\n\f]+/g;
    ge.Callbacks = function (e) {
        e = "string" == typeof e ? a(e) : ge.extend({}, e);
        var t, n, i, r, o = [],
            s = [],
            l = -1,
            c = function () {
                for (r = r || e.once, i = t = !0; s.length; l = -1)
                    for (n = s.shift(); ++l < o.length;) !1 === o[l].apply(n[0], n[1]) && e.stopOnFalse && (l = o.length, n = !1);
                e.memory || (n = !1), t = !1, r && (o = n ? [] : "")
            },
            u = {
                add: function () {
                    return o && (n && !t && (l = o.length - 1, s.push(n)), function t(n) {
                        ge.each(n, function (n, i) {
                            ge.isFunction(i) ? e.unique && u.has(i) || o.push(i) : i && i.length && "string" !== ge.type(i) && t(i)
                        })
                    }(arguments), n && !t && c()), this
                },
                remove: function () {
                    return ge.each(arguments, function (e, t) {
                        for (var n;
                            (n = ge.inArray(t, o, n)) > -1;) o.splice(n, 1), n <= l && l--
                    }), this
                },
                has: function (e) {
                    return e ? ge.inArray(e, o) > -1 : o.length > 0
                },
                empty: function () {
                    return o && (o = []), this
                },
                disable: function () {
                    return r = s = [], o = n = "", this
                },
                disabled: function () {
                    return !o
                },
                lock: function () {
                    return r = s = [], n || t || (o = n = ""), this
                },
                locked: function () {
                    return !!r
                },
                fireWith: function (e, n) {
                    return r || (n = n || [], n = [e, n.slice ? n.slice() : n], s.push(n), t || c()), this
                },
                fire: function () {
                    return u.fireWith(this, arguments), this
                },
                fired: function () {
                    return !!i
                }
            };
        return u
    }, ge.extend({
        Deferred: function (t) {
            var n = [
                    ["notify", "progress", ge.Callbacks("memory"), ge.Callbacks("memory"), 2],
                    ["resolve", "done", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 1, "rejected"]
                ],
                i = "pending",
                r = {
                    state: function () {
                        return i
                    },
                    always: function () {
                        return o.done(arguments).fail(arguments), this
                    },
                    catch: function (e) {
                        return r.then(null, e)
                    },
                    pipe: function () {
                        var e = arguments;
                        return ge.Deferred(function (t) {
                            ge.each(n, function (n, i) {
                                var r = ge.isFunction(e[i[4]]) && e[i[4]];
                                o[i[1]](function () {
                                    var e = r && r.apply(this, arguments);
                                    e && ge.isFunction(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[i[0] + "With"](this, r ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    then: function (t, i, r) {
                        function o(t, n, i, r) {
                            return function () {
                                var a = this,
                                    u = arguments,
                                    h = function () {
                                        var e, h;
                                        if (!(t < s)) {
                                            if ((e = i.apply(a, u)) === n.promise()) throw new TypeError("Thenable self-resolution");
                                            h = e && ("object" == typeof e || "function" == typeof e) && e.then, ge.isFunction(h) ? r ? h.call(e, o(s, n, l, r), o(s, n, c, r)) : (s++, h.call(e, o(s, n, l, r), o(s, n, c, r), o(s, n, l, n.notifyWith))) : (i !== l && (a = void 0, u = [e]), (r || n.resolveWith)(a, u))
                                        }
                                    },
                                    p = r ? h : function () {
                                        try {
                                            h()
                                        } catch (e) {
                                            ge.Deferred.exceptionHook && ge.Deferred.exceptionHook(e, p.stackTrace), t + 1 >= s && (i !== c && (a = void 0, u = [e]), n.rejectWith(a, u))
                                        }
                                    };
                                t ? p() : (ge.Deferred.getStackHook && (p.stackTrace = ge.Deferred.getStackHook()), e.setTimeout(p))
                            }
                        }
                        var s = 0;
                        return ge.Deferred(function (e) {
                            n[0][3].add(o(0, e, ge.isFunction(r) ? r : l, e.notifyWith)), n[1][3].add(o(0, e, ge.isFunction(t) ? t : l)), n[2][3].add(o(0, e, ge.isFunction(i) ? i : c))
                        }).promise()
                    },
                    promise: function (e) {
                        return null != e ? ge.extend(e, r) : r
                    }
                },
                o = {};
            return ge.each(n, function (e, t) {
                var s = t[2],
                    a = t[5];
                r[t[1]] = s.add, a && s.add(function () {
                    i = a
                }, n[3 - e][2].disable, n[0][2].lock), s.add(t[3].fire), o[t[0]] = function () {
                    return o[t[0] + "With"](this === o ? void 0 : this, arguments), this
                }, o[t[0] + "With"] = s.fireWith
            }), r.promise(o), t && t.call(o, o), o
        },
        when: function (e) {
            var t = arguments.length,
                n = t,
                i = Array(n),
                r = re.call(arguments),
                o = ge.Deferred(),
                s = function (e) {
                    return function (n) {
                        i[e] = this, r[e] = arguments.length > 1 ? re.call(arguments) : n, --t || o.resolveWith(i, r)
                    }
                };
            if (t <= 1 && (u(e, o.done(s(n)).resolve, o.reject, !t), "pending" === o.state() || ge.isFunction(r[n] && r[n].then))) return o.then();
            for (; n--;) u(r[n], s(n), o.reject);
            return o.promise()
        }
    });
    var Ne = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    ge.Deferred.exceptionHook = function (t, n) {
        e.console && e.console.warn && t && Ne.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, n)
    }, ge.readyException = function (t) {
        e.setTimeout(function () {
            throw t
        })
    };
    var $e = ge.Deferred();
    ge.fn.ready = function (e) {
        return $e.then(e).catch(function (e) {
            ge.readyException(e)
        }), this
    }, ge.extend({
        isReady: !1,
        readyWait: 1,
        ready: function (e) {
            (!0 === e ? --ge.readyWait : ge.isReady) || (ge.isReady = !0, !0 !== e && --ge.readyWait > 0 || $e.resolveWith(ne, [ge]))
        }
    }), ge.ready.then = $e.then, "complete" === ne.readyState || "loading" !== ne.readyState && !ne.documentElement.doScroll ? e.setTimeout(ge.ready) : (ne.addEventListener("DOMContentLoaded", h), e.addEventListener("load", h));
    var Oe = function (e, t, n, i, r, o, s) {
            var a = 0,
                l = e.length,
                c = null == n;
            if ("object" === ge.type(n)) {
                r = !0;
                for (a in n) Oe(e, t, a, n[a], !0, o, s)
            } else if (void 0 !== i && (r = !0, ge.isFunction(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function (e, t, n) {
                    return c.call(ge(e), n)
                })), t))
                for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
            return r ? e : c ? t.call(e) : l ? t(e[0], n) : o
        },
        Pe = function (e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        };
    p.uid = 1, p.prototype = {
        cache: function (e) {
            var t = e[this.expando];
            return t || (t = {}, Pe(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function (e, t, n) {
            var i, r = this.cache(e);
            if ("string" == typeof t) r[ge.camelCase(t)] = n;
            else
                for (i in t) r[ge.camelCase(i)] = t[i];
            return r
        },
        get: function (e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][ge.camelCase(t)]
        },
        access: function (e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        },
        remove: function (e, t) {
            var n, i = e[this.expando];
            if (void 0 !== i) {
                if (void 0 !== t) {
                    Array.isArray(t) ? t = t.map(ge.camelCase) : (t = ge.camelCase(t), t = t in i ? [t] : t.match(je) || []), n = t.length;
                    for (; n--;) delete i[t[n]]
                }(void 0 === t || ge.isEmptyObject(i)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        },
        hasData: function (e) {
            var t = e[this.expando];
            return void 0 !== t && !ge.isEmptyObject(t)
        }
    };
    var Ie = new p,
        Le = new p,
        He = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        ze = /[A-Z]/g;
    ge.extend({
        hasData: function (e) {
            return Le.hasData(e) || Ie.hasData(e)
        },
        data: function (e, t, n) {
            return Le.access(e, t, n)
        },
        removeData: function (e, t) {
            Le.remove(e, t)
        },
        _data: function (e, t, n) {
            return Ie.access(e, t, n)
        },
        _removeData: function (e, t) {
            Ie.remove(e, t)
        }
    }), ge.fn.extend({
        data: function (e, t) {
            var n, i, r, o = this[0],
                s = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (r = Le.get(o), 1 === o.nodeType && !Ie.get(o, "hasDataAttrs"))) {
                    for (n = s.length; n--;) s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = ge.camelCase(i.slice(5)), f(o, i, r[i])));
                    Ie.set(o, "hasDataAttrs", !0)
                }
                return r
            }
            return "object" == typeof e ? this.each(function () {
                Le.set(this, e)
            }) : Oe(this, function (t) {
                var n;
                if (o && void 0 === t) {
                    if (void 0 !== (n = Le.get(o, e))) return n;
                    if (void 0 !== (n = f(o, e))) return n
                } else this.each(function () {
                    Le.set(this, e, t)
                })
            }, null, t, arguments.length > 1, null, !0)
        },
        removeData: function (e) {
            return this.each(function () {
                Le.remove(this, e)
            })
        }
    }), ge.extend({
        queue: function (e, t, n) {
            var i;
            if (e) return t = (t || "fx") + "queue", i = Ie.get(e, t), n && (!i || Array.isArray(n) ? i = Ie.access(e, t, ge.makeArray(n)) : i.push(n)), i || []
        },
        dequeue: function (e, t) {
            t = t || "fx";
            var n = ge.queue(e, t),
                i = n.length,
                r = n.shift(),
                o = ge._queueHooks(e, t),
                s = function () {
                    ge.dequeue(e, t)
                };
            "inprogress" === r && (r = n.shift(), i--), r && ("fx" === t && n.unshift("inprogress"), delete o.stop, r.call(e, s, o)), !i && o && o.empty.fire()
        },
        _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return Ie.get(e, n) || Ie.access(e, n, {
                empty: ge.Callbacks("once memory").add(function () {
                    Ie.remove(e, [t + "queue", n])
                })
            })
        }
    }), ge.fn.extend({
        queue: function (e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? ge.queue(this[0], e) : void 0 === t ? this : this.each(function () {
                var n = ge.queue(this, e, t);
                ge._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && ge.dequeue(this, e)
            })
        },
        dequeue: function (e) {
            return this.each(function () {
                ge.dequeue(this, e)
            })
        },
        clearQueue: function (e) {
            return this.queue(e || "fx", [])
        },
        promise: function (e, t) {
            var n, i = 1,
                r = ge.Deferred(),
                o = this,
                s = this.length,
                a = function () {
                    --i || r.resolveWith(o, [o])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;)(n = Ie.get(o[s], e + "queueHooks")) && n.empty && (i++, n.empty.add(a));
            return a(), r.promise(t)
        }
    });
    var Me = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        qe = new RegExp("^(?:([+-])=|)(" + Me + ")([a-z%]*)$", "i"),
        Fe = ["Top", "Right", "Bottom", "Left"],
        Re = function (e, t) {
            return e = t || e, "none" === e.style.display || "" === e.style.display && ge.contains(e.ownerDocument, e) && "none" === ge.css(e, "display")
        },
        Be = function (e, t, n, i) {
            var r, o, s = {};
            for (o in t) s[o] = e.style[o], e.style[o] = t[o];
            r = n.apply(e, i || []);
            for (o in t) e.style[o] = s[o];
            return r
        },
        We = {};
    ge.fn.extend({
        show: function () {
            return v(this, !0)
        },
        hide: function () {
            return v(this)
        },
        toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                Re(this) ? ge(this).show() : ge(this).hide()
            })
        }
    });
    var Ye = /^(?:checkbox|radio)$/i,
        Xe = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        Qe = /^$|\/(?:java|ecma)script/i,
        Ue = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Ue.optgroup = Ue.option, Ue.tbody = Ue.tfoot = Ue.colgroup = Ue.caption = Ue.thead, Ue.th = Ue.td;
    var Ve = /<|&#?\w+;/;
    ! function () {
        var e = ne.createDocumentFragment(),
            t = e.appendChild(ne.createElement("div")),
            n = ne.createElement("input");
        n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), de.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", de.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var Ke = ne.documentElement,
        Ze = /^key/,
        Ge = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Je = /^([^.]*)(?:\.(.+)|)/;
    ge.event = {
        global: {},
        add: function (e, t, n, i, r) {
            var o, s, a, l, c, u, h, p, d, f, g, m = Ie.get(e);
            if (m)
                for (n.handler && (o = n, n = o.handler, r = o.selector), r && ge.find.matchesSelector(Ke, r), n.guid || (n.guid = ge.guid++), (l = m.events) || (l = m.events = {}), (s = m.handle) || (s = m.handle = function (t) {
                        return void 0 !== ge && ge.event.triggered !== t.type ? ge.event.dispatch.apply(e, arguments) : void 0
                    }), t = (t || "").match(je) || [""], c = t.length; c--;) a = Je.exec(t[c]) || [], d = g = a[1], f = (a[2] || "").split(".").sort(), d && (h = ge.event.special[d] || {}, d = (r ? h.delegateType : h.bindType) || d, h = ge.event.special[d] || {}, u = ge.extend({
                    type: d,
                    origType: g,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: r,
                    needsContext: r && ge.expr.match.needsContext.test(r),
                    namespace: f.join(".")
                }, o), (p = l[d]) || (p = l[d] = [], p.delegateCount = 0, h.setup && !1 !== h.setup.call(e, i, f, s) || e.addEventListener && e.addEventListener(d, s)), h.add && (h.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), r ? p.splice(p.delegateCount++, 0, u) : p.push(u), ge.event.global[d] = !0)
        },
        remove: function (e, t, n, i, r) {
            var o, s, a, l, c, u, h, p, d, f, g, m = Ie.hasData(e) && Ie.get(e);
            if (m && (l = m.events)) {
                for (t = (t || "").match(je) || [""], c = t.length; c--;)
                    if (a = Je.exec(t[c]) || [], d = g = a[1], f = (a[2] || "").split(".").sort(), d) {
                        for (h = ge.event.special[d] || {}, d = (i ? h.delegateType : h.bindType) || d, p = l[d] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = o = p.length; o--;) u = p[o], !r && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(o, 1), u.selector && p.delegateCount--, h.remove && h.remove.call(e, u));
                        s && !p.length && (h.teardown && !1 !== h.teardown.call(e, f, m.handle) || ge.removeEvent(e, d, m.handle), delete l[d])
                    } else
                        for (d in l) ge.event.remove(e, d + t[c], n, i, !0);
                ge.isEmptyObject(l) && Ie.remove(e, "handle events")
            }
        },
        dispatch: function (e) {
            var t, n, i, r, o, s, a = ge.event.fix(e),
                l = new Array(arguments.length),
                c = (Ie.get(this, "events") || {})[a.type] || [],
                u = ge.event.special[a.type] || {};
            for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
            if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
                for (s = ge.event.handlers.call(this, a, c), t = 0;
                    (r = s[t++]) && !a.isPropagationStopped();)
                    for (a.currentTarget = r.elem, n = 0;
                        (o = r.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(o.namespace) || (a.handleObj = o, a.data = o.data, void 0 !== (i = ((ge.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, a), a.result
            }
        },
        handlers: function (e, t) {
            var n, i, r, o, s, a = [],
                l = t.delegateCount,
                c = e.target;
            if (l && c.nodeType && !("click" === e.type && e.button >= 1))
                for (; c !== this; c = c.parentNode || this)
                    if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                        for (o = [], s = {}, n = 0; n < l; n++) i = t[n], r = i.selector + " ", void 0 === s[r] && (s[r] = i.needsContext ? ge(r, this).index(c) > -1 : ge.find(r, this, null, [c]).length), s[r] && o.push(i);
                        o.length && a.push({
                            elem: c,
                            handlers: o
                        })
                    } return c = this, l < t.length && a.push({
                elem: c,
                handlers: t.slice(l)
            }), a
        },
        addProp: function (e, t) {
            Object.defineProperty(ge.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: ge.isFunction(t) ? function () {
                    if (this.originalEvent) return t(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[e]
                },
                set: function (t) {
                    Object.defineProperty(this, e, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: t
                    })
                }
            })
        },
        fix: function (e) {
            return e[ge.expando] ? e : new ge.Event(e)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function () {
                    if (this !== C() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function () {
                    if (this === C() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && r(this, "input")) return this.click(), !1
                },
                _default: function (e) {
                    return r(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, ge.removeEvent = function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, ge.Event = function (e, t) {
        return this instanceof ge.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? b : _, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && ge.extend(this, t), this.timeStamp = e && e.timeStamp || ge.now(), void(this[ge.expando] = !0)) : new ge.Event(e, t)
    }, ge.Event.prototype = {
        constructor: ge.Event,
        isDefaultPrevented: _,
        isPropagationStopped: _,
        isImmediatePropagationStopped: _,
        isSimulated: !1,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = b, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = b, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = b, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ge.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (e) {
            var t = e.button;
            return null == e.which && Ze.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Ge.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, ge.event.addProp), ge.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (e, t) {
        ge.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function (e) {
                var n, i = this,
                    r = e.relatedTarget,
                    o = e.handleObj;
                return r && (r === i || ge.contains(i, r)) || (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), ge.fn.extend({
        on: function (e, t, n, i) {
            return T(this, e, t, n, i)
        },
        one: function (e, t, n, i) {
            return T(this, e, t, n, i, 1)
        },
        off: function (e, t, n) {
            var i, r;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, ge(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof e) {
                for (r in e) this.off(r, t, e[r]);
                return this
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = _), this.each(function () {
                ge.event.remove(this, e, n, t)
            })
        }
    });
    var et = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        tt = /<script|<style|<link/i,
        nt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        it = /^true\/(.*)/,
        rt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    ge.extend({
        htmlPrefilter: function (e) {
            return e.replace(et, "<$1></$2>")
        },
        clone: function (e, t, n) {
            var i, r, o, s, a = e.cloneNode(!0),
                l = ge.contains(e.ownerDocument, e);
            if (!(de.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ge.isXMLDoc(e)))
                for (s = y(a), o = y(e), i = 0, r = o.length; i < r; i++) D(o[i], s[i]);
            if (t)
                if (n)
                    for (o = o || y(e), s = s || y(a), i = 0, r = o.length; i < r; i++) A(o[i], s[i]);
                else A(e, a);
            return s = y(a, "script"), s.length > 0 && w(s, !l && y(e, "script")), a
        },
        cleanData: function (e) {
            for (var t, n, i, r = ge.event.special, o = 0; void 0 !== (n = e[o]); o++)
                if (Pe(n)) {
                    if (t = n[Ie.expando]) {
                        if (t.events)
                            for (i in t.events) r[i] ? ge.event.remove(n, i) : ge.removeEvent(n, i, t.handle);
                        n[Ie.expando] = void 0
                    }
                    n[Le.expando] && (n[Le.expando] = void 0)
                }
        }
    }), ge.fn.extend({
        detach: function (e) {
            return N(this, e, !0)
        },
        remove: function (e) {
            return N(this, e)
        },
        text: function (e) {
            return Oe(this, function (e) {
                return void 0 === e ? ge.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        },
        append: function () {
            return j(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    S(this, e).appendChild(e)
                }
            })
        },
        prepend: function () {
            return j(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = S(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function () {
            return j(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function () {
            return j(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (ge.cleanData(y(e, !1)), e.textContent = "");
            return this
        },
        clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function () {
                return ge.clone(this, e, t)
            })
        },
        html: function (e) {
            return Oe(this, function (e) {
                var t = this[0] || {},
                    n = 0,
                    i = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !tt.test(e) && !Ue[(Xe.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = ge.htmlPrefilter(e);
                    try {
                        for (; n < i; n++) t = this[n] || {}, 1 === t.nodeType && (ge.cleanData(y(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function () {
            var e = [];
            return j(this, arguments, function (t) {
                var n = this.parentNode;
                ge.inArray(this, e) < 0 && (ge.cleanData(y(this)), n && n.replaceChild(t, this))
            }, e)
        }
    }), ge.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, t) {
        ge.fn[e] = function (e) {
            for (var n, i = [], r = ge(e), o = r.length - 1, s = 0; s <= o; s++) n = s === o ? this : this.clone(!0), ge(r[s])[t](n), se.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var ot = /^margin/,
        st = new RegExp("^(" + Me + ")(?!px)[a-z%]+$", "i"),
        at = function (t) {
            var n = t.ownerDocument.defaultView;
            return n && n.opener || (n = e), n.getComputedStyle(t)
        };
    ! function () {
        function t() {
            if (a) {
                a.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Ke.appendChild(s);
                var t = e.getComputedStyle(a);
                n = "1%" !== t.top, o = "2px" === t.marginLeft, i = "4px" === t.width, a.style.marginRight = "50%", r = "4px" === t.marginRight, Ke.removeChild(s), a = null
            }
        }
        var n, i, r, o, s = ne.createElement("div"),
            a = ne.createElement("div");
        a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", de.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", s.appendChild(a), ge.extend(de, {
            pixelPosition: function () {
                return t(), n
            },
            boxSizingReliable: function () {
                return t(), i
            },
            pixelMarginRight: function () {
                return t(), r
            },
            reliableMarginLeft: function () {
                return t(), o
            }
        }))
    }();
    var lt = /^(none|table(?!-c[ea]).+)/,
        ct = /^--/,
        ut = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        ht = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        pt = ["Webkit", "Moz", "ms"],
        dt = ne.createElement("div").style;
    ge.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = $(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: "cssFloat"
        },
        style: function (e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var r, o, s, a = ge.camelCase(t),
                    l = ct.test(t),
                    c = e.style;
                return l || (t = I(a)), s = ge.cssHooks[t] || ge.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (r = s.get(e, !1, i)) ? r : c[t] : (o = typeof n, "string" === o && (r = qe.exec(n)) && r[1] && (n = g(e, t, r), o = "number"), void(null != n && n === n && ("number" === o && (n += r && r[3] || (ge.cssNumber[a] ? "" : "px")), de.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))))
            }
        },
        css: function (e, t, n, i) {
            var r, o, s, a = ge.camelCase(t);
            return ct.test(t) || (t = I(a)), s = ge.cssHooks[t] || ge.cssHooks[a], s && "get" in s && (r = s.get(e, !0, n)), void 0 === r && (r = $(e, t, i)), "normal" === r && t in ht && (r = ht[t]), "" === n || n ? (o = parseFloat(r), !0 === n || isFinite(o) ? o || 0 : r) : r
        }
    }), ge.each(["height", "width"], function (e, t) {
        ge.cssHooks[t] = {
            get: function (e, n, i) {
                if (n) return !lt.test(ge.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? z(e, t, i) : Be(e, ut, function () {
                    return z(e, t, i)
                })
            },
            set: function (e, n, i) {
                var r, o = i && at(e),
                    s = i && H(e, t, i, "border-box" === ge.css(e, "boxSizing", !1, o), o);
                return s && (r = qe.exec(n)) && "px" !== (r[3] || "px") && (e.style[t] = n, n = ge.css(e, t)), L(e, n, s)
            }
        }
    }), ge.cssHooks.marginLeft = O(de.reliableMarginLeft, function (e, t) {
        if (t) return (parseFloat($(e, "marginLeft")) || e.getBoundingClientRect().left - Be(e, {
            marginLeft: 0
        }, function () {
            return e.getBoundingClientRect().left
        })) + "px"
    }), ge.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function (e, t) {
        ge.cssHooks[e + t] = {
            expand: function (n) {
                for (var i = 0, r = {}, o = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) r[e + Fe[i] + t] = o[i] || o[i - 2] || o[0];
                return r
            }
        }, ot.test(e) || (ge.cssHooks[e + t].set = L)
    }), ge.fn.extend({
        css: function (e, t) {
            return Oe(this, function (e, t, n) {
                var i, r, o = {},
                    s = 0;
                if (Array.isArray(t)) {
                    for (i = at(e), r = t.length; s < r; s++) o[t[s]] = ge.css(e, t[s], !1, i);
                    return o
                }
                return void 0 !== n ? ge.style(e, t, n) : ge.css(e, t)
            }, e, t, arguments.length > 1)
        }
    }), ge.Tween = M, M.prototype = {
        constructor: M,
        init: function (e, t, n, i, r, o) {
            this.elem = e, this.prop = n, this.easing = r || ge.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = o || (ge.cssNumber[n] ? "" : "px")
        },
        cur: function () {
            var e = M.propHooks[this.prop];
            return e && e.get ? e.get(this) : M.propHooks._default.get(this)
        },
        run: function (e) {
            var t, n = M.propHooks[this.prop];
            return this.options.duration ? this.pos = t = ge.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : M.propHooks._default.set(this), this
        }
    }, M.prototype.init.prototype = M.prototype, M.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = ge.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0)
            },
            set: function (e) {
                ge.fx.step[e.prop] ? ge.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[ge.cssProps[e.prop]] && !ge.cssHooks[e.prop] ? e.elem[e.prop] = e.now : ge.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, M.propHooks.scrollTop = M.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ge.easing = {
        linear: function (e) {
            return e
        },
        swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, ge.fx = M.prototype.init, ge.fx.step = {};
    var ft, gt, mt = /^(?:toggle|show|hide)$/,
        vt = /queueHooks$/;
    ge.Animation = ge.extend(X, {
            tweeners: {
                "*": [function (e, t) {
                    var n = this.createTween(e, t);
                    return g(n.elem, e, qe.exec(t), n), n
                }]
            },
            tweener: function (e, t) {
                ge.isFunction(e) ? (t = e, e = ["*"]) : e = e.match(je);
                for (var n, i = 0, r = e.length; i < r; i++) n = e[i], X.tweeners[n] = X.tweeners[n] || [], X.tweeners[n].unshift(t)
            },
            prefilters: [W],
            prefilter: function (e, t) {
                t ? X.prefilters.unshift(e) : X.prefilters.push(e)
            }
        }), ge.speed = function (e, t, n) {
            var i = e && "object" == typeof e ? ge.extend({}, e) : {
                complete: n || !n && t || ge.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !ge.isFunction(t) && t
            };
            return ge.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in ge.fx.speeds ? i.duration = ge.fx.speeds[i.duration] : i.duration = ge.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
                ge.isFunction(i.old) && i.old.call(this), i.queue && ge.dequeue(this, i.queue)
            }, i
        }, ge.fn.extend({
            fadeTo: function (e, t, n, i) {
                return this.filter(Re).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, i)
            },
            animate: function (e, t, n, i) {
                var r = ge.isEmptyObject(e),
                    o = ge.speed(t, n, i),
                    s = function () {
                        var t = X(this, ge.extend({}, e), o);
                        (r || Ie.get(this, "finish")) && t.stop(!0)
                    };
                return s.finish = s, r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s)
            },
            stop: function (e, t, n) {
                var i = function (e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function () {
                    var t = !0,
                        r = null != e && e + "queueHooks",
                        o = ge.timers,
                        s = Ie.get(this);
                    if (r) s[r] && s[r].stop && i(s[r]);
                    else
                        for (r in s) s[r] && s[r].stop && vt.test(r) && i(s[r]);
                    for (r = o.length; r--;) o[r].elem !== this || null != e && o[r].queue !== e || (o[r].anim.stop(n), t = !1, o.splice(r, 1));
                    !t && n || ge.dequeue(this, e)
                })
            },
            finish: function (e) {
                return !1 !== e && (e = e || "fx"), this.each(function () {
                    var t, n = Ie.get(this),
                        i = n[e + "queue"],
                        r = n[e + "queueHooks"],
                        o = ge.timers,
                        s = i ? i.length : 0;
                    for (n.finish = !0, ge.queue(this, e, []), r && r.stop && r.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                    for (t = 0; t < s; t++) i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish
                })
            }
        }), ge.each(["toggle", "show", "hide"], function (e, t) {
            var n = ge.fn[t];
            ge.fn[t] = function (e, i, r) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(R(t, !0), e, i, r)
            }
        }), ge.each({
            slideDown: R("show"),
            slideUp: R("hide"),
            slideToggle: R("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function (e, t) {
            ge.fn[e] = function (e, n, i) {
                return this.animate(t, e, n, i)
            }
        }), ge.timers = [], ge.fx.tick = function () {
            var e, t = 0,
                n = ge.timers;
            for (ft = ge.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || ge.fx.stop(), ft = void 0
        }, ge.fx.timer = function (e) {
            ge.timers.push(e), ge.fx.start()
        }, ge.fx.interval = 13, ge.fx.start = function () {
            gt || (gt = !0, q())
        }, ge.fx.stop = function () {
            gt = null
        }, ge.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, ge.fn.delay = function (t, n) {
            return t = ge.fx ? ge.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function (n, i) {
                var r = e.setTimeout(n, t);
                i.stop = function () {
                    e.clearTimeout(r)
                }
            })
        },
        function () {
            var e = ne.createElement("input"),
                t = ne.createElement("select"),
                n = t.appendChild(ne.createElement("option"));
            e.type = "checkbox", de.checkOn = "" !== e.value, de.optSelected = n.selected, e = ne.createElement("input"), e.value = "t", e.type = "radio", de.radioValue = "t" === e.value
        }();
    var yt, wt = ge.expr.attrHandle;
    ge.fn.extend({
        attr: function (e, t) {
            return Oe(this, ge.attr, e, t, arguments.length > 1)
        },
        removeAttr: function (e) {
            return this.each(function () {
                ge.removeAttr(this, e)
            })
        }
    }), ge.extend({
        attr: function (e, t, n) {
            var i, r, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return void 0 === e.getAttribute ? ge.prop(e, t, n) : (1 === o && ge.isXMLDoc(e) || (r = ge.attrHooks[t.toLowerCase()] || (ge.expr.match.bool.test(t) ? yt : void 0)), void 0 !== n ? null === n ? void ge.removeAttr(e, t) : r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : r && "get" in r && null !== (i = r.get(e, t)) ? i : (i = ge.find.attr(e, t), null == i ? void 0 : i))
        },
        attrHooks: {
            type: {
                set: function (e, t) {
                    if (!de.radioValue && "radio" === t && r(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        removeAttr: function (e, t) {
            var n, i = 0,
                r = t && t.match(je);
            if (r && 1 === e.nodeType)
                for (; n = r[i++];) e.removeAttribute(n)
        }
    }), yt = {
        set: function (e, t, n) {
            return !1 === t ? ge.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, ge.each(ge.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var n = wt[t] || ge.find.attr;
        wt[t] = function (e, t, i) {
            var r, o, s = t.toLowerCase();
            return i || (o = wt[s], wt[s] = r, r = null != n(e, t, i) ? s : null, wt[s] = o), r
        }
    });
    var xt = /^(?:input|select|textarea|button)$/i,
        bt = /^(?:a|area)$/i;
    ge.fn.extend({
        prop: function (e, t) {
            return Oe(this, ge.prop, e, t, arguments.length > 1)
        },
        removeProp: function (e) {
            return this.each(function () {
                delete this[ge.propFix[e] || e]
            })
        }
    }), ge.extend({
        prop: function (e, t, n) {
            var i, r, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return 1 === o && ge.isXMLDoc(e) || (t = ge.propFix[t] || t, r = ge.propHooks[t]), void 0 !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : e[t] = n : r && "get" in r && null !== (i = r.get(e, t)) ? i : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = ge.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : xt.test(e.nodeName) || bt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), de.optSelected || (ge.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function (e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), ge.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        ge.propFix[this.toLowerCase()] = this
    }), ge.fn.extend({
        addClass: function (e) {
            var t, n, i, r, o, s, a, l = 0;
            if (ge.isFunction(e)) return this.each(function (t) {
                ge(this).addClass(e.call(this, t, U(this)))
            });
            if ("string" == typeof e && e)
                for (t = e.match(je) || []; n = this[l++];)
                    if (r = U(n), i = 1 === n.nodeType && " " + Q(r) + " ") {
                        for (s = 0; o = t[s++];) i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                        a = Q(i), r !== a && n.setAttribute("class", a)
                    } return this
        },
        removeClass: function (e) {
            var t, n, i, r, o, s, a, l = 0;
            if (ge.isFunction(e)) return this.each(function (t) {
                ge(this).removeClass(e.call(this, t, U(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof e && e)
                for (t = e.match(je) || []; n = this[l++];)
                    if (r = U(n), i = 1 === n.nodeType && " " + Q(r) + " ") {
                        for (s = 0; o = t[s++];)
                            for (; i.indexOf(" " + o + " ") > -1;) i = i.replace(" " + o + " ", " ");
                        a = Q(i), r !== a && n.setAttribute("class", a)
                    } return this
        },
        toggleClass: function (e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : ge.isFunction(e) ? this.each(function (n) {
                ge(this).toggleClass(e.call(this, n, U(this), t), t)
            }) : this.each(function () {
                var t, i, r, o;
                if ("string" === n)
                    for (i = 0, r = ge(this), o = e.match(je) || []; t = o[i++];) r.hasClass(t) ? r.removeClass(t) : r.addClass(t);
                else void 0 !== e && "boolean" !== n || (t = U(this), t && Ie.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : Ie.get(this, "__className__") || ""))
            })
        },
        hasClass: function (e) {
            var t, n, i = 0;
            for (t = " " + e + " "; n = this[i++];)
                if (1 === n.nodeType && (" " + Q(U(n)) + " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var _t = /\r/g;
    ge.fn.extend({
        val: function (e) {
            var t, n, i, r = this[0];
            return arguments.length ? (i = ge.isFunction(e), this.each(function (n) {
                var r;
                1 === this.nodeType && (r = i ? e.call(this, n, ge(this).val()) : e, null == r ? r = "" : "number" == typeof r ? r += "" : Array.isArray(r) && (r = ge.map(r, function (e) {
                    return null == e ? "" : e + ""
                })), (t = ge.valHooks[this.type] || ge.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, r, "value") || (this.value = r))
            })) : r ? (t = ge.valHooks[r.type] || ge.valHooks[r.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(r, "value")) ? n : (n = r.value, "string" == typeof n ? n.replace(_t, "") : null == n ? "" : n)) : void 0
        }
    }), ge.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = ge.find.attr(e, "value");
                    return null != t ? t : Q(ge.text(e))
                }
            },
            select: {
                get: function (e) {
                    var t, n, i, o = e.options,
                        s = e.selectedIndex,
                        a = "select-one" === e.type,
                        l = a ? null : [],
                        c = a ? s + 1 : o.length;
                    for (i = s < 0 ? c : a ? s : 0; i < c; i++)
                        if (n = o[i], (n.selected || i === s) && !n.disabled && (!n.parentNode.disabled || !r(n.parentNode, "optgroup"))) {
                            if (t = ge(n).val(), a) return t;
                            l.push(t)
                        } return l
                },
                set: function (e, t) {
                    for (var n, i, r = e.options, o = ge.makeArray(t), s = r.length; s--;) i = r[s], (i.selected = ge.inArray(ge.valHooks.option.get(i), o) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), ge.each(["radio", "checkbox"], function () {
        ge.valHooks[this] = {
            set: function (e, t) {
                if (Array.isArray(t)) return e.checked = ge.inArray(ge(e).val(), t) > -1
            }
        }, de.checkOn || (ge.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var Ct = /^(?:focusinfocus|focusoutblur)$/;
    ge.extend(ge.event, {
        trigger: function (t, n, i, r) {
            var o, s, a, l, c, u, h, p = [i || ne],
                d = ue.call(t, "type") ? t.type : t,
                f = ue.call(t, "namespace") ? t.namespace.split(".") : [];
            if (s = a = i = i || ne, 3 !== i.nodeType && 8 !== i.nodeType && !Ct.test(d + ge.event.triggered) && (d.indexOf(".") > -1 && (f = d.split("."), d = f.shift(), f.sort()), c = d.indexOf(":") < 0 && "on" + d, t = t[ge.expando] ? t : new ge.Event(d, "object" == typeof t && t), t.isTrigger = r ? 2 : 3, t.namespace = f.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : ge.makeArray(n, [t]), h = ge.event.special[d] || {}, r || !h.trigger || !1 !== h.trigger.apply(i, n))) {
                if (!r && !h.noBubble && !ge.isWindow(i)) {
                    for (l = h.delegateType || d, Ct.test(l + d) || (s = s.parentNode); s; s = s.parentNode) p.push(s), a = s;
                    a === (i.ownerDocument || ne) && p.push(a.defaultView || a.parentWindow || e)
                }
                for (o = 0;
                    (s = p[o++]) && !t.isPropagationStopped();) t.type = o > 1 ? l : h.bindType || d, u = (Ie.get(s, "events") || {})[t.type] && Ie.get(s, "handle"), u && u.apply(s, n), (u = c && s[c]) && u.apply && Pe(s) && (t.result = u.apply(s, n), !1 === t.result && t.preventDefault());
                return t.type = d, r || t.isDefaultPrevented() || h._default && !1 !== h._default.apply(p.pop(), n) || !Pe(i) || c && ge.isFunction(i[d]) && !ge.isWindow(i) && (a = i[c], a && (i[c] = null), ge.event.triggered = d, i[d](), ge.event.triggered = void 0, a && (i[c] = a)), t.result
            }
        },
        simulate: function (e, t, n) {
            var i = ge.extend(new ge.Event, n, {
                type: e,
                isSimulated: !0
            });
            ge.event.trigger(i, null, t)
        }
    }), ge.fn.extend({
        trigger: function (e, t) {
            return this.each(function () {
                ge.event.trigger(e, t, this)
            })
        },
        triggerHandler: function (e, t) {
            var n = this[0];
            if (n) return ge.event.trigger(e, t, n, !0)
        }
    }), ge.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, t) {
        ge.fn[t] = function (e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), ge.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), de.focusin = "onfocusin" in e, de.focusin || ge.each({
        focus: "focusin",
        blur: "focusout"
    }, function (e, t) {
        var n = function (e) {
            ge.event.simulate(t, e.target, ge.event.fix(e))
        };
        ge.event.special[t] = {
            setup: function () {
                var i = this.ownerDocument || this,
                    r = Ie.access(i, t);
                r || i.addEventListener(e, n, !0), Ie.access(i, t, (r || 0) + 1)
            },
            teardown: function () {
                var i = this.ownerDocument || this,
                    r = Ie.access(i, t) - 1;
                r ? Ie.access(i, t, r) : (i.removeEventListener(e, n, !0), Ie.remove(i, t))
            }
        }
    });
    var Tt = e.location,
        St = ge.now(),
        Et = /\?/;
    ge.parseXML = function (t) {
        var n;
        if (!t || "string" != typeof t) return null;
        try {
            n = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            n = void 0
        }
        return n && !n.getElementsByTagName("parsererror").length || ge.error("Invalid XML: " + t), n
    };
    var kt = /\[\]$/,
        At = /\r?\n/g,
        Dt = /^(?:submit|button|image|reset|file)$/i,
        jt = /^(?:input|select|textarea|keygen)/i;
    ge.param = function (e, t) {
        var n, i = [],
            r = function (e, t) {
                var n = ge.isFunction(t) ? t() : t;
                i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
            };
        if (Array.isArray(e) || e.jquery && !ge.isPlainObject(e)) ge.each(e, function () {
            r(this.name, this.value)
        });
        else
            for (n in e) V(n, e[n], t, r);
        return i.join("&")
    }, ge.fn.extend({
        serialize: function () {
            return ge.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map(function () {
                var e = ge.prop(this, "elements");
                return e ? ge.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !ge(this).is(":disabled") && jt.test(this.nodeName) && !Dt.test(e) && (this.checked || !Ye.test(e))
            }).map(function (e, t) {
                var n = ge(this).val();
                return null == n ? null : Array.isArray(n) ? ge.map(n, function (e) {
                    return {
                        name: t.name,
                        value: e.replace(At, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(At, "\r\n")
                }
            }).get()
        }
    });
    var Nt = /%20/g,
        $t = /#.*$/,
        Ot = /([?&])_=[^&]*/,
        Pt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        It = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Lt = /^(?:GET|HEAD)$/,
        Ht = /^\/\//,
        zt = {},
        Mt = {},
        qt = "*/".concat("*"),
        Ft = ne.createElement("a");
    Ft.href = Tt.href, ge.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Tt.href,
            type: "GET",
            isLocal: It.test(Tt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": qt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": ge.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function (e, t) {
            return t ? G(G(e, ge.ajaxSettings), t) : G(ge.ajaxSettings, e)
        },
        ajaxPrefilter: K(zt),
        ajaxTransport: K(Mt),
        ajax: function (t, n) {
            function i(t, n, i, a) {
                var c, p, d, x, b, _ = n;
                u || (u = !0, l && e.clearTimeout(l), r = void 0, s = a || "", C.readyState = t > 0 ? 4 : 0, c = t >= 200 && t < 300 || 304 === t, i && (x = J(f, C, i)), x = ee(f, x, C, c), c ? (f.ifModified && (b = C.getResponseHeader("Last-Modified"), b && (ge.lastModified[o] = b), (b = C.getResponseHeader("etag")) && (ge.etag[o] = b)), 204 === t || "HEAD" === f.type ? _ = "nocontent" : 304 === t ? _ = "notmodified" : (_ = x.state, p = x.data, d = x.error, c = !d)) : (d = _, !t && _ || (_ = "error", t < 0 && (t = 0))), C.status = t, C.statusText = (n || _) + "", c ? v.resolveWith(g, [p, _, C]) : v.rejectWith(g, [C, _, d]), C.statusCode(w), w = void 0, h && m.trigger(c ? "ajaxSuccess" : "ajaxError", [C, f, c ? p : d]), y.fireWith(g, [C, _]), h && (m.trigger("ajaxComplete", [C, f]), --ge.active || ge.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (n = t, t = void 0), n = n || {};
            var r, o, s, a, l, c, u, h, p, d, f = ge.ajaxSetup({}, n),
                g = f.context || f,
                m = f.context && (g.nodeType || g.jquery) ? ge(g) : ge.event,
                v = ge.Deferred(),
                y = ge.Callbacks("once memory"),
                w = f.statusCode || {},
                x = {},
                b = {},
                _ = "canceled",
                C = {
                    readyState: 0,
                    getResponseHeader: function (e) {
                        var t;
                        if (u) {
                            if (!a)
                                for (a = {}; t = Pt.exec(s);) a[t[1].toLowerCase()] = t[2];
                            t = a[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function () {
                        return u ? s : null
                    },
                    setRequestHeader: function (e, t) {
                        return null == u && (e = b[e.toLowerCase()] = b[e.toLowerCase()] || e, x[e] = t), this
                    },
                    overrideMimeType: function (e) {
                        return null == u && (f.mimeType = e), this
                    },
                    statusCode: function (e) {
                        var t;
                        if (e)
                            if (u) C.always(e[C.status]);
                            else
                                for (t in e) w[t] = [w[t], e[t]];
                        return this
                    },
                    abort: function (e) {
                        var t = e || _;
                        return r && r.abort(t), i(0, t), this
                    }
                };
            if (v.promise(C), f.url = ((t || f.url || Tt.href) + "").replace(Ht, Tt.protocol + "//"), f.type = n.method || n.type || f.method || f.type, f.dataTypes = (f.dataType || "*").toLowerCase().match(je) || [""], null == f.crossDomain) {
                c = ne.createElement("a");
                try {
                    c.href = f.url, c.href = c.href, f.crossDomain = Ft.protocol + "//" + Ft.host != c.protocol + "//" + c.host
                } catch (e) {
                    f.crossDomain = !0
                }
            }
            if (f.data && f.processData && "string" != typeof f.data && (f.data = ge.param(f.data, f.traditional)), Z(zt, f, n, C), u) return C;
            h = ge.event && f.global, h && 0 == ge.active++ && ge.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !Lt.test(f.type), o = f.url.replace($t, ""), f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(Nt, "+")) : (d = f.url.slice(o.length), f.data && (o += (Et.test(o) ? "&" : "?") + f.data, delete f.data), !1 === f.cache && (o = o.replace(Ot, "$1"), d = (Et.test(o) ? "&" : "?") + "_=" + St++ + d), f.url = o + d), f.ifModified && (ge.lastModified[o] && C.setRequestHeader("If-Modified-Since", ge.lastModified[o]), ge.etag[o] && C.setRequestHeader("If-None-Match", ge.etag[o])), (f.data && f.hasContent && !1 !== f.contentType || n.contentType) && C.setRequestHeader("Content-Type", f.contentType), C.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + qt + "; q=0.01" : "") : f.accepts["*"]);
            for (p in f.headers) C.setRequestHeader(p, f.headers[p]);
            if (f.beforeSend && (!1 === f.beforeSend.call(g, C, f) || u)) return C.abort();
            if (_ = "abort", y.add(f.complete), C.done(f.success), C.fail(f.error), r = Z(Mt, f, n, C)) {
                if (C.readyState = 1, h && m.trigger("ajaxSend", [C, f]), u) return C;
                f.async && f.timeout > 0 && (l = e.setTimeout(function () {
                    C.abort("timeout")
                }, f.timeout));
                try {
                    u = !1, r.send(x, i)
                } catch (e) {
                    if (u) throw e;
                    i(-1, e)
                }
            } else i(-1, "No Transport");
            return C
        },
        getJSON: function (e, t, n) {
            return ge.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return ge.get(e, void 0, t, "script")
        }
    }), ge.each(["get", "post"], function (e, t) {
        ge[t] = function (e, n, i, r) {
            return ge.isFunction(n) && (r = r || i, i = n, n = void 0), ge.ajax(ge.extend({
                url: e,
                type: t,
                dataType: r,
                data: n,
                success: i
            }, ge.isPlainObject(e) && e))
        }
    }), ge._evalUrl = function (e) {
        return ge.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            throws: !0
        })
    }, ge.fn.extend({
        wrapAll: function (e) {
            var t;
            return this[0] && (ge.isFunction(e) && (e = e.call(this[0])), t = ge(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this
        },
        wrapInner: function (e) {
            return ge.isFunction(e) ? this.each(function (t) {
                ge(this).wrapInner(e.call(this, t))
            }) : this.each(function () {
                var t = ge(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function (e) {
            var t = ge.isFunction(e);
            return this.each(function (n) {
                ge(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function (e) {
            return this.parent(e).not("body").each(function () {
                ge(this).replaceWith(this.childNodes)
            }), this
        }
    }), ge.expr.pseudos.hidden = function (e) {
        return !ge.expr.pseudos.visible(e)
    }, ge.expr.pseudos.visible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, ge.ajaxSettings.xhr = function () {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    };
    var Rt = {
            0: 200,
            1223: 204
        },
        Bt = ge.ajaxSettings.xhr();
    de.cors = !!Bt && "withCredentials" in Bt, de.ajax = Bt = !!Bt, ge.ajaxTransport(function (t) {
        var n, i;
        if (de.cors || Bt && !t.crossDomain) return {
            send: function (r, o) {
                var s, a = t.xhr();
                if (a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                    for (s in t.xhrFields) a[s] = t.xhrFields[s];
                t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType), t.crossDomain || r["X-Requested-With"] || (r["X-Requested-With"] = "XMLHttpRequest");
                for (s in r) a.setRequestHeader(s, r[s]);
                n = function (e) {
                    return function () {
                        n && (n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === e ? a.abort() : "error" === e ? "number" != typeof a.status ? o(0, "error") : o(a.status, a.statusText) : o(Rt[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                            binary: a.response
                        } : {
                            text: a.responseText
                        }, a.getAllResponseHeaders()))
                    }
                }, a.onload = n(), i = a.onerror = n("error"), void 0 !== a.onabort ? a.onabort = i : a.onreadystatechange = function () {
                    4 === a.readyState && e.setTimeout(function () {
                        n && i()
                    })
                }, n = n("abort");
                try {
                    a.send(t.hasContent && t.data || null)
                } catch (e) {
                    if (n) throw e
                }
            },
            abort: function () {
                n && n()
            }
        }
    }), ge.ajaxPrefilter(function (e) {
        e.crossDomain && (e.contents.script = !1)
    }), ge.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function (e) {
                return ge.globalEval(e), e
            }
        }
    }), ge.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), ge.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var t, n;
            return {
                send: function (i, r) {
                    t = ge("<script>").prop({
                        charset: e.scriptCharset,
                        src: e.url
                    }).on("load error", n = function (e) {
                        t.remove(), n = null, e && r("error" === e.type ? 404 : 200, e.type)
                    }), ne.head.appendChild(t[0])
                },
                abort: function () {
                    n && n()
                }
            }
        }
    });
    var Wt = [],
        Yt = /(=)\?(?=&|$)|\?\?/;
    ge.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var e = Wt.pop() || ge.expando + "_" + St++;
            return this[e] = !0, e
        }
    }), ge.ajaxPrefilter("json jsonp", function (t, n, i) {
        var r, o, s, a = !1 !== t.jsonp && (Yt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(t.data) && "data");
        if (a || "jsonp" === t.dataTypes[0]) return r = t.jsonpCallback = ge.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(Yt, "$1" + r) : !1 !== t.jsonp && (t.url += (Et.test(t.url) ? "&" : "?") + t.jsonp + "=" + r), t.converters["script json"] = function () {
            return s || ge.error(r + " was not called"), s[0]
        }, t.dataTypes[0] = "json", o = e[r], e[r] = function () {
            s = arguments
        }, i.always(function () {
            void 0 === o ? ge(e).removeProp(r) : e[r] = o, t[r] && (t.jsonpCallback = n.jsonpCallback, Wt.push(r)), s && ge.isFunction(o) && o(s[0]), s = o = void 0
        }), "script"
    }), de.createHTMLDocument = function () {
        var e = ne.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
    }(), ge.parseHTML = function (e, t, n) {
        if ("string" != typeof e) return [];
        "boolean" == typeof t && (n = t, t = !1);
        var i, r, o;
        return t || (de.createHTMLDocument ? (t = ne.implementation.createHTMLDocument(""), i = t.createElement("base"), i.href = ne.location.href, t.head.appendChild(i)) : t = ne), r = Te.exec(e), o = !n && [], r ? [t.createElement(r[1])] : (r = x([e], t, o), o && o.length && ge(o).remove(), ge.merge([], r.childNodes))
    }, ge.fn.load = function (e, t, n) {
        var i, r, o, s = this,
            a = e.indexOf(" ");
        return a > -1 && (i = Q(e.slice(a)), e = e.slice(0, a)), ge.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (r = "POST"), s.length > 0 && ge.ajax({
            url: e,
            type: r || "GET",
            dataType: "html",
            data: t
        }).done(function (e) {
            o = arguments, s.html(i ? ge("<div>").append(ge.parseHTML(e)).find(i) : e)
        }).always(n && function (e, t) {
            s.each(function () {
                n.apply(this, o || [e.responseText, t, e])
            })
        }), this
    }, ge.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        ge.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), ge.expr.pseudos.animated = function (e) {
        return ge.grep(ge.timers, function (t) {
            return e === t.elem
        }).length
    }, ge.offset = {
        setOffset: function (e, t, n) {
            var i, r, o, s, a, l, c, u = ge.css(e, "position"),
                h = ge(e),
                p = {};
            "static" === u && (e.style.position = "relative"), a = h.offset(), o = ge.css(e, "top"), l = ge.css(e, "left"), c = ("absolute" === u || "fixed" === u) && (o + l).indexOf("auto") > -1, c ? (i = h.position(), s = i.top, r = i.left) : (s = parseFloat(o) || 0, r = parseFloat(l) || 0), ge.isFunction(t) && (t = t.call(e, n, ge.extend({}, a))), null != t.top && (p.top = t.top - a.top + s), null != t.left && (p.left = t.left - a.left + r), "using" in t ? t.using.call(e, p) : h.css(p)
        }
    }, ge.fn.extend({
        offset: function (e) {
            if (arguments.length) return void 0 === e ? this : this.each(function (t) {
                ge.offset.setOffset(this, e, t)
            });
            var t, n, i, r, o = this[0];
            return o ? o.getClientRects().length ? (i = o.getBoundingClientRect(), t = o.ownerDocument, n = t.documentElement, r = t.defaultView, {
                top: i.top + r.pageYOffset - n.clientTop,
                left: i.left + r.pageXOffset - n.clientLeft
            }) : {
                top: 0,
                left: 0
            } : void 0
        },
        position: function () {
            if (this[0]) {
                var e, t, n = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === ge.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), r(e[0], "html") || (i = e.offset()), i = {
                    top: i.top + ge.css(e[0], "borderTopWidth", !0),
                    left: i.left + ge.css(e[0], "borderLeftWidth", !0)
                }), {
                    top: t.top - i.top - ge.css(n, "marginTop", !0),
                    left: t.left - i.left - ge.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var e = this.offsetParent; e && "static" === ge.css(e, "position");) e = e.offsetParent;
                return e || Ke
            })
        }
    }), ge.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function (e, t) {
        var n = "pageYOffset" === t;
        ge.fn[e] = function (i) {
            return Oe(this, function (e, i, r) {
                var o;
                return ge.isWindow(e) ? o = e : 9 === e.nodeType && (o = e.defaultView), void 0 === r ? o ? o[t] : e[i] : void(o ? o.scrollTo(n ? o.pageXOffset : r, n ? r : o.pageYOffset) : e[i] = r)
            }, e, i, arguments.length)
        }
    }), ge.each(["top", "left"], function (e, t) {
        ge.cssHooks[t] = O(de.pixelPosition, function (e, n) {
            if (n) return n = $(e, t), st.test(n) ? ge(e).position()[t] + "px" : n
        })
    }), ge.each({
        Height: "height",
        Width: "width"
    }, function (e, t) {
        ge.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function (n, i) {
            ge.fn[i] = function (r, o) {
                var s = arguments.length && (n || "boolean" != typeof r),
                    a = n || (!0 === r || !0 === o ? "margin" : "border");
                return Oe(this, function (t, n, r) {
                    var o;
                    return ge.isWindow(t) ? 0 === i.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === r ? ge.css(t, n, a) : ge.style(t, n, r, a)
                }, t, s ? r : void 0, s)
            }
        })
    }), ge.fn.extend({
        bind: function (e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function (e, t) {
            return this.off(e, null, t)
        },
        delegate: function (e, t, n, i) {
            return this.on(t, e, n, i)
        },
        undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), ge.holdReady = function (e) {
        e ? ge.readyWait++ : ge.ready(!0)
    }, ge.isArray = Array.isArray, ge.parseJSON = JSON.parse, ge.nodeName = r, "function" == typeof define && define.amd && define("jquery", [], function () {
        return ge
    });
    var Xt = e.jQuery,
        Qt = e.$;
    return ge.noConflict = function (t) {
        return e.$ === ge && (e.$ = Qt), t && e.jQuery === ge && (e.jQuery = Xt), ge
    }, t || (e.jQuery = e.$ = ge), ge
}),
function (e, t, n, i) {
    function r(t, n) {
        this.settings = null, this.options = e.extend({}, r.Defaults, n), this.$element = e(t), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        }, this._states = {
            current: {},
            tags: {
                initializing: ["busy"],
                animating: ["busy"],
                dragging: ["interacting"]
            }
        }, e.each(["onResize", "onThrottledResize"], e.proxy(function (t, n) {
            this._handlers[n] = e.proxy(this[n], this)
        }, this)), e.each(r.Plugins, e.proxy(function (e, t) {
            this._plugins[e.charAt(0).toLowerCase() + e.slice(1)] = new t(this)
        }, this)), e.each(r.Workers, e.proxy(function (t, n) {
            this._pipe.push({
                filter: n.filter,
                run: e.proxy(n.run, this)
            })
        }, this)), this.setup(), this.initialize()
    }
    r.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: t,
        fallbackEasing: "swing",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }, r.Width = {
        Default: "default",
        Inner: "inner",
        Outer: "outer"
    }, r.Type = {
        Event: "event",
        State: "state"
    }, r.Plugins = {}, r.Workers = [{
        filter: ["width", "settings"],
        run: function () {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (e) {
            e.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"],
        run: function () {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (e) {
            var t = this.settings.margin || "",
                n = !this.settings.autoWidth,
                i = this.settings.rtl,
                r = {
                    width: "auto",
                    "margin-left": i ? t : "",
                    "margin-right": i ? "" : t
                };
            !n && this.$stage.children().css(r), e.css = r
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (e) {
            var t = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                n = null,
                i = this._items.length,
                r = !this.settings.autoWidth,
                o = [];
            for (e.items = {
                    merge: !1,
                    width: t
                }; i--;) n = this._mergers[i], n = this.settings.mergeFit && Math.min(n, this.settings.items) || n, e.items.merge = n > 1 || e.items.merge, o[i] = r ? t * n : this._items[i].width();
            this._widths = o
        }
    }, {
        filter: ["items", "settings"],
        run: function () {
            var t = [],
                n = this._items,
                i = this.settings,
                r = Math.max(2 * i.items, 4),
                o = 2 * Math.ceil(n.length / 2),
                s = i.loop && n.length ? i.rewind ? r : Math.max(r, o) : 0,
                a = "",
                l = "";
            for (s /= 2; s--;) t.push(this.normalize(t.length / 2, !0)), a += n[t[t.length - 1]][0].outerHTML, t.push(this.normalize(n.length - 1 - (t.length - 1) / 2, !0)), l = n[t[t.length - 1]][0].outerHTML + l;
            this._clones = t, e(a).addClass("cloned").appendTo(this.$stage), e(l).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function () {
            for (var e = this.settings.rtl ? 1 : -1, t = this._clones.length + this._items.length, n = -1, i = 0, r = 0, o = []; ++n < t;) i = o[n - 1] || 0, r = this._widths[this.relative(n)] + this.settings.margin, o.push(i + r * e);
            this._coordinates = o
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function () {
            var e = this.settings.stagePadding,
                t = this._coordinates,
                n = {
                    width: Math.ceil(Math.abs(t[t.length - 1])) + 2 * e,
                    "padding-left": e || "",
                    "padding-right": e || ""
                };
            this.$stage.css(n)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (e) {
            var t = this._coordinates.length,
                n = !this.settings.autoWidth,
                i = this.$stage.children();
            if (n && e.items.merge)
                for (; t--;) e.css.width = this._widths[this.relative(t)], i.eq(t).css(e.css);
            else n && (e.css.width = e.items.width, i.css(e.css))
        }
    }, {
        filter: ["items"],
        run: function () {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function (e) {
            e.current = e.current ? this.$stage.children().index(e.current) : 0, e.current = Math.max(this.minimum(), Math.min(this.maximum(), e.current)), this.reset(e.current)
        }
    }, {
        filter: ["position"],
        run: function () {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"],
        run: function () {
            var e, t, n, i, r = this.settings.rtl ? 1 : -1,
                o = 2 * this.settings.stagePadding,
                s = this.coordinates(this.current()) + o,
                a = s + this.width() * r,
                l = [];
            for (n = 0, i = this._coordinates.length; n < i; n++) e = this._coordinates[n - 1] || 0, t = Math.abs(this._coordinates[n]) + o * r, (this.op(e, "<=", s) && this.op(e, ">", a) || this.op(t, "<", s) && this.op(t, ">", a)) && l.push(n);
            this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
        }
    }], r.prototype.initialize = function () {
        if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
            var t, n, r;
            t = this.$element.find("img"), n = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : i, r = this.$element.children(n).width(), t.length && r <= 0 && this.preloadAutoWidthImages(t)
        }
        this.$element.addClass(this.options.loadingClass), this.$stage = e("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
    }, r.prototype.setup = function () {
        var t = this.viewport(),
            n = this.options.responsive,
            i = -1,
            r = null;
        n ? (e.each(n, function (e) {
            e <= t && e > i && (i = Number(e))
        }), r = e.extend({}, this.options, n[i]), "function" == typeof r.stagePadding && (r.stagePadding = r.stagePadding()), delete r.responsive, r.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + i))) : r = e.extend({}, this.options), this.trigger("change", {
            property: {
                name: "settings",
                value: r
            }
        }), this._breakpoint = i, this.settings = r, this.invalidate("settings"), this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    }, r.prototype.optionsLogic = function () {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, r.prototype.prepare = function (t) {
        var n = this.trigger("prepare", {
            content: t
        });
        return n.data || (n.data = e("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(t)), this.trigger("prepared", {
            content: n.data
        }), n.data
    }, r.prototype.update = function () {
        for (var t = 0, n = this._pipe.length, i = e.proxy(function (e) {
                return this[e]
            }, this._invalidated), r = {}; t < n;)(this._invalidated.all || e.grep(this._pipe[t].filter, i).length > 0) && this._pipe[t].run(r), t++;
        this._invalidated = {}, !this.is("valid") && this.enter("valid")
    }, r.prototype.width = function (e) {
        switch (e = e || r.Width.Default) {
            case r.Width.Inner:
            case r.Width.Outer:
                return this._width;
            default:
                return this._width - 2 * this.settings.stagePadding + this.settings.margin
        }
    }, r.prototype.refresh = function () {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
    }, r.prototype.onThrottledResize = function () {
        t.clearTimeout(this.resizeTimer), this.resizeTimer = t.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    }, r.prototype.onResize = function () {
        return !!this._items.length && this._width !== this.$element.width() && !!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
    }, r.prototype.registerEventHandlers = function () {
        e.support.transition && this.$stage.on(e.support.transition.end + ".owl.core", e.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(t, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", e.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
            return !1
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", e.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", e.proxy(this.onDragEnd, this)))
    }, r.prototype.onDragStart = function (t) {
        var i = null;
        3 !== t.which && (e.support.transform ? (i = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), i = {
            x: i[16 === i.length ? 12 : 4],
            y: i[16 === i.length ? 13 : 5]
        }) : (i = this.$stage.position(), i = {
            x: this.settings.rtl ? i.left + this.$stage.width() - this.width() + this.settings.margin : i.left,
            y: i.top
        }), this.is("animating") && (e.support.transform ? this.animate(i.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === t.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = e(t.target), this._drag.stage.start = i, this._drag.stage.current = i, this._drag.pointer = this.pointer(t), e(n).on("mouseup.owl.core touchend.owl.core", e.proxy(this.onDragEnd, this)), e(n).one("mousemove.owl.core touchmove.owl.core", e.proxy(function (t) {
            var i = this.difference(this._drag.pointer, this.pointer(t));
            e(n).on("mousemove.owl.core touchmove.owl.core", e.proxy(this.onDragMove, this)), Math.abs(i.x) < Math.abs(i.y) && this.is("valid") || (t.preventDefault(), this.enter("dragging"), this.trigger("drag"))
        }, this)))
    }, r.prototype.onDragMove = function (e) {
        var t = null,
            n = null,
            i = null,
            r = this.difference(this._drag.pointer, this.pointer(e)),
            o = this.difference(this._drag.stage.start, r);
        this.is("dragging") && (e.preventDefault(), this.settings.loop ? (t = this.coordinates(this.minimum()), n = this.coordinates(this.maximum() + 1) - t, o.x = ((o.x - t) % n + n) % n + t) : (t = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), n = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), i = this.settings.pullDrag ? -1 * r.x / 5 : 0, o.x = Math.max(Math.min(o.x, t + i), n + i)), this._drag.stage.current = o, this.animate(o.x))
    }, r.prototype.onDragEnd = function (t) {
        var i = this.difference(this._drag.pointer, this.pointer(t)),
            r = this._drag.stage.current,
            o = i.x > 0 ^ this.settings.rtl ? "left" : "right";
        e(n).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== i.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(r.x, 0 !== i.x ? o : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = o, (Math.abs(i.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
            return !1
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
    }, r.prototype.closest = function (t, n) {
        var i = -1,
            r = 30,
            o = this.width(),
            s = this.coordinates();
        return this.settings.freeDrag || e.each(s, e.proxy(function (e, a) {
            return "left" === n && t > a - r && t < a + r ? i = e : "right" === n && t > a - o - r && t < a - o + r ? i = e + 1 : this.op(t, "<", a) && this.op(t, ">", s[e + 1] || a - o) && (i = "left" === n ? e + 1 : e), -1 === i
        }, this)), this.settings.loop || (this.op(t, ">", s[this.minimum()]) ? i = t = this.minimum() : this.op(t, "<", s[this.maximum()]) && (i = t = this.maximum())), i
    }, r.prototype.animate = function (t) {
        var n = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(), n && (this.enter("animating"), this.trigger("translate")), e.support.transform3d && e.support.transition ? this.$stage.css({
            transform: "translate3d(" + t + "px,0px,0px)",
            transition: this.speed() / 1e3 + "s"
        }) : n ? this.$stage.animate({
            left: t + "px"
        }, this.speed(), this.settings.fallbackEasing, e.proxy(this.onTransitionEnd, this)) : this.$stage.css({
            left: t + "px"
        })
    }, r.prototype.is = function (e) {
        return this._states.current[e] && this._states.current[e] > 0
    }, r.prototype.current = function (e) {
        if (e === i) return this._current;
        if (0 === this._items.length) return i;
        if (e = this.normalize(e), this._current !== e) {
            var t = this.trigger("change", {
                property: {
                    name: "position",
                    value: e
                }
            });
            t.data !== i && (e = this.normalize(t.data)), this._current = e, this.invalidate("position"), this.trigger("changed", {
                property: {
                    name: "position",
                    value: this._current
                }
            })
        }
        return this._current
    }, r.prototype.invalidate = function (t) {
        return "string" === e.type(t) && (this._invalidated[t] = !0, this.is("valid") && this.leave("valid")), e.map(this._invalidated, function (e, t) {
            return t
        })
    }, r.prototype.reset = function (e) {
        (e = this.normalize(e)) !== i && (this._speed = 0, this._current = e, this.suppress(["translate", "translated"]), this.animate(this.coordinates(e)), this.release(["translate", "translated"]))
    }, r.prototype.normalize = function (e, t) {
        var n = this._items.length,
            r = t ? 0 : this._clones.length;
        return !this.isNumeric(e) || n < 1 ? e = i : (e < 0 || e >= n + r) && (e = ((e - r / 2) % n + n) % n + r / 2), e
    }, r.prototype.relative = function (e) {
        return e -= this._clones.length / 2, this.normalize(e, !0)
    }, r.prototype.maximum = function (e) {
        var t, n, i, r = this.settings,
            o = this._coordinates.length;
        if (r.loop) o = this._clones.length / 2 + this._items.length - 1;
        else if (r.autoWidth || r.merge) {
            for (t = this._items.length, n = this._items[--t].width(), i = this.$element.width(); t-- && !((n += this._items[t].width() + this.settings.margin) > i););
            o = t + 1
        } else o = r.center ? this._items.length - 1 : this._items.length - r.items;
        return e && (o -= this._clones.length / 2), Math.max(o, 0)
    }, r.prototype.minimum = function (e) {
        return e ? 0 : this._clones.length / 2
    }, r.prototype.items = function (e) {
        return e === i ? this._items.slice() : (e = this.normalize(e, !0), this._items[e])
    }, r.prototype.mergers = function (e) {
        return e === i ? this._mergers.slice() : (e = this.normalize(e, !0), this._mergers[e])
    }, r.prototype.clones = function (t) {
        var n = this._clones.length / 2,
            r = n + this._items.length,
            o = function (e) {
                return e % 2 == 0 ? r + e / 2 : n - (e + 1) / 2
            };
        return t === i ? e.map(this._clones, function (e, t) {
            return o(t)
        }) : e.map(this._clones, function (e, n) {
            return e === t ? o(n) : null
        })
    }, r.prototype.speed = function (e) {
        return e !== i && (this._speed = e), this._speed
    }, r.prototype.coordinates = function (t) {
        var n, r = 1,
            o = t - 1;
        return t === i ? e.map(this._coordinates, e.proxy(function (e, t) {
            return this.coordinates(t)
        }, this)) : (this.settings.center ? (this.settings.rtl && (r = -1, o = t + 1), n = this._coordinates[t], n += (this.width() - n + (this._coordinates[o] || 0)) / 2 * r) : n = this._coordinates[o] || 0, n = Math.ceil(n))
    }, r.prototype.duration = function (e, t, n) {
        return 0 === n ? 0 : Math.min(Math.max(Math.abs(t - e), 1), 6) * Math.abs(n || this.settings.smartSpeed)
    }, r.prototype.to = function (e, t) {
        var n = this.current(),
            i = null,
            r = e - this.relative(n),
            o = (r > 0) - (r < 0),
            s = this._items.length,
            a = this.minimum(),
            l = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(r) > s / 2 && (r += -1 * o * s), e = n + r, (i = ((e - a) % s + s) % s + a) !== e && i - r <= l && i - r > 0 && (n = i - r, e = i, this.reset(n))) : this.settings.rewind ? (l += 1, e = (e % l + l) % l) : e = Math.max(a, Math.min(l, e)), this.speed(this.duration(n, e, t)), this.current(e), this.$element.is(":visible") && this.update()
    }, r.prototype.next = function (e) {
        e = e || !1, this.to(this.relative(this.current()) + 1, e)
    }, r.prototype.prev = function (e) {
        e = e || !1, this.to(this.relative(this.current()) - 1, e)
    }, r.prototype.onTransitionEnd = function (e) {
        if (e !== i && (e.stopPropagation(), (e.target || e.srcElement || e.originalTarget) !== this.$stage.get(0))) return !1;
        this.leave("animating"), this.trigger("translated")
    }, r.prototype.viewport = function () {
        var i;
        return this.options.responsiveBaseElement !== t ? i = e(this.options.responsiveBaseElement).width() : t.innerWidth ? i = t.innerWidth : n.documentElement && n.documentElement.clientWidth ? i = n.documentElement.clientWidth : console.warn("Can not detect viewport width."), i
    }, r.prototype.replace = function (t) {
        this.$stage.empty(), this._items = [], t && (t = t instanceof jQuery ? t : e(t)), this.settings.nestedItemSelector && (t = t.find("." + this.settings.nestedItemSelector)), t.filter(function () {
            return 1 === this.nodeType
        }).each(e.proxy(function (e, t) {
            t = this.prepare(t), this.$stage.append(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, r.prototype.add = function (t, n) {
        var r = this.relative(this._current);
        n = n === i ? this._items.length : this.normalize(n, !0), t = t instanceof jQuery ? t : e(t), this.trigger("add", {
            content: t,
            position: n
        }), t = this.prepare(t), 0 === this._items.length || n === this._items.length ? (0 === this._items.length && this.$stage.append(t), 0 !== this._items.length && this._items[n - 1].after(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[n].before(t), this._items.splice(n, 0, t), this._mergers.splice(n, 0, 1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[r] && this.reset(this._items[r].index()), this.invalidate("items"), this.trigger("added", {
            content: t,
            position: n
        })
    }, r.prototype.remove = function (e) {
        (e = this.normalize(e, !0)) !== i && (this.trigger("remove", {
            content: this._items[e],
            position: e
        }), this._items[e].remove(), this._items.splice(e, 1), this._mergers.splice(e, 1), this.invalidate("items"), this.trigger("removed", {
            content: null,
            position: e
        }))
    }, r.prototype.preloadAutoWidthImages = function (t) {
        t.each(e.proxy(function (t, n) {
            this.enter("pre-loading"), n = e(n), e(new Image).one("load", e.proxy(function (e) {
                n.attr("src", e.target.src), n.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", n.attr("src") || n.attr("data-src") || n.attr("data-src-retina"))
        }, this))
    }, r.prototype.destroy = function () {
        this.$element.off(".owl.core"), this.$stage.off(".owl.core"), e(n).off(".owl.core"), !1 !== this.settings.responsive && (t.clearTimeout(this.resizeTimer), this.off(t, "resize", this._handlers.onThrottledResize));
        for (var i in this._plugins) this._plugins[i].destroy();
        this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    }, r.prototype.op = function (e, t, n) {
        var i = this.settings.rtl;
        switch (t) {
            case "<":
                return i ? e > n : e < n;
            case ">":
                return i ? e < n : e > n;
            case ">=":
                return i ? e <= n : e >= n;
            case "<=":
                return i ? e >= n : e <= n
        }
    }, r.prototype.on = function (e, t, n, i) {
        e.addEventListener ? e.addEventListener(t, n, i) : e.attachEvent && e.attachEvent("on" + t, n)
    }, r.prototype.off = function (e, t, n, i) {
        e.removeEventListener ? e.removeEventListener(t, n, i) : e.detachEvent && e.detachEvent("on" + t, n)
    }, r.prototype.trigger = function (t, n, i, o, s) {
        var a = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            l = e.camelCase(e.grep(["on", t, i], function (e) {
                return e
            }).join("-").toLowerCase()),
            c = e.Event([t, "owl", i || "carousel"].join(".").toLowerCase(), e.extend({
                relatedTarget: this
            }, a, n));
        return this._supress[t] || (e.each(this._plugins, function (e, t) {
            t.onTrigger && t.onTrigger(c)
        }), this.register({
            type: r.Type.Event,
            name: t
        }), this.$element.trigger(c), this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, c)), c
    }, r.prototype.enter = function (t) {
        e.each([t].concat(this._states.tags[t] || []), e.proxy(function (e, t) {
            this._states.current[t] === i && (this._states.current[t] = 0), this._states.current[t]++
        }, this))
    }, r.prototype.leave = function (t) {
        e.each([t].concat(this._states.tags[t] || []), e.proxy(function (e, t) {
            this._states.current[t]--
        }, this))
    }, r.prototype.register = function (t) {
        if (t.type === r.Type.Event) {
            if (e.event.special[t.name] || (e.event.special[t.name] = {}), !e.event.special[t.name].owl) {
                var n = e.event.special[t.name]._default;
                e.event.special[t.name]._default = function (e) {
                    return !n || !n.apply || e.namespace && -1 !== e.namespace.indexOf("owl") ? e.namespace && e.namespace.indexOf("owl") > -1 : n.apply(this, arguments)
                }, e.event.special[t.name].owl = !0
            }
        } else t.type === r.Type.State && (this._states.tags[t.name] ? this._states.tags[t.name] = this._states.tags[t.name].concat(t.tags) : this._states.tags[t.name] = t.tags, this._states.tags[t.name] = e.grep(this._states.tags[t.name], e.proxy(function (n, i) {
            return e.inArray(n, this._states.tags[t.name]) === i
        }, this)))
    }, r.prototype.suppress = function (t) {
        e.each(t, e.proxy(function (e, t) {
            this._supress[t] = !0
        }, this))
    }, r.prototype.release = function (t) {
        e.each(t, e.proxy(function (e, t) {
            delete this._supress[t]
        }, this))
    }, r.prototype.pointer = function (e) {
        var n = {
            x: null,
            y: null
        };
        return e = e.originalEvent || e || t.event, e = e.touches && e.touches.length ? e.touches[0] : e.changedTouches && e.changedTouches.length ? e.changedTouches[0] : e, e.pageX ? (n.x = e.pageX, n.y = e.pageY) : (n.x = e.clientX, n.y = e.clientY), n
    }, r.prototype.isNumeric = function (e) {
        return !isNaN(parseFloat(e))
    }, r.prototype.difference = function (e, t) {
        return {
            x: e.x - t.x,
            y: e.y - t.y
        }
    }, e.fn.owlCarousel = function (t) {
        var n = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var i = e(this),
                o = i.data("owl.carousel");
            o || (o = new r(this, "object" == typeof t && t), i.data("owl.carousel", o), e.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (t, n) {
                o.register({
                    type: r.Type.Event,
                    name: n
                }), o.$element.on(n + ".owl.carousel.core", e.proxy(function (e) {
                    e.namespace && e.relatedTarget !== this && (this.suppress([n]), o[n].apply(this, [].slice.call(arguments, 1)), this.release([n]))
                }, o))
            })), "string" == typeof t && "_" !== t.charAt(0) && o[t].apply(o, n)
        })
    }, e.fn.owlCarousel.Constructor = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this._core = t, this._interval = null, this._visible = null, this._handlers = {
            "initialized.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.autoRefresh && this.watch()
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    r.Defaults = {
        autoRefresh: !0,
        autoRefreshInterval: 500
    }, r.prototype.watch = function () {
        this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = t.setInterval(e.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
    }, r.prototype.refresh = function () {
        this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
    }, r.prototype.destroy = function () {
        var e, n;
        t.clearInterval(this._interval);
        for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
        for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.AutoRefresh = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this._core = t, this._loaded = [], this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": e.proxy(function (t) {
                if (t.namespace && this._core.settings && this._core.settings.lazyLoad && (t.property && "position" == t.property.name || "initialized" == t.type))
                    for (var n = this._core.settings, r = n.center && Math.ceil(n.items / 2) || n.items, o = n.center && -1 * r || 0, s = (t.property && t.property.value !== i ? t.property.value : this._core.current()) + o, a = this._core.clones().length, l = e.proxy(function (e, t) {
                            this.load(t)
                        }, this); o++ < r;) this.load(a / 2 + this._core.relative(s)), a && e.each(this._core.clones(this._core.relative(s)), l), s++
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    r.Defaults = {
        lazyLoad: !1
    }, r.prototype.load = function (n) {
        var i = this._core.$stage.children().eq(n),
            r = i && i.find(".owl-lazy");
        !r || e.inArray(i.get(0), this._loaded) > -1 || (r.each(e.proxy(function (n, i) {
            var r, o = e(i),
                s = t.devicePixelRatio > 1 && o.attr("data-src-retina") || o.attr("data-src");
            this._core.trigger("load", {
                element: o,
                url: s
            }, "lazy"), o.is("img") ? o.one("load.owl.lazy", e.proxy(function () {
                o.css("opacity", 1), this._core.trigger("loaded", {
                    element: o,
                    url: s
                }, "lazy")
            }, this)).attr("src", s) : (r = new Image, r.onload = e.proxy(function () {
                o.css({
                    "background-image": 'url("' + s + '")',
                    opacity: "1"
                }), this._core.trigger("loaded", {
                    element: o,
                    url: s
                }, "lazy")
            }, this), r.src = s)
        }, this)), this._loaded.push(i.get(0)))
    }, r.prototype.destroy = function () {
        var e, t;
        for (e in this.handlers) this._core.$element.off(e, this.handlers[e]);
        for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.Lazy = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this._core = t, this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.autoHeight && this.update()
            }, this),
            "changed.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.autoHeight && "position" == e.property.name && this.update()
            }, this),
            "loaded.owl.lazy": e.proxy(function (e) {
                e.namespace && this._core.settings.autoHeight && e.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    r.Defaults = {
        autoHeight: !1,
        autoHeightClass: "owl-height"
    }, r.prototype.update = function () {
        var t = this._core._current,
            n = t + this._core.settings.items,
            i = this._core.$stage.children().toArray().slice(t, n),
            r = [],
            o = 0;
        e.each(i, function (t, n) {
            r.push(e(n).height())
        }), o = Math.max.apply(null, r), this._core.$stage.parent().height(o).addClass(this._core.settings.autoHeightClass)
    }, r.prototype.destroy = function () {
        var e, t;
        for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
        for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.AutoHeight = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this._core = t, this._videos = {}, this._playing = null, this._handlers = {
            "initialized.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.register({
                    type: "state",
                    name: "playing",
                    tags: ["interacting"]
                })
            }, this),
            "resize.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.video && this.isInFullScreen() && e.preventDefault()
            }, this),
            "refreshed.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
            }, this),
            "changed.owl.carousel": e.proxy(function (e) {
                e.namespace && "position" === e.property.name && this._playing && this.stop()
            }, this),
            "prepared.owl.carousel": e.proxy(function (t) {
                if (t.namespace) {
                    var n = e(t.content).find(".owl-video");
                    n.length && (n.css("display", "none"), this.fetch(n, e(t.content)))
                }
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", e.proxy(function (e) {
            this.play(e)
        }, this))
    };
    r.Defaults = {
        video: !1,
        videoHeight: !1,
        videoWidth: !1
    }, r.prototype.fetch = function (e, t) {
        var n = function () {
                return e.attr("data-vimeo-id") ? "vimeo" : e.attr("data-vzaar-id") ? "vzaar" : "youtube"
            }(),
            i = e.attr("data-vimeo-id") || e.attr("data-youtube-id") || e.attr("data-vzaar-id"),
            r = e.attr("data-width") || this._core.settings.videoWidth,
            o = e.attr("data-height") || this._core.settings.videoHeight,
            s = e.attr("href");
        if (!s) throw new Error("Missing video URL.");
        if (i = s.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), i[3].indexOf("youtu") > -1) n = "youtube";
        else if (i[3].indexOf("vimeo") > -1) n = "vimeo";
        else {
            if (!(i[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
            n = "vzaar"
        }
        i = i[6], this._videos[s] = {
            type: n,
            id: i,
            width: r,
            height: o
        }, t.attr("data-video", s), this.thumbnail(e, this._videos[s])
    }, r.prototype.thumbnail = function (t, n) {
        var i, r, o, s = n.width && n.height ? 'style="width:' + n.width + "px;height:" + n.height + 'px;"' : "",
            a = t.find("img"),
            l = "src",
            c = "",
            u = this._core.settings,
            h = function (e) {
                r = '<div class="owl-video-play-icon"></div>', i = u.lazyLoad ? '<div class="owl-video-tn ' + c + '" ' + l + '="' + e + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + e + ')"></div>', t.after(i), t.after(r)
            };
        if (t.wrap('<div class="owl-video-wrapper"' + s + "></div>"), this._core.settings.lazyLoad && (l = "data-src", c = "owl-lazy"), a.length) return h(a.attr(l)), a.remove(), !1;
        "youtube" === n.type ? (o = "//img.youtube.com/vi/" + n.id + "/hqdefault.jpg", h(o)) : "vimeo" === n.type ? e.ajax({
            type: "GET",
            url: "//vimeo.com/api/v2/video/" + n.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (e) {
                o = e[0].thumbnail_large, h(o)
            }
        }) : "vzaar" === n.type && e.ajax({
            type: "GET",
            url: "//vzaar.com/api/videos/" + n.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (e) {
                o = e.framegrab_url, h(o)
            }
        })
    }, r.prototype.stop = function () {
        this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
    }, r.prototype.play = function (t) {
        var n, i = e(t.target),
            r = i.closest("." + this._core.settings.itemClass),
            o = this._videos[r.attr("data-video")],
            s = o.width || "100%",
            a = o.height || this._core.$stage.height();
        this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), r = this._core.items(this._core.relative(r.index())), this._core.reset(r.index()), "youtube" === o.type ? n = '<iframe width="' + s + '" height="' + a + '" src="//www.youtube.com/embed/' + o.id + "?autoplay=1&rel=0&v=" + o.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === o.type ? n = '<iframe src="//player.vimeo.com/video/' + o.id + '?autoplay=1" width="' + s + '" height="' + a + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === o.type && (n = '<iframe frameborder="0"height="' + a + '"width="' + s + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + o.id + '/player?autoplay=true"></iframe>'), e('<div class="owl-video-frame">' + n + "</div>").insertAfter(r.find(".owl-video")), this._playing = r.addClass("owl-video-playing"))
    }, r.prototype.isInFullScreen = function () {
        var t = n.fullscreenElement || n.mozFullScreenElement || n.webkitFullscreenElement;
        return t && e(t).parent().hasClass("owl-video-frame")
    }, r.prototype.destroy = function () {
        var e, t;
        this._core.$element.off("click.owl.video");
        for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
        for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.Video = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this.core = t, this.core.options = e.extend({}, r.Defaults, this.core.options), this.swapping = !0, this.previous = i, this.next = i, this.handlers = {
            "change.owl.carousel": e.proxy(function (e) {
                e.namespace && "position" == e.property.name && (this.previous = this.core.current(), this.next = e.property.value)
            }, this),
            "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": e.proxy(function (e) {
                e.namespace && (this.swapping = "translated" == e.type)
            }, this),
            "translate.owl.carousel": e.proxy(function (e) {
                e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
            }, this)
        }, this.core.$element.on(this.handlers)
    };
    r.Defaults = {
        animateOut: !1,
        animateIn: !1
    }, r.prototype.swap = function () {
        if (1 === this.core.settings.items && e.support.animation && e.support.transition) {
            this.core.speed(0);
            var t, n = e.proxy(this.clear, this),
                i = this.core.$stage.children().eq(this.previous),
                r = this.core.$stage.children().eq(this.next),
                o = this.core.settings.animateIn,
                s = this.core.settings.animateOut;
            this.core.current() !== this.previous && (s && (t = this.core.coordinates(this.previous) - this.core.coordinates(this.next), i.one(e.support.animation.end, n).css({
                left: t + "px"
            }).addClass("animated owl-animated-out").addClass(s)), o && r.one(e.support.animation.end, n).addClass("animated owl-animated-in").addClass(o))
        }
    }, r.prototype.clear = function (t) {
        e(t.target).css({
            left: ""
        }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
    }, r.prototype.destroy = function () {
        var e, t;
        for (e in this.handlers) this.core.$element.off(e, this.handlers[e]);
        for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.Animate = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    var r = function (t) {
        this._core = t, this._timeout = null, this._paused = !1, this._handlers = {
            "changed.owl.carousel": e.proxy(function (e) {
                e.namespace && "settings" === e.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : e.namespace && "position" === e.property.name && this._core.settings.autoplay && this._setAutoPlayInterval()
            }, this),
            "initialized.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.autoplay && this.play()
            }, this),
            "play.owl.autoplay": e.proxy(function (e, t, n) {
                e.namespace && this.play(t, n)
            }, this),
            "stop.owl.autoplay": e.proxy(function (e) {
                e.namespace && this.stop()
            }, this),
            "mouseover.owl.autoplay": e.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this),
            "mouseleave.owl.autoplay": e.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
            }, this),
            "touchstart.owl.core": e.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this),
            "touchend.owl.core": e.proxy(function () {
                this._core.settings.autoplayHoverPause && this.play()
            }, this)
        }, this._core.$element.on(this._handlers), this._core.options = e.extend({}, r.Defaults, this._core.options)
    };
    r.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    }, r.prototype.play = function (e, t) {
        this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval())
    }, r.prototype._getNextTimeout = function (i, r) {
        return this._timeout && t.clearTimeout(this._timeout), t.setTimeout(e.proxy(function () {
            this._paused || this._core.is("busy") || this._core.is("interacting") || n.hidden || this._core.next(r || this._core.settings.autoplaySpeed)
        }, this), i || this._core.settings.autoplayTimeout)
    }, r.prototype._setAutoPlayInterval = function () {
        this._timeout = this._getNextTimeout()
    }, r.prototype.stop = function () {
        this._core.is("rotating") && (t.clearTimeout(this._timeout), this._core.leave("rotating"))
    }, r.prototype.pause = function () {
        this._core.is("rotating") && (this._paused = !0)
    }, r.prototype.destroy = function () {
        var e, t;
        this.stop();
        for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
        for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.autoplay = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    "use strict";
    var r = function (t) {
        this._core = t, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        }, this._handlers = {
            "prepared.owl.carousel": e.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + e(t.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
            }, this),
            "added.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 0, this._templates.pop())
            }, this),
            "remove.owl.carousel": e.proxy(function (e) {
                e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 1)
            }, this),
            "changed.owl.carousel": e.proxy(function (e) {
                e.namespace && "position" == e.property.name && this.draw()
            }, this),
            "initialized.owl.carousel": e.proxy(function (e) {
                e.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
            }, this),
            "refreshed.owl.carousel": e.proxy(function (e) {
                e.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this.$element.on(this._handlers)
    };
    r.Defaults = {
        nav: !1,
        navText: ["prev", "next"],
        navSpeed: !1,
        navElement: "div",
        navContainer: !1,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    }, r.prototype.initialize = function () {
        var t, n = this._core.settings;
        this._controls.$relative = (n.navContainer ? e(n.navContainer) : e("<div>").addClass(n.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = e("<" + n.navElement + ">").addClass(n.navClass[0]).html(n.navText[0]).prependTo(this._controls.$relative).on("click", e.proxy(function (e) {
            this.prev(n.navSpeed)
        }, this)), this._controls.$next = e("<" + n.navElement + ">").addClass(n.navClass[1]).html(n.navText[1]).appendTo(this._controls.$relative).on("click", e.proxy(function (e) {
            this.next(n.navSpeed)
        }, this)), n.dotsData || (this._templates = [e("<div>").addClass(n.dotClass).append(e("<span>")).prop("outerHTML")]), this._controls.$absolute = (n.dotsContainer ? e(n.dotsContainer) : e("<div>").addClass(n.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", e.proxy(function (t) {
            var i = e(t.target).parent().is(this._controls.$absolute) ? e(t.target).index() : e(t.target).parent().index();
            t.preventDefault(), this.to(i, n.dotsSpeed)
        }, this));
        for (t in this._overrides) this._core[t] = e.proxy(this[t], this)
    }, r.prototype.destroy = function () {
        var e, t, n, i;
        for (e in this._handlers) this.$element.off(e, this._handlers[e]);
        for (t in this._controls) this._controls[t].remove();
        for (i in this.overides) this._core[i] = this._overrides[i];
        for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
    }, r.prototype.update = function () {
        var e, t, n, i = this._core.clones().length / 2,
            r = i + this._core.items().length,
            o = this._core.maximum(!0),
            s = this._core.settings,
            a = s.center || s.autoWidth || s.dotsData ? 1 : s.dotsEach || s.items;
        if ("page" !== s.slideBy && (s.slideBy = Math.min(s.slideBy, s.items)), s.dots || "page" == s.slideBy)
            for (this._pages = [], e = i, t = 0, n = 0; e < r; e++) {
                if (t >= a || 0 === t) {
                    if (this._pages.push({
                            start: Math.min(o, e - i),
                            end: e - i + a - 1
                        }), Math.min(o, e - i) === o) break;
                    t = 0, ++n
                }
                t += this._core.mergers(this._core.relative(e))
            }
    }, r.prototype.draw = function () {
        var t, n = this._core.settings,
            i = this._core.items().length <= n.items,
            r = this._core.relative(this._core.current()),
            o = n.loop || n.rewind;
        this._controls.$relative.toggleClass("disabled", !n.nav || i), n.nav && (this._controls.$previous.toggleClass("disabled", !o && r <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !o && r >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !n.dots || i), n.dots && (t = this._pages.length - this._controls.$absolute.children().length, n.dotsData && 0 !== t ? this._controls.$absolute.html(this._templates.join("")) : t > 0 ? this._controls.$absolute.append(new Array(t + 1).join(this._templates[0])) : t < 0 && this._controls.$absolute.children().slice(t).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(e.inArray(this.current(), this._pages)).addClass("active"))
    }, r.prototype.onTrigger = function (t) {
        var n = this._core.settings;
        t.page = {
            index: e.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: n && (n.center || n.autoWidth || n.dotsData ? 1 : n.dotsEach || n.items)
        }
    }, r.prototype.current = function () {
        var t = this._core.relative(this._core.current());
        return e.grep(this._pages, e.proxy(function (e, n) {
            return e.start <= t && e.end >= t
        }, this)).pop()
    }, r.prototype.getPosition = function (t) {
        var n, i, r = this._core.settings;
        return "page" == r.slideBy ? (n = e.inArray(this.current(), this._pages), i = this._pages.length, t ? ++n : --n, n = this._pages[(n % i + i) % i].start) : (n = this._core.relative(this._core.current()), i = this._core.items().length, t ? n += r.slideBy : n -= r.slideBy), n
    }, r.prototype.next = function (t) {
        e.proxy(this._overrides.to, this._core)(this.getPosition(!0), t)
    }, r.prototype.prev = function (t) {
        e.proxy(this._overrides.to, this._core)(this.getPosition(!1), t)
    }, r.prototype.to = function (t, n, i) {
        var r;
        !i && this._pages.length ? (r = this._pages.length, e.proxy(this._overrides.to, this._core)(this._pages[(t % r + r) % r].start, n)) : e.proxy(this._overrides.to, this._core)(t, n)
    }, e.fn.owlCarousel.Constructor.Plugins.Navigation = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    "use strict";
    var r = function (n) {
        this._core = n, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
            "initialized.owl.carousel": e.proxy(function (n) {
                n.namespace && "URLHash" === this._core.settings.startPosition && e(t).trigger("hashchange.owl.navigation")
            }, this),
            "prepared.owl.carousel": e.proxy(function (t) {
                if (t.namespace) {
                    var n = e(t.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                    if (!n) return;
                    this._hashes[n] = t.content
                }
            }, this),
            "changed.owl.carousel": e.proxy(function (n) {
                if (n.namespace && "position" === n.property.name) {
                    var i = this._core.items(this._core.relative(this._core.current())),
                        r = e.map(this._hashes, function (e, t) {
                            return e === i ? t : null
                        }).join();
                    if (!r || t.location.hash.slice(1) === r) return;
                    t.location.hash = r
                }
            }, this)
        }, this._core.options = e.extend({}, r.Defaults, this._core.options), this.$element.on(this._handlers), e(t).on("hashchange.owl.navigation", e.proxy(function (e) {
            var n = t.location.hash.substring(1),
                r = this._core.$stage.children(),
                o = this._hashes[n] && r.index(this._hashes[n]);
            o !== i && o !== this._core.current() && this._core.to(this._core.relative(o), !1, !0)
        }, this))
    };
    r.Defaults = {
        URLhashListener: !1
    }, r.prototype.destroy = function () {
        var n, i;
        e(t).off("hashchange.owl.navigation");
        for (n in this._handlers) this._core.$element.off(n, this._handlers[n]);
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, e.fn.owlCarousel.Constructor.Plugins.Hash = r
}(window.Zepto || window.jQuery, window, document),
function (e, t, n, i) {
    function r(t, n) {
        var r = !1,
            o = t.charAt(0).toUpperCase() + t.slice(1);
        return e.each((t + " " + a.join(o + " ") + o).split(" "), function (e, t) {
            if (s[t] !== i) return r = !n || t, !1
        }), r
    }

    function o(e) {
        return r(e, !0)
    }
    var s = e("<support>").get(0).style,
        a = "Webkit Moz O ms".split(" "),
        l = {
            transition: {
                end: {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd",
                    transition: "transitionend"
                }
            },
            animation: {
                end: {
                    WebkitAnimation: "webkitAnimationEnd",
                    MozAnimation: "animationend",
                    OAnimation: "oAnimationEnd",
                    animation: "animationend"
                }
            }
        },
        c = {
            csstransforms: function () {
                return !!r("transform")
            },
            csstransforms3d: function () {
                return !!r("perspective")
            },
            csstransitions: function () {
                return !!r("transition")
            },
            cssanimations: function () {
                return !!r("animation")
            }
        };
    c.csstransitions() && (e.support.transition = new String(o("transition")), e.support.transition.end = l.transition.end[e.support.transition]), c.cssanimations() && (e.support.animation = new String(o("animation")), e.support.animation.end = l.animation.end[e.support.animation]), c.csstransforms() && (e.support.transform = new String(o("transform")), e.support.transform3d = c.csstransforms3d())
}(window.Zepto || window.jQuery, window, document),
function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
}(function (e) {
    var t, n = navigator.userAgent,
        i = /iphone/i.test(n),
        r = /chrome/i.test(n),
        o = /android/i.test(n);
    e.mask = {
        definitions: {
            9: "[0-9]",
            a: "[A-Za-z]",
            "*": "[A-Za-z0-9]"
        },
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    }, e.fn.extend({
        caret: function (e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden")) return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                begin: e,
                end: t
            })
        },
        unmask: function () {
            return this.trigger("unmask")
        },
        mask: function (n, s) {
            var a, l, c, u, h, p, d, f;
            if (!n && this.length > 0) {
                a = e(this[0]);
                var g = a.data(e.mask.dataName);
                return g ? g() : void 0
            }
            return s = e.extend({
                autoclear: e.mask.autoclear,
                placeholder: e.mask.placeholder,
                completed: null
            }, s), l = e.mask.definitions, c = [], u = d = n.length, h = null, e.each(n.split(""), function (e, t) {
                "?" == t ? (d--, u = e) : l[t] ? (c.push(new RegExp(l[t])), null === h && (h = c.length - 1), e < u && (p = c.length - 1)) : c.push(null)
            }), this.trigger("unmask").each(function () {
                function a() {
                    if (s.completed) {
                        for (var e = h; e <= p; e++)
                            if (c[e] && A[e] === g(e)) return;
                        s.completed.call(k)
                    }
                }

                function g(e) {
                    return e < s.placeholder.length ? s.placeholder.charAt(e) : s.placeholder.charAt(0)
                }

                function m(e) {
                    for (; ++e < d && !c[e];);
                    return e
                }

                function v(e) {
                    for (; --e >= 0 && !c[e];);
                    return e
                }

                function y(e, t) {
                    var n, i;
                    if (!(e < 0)) {
                        for (n = e, i = m(t); n < d; n++)
                            if (c[n]) {
                                if (!(i < d && c[n].test(A[i]))) break;
                                A[n] = A[i], A[i] = g(i), i = m(i)
                            } S(), k.caret(Math.max(h, e))
                    }
                }

                function w(e) {
                    var t, n, i, r;
                    for (t = e, n = g(e); t < d; t++)
                        if (c[t]) {
                            if (i = m(t), r = A[t], A[t] = n, !(i < d && c[i].test(r))) break;
                            n = r
                        }
                }

                function x(e) {
                    var t = k.val(),
                        n = k.caret();
                    if (f && f.length && f.length > t.length) {
                        for (E(!0); n.begin > 0 && !c[n.begin - 1];) n.begin--;
                        if (0 === n.begin)
                            for (; n.begin < h && !c[n.begin];) n.begin++;
                        k.caret(n.begin, n.begin)
                    } else {
                        for (E(!0); n.begin < d && !c[n.begin];) n.begin++;
                        k.caret(n.begin, n.begin)
                    }
                    a()
                }

                function b(e) {
                    E(), k.val() != j && k.change()
                }

                function _(e) {
                    if (!k.prop("readonly")) {
                        var t, n, r, o = e.which || e.keyCode;
                        f = k.val(), 8 === o || 46 === o || i && 127 === o ? (t = k.caret(), n = t.begin, r = t.end, r - n == 0 && (n = 46 !== o ? v(n) : r = m(n - 1), r = 46 === o ? m(r) : r), T(n, r), y(n, r - 1), e.preventDefault()) : 13 === o ? b.call(this, e) : 27 === o && (k.val(j), k.caret(0, E()), e.preventDefault())
                    }
                }

                function C(t) {
                    if (!k.prop("readonly")) {
                        var n, i, r, s = t.which || t.keyCode,
                            l = k.caret();
                        if (!(t.ctrlKey || t.altKey || t.metaKey || s < 32) && s && 13 !== s) {
                            if (l.end - l.begin != 0 && (T(l.begin, l.end), y(l.begin, l.end - 1)), (n = m(l.begin - 1)) < d && (i = String.fromCharCode(s), c[n].test(i))) {
                                if (w(n), A[n] = i, S(), r = m(n), o) {
                                    var u = function () {
                                        e.proxy(e.fn.caret, k, r)()
                                    };
                                    setTimeout(u, 0)
                                } else k.caret(r);
                                l.begin <= p && a()
                            }
                            t.preventDefault()
                        }
                    }
                }

                function T(e, t) {
                    var n;
                    for (n = e; n < t && n < d; n++) c[n] && (A[n] = g(n))
                }

                function S() {
                    k.val(A.join(""))
                }

                function E(e) {
                    var t, n, i, r = k.val(),
                        o = -1;
                    for (t = 0, i = 0; t < d; t++)
                        if (c[t]) {
                            for (A[t] = g(t); i++ < r.length;)
                                if (n = r.charAt(i - 1), c[t].test(n)) {
                                    A[t] = n, o = t;
                                    break
                                } if (i > r.length) {
                                T(t + 1, d);
                                break
                            }
                        } else A[t] === r.charAt(i) && i++, t < u && (o = t);
                    return e ? S() : o + 1 < u ? s.autoclear || A.join("") === D ? (k.val() && k.val(""), T(0, d)) : S() : (S(), k.val(k.val().substring(0, o + 1))), u ? t : h
                }
                var k = e(this),
                    A = e.map(n.split(""), function (e, t) {
                        if ("?" != e) return l[e] ? g(t) : e
                    }),
                    D = A.join(""),
                    j = k.val();
                k.data(e.mask.dataName, function () {
                    return e.map(A, function (e, t) {
                        return c[t] && e != g(t) ? e : null
                    }).join("")
                }), k.one("unmask", function () {
                    k.off(".mask").removeData(e.mask.dataName)
                }).on("focus.mask", function () {
                    if (!k.prop("readonly")) {
                        clearTimeout(t);
                        var e;
                        j = k.val(), e = E(), t = setTimeout(function () {
                            k.get(0) === document.activeElement && (S(), e == n.replace("?", "").length ? k.caret(0, e) : k.caret(e))
                        }, 10)
                    }
                }).on("blur.mask", b).on("keydown.mask", _).on("keypress.mask", C).on("input.mask paste.mask", function () {
                    k.prop("readonly") || setTimeout(function () {
                        var e = E(!0);
                        k.caret(e), a()
                    }, 0)
                }), r && o && k.off("input.mask").on("input.mask", x), E()
            })
        }
    })
}),
function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
}(function (e) {
    var t, n, i, r, o, s, a = "Close",
        l = "BeforeClose",
        c = "MarkupParse",
        u = "Open",
        h = "Change",
        p = "mfp",
        d = "." + p,
        f = "mfp-ready",
        g = "mfp-removing",
        m = "mfp-prevent-close",
        v = function () {},
        y = !!window.jQuery,
        w = e(window),
        x = function (e, n) {
            t.ev.on(p + e + d, n)
        },
        b = function (t, n, i, r) {
            var o = document.createElement("div");
            return o.className = "mfp-" + t, i && (o.innerHTML = i), r ? n && n.appendChild(o) : (o = e(o), n && o.appendTo(n)), o
        },
        _ = function (n, i) {
            t.ev.triggerHandler(p + n, i), t.st.callbacks && (n = n.charAt(0).toLowerCase() + n.slice(1), t.st.callbacks[n] && t.st.callbacks[n].apply(t, e.isArray(i) ? i : [i]))
        },
        C = function (n) {
            return n === s && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)), s = n), t.currTemplate.closeBtn
        },
        T = function () {
            e.magnificPopup.instance || (t = new v, t.init(), e.magnificPopup.instance = t)
        },
        S = function () {
            var e = document.createElement("p").style,
                t = ["ms", "O", "Moz", "Webkit"];
            if (void 0 !== e.transition) return !0;
            for (; t.length;)
                if (t.pop() + "Transition" in e) return !0;
            return !1
        };
    v.prototype = {
        constructor: v,
        init: function () {
            var n = navigator.appVersion;
            t.isLowIE = t.isIE8 = document.all && !document.addEventListener, t.isAndroid = /android/gi.test(n), t.isIOS = /iphone|ipad|ipod/gi.test(n), t.supportsTransition = S(), t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), i = e(document), t.popupsCache = {}
        },
        open: function (n) {
            var r;
            if (!1 === n.isObj) {
                t.items = n.items.toArray(), t.index = 0;
                var s, a = n.items;
                for (r = 0; r < a.length; r++)
                    if (s = a[r], s.parsed && (s = s.el[0]), s === n.el[0]) {
                        t.index = r;
                        break
                    }
            } else t.items = e.isArray(n.items) ? n.items : [n.items], t.index = n.index || 0;
            if (t.isOpen) return void t.updateItemHTML();
            t.types = [], o = "", n.mainEl && n.mainEl.length ? t.ev = n.mainEl.eq(0) : t.ev = i, n.key ? (t.popupsCache[n.key] || (t.popupsCache[n.key] = {}), t.currTemplate = t.popupsCache[n.key]) : t.currTemplate = {}, t.st = e.extend(!0, {}, e.magnificPopup.defaults, n), t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = b("bg").on("click" + d, function () {
                t.close()
            }), t.wrap = b("wrap").attr("tabindex", -1).on("click" + d, function (e) {
                t._checkIfClose(e.target) && t.close()
            }), t.container = b("container", t.wrap)), t.contentContainer = b("content"), t.st.preloader && (t.preloader = b("preloader", t.container, t.st.tLoading));
            var l = e.magnificPopup.modules;
            for (r = 0; r < l.length; r++) {
                var h = l[r];
                h = h.charAt(0).toUpperCase() + h.slice(1), t["init" + h].call(t)
            }
            _("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (x(c, function (e, t, n, i) {
                n.close_replaceWith = C(i.type)
            }), o += " mfp-close-btn-in") : t.wrap.append(C())), t.st.alignTop && (o += " mfp-align-top"), t.fixedContentPos ? t.wrap.css({
                overflow: t.st.overflowY,
                overflowX: "hidden",
                overflowY: t.st.overflowY
            }) : t.wrap.css({
                top: w.scrollTop(),
                position: "absolute"
            }), (!1 === t.st.fixedBgPos || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                height: i.height(),
                position: "absolute"
            }), t.st.enableEscapeKey && i.on("keyup" + d, function (e) {
                27 === e.keyCode && t.close()
            }), w.on("resize" + d, function () {
                t.updateSize()
            }), t.st.closeOnContentClick || (o += " mfp-auto-cursor"), o && t.wrap.addClass(o);
            var p = t.wH = w.height(),
                g = {};
            if (t.fixedContentPos && t._hasScrollBar(p)) {
                var m = t._getScrollbarSize();
                m && (g.marginRight = m)
            }
            t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : g.overflow = "hidden");
            var v = t.st.mainClass;
            return t.isIE7 && (v += " mfp-ie7"), v && t._addClassToMFP(v), t.updateItemHTML(), _("BuildControls"), e("html").css(g), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || e(document.body)), t._lastFocusedEl = document.activeElement, setTimeout(function () {
                t.content ? (t._addClassToMFP(f), t._setFocus()) : t.bgOverlay.addClass(f), i.on("focusin" + d, t._onFocusIn)
            }, 16), t.isOpen = !0, t.updateSize(p), _(u), n
        },
        close: function () {
            t.isOpen && (_(l), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(g), setTimeout(function () {
                t._close()
            }, t.st.removalDelay)) : t._close())
        },
        _close: function () {
            _(a);
            var n = g + " " + f + " ";
            if (t.bgOverlay.detach(), t.wrap.detach(), t.container.empty(), t.st.mainClass && (n += t.st.mainClass + " "), t._removeClassFromMFP(n), t.fixedContentPos) {
                var r = {
                    marginRight: ""
                };
                t.isIE7 ? e("body, html").css("overflow", "") : r.overflow = "", e("html").css(r)
            }
            i.off("keyup" + d + " focusin" + d), t.ev.off(d), t.wrap.attr("class", "mfp-wrap").removeAttr("style"), t.bgOverlay.attr("class", "mfp-bg"), t.container.attr("class", "mfp-container"), !t.st.showCloseBtn || t.st.closeBtnInside && !0 !== t.currTemplate[t.currItem.type] || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(), t.st.autoFocusLast && t._lastFocusedEl && e(t._lastFocusedEl).focus(), t.currItem = null, t.content = null, t.currTemplate = null, t.prevHeight = 0, _("AfterClose")
        },
        updateSize: function (e) {
            if (t.isIOS) {
                var n = document.documentElement.clientWidth / window.innerWidth,
                    i = window.innerHeight * n;
                t.wrap.css("height", i), t.wH = i
            } else t.wH = e || w.height();
            t.fixedContentPos || t.wrap.css("height", t.wH), _("Resize")
        },
        updateItemHTML: function () {
            var n = t.items[t.index];
            t.contentContainer.detach(), t.content && t.content.detach(), n.parsed || (n = t.parseEl(t.index));
            var i = n.type;
            if (_("BeforeChange", [t.currItem ? t.currItem.type : "", i]), t.currItem = n, !t.currTemplate[i]) {
                var o = !!t.st[i] && t.st[i].markup;
                _("FirstMarkupParse", o), t.currTemplate[i] = !o || e(o)
            }
            r && r !== n.type && t.container.removeClass("mfp-" + r + "-holder");
            var s = t["get" + i.charAt(0).toUpperCase() + i.slice(1)](n, t.currTemplate[i]);
            t.appendContent(s, i), n.preloaded = !0, _(h, n), r = n.type, t.container.prepend(t.contentContainer), _("AfterChange")
        },
        appendContent: function (e, n) {
            t.content = e, e ? t.st.showCloseBtn && t.st.closeBtnInside && !0 === t.currTemplate[n] ? t.content.find(".mfp-close").length || t.content.append(C()) : t.content = e : t.content = "", _("BeforeAppend"), t.container.addClass("mfp-" + n + "-holder"), t.contentContainer.append(t.content)
        },
        parseEl: function (n) {
            var i, r = t.items[n];
            if (r.tagName ? r = {
                    el: e(r)
                } : (i = r.type, r = {
                    data: r,
                    src: r.src
                }), r.el) {
                for (var o = t.types, s = 0; s < o.length; s++)
                    if (r.el.hasClass("mfp-" + o[s])) {
                        i = o[s];
                        break
                    } r.src = r.el.attr("data-mfp-src"), r.src || (r.src = r.el.attr("href"))
            }
            return r.type = i || t.st.type || "inline", r.index = n, r.parsed = !0, t.items[n] = r, _("ElementParse", r), t.items[n]
        },
        addGroup: function (e, n) {
            var i = function (i) {
                i.mfpEl = this, t._openClick(i, e, n)
            };
            n || (n = {});
            var r = "click.magnificPopup";
            n.mainEl = e, n.items ? (n.isObj = !0, e.off(r).on(r, i)) : (n.isObj = !1, n.delegate ? e.off(r).on(r, n.delegate, i) : (n.items = e, e.off(r).on(r, i)))
        },
        _openClick: function (n, i, r) {
            if ((void 0 !== r.midClick ? r.midClick : e.magnificPopup.defaults.midClick) || !(2 === n.which || n.ctrlKey || n.metaKey || n.altKey || n.shiftKey)) {
                var o = void 0 !== r.disableOn ? r.disableOn : e.magnificPopup.defaults.disableOn;
                if (o)
                    if (e.isFunction(o)) {
                        if (!o.call(t)) return !0
                    } else if (w.width() < o) return !0;
                n.type && (n.preventDefault(), t.isOpen && n.stopPropagation()), r.el = e(n.mfpEl), r.delegate && (r.items = i.find(r.delegate)), t.open(r)
            }
        },
        updateStatus: function (e, i) {
            if (t.preloader) {
                n !== e && t.container.removeClass("mfp-s-" + n), i || "loading" !== e || (i = t.st.tLoading);
                var r = {
                    status: e,
                    text: i
                };
                _("UpdateStatus", r), e = r.status, i = r.text, t.preloader.html(i), t.preloader.find("a").on("click", function (e) {
                    e.stopImmediatePropagation()
                }), t.container.addClass("mfp-s-" + e), n = e
            }
        },
        _checkIfClose: function (n) {
            if (!e(n).hasClass(m)) {
                var i = t.st.closeOnContentClick,
                    r = t.st.closeOnBgClick;
                if (i && r) return !0;
                if (!t.content || e(n).hasClass("mfp-close") || t.preloader && n === t.preloader[0]) return !0;
                if (n === t.content[0] || e.contains(t.content[0], n)) {
                    if (i) return !0
                } else if (r && e.contains(document, n)) return !0;
                return !1
            }
        },
        _addClassToMFP: function (e) {
            t.bgOverlay.addClass(e), t.wrap.addClass(e)
        },
        _removeClassFromMFP: function (e) {
            this.bgOverlay.removeClass(e), t.wrap.removeClass(e)
        },
        _hasScrollBar: function (e) {
            return (t.isIE7 ? i.height() : document.body.scrollHeight) > (e || w.height())
        },
        _setFocus: function () {
            (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
        },
        _onFocusIn: function (n) {
            return n.target === t.wrap[0] || e.contains(t.wrap[0], n.target) ? void 0 : (t._setFocus(), !1)
        },
        _parseMarkup: function (t, n, i) {
            var r;
            i.data && (n = e.extend(i.data, n)), _(c, [t, n, i]), e.each(n, function (n, i) {
                if (void 0 === i || !1 === i) return !0;
                if (r = n.split("_"), r.length > 1) {
                    var o = t.find(d + "-" + r[0]);
                    if (o.length > 0) {
                        var s = r[1];
                        "replaceWith" === s ? o[0] !== i[0] && o.replaceWith(i) : "img" === s ? o.is("img") ? o.attr("src", i) : o.replaceWith(e("<img>").attr("src", i).attr("class", o.attr("class"))) : o.attr(r[1], i)
                    }
                } else t.find(d + "-" + n).html(i)
            })
        },
        _getScrollbarSize: function () {
            if (void 0 === t.scrollbarSize) {
                var e = document.createElement("div");
                e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(e), t.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e)
            }
            return t.scrollbarSize
        }
    }, e.magnificPopup = {
        instance: null,
        proto: v.prototype,
        modules: [],
        open: function (t, n) {
            return T(), t = t ? e.extend(!0, {}, t) : {}, t.isObj = !0, t.index = n || 0, this.instance.open(t)
        },
        close: function () {
            return e.magnificPopup.instance && e.magnificPopup.instance.close()
        },
        registerModule: function (t, n) {
            n.options && (e.magnificPopup.defaults[t] = n.options), e.extend(this.proto, n.proto), this.modules.push(t)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading...",
            autoFocusLast: !0
        }
    }, e.fn.magnificPopup = function (n) {
        T();
        var i = e(this);
        if ("string" == typeof n)
            if ("open" === n) {
                var r, o = y ? i.data("magnificPopup") : i[0].magnificPopup,
                    s = parseInt(arguments[1], 10) || 0;
                o.items ? r = o.items[s] : (r = i, o.delegate && (r = r.find(o.delegate)), r = r.eq(s)), t._openClick({
                    mfpEl: r
                }, i, o)
            } else t.isOpen && t[n].apply(t, Array.prototype.slice.call(arguments, 1));
        else n = e.extend(!0, {}, n), y ? i.data("magnificPopup", n) : i[0].magnificPopup = n, t.addGroup(i, n);
        return i
    };
    var E, k, A, D = "inline",
        j = function () {
            A && (k.after(A.addClass(E)).detach(), A = null)
        };
    e.magnificPopup.registerModule(D, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function () {
                t.types.push(D), x(a + "." + D, function () {
                    j()
                })
            },
            getInline: function (n, i) {
                if (j(), n.src) {
                    var r = t.st.inline,
                        o = e(n.src);
                    if (o.length) {
                        var s = o[0].parentNode;
                        s && s.tagName && (k || (E = r.hiddenClass, k = b(E), E = "mfp-" + E), A = o.after(k).detach().removeClass(E)), t.updateStatus("ready")
                    } else t.updateStatus("error", r.tNotFound), o = e("<div>");
                    return n.inlineElement = o, o
                }
                return t.updateStatus("ready"), t._parseMarkup(i, {}, n), i
            }
        }
    });
    var N, $ = "ajax",
        O = function () {
            N && e(document.body).removeClass(N)
        },
        P = function () {
            O(), t.req && t.req.abort()
        };
    e.magnificPopup.registerModule($, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        },
        proto: {
            initAjax: function () {
                t.types.push($), N = t.st.ajax.cursor, x(a + "." + $, P), x("BeforeChange." + $, P)
            },
            getAjax: function (n) {
                N && e(document.body).addClass(N), t.updateStatus("loading");
                var i = e.extend({
                    url: n.src,
                    success: function (i, r, o) {
                        var s = {
                            data: i,
                            xhr: o
                        };
                        _("ParseAjax", s), t.appendContent(e(s.data), $), n.finished = !0, O(), t._setFocus(), setTimeout(function () {
                            t.wrap.addClass(f)
                        }, 16), t.updateStatus("ready"), _("AjaxContentAdded")
                    },
                    error: function () {
                        O(), n.finished = n.loadError = !0, t.updateStatus("error", t.st.ajax.tError.replace("%url%", n.src))
                    }
                }, t.st.ajax.settings);
                return t.req = e.ajax(i), ""
            }
        }
    });
    var I, L = function (n) {
        if (n.data && void 0 !== n.data.title) return n.data.title;
        var i = t.st.image.titleSrc;
        if (i) {
            if (e.isFunction(i)) return i.call(t, n);
            if (n.el) return n.el.attr(i) || ""
        }
        return ""
    };
    e.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        },
        proto: {
            initImage: function () {
                var n = t.st.image,
                    i = ".image";
                t.types.push("image"), x(u + i, function () {
                    "image" === t.currItem.type && n.cursor && e(document.body).addClass(n.cursor)
                }), x(a + i, function () {
                    n.cursor && e(document.body).removeClass(n.cursor), w.off("resize" + d)
                }), x("Resize" + i, t.resizeImage), t.isLowIE && x("AfterChange", t.resizeImage)
            },
            resizeImage: function () {
                var e = t.currItem;
                if (e && e.img && t.st.image.verticalFit) {
                    var n = 0;
                    t.isLowIE && (n = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", t.wH - n)
                }
            },
            _onImageHasSize: function (e) {
                e.img && (e.hasSize = !0, I && clearInterval(I), e.isCheckingImgSize = !1, _("ImageHasSize", e), e.imgHidden && (t.content && t.content.removeClass("mfp-loading"), e.imgHidden = !1))
            },
            findImageSize: function (e) {
                var n = 0,
                    i = e.img[0],
                    r = function (o) {
                        I && clearInterval(I), I = setInterval(function () {
                            return i.naturalWidth > 0 ? void t._onImageHasSize(e) : (n > 200 && clearInterval(I), n++, void(3 === n ? r(10) : 40 === n ? r(50) : 100 === n && r(500)))
                        }, o)
                    };
                r(1)
            },
            getImage: function (n, i) {
                var r = 0,
                    o = function () {
                        n && (n.img[0].complete ? (n.img.off(".mfploader"), n === t.currItem && (t._onImageHasSize(n), t.updateStatus("ready")), n.hasSize = !0, n.loaded = !0, _("ImageLoadComplete")) : (r++, 200 > r ? setTimeout(o, 100) : s()))
                    },
                    s = function () {
                        n && (n.img.off(".mfploader"), n === t.currItem && (t._onImageHasSize(n), t.updateStatus("error", a.tError.replace("%url%", n.src))), n.hasSize = !0, n.loaded = !0, n.loadError = !0)
                    },
                    a = t.st.image,
                    l = i.find(".mfp-img");
                if (l.length) {
                    var c = document.createElement("img");
                    c.className = "mfp-img", n.el && n.el.find("img").length && (c.alt = n.el.find("img").attr("alt")), n.img = e(c).on("load.mfploader", o).on("error.mfploader", s), c.src = n.src, l.is("img") && (n.img = n.img.clone()), c = n.img[0], c.naturalWidth > 0 ? n.hasSize = !0 : c.width || (n.hasSize = !1)
                }
                return t._parseMarkup(i, {
                    title: L(n),
                    img_replaceWith: n.img
                }, n), t.resizeImage(), n.hasSize ? (I && clearInterval(I), n.loadError ? (i.addClass("mfp-loading"), t.updateStatus("error", a.tError.replace("%url%", n.src))) : (i.removeClass("mfp-loading"), t.updateStatus("ready")), i) : (t.updateStatus("loading"), n.loading = !0, n.hasSize || (n.imgHidden = !0, i.addClass("mfp-loading"), t.findImageSize(n)), i)
            }
        }
    });
    var H, z = function () {
        return void 0 === H && (H = void 0 !== document.createElement("p").style.MozTransform), H
    };
    e.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function (e) {
                return e.is("img") ? e : e.find("img")
            }
        },
        proto: {
            initZoom: function () {
                var e, n = t.st.zoom,
                    i = ".zoom";
                if (n.enabled && t.supportsTransition) {
                    var r, o, s = n.duration,
                        c = function (e) {
                            var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                i = "all " + n.duration / 1e3 + "s " + n.easing,
                                r = {
                                    position: "fixed",
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    "-webkit-backface-visibility": "hidden"
                                },
                                o = "transition";
                            return r["-webkit-" + o] = r["-moz-" + o] = r["-o-" + o] = r[o] = i, t.css(r), t
                        },
                        u = function () {
                            t.content.css("visibility", "visible")
                        };
                    x("BuildControls" + i, function () {
                        if (t._allowZoom()) {
                            if (clearTimeout(r), t.content.css("visibility", "hidden"), !(e = t._getItemToZoom())) return void u();
                            o = c(e), o.css(t._getOffset()), t.wrap.append(o), r = setTimeout(function () {
                                o.css(t._getOffset(!0)), r = setTimeout(function () {
                                    u(), setTimeout(function () {
                                        o.remove(), e = o = null, _("ZoomAnimationEnded")
                                    }, 16)
                                }, s)
                            }, 16)
                        }
                    }), x(l + i, function () {
                        if (t._allowZoom()) {
                            if (clearTimeout(r), t.st.removalDelay = s, !e) {
                                if (!(e = t._getItemToZoom())) return;
                                o = c(e)
                            }
                            o.css(t._getOffset(!0)), t.wrap.append(o), t.content.css("visibility", "hidden"), setTimeout(function () {
                                o.css(t._getOffset())
                            }, 16)
                        }
                    }), x(a + i, function () {
                        t._allowZoom() && (u(), o && o.remove(), e = null)
                    })
                }
            },
            _allowZoom: function () {
                return "image" === t.currItem.type
            },
            _getItemToZoom: function () {
                return !!t.currItem.hasSize && t.currItem.img
            },
            _getOffset: function (n) {
                var i;
                i = n ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem);
                var r = i.offset(),
                    o = parseInt(i.css("padding-top"), 10),
                    s = parseInt(i.css("padding-bottom"), 10);
                r.top -= e(window).scrollTop() - o;
                var a = {
                    width: i.width(),
                    height: (y ? i.innerHeight() : i[0].offsetHeight) - s - o
                };
                return z() ? a["-moz-transform"] = a.transform = "translate(" + r.left + "px," + r.top + "px)" : (a.left = r.left, a.top = r.top), a
            }
        }
    });
    var M = "iframe",
        q = "//about:blank",
        F = function (e) {
            if (t.currTemplate[M]) {
                var n = t.currTemplate[M].find("iframe");
                n.length && (e || (n[0].src = q), t.isIE8 && n.css("display", e ? "block" : "none"))
            }
        };
    e.magnificPopup.registerModule(M, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {
                    index: "youtube.com",
                    id: "v=",
                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                },
                vimeo: {
                    index: "vimeo.com/",
                    id: "/",
                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                },
                gmaps: {
                    index: "//maps.google.",
                    src: "%id%&output=embed"
                }
            }
        },
        proto: {
            initIframe: function () {
                t.types.push(M), x("BeforeChange", function (e, t, n) {
                    t !== n && (t === M ? F() : n === M && F(!0))
                }), x(a + "." + M, function () {
                    F()
                })
            },
            getIframe: function (n, i) {
                var r = n.src,
                    o = t.st.iframe;
                e.each(o.patterns, function () {
                    return r.indexOf(this.index) > -1 ? (this.id && (r = "string" == typeof this.id ? r.substr(r.lastIndexOf(this.id) + this.id.length, r.length) : this.id.call(this, r)), r = this.src.replace("%id%", r), !1) : void 0
                });
                var s = {};
                return o.srcAction && (s[o.srcAction] = r), t._parseMarkup(i, s, n), t.updateStatus("ready"), i
            }
        }
    });
    var R = function (e) {
            var n = t.items.length;
            return e > n - 1 ? e - n : 0 > e ? n + e : e
        },
        B = function (e, t, n) {
            return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, n)
        };
    e.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [0, 2],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        },
        proto: {
            initGallery: function () {
                var n = t.st.gallery,
                    r = ".mfp-gallery";
                return t.direction = !0, !(!n || !n.enabled) && (o += " mfp-gallery", x(u + r, function () {
                    n.navigateByImgClick && t.wrap.on("click" + r, ".mfp-img", function () {
                        return t.items.length > 1 ? (t.next(), !1) : void 0
                    }), i.on("keydown" + r, function (e) {
                        37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                    })
                }), x("UpdateStatus" + r, function (e, n) {
                    n.text && (n.text = B(n.text, t.currItem.index, t.items.length))
                }), x(c + r, function (e, i, r, o) {
                    var s = t.items.length;
                    r.counter = s > 1 ? B(n.tCounter, o.index, s) : ""
                }), x("BuildControls" + r, function () {
                    if (t.items.length > 1 && n.arrows && !t.arrowLeft) {
                        var i = n.arrowMarkup,
                            r = t.arrowLeft = e(i.replace(/%title%/gi, n.tPrev).replace(/%dir%/gi, "left")).addClass(m),
                            o = t.arrowRight = e(i.replace(/%title%/gi, n.tNext).replace(/%dir%/gi, "right")).addClass(m);
                        r.click(function () {
                            t.prev()
                        }), o.click(function () {
                            t.next()
                        }), t.container.append(r.add(o))
                    }
                }), x(h + r, function () {
                    t._preloadTimeout && clearTimeout(t._preloadTimeout), t._preloadTimeout = setTimeout(function () {
                        t.preloadNearbyImages(), t._preloadTimeout = null
                    }, 16)
                }), void x(a + r, function () {
                    i.off(r), t.wrap.off("click" + r), t.arrowRight = t.arrowLeft = null
                }))
            },
            next: function () {
                t.direction = !0, t.index = R(t.index + 1), t.updateItemHTML()
            },
            prev: function () {
                t.direction = !1, t.index = R(t.index - 1), t.updateItemHTML()
            },
            goTo: function (e) {
                t.direction = e >= t.index, t.index = e, t.updateItemHTML()
            },
            preloadNearbyImages: function () {
                var e, n = t.st.gallery.preload,
                    i = Math.min(n[0], t.items.length),
                    r = Math.min(n[1], t.items.length);
                for (e = 1; e <= (t.direction ? r : i); e++) t._preloadItem(t.index + e);
                for (e = 1; e <= (t.direction ? i : r); e++) t._preloadItem(t.index - e)
            },
            _preloadItem: function (n) {
                if (n = R(n), !t.items[n].preloaded) {
                    var i = t.items[n];
                    i.parsed || (i = t.parseEl(n)), _("LazyLoad", i), "image" === i.type && (i.img = e('<img class="mfp-img" />').on("load.mfploader", function () {
                        i.hasSize = !0
                    }).on("error.mfploader", function () {
                        i.hasSize = !0, i.loadError = !0, _("LazyLoadError", i)
                    }).attr("src", i.src)), i.preloaded = !0
                }
            }
        }
    });
    var W = "retina";
    e.magnificPopup.registerModule(W, {
        options: {
            replaceSrc: function (e) {
                return e.src.replace(/\.\w+$/, function (e) {
                    return "@2x" + e
                })
            },
            ratio: 1
        },
        proto: {
            initRetina: function () {
                if (window.devicePixelRatio > 1) {
                    var e = t.st.retina,
                        n = e.ratio;
                    (n = isNaN(n) ? n() : n) > 1 && (x("ImageHasSize." + W, function (e, t) {
                        t.img.css({
                            "max-width": t.img[0].naturalWidth / n,
                            width: "100%"
                        })
                    }), x("ElementParse." + W, function (t, i) {
                        i.src = e.replaceSrc(i, n)
                    }))
                }
            }
        }
    }), T()
}),
function (e) {
    var t = e.document,
        n = t.documentElement,
        i = "ontouchmove" in t,
        r = "WebkitOverflowScrolling" in n.style || "msOverflowStyle" in n.style || !i && e.screen.width > 800 || function () {
            var t = e.navigator.userAgent,
                n = t.match(/AppleWebKit\/([0-9]+)/),
                i = n && n[1],
                r = n && i >= 534;
            return t.match(/Android ([0-9]+)/) && RegExp.$1 >= 3 && r || t.match(/ Version\/([0-9]+)/) && RegExp.$1 >= 0 && e.blackberry && r || t.indexOf("PlayBook") > -1 && r && -1 === !t.indexOf("Android 2") || t.match(/Firefox\/([0-9]+)/) && RegExp.$1 >= 4 || t.match(/wOSBrowser\/([0-9]+)/) && RegExp.$1 >= 233 && r || t.match(/NokiaBrowser\/([0-9\.]+)/) && 7.3 === parseFloat(RegExp.$1) && n && i >= 533
        }();
    e.overthrow = {}, e.overthrow.enabledClassName = "overthrow-enabled", e.overthrow.addClass = function () {
        -1 === n.className.indexOf(e.overthrow.enabledClassName) && (n.className += " " + e.overthrow.enabledClassName)
    }, e.overthrow.removeClass = function () {
        n.className = n.className.replace(e.overthrow.enabledClassName, "")
    }, e.overthrow.set = function () {
        r && e.overthrow.addClass()
    }, e.overthrow.canBeFilledWithPoly = i, e.overthrow.forget = function () {
        e.overthrow.removeClass()
    }, e.overthrow.support = r ? "native" : "none"
}(this),
function (e, t, n) {
    if (t !== n) {
        t.easing = function (e, t, n, i) {
            return n * ((e = e / i - 1) * e * e + 1) + t
        }, t.tossing = !1;
        var i;
        t.toss = function (e, r) {
            t.intercept();
            var o, s, a = 0,
                l = e.scrollLeft,
                c = e.scrollTop,
                u = {
                    top: "+0",
                    left: "+0",
                    duration: 50,
                    easing: t.easing,
                    finished: function () {}
                },
                h = !1;
            if (r)
                for (var p in u) r[p] !== n && (u[p] = r[p]);
            return "string" == typeof u.left ? (u.left = parseFloat(u.left), o = u.left + l) : (o = u.left, u.left = u.left - l), "string" == typeof u.top ? (u.top = parseFloat(u.top), s = u.top + c) : (s = u.top, u.top = u.top - c), t.tossing = !0, i = setInterval(function () {
                a++ < u.duration ? (e.scrollLeft = u.easing(a, l, u.left, u.duration), e.scrollTop = u.easing(a, c, u.top, u.duration)) : (o !== e.scrollLeft ? e.scrollLeft = o : (h && u.finished(), h = !0), s !== e.scrollTop ? e.scrollTop = s : (h && u.finished(), h = !0), t.intercept())
            }, 1), {
                top: s,
                left: o,
                duration: t.duration,
                easing: t.easing
            }
        }, t.intercept = function () {
            clearInterval(i), t.tossing = !1
        }
    }
}(0, this.overthrow),
function (e, t, n) {
    if (t !== n) {
        t.scrollIndicatorClassName = "overthrow";
        var i = e.document,
            r = i.documentElement,
            o = "native" === t.support,
            s = t.canBeFilledWithPoly,
            a = (t.configure, t.set),
            l = t.forget,
            c = t.scrollIndicatorClassName;
        t.closest = function (e, n) {
            return !n && e.className && e.className.indexOf(c) > -1 && e || t.closest(e.parentNode)
        };
        var u = !1;
        t.set = function () {
            if (a(), !u && !o && s) {
                e.overthrow.addClass(), u = !0, t.support = "polyfilled", t.forget = function () {
                    l(), u = !1, i.removeEventListener && i.removeEventListener("touchstart", x, !1)
                };
                var c, h, p, d, f = [],
                    g = [],
                    m = function () {
                        f = [], h = null
                    },
                    v = function () {
                        g = [], p = null
                    },
                    y = function (e) {
                        d = c.querySelectorAll("textarea, input");
                        for (var t = 0, n = d.length; n > t; t++) d[t].style.pointerEvents = e
                    },
                    w = function (e, t) {
                        if (i.createEvent) {
                            var r, o = (!t || t === n) && c.parentNode || c.touchchild || c;
                            o !== c && (r = i.createEvent("HTMLEvents"), r.initEvent("touchend", !0, !0), c.dispatchEvent(r), o.touchchild = c, c = o, o.dispatchEvent(e))
                        }
                    },
                    x = function (e) {
                        if (t.intercept && t.intercept(), m(), v(), (c = t.closest(e.target)) && c !== r && !(e.touches.length > 1)) {
                            y("none");
                            var n = e,
                                i = c.scrollTop,
                                o = c.scrollLeft,
                                s = c.offsetHeight,
                                a = c.offsetWidth,
                                l = e.touches[0].pageY,
                                u = e.touches[0].pageX,
                                d = c.scrollHeight,
                                x = c.scrollWidth,
                                b = function (e) {
                                    var t = i + l - e.touches[0].pageY,
                                        r = o + u - e.touches[0].pageX,
                                        y = t >= (f.length ? f[0] : 0),
                                        b = r >= (g.length ? g[0] : 0);
                                    t > 0 && d - s > t || r > 0 && x - a > r ? e.preventDefault() : w(n), h && y !== h && m(), p && b !== p && v(), h = y, p = b, c.scrollTop = t, c.scrollLeft = r, f.unshift(t), g.unshift(r), f.length > 3 && f.pop(), g.length > 3 && g.pop()
                                },
                                _ = function () {
                                    y("auto"), setTimeout(function () {
                                        y("none")
                                    }, 450), c.removeEventListener("touchmove", b, !1), c.removeEventListener("touchend", _, !1)
                                };
                            c.addEventListener("touchmove", b, !1), c.addEventListener("touchend", _, !1)
                        }
                    };
                i.addEventListener("touchstart", x, !1)
            }
        }
    }
}(this, this.overthrow),
function (e) {
    e.overthrow.set()
}(this),
function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], function (t) {
        return e(t, window, document)
    }) : "object" == typeof exports ? module.exports = e(require("jquery"), window, document) : e(jQuery, window, document)
}(function (e, t, n) {
    "use strict";
    var i, r, o, s, a, l, c, u, h, p, d, f, g, m, v, y, w, x, b, _, C, T, S, E, k, A, D, j;
    _ = {
        paneClass: "nano-pane",
        sliderClass: "nano-slider",
        contentClass: "nano-content",
        iOSNativeScrolling: !1,
        preventPageScrolling: !1,
        disableResize: !1,
        alwaysVisible: !1,
        flashDelay: 1500,
        sliderMinHeight: 20,
        sliderMaxHeight: null,
        documentContext: null,
        windowContext: null
    }, "scrollbar", v = "scroll", c = "mousedown", u = "mouseenter", h = "mousemove", d = "mousewheel", p = "mouseup", m = "resize", a = "drag", l = "enter", w = "up", g = "panedown", o = "DOMMouseScroll", s = "down", x = "wheel", "keydown", "keyup", y = "touchmove", i = "Microsoft Internet Explorer" === t.navigator.appName && /msie 7./i.test(t.navigator.appVersion) && t.ActiveXObject, r = null, E = t.requestAnimationFrame, b = t.cancelAnimationFrame, A = n.createElement("div").style, j = function () {
        var e, t, n, i;
        for (t = ["t", "webkitT", "MozT", "msT", "OT"], e = n = 0, i = t.length; i > n; e = ++n)
            if (t[e], t[e] + "ransform" in A) return t[e].substr(0, t[e].length - 1);
        return !1
    }(), D = function (e) {
        return !1 !== j && ("" === j ? e : j + e.charAt(0).toUpperCase() + e.substr(1))
    }, k = D("transform"), T = !1 !== k, C = function () {
        var e, t, i;
        return e = n.createElement("div"), t = e.style, t.position = "absolute", t.width = "100px", t.height = "100px", t.overflow = v, t.top = "-9999px", n.body.appendChild(e), i = e.offsetWidth - e.clientWidth, n.body.removeChild(e), i
    }, S = function () {
        var e, n, i;
        return n = t.navigator.userAgent, !!(e = /(?=.+Mac OS X)(?=.+Firefox)/.test(n)) && (i = /Firefox\/\d{2}\./.exec(n), i && (i = i[0].replace(/\D+/g, "")), e && +i > 23)
    }, f = function () {
        function f(i, o) {
            this.el = i, this.options = o, r || (r = C()), this.$el = e(this.el), this.doc = e(this.options.documentContext || n), this.win = e(this.options.windowContext || t), this.body = this.doc.find("body"), this.$content = this.$el.children("." + this.options.contentClass), this.$content.attr("tabindex", this.options.tabIndex || 0), this.content = this.$content[0], this.previousPosition = 0, this.options.iOSNativeScrolling && null != this.el.style.WebkitOverflowScrolling ? this.nativeScrolling() : this.generate(), this.createEvents(), this.addEvents(), this.reset()
        }
        return f.prototype.preventScrolling = function (e, t) {
            if (this.isActive)
                if (e.type === o)(t === s && e.originalEvent.detail > 0 || t === w && e.originalEvent.detail < 0) && e.preventDefault();
                else if (e.type === d) {
                if (!e.originalEvent || !e.originalEvent.wheelDelta) return;
                (t === s && e.originalEvent.wheelDelta < 0 || t === w && e.originalEvent.wheelDelta > 0) && e.preventDefault()
            }
        }, f.prototype.nativeScrolling = function () {
            this.$content.css({
                WebkitOverflowScrolling: "touch"
            }), this.iOSNativeScrolling = !0, this.isActive = !0
        }, f.prototype.updateScrollValues = function () {
            var e, t;
            e = this.content, this.maxScrollTop = e.scrollHeight - e.clientHeight, this.prevScrollTop = this.contentScrollTop || 0, this.contentScrollTop = e.scrollTop, t = this.contentScrollTop > this.previousPosition ? "down" : this.contentScrollTop < this.previousPosition ? "up" : "same", this.previousPosition = this.contentScrollTop, "same" !== t && this.$el.trigger("update", {
                position: this.contentScrollTop,
                maximum: this.maxScrollTop,
                direction: t
            }), this.iOSNativeScrolling || (this.maxSliderTop = this.paneHeight - this.sliderHeight, this.sliderTop = 0 === this.maxScrollTop ? 0 : this.contentScrollTop * this.maxSliderTop / this.maxScrollTop)
        }, f.prototype.setOnScrollStyles = function () {
            var e;
            T ? (e = {}, e[k] = "translate(0, " + this.sliderTop + "px)") : e = {
                top: this.sliderTop
            }, E ? (b && this.scrollRAF && b(this.scrollRAF), this.scrollRAF = E(function (t) {
                return function () {
                    return t.scrollRAF = null, t.slider.css(e)
                }
            }(this))) : this.slider.css(e)
        }, f.prototype.createEvents = function () {
            this.events = {
                down: function (e) {
                    return function (t) {
                        return e.isBeingDragged = !0, e.offsetY = t.pageY - e.slider.offset().top, e.slider.is(t.target) || (e.offsetY = 0), e.pane.addClass("active"), e.doc.bind(h, e.events[a]).bind(p, e.events[w]), e.body.bind(u, e.events[l]), !1
                    }
                }(this),
                drag: function (e) {
                    return function (t) {
                        return e.sliderY = t.pageY - e.$el.offset().top - e.paneTop - (e.offsetY || .5 * e.sliderHeight), e.scroll(), e.contentScrollTop >= e.maxScrollTop && e.prevScrollTop !== e.maxScrollTop ? e.$el.trigger("scrollend") : 0 === e.contentScrollTop && 0 !== e.prevScrollTop && e.$el.trigger("scrolltop"), !1
                    }
                }(this),
                up: function (e) {
                    return function (t) {
                        return e.isBeingDragged = !1, e.pane.removeClass("active"), e.doc.unbind(h, e.events[a]).unbind(p, e.events[w]), e.body.unbind(u, e.events[l]), !1
                    }
                }(this),
                resize: function (e) {
                    return function (t) {
                        e.reset()
                    }
                }(this),
                panedown: function (e) {
                    return function (t) {
                        return e.sliderY = (t.offsetY || t.originalEvent.layerY) - .5 * e.sliderHeight, e.scroll(), e.events.down(t), !1
                    }
                }(this),
                scroll: function (e) {
                    return function (t) {
                        e.updateScrollValues(), e.isBeingDragged || (e.iOSNativeScrolling || (e.sliderY = e.sliderTop, e.setOnScrollStyles()), null != t && (e.contentScrollTop >= e.maxScrollTop ? (e.options.preventPageScrolling && e.preventScrolling(t, s), e.prevScrollTop !== e.maxScrollTop && e.$el.trigger("scrollend")) : 0 === e.contentScrollTop && (e.options.preventPageScrolling && e.preventScrolling(t, w), 0 !== e.prevScrollTop && e.$el.trigger("scrolltop"))))
                    }
                }(this),
                wheel: function (e) {
                    return function (t) {
                        var n;
                        if (null != t) return n = t.delta || t.wheelDelta || t.originalEvent && t.originalEvent.wheelDelta || -t.detail || t.originalEvent && -t.originalEvent.detail, n && (e.sliderY += -n / 3), e.scroll(), !1
                    }
                }(this),
                enter: function (e) {
                    return function (t) {
                        var n;
                        if (e.isBeingDragged) return 1 !== (t.buttons || t.which) ? (n = e.events)[w].apply(n, arguments) : void 0
                    }
                }(this)
            }
        }, f.prototype.addEvents = function () {
            var e;
            this.removeEvents(), e = this.events, this.options.disableResize || this.win.bind(m, e[m]), this.iOSNativeScrolling || (this.slider.bind(c, e[s]), this.pane.bind(c, e[g]).bind(d + " " + o, e[x])), this.$content.bind(v + " " + d + " " + o + " " + y, e[v])
        }, f.prototype.removeEvents = function () {
            var e;
            e = this.events, this.win.unbind(m, e[m]), this.iOSNativeScrolling || (this.slider.unbind(), this.pane.unbind()), this.$content.unbind(v + " " + d + " " + o + " " + y, e[v])
        }, f.prototype.generate = function () {
            var e, n, i, o, s, a;
            return i = this.options, s = i.paneClass, a = i.sliderClass, i.contentClass, (o = this.$el.children("." + s)).length || o.children("." + a).length || this.$el.append('<div class="' + s + '"><div class="' + a + '" /></div>'), this.pane = this.$el.children("." + s), this.slider = this.pane.find("." + a), 0 === r && S() ? (n = t.getComputedStyle(this.content, null).getPropertyValue("padding-right").replace(/[^0-9.]+/g, ""), e = {
                right: -14,
                paddingRight: +n + 14
            }) : r && (e = {
                right: -r
            }, this.$el.addClass("has-scrollbar")), null != e && this.$content.css(e), this
        }, f.prototype.restore = function () {
            this.stopped = !1, this.iOSNativeScrolling || this.pane.show(), this.addEvents()
        }, f.prototype.reset = function () {
            var e, t, n, o, s, a, l, c, u, h, p, d;
            return this.iOSNativeScrolling ? void(this.contentHeight = this.content.scrollHeight) : (this.$el.find("." + this.options.paneClass).length || this.generate().stop(), this.stopped && this.restore(), e = this.content, o = e.style, s = o.overflowY, i && this.$content.css({
                height: this.$content.height()
            }), t = e.scrollHeight + r, h = parseInt(this.$el.css("max-height"), 10), h > 0 && (this.$el.height(""), this.$el.height(e.scrollHeight > h ? h : e.scrollHeight)), l = this.pane.outerHeight(!1), u = parseInt(this.pane.css("top"), 10), a = parseInt(this.pane.css("bottom"), 10), c = l + u + a, d = Math.round(c / t * l), d < this.options.sliderMinHeight ? d = this.options.sliderMinHeight : null != this.options.sliderMaxHeight && d > this.options.sliderMaxHeight && (d = this.options.sliderMaxHeight), s === v && o.overflowX !== v && (d += r), this.maxSliderTop = c - d, this.contentHeight = t, this.paneHeight = l, this.paneOuterHeight = c, this.sliderHeight = d, this.paneTop = u, this.slider.height(d), this.events.scroll(), this.pane.show(), this.isActive = !0, e.scrollHeight === e.clientHeight || this.pane.outerHeight(!0) >= e.scrollHeight && s !== v ? (this.pane.hide(), this.isActive = !1) : this.el.clientHeight === e.scrollHeight && s === v ? this.slider.hide() : this.slider.show(), this.pane.css({
                opacity: this.options.alwaysVisible ? 1 : "",
                visibility: this.options.alwaysVisible ? "visible" : ""
            }), n = this.$content.css("position"), ("static" === n || "relative" === n) && (p = parseInt(this.$content.css("right"), 10)) && this.$content.css({
                right: "",
                marginRight: p
            }), this)
        }, f.prototype.scroll = function () {
            return this.isActive ? (this.sliderY = Math.max(0, this.sliderY), this.sliderY = Math.min(this.maxSliderTop, this.sliderY), this.$content.scrollTop(this.maxScrollTop * this.sliderY / this.maxSliderTop), this.iOSNativeScrolling || (this.updateScrollValues(), this.setOnScrollStyles()), this) : void 0
        }, f.prototype.scrollBottom = function (e) {
            return this.isActive ? (this.$content.scrollTop(this.contentHeight - this.$content.height() - e).trigger(d), this.stop().restore(), this) : void 0
        }, f.prototype.scrollTop = function (e) {
            return this.isActive ? (this.$content.scrollTop(+e).trigger(d), this.stop().restore(), this) : void 0
        }, f.prototype.scrollTo = function (e) {
            return this.isActive ? (this.scrollTop(this.$el.find(e).get(0).offsetTop), this) : void 0
        }, f.prototype.stop = function () {
            return b && this.scrollRAF && (b(this.scrollRAF), this.scrollRAF = null), this.stopped = !0, this.removeEvents(), this.iOSNativeScrolling || this.pane.hide(), this
        }, f.prototype.destroy = function () {
            return this.stopped || this.stop(), !this.iOSNativeScrolling && this.pane.length && this.pane.remove(), i && this.$content.height(""), this.$content.removeAttr("tabindex"), this.$el.hasClass("has-scrollbar") && (this.$el.removeClass("has-scrollbar"), this.$content.css({
                right: ""
            })), this
        }, f.prototype.flash = function () {
            return !this.iOSNativeScrolling && this.isActive ? (this.reset(), this.pane.addClass("flashed"), setTimeout(function (e) {
                return function () {
                    e.pane.removeClass("flashed")
                }
            }(this), this.options.flashDelay), this) : void 0
        }, f
    }(), e.fn.nanoScroller = function (t) {
        return this.each(function () {
            var n, i;
            if ((i = this.nanoscroller) || (n = e.extend({}, _, t), this.nanoscroller = i = new f(this, n)), t && "object" == typeof t) {
                if (e.extend(i.options, t), null != t.scrollBottom) return i.scrollBottom(t.scrollBottom);
                if (null != t.scrollTop) return i.scrollTop(t.scrollTop);
                if (t.scrollTo) return i.scrollTo(t.scrollTo);
                if ("bottom" === t.scroll) return i.scrollBottom(0);
                if ("top" === t.scroll) return i.scrollTop(0);
                if (t.scroll && t.scroll instanceof e) return i.scrollTo(t.scroll);
                if (t.stop) return i.stop();
                if (t.destroy) return i.destroy();
                if (t.flash) return i.flash()
            }
            return i.reset()
        })
    }, e.fn.nanoScroller.Constructor = f
}),
function (e) {
    "function" == typeof define && define.amd ? define([], e) : "undefined" != typeof module && null !== module && module.exports ? module.exports = e : e()
}(function () {
    function e(e) {
        return new CustomEvent(e, R)
    }

    function t(e) {
        return e[B] || (e[B] = {})
    }

    function n(e, n, i, r, o) {
        function s(e) {
            i(e, r)
        }
        n = n.split(F);
        for (var a, l, c = t(e), u = n.length; u--;) l = n[u], a = c[l] || (c[l] = []), a.push([i, s]), e.addEventListener(l, s)
    }

    function i(e, n, i, r) {
        n = n.split(F);
        var o, s, a, l = t(e),
            c = n.length;
        if (l)
            for (; c--;)
                if (o = n[c], s = l[o])
                    for (a = s.length; a--;) s[a][0] === i && (e.removeEventListener(o, s[a][1]), s.splice(a, 1))
    }

    function r(t, n, i) {
        var r = e(n);
        i && I(r, i), t.dispatchEvent(r)
    }

    function o(e) {
        function t(e) {
            i ? (n(), H(t), r = !0, i = !1) : r = !1
        }
        var n = e,
            i = !1,
            r = !1;
        this.kick = function (e) {
            i = !0, r || t()
        }, this.end = function (e) {
            var t = n;
            e && (r ? (n = i ? function () {
                t(), e()
            } : e, i = !0) : e())
        }
    }

    function s() {}

    function a(e) {
        e.preventDefault()
    }

    function l(e) {
        return !!z[e.target.tagName.toLowerCase()]
    }

    function c(e) {
        return 1 === e.which && !e.ctrlKey && !e.altKey
    }

    function u(e, t) {
        var n, i;
        if (e.identifiedTouch) return e.identifiedTouch(t);
        for (n = -1, i = e.length; ++n < i;)
            if (e[n].identifier === t) return e[n]
    }

    function h(e, t) {
        var n = u(e.changedTouches, t.identifier);
        if (n && (n.pageX !== t.pageX || n.pageY !== t.pageY)) return n
    }

    function p(e) {
        c(e) && (l(e) || (n(document, M.move, d, e), n(document, M.cancel, f, e)))
    }

    function d(e, t) {
        x(e, t, e, g)
    }

    function f(e, t) {
        g()
    }

    function g() {
        i(document, M.move, d), i(document, M.cancel, f)
    }

    function m(e) {
        if (!z[e.target.tagName.toLowerCase()]) {
            var t = e.changedTouches[0],
                i = {
                    target: t.target,
                    pageX: t.pageX,
                    pageY: t.pageY,
                    identifier: t.identifier,
                    touchmove: function (e, t) {
                        v(e, t)
                    },
                    touchend: function (e, t) {
                        y(e, t)
                    }
                };
            n(document, q.move, i.touchmove, i), n(document, q.cancel, i.touchend, i)
        }
    }

    function v(e, t) {
        var n = h(e, t);
        n && x(e, t, n, w)
    }

    function y(e, t) {
        u(e.changedTouches, t.identifier) && w(t)
    }

    function w(e) {
        i(document, q.move, e.touchmove), i(document, q.cancel, e.touchend)
    }

    function x(e, t, n, i) {
        var r = n.pageX - t.pageX,
            o = n.pageY - t.pageY;
        r * r + o * o < L * L || b(e, t, n, r, o, i)
    }

    function b(e, t, n, i, o, a) {
        var l = e.targetTouches,
            c = e.timeStamp - t.timeStamp,
            u = {
                altKey: e.altKey,
                ctrlKey: e.ctrlKey,
                shiftKey: e.shiftKey,
                startX: t.pageX,
                startY: t.pageY,
                distX: i,
                distY: o,
                deltaX: i,
                deltaY: o,
                pageX: n.pageX,
                pageY: n.pageY,
                velocityX: i / c,
                velocityY: o / c,
                identifier: t.identifier,
                targetTouches: l,
                finger: l ? l.length : 1,
                enableMove: function () {
                    this.moveEnabled = !0, this.enableMove = s, e.preventDefault()
                }
            };
        r(t.target, "movestart", u), a(t)
    }

    function _(e, t) {
        var n = t.timer;
        t.touch = e, t.timeStamp = e.timeStamp, n.kick()
    }

    function C(e, t) {
        var n = t.target,
            r = t.event,
            o = t.timer;
        T(), D(n, r, o, function () {
            setTimeout(function () {
                i(n, "click", a)
            }, 0)
        })
    }

    function T() {
        i(document, M.move, _), i(document, M.end, C)
    }

    function S(e, t) {
        var n = t.event,
            i = t.timer,
            r = h(e, n);
        r && (e.preventDefault(), n.targetTouches = e.targetTouches, t.touch = r, t.timeStamp = e.timeStamp, i.kick())
    }

    function E(e, t) {
        var n = t.target,
            i = t.event,
            r = t.timer;
        u(e.changedTouches, i.identifier) && (k(t), D(n, i, r))
    }

    function k(e) {
        i(document, q.move, e.activeTouchmove), i(document, q.end, e.activeTouchend)
    }

    function A(e, t, n) {
        var i = n - e.timeStamp;
        e.distX = t.pageX - e.startX, e.distY = t.pageY - e.startY, e.deltaX = t.pageX - e.pageX, e.deltaY = t.pageY - e.pageY, e.velocityX = .3 * e.velocityX + .7 * e.deltaX / i, e.velocityY = .3 * e.velocityY + .7 * e.deltaY / i, e.pageX = t.pageX, e.pageY = t.pageY
    }

    function D(e, t, n, i) {
        n.end(function () {
            return r(e, "moveend", t), i && i()
        })
    }

    function j(e) {
        function t(e) {
            A(i, s.touch, s.timeStamp), r(s.target, "move", i)
        }
        if (!e.defaultPrevented && e.moveEnabled) {
            var i = {
                    startX: e.startX,
                    startY: e.startY,
                    pageX: e.pageX,
                    pageY: e.pageY,
                    distX: e.distX,
                    distY: e.distY,
                    deltaX: e.deltaX,
                    deltaY: e.deltaY,
                    velocityX: e.velocityX,
                    velocityY: e.velocityY,
                    identifier: e.identifier,
                    targetTouches: e.targetTouches,
                    finger: e.finger
                },
                s = {
                    target: e.target,
                    event: i,
                    timer: new o(t),
                    touch: void 0,
                    timeStamp: e.timeStamp
                };
            void 0 === e.identifier ? (n(e.target, "click", a), n(document, M.move, _, s), n(document, M.end, C, s)) : (s.activeTouchmove = function (e, t) {
                S(e, t)
            }, s.activeTouchend = function (e, t) {
                E(e, t)
            }, n(document, q.move, s.activeTouchmove, s), n(document, q.end, s.activeTouchend, s))
        }
    }

    function N(e) {
        e.enableMove()
    }

    function $(e) {
        e.enableMove()
    }

    function O(e) {
        e.enableMove()
    }

    function P(e) {
        var t = e.handler;
        e.handler = function (e) {
            for (var n, i = W.length; i--;) n = W[i], e[n] = e.originalEvent[n];
            t.apply(this, arguments)
        }
    }
    var I = Object.assign || window.jQuery && jQuery.extend,
        L = 8,
        H = function () {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (e, t) {
                return window.setTimeout(function () {
                    e()
                }, 25)
            }
        }(),
        z = {
            textarea: !0,
            input: !0,
            select: !0,
            button: !0
        },
        M = {
            move: "mousemove",
            cancel: "mouseup dragstart",
            end: "mouseup"
        },
        q = {
            move: "touchmove",
            cancel: "touchend",
            end: "touchend"
        },
        F = /\s+/,
        R = {
            bubbles: !0,
            cancelable: !0
        },
        B = Symbol("events");
    if (n(document, "mousedown", p), n(document, "touchstart", m), n(document, "movestart", j), window.jQuery) {
        var W = "startX startY pageX pageY distX distY deltaX deltaY velocityX velocityY".split(" ");
        jQuery.event.special.movestart = {
            setup: function () {
                return n(this, "movestart", N), !1
            },
            teardown: function () {
                return i(this, "movestart", N), !1
            },
            add: P
        }, jQuery.event.special.move = {
            setup: function () {
                return n(this, "movestart", $), !1
            },
            teardown: function () {
                return i(this, "movestart", $), !1
            },
            add: P
        }, jQuery.event.special.moveend = {
            setup: function () {
                return n(this, "movestart", O), !1
            },
            teardown: function () {
                return i(this, "movestart", O), !1
            },
            add: P
        }
    }
}),
function (e) {
    e.fn.twentytwenty = function (t) {
        var t = e.extend({
            default_offset_pct: .5,
            orientation: "horizontal",
            before_label: "Before",
            after_label: "After",
            no_overlay: !1
        }, t);
        return this.each(function () {
            var n = t.default_offset_pct,
                i = e(this),
                r = t.orientation,
                o = "vertical" === r ? "down" : "left",
                s = "vertical" === r ? "up" : "right";
            i.wrap("<div class='twentytwenty-wrapper twentytwenty-" + r + "'></div>"), t.no_overlay || i.append("<div class='twentytwenty-overlay'></div>");
            var a = i.find("img:first"),
                l = i.find("img:last");
            i.append("<div class='twentytwenty-handle'></div>");
            var c = i.find(".twentytwenty-handle");
            c.append("<span class='twentytwenty-" + o + "-arrow'></span>"), c.append("<span class='twentytwenty-" + s + "-arrow'></span>"), i.addClass("twentytwenty-container"), a.addClass("twentytwenty-before"), l.addClass("twentytwenty-after");
            var u = i.find(".twentytwenty-overlay");
            u.append("<div class='twentytwenty-before-label' data-content='" + t.before_label + "'></div>"), u.append("<div class='twentytwenty-after-label' data-content='" + t.after_label + "'></div>");
            var h = function (e) {
                    var t = a.width(),
                        n = a.height();
                    return {
                        w: t + "px",
                        h: n + "px",
                        cw: e * t + "px",
                        ch: e * n + "px"
                    }
                },
                p = function (e) {
                    "vertical" === r ? (a.css("clip", "rect(0," + e.w + "," + e.ch + ",0)"), l.css("clip", "rect(" + e.ch + "," + e.w + "," + e.h + ",0)")) : (a.css("clip", "rect(0," + e.cw + "," + e.h + ",0)"), l.css("clip", "rect(0," + e.w + "," + e.h + "," + e.cw + ")")), i.css("height", e.h)
                },
                d = function (e) {
                    var t = h(e);
                    c.css("vertical" === r ? "top" : "left", "vertical" === r ? t.ch : t.cw), p(t)
                };
            e(window).on("resize.twentytwenty", function (e) {
                d(n)
            });
            var f = 0,
                g = 0,
                m = 0,
                v = 0;
            c.on("movestart", function (e) {
                (e.distX > e.distY && e.distX < -e.distY || e.distX < e.distY && e.distX > -e.distY) && "vertical" !== r ? e.preventDefault() : (e.distX < e.distY && e.distX < -e.distY || e.distX > e.distY && e.distX > -e.distY) && "vertical" === r && e.preventDefault(), i.addClass("active"), f = i.offset().left, g = i.offset().top, m = a.width(), v = a.height()
            }), c.on("moveend", function (e) {
                i.removeClass("active")
            }), c.on("move", function (e) {
                i.hasClass("active") && (n = "vertical" === r ? (e.pageY - g) / v : (e.pageX - f) / m, n < 0 && (n = 0), n > 1 && (n = 1), d(n))
            }), i.find("img").on("mousedown", function (e) {
                e.preventDefault()
            }), e(window).trigger("resize.twentytwenty")
        })
    }
}(jQuery);
/*
    e.page.index - порядковый номер текущего исходного итема
    e.page.count - количество всех исходных итемов
*/
$(document).ready(function () {
    var $owlWorks = $('.js-carousel-works');
    var $owlRecall = $('.js-carousel-recall');

    var __wrap = '.js-carousel-wrap',
        __carousel = '.js-carousel',
        __control = '.js-carousel__control',
        __counter = '.js-carousel-counter',
        __total = '.js-carousel-counter-total';


    $(__carousel).on('initialize.owl.carousel', function (e) {
        var $curCarousel = $(this);
        if (!$curCarousel.hasClass('owl-carousel'))
            $curCarousel.addClass('owl-carousel')

        changeSlide(e, true);
    })

    $owlWorks.owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        smartSpeed: 500,
        mouseDrag: false,
        touchDrag: false
    });

    $owlWorks.on('changed.owl.carousel', function (e) {
        changeSlide(e);
    });

    $owlRecall.owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        smartSpeed: 500,
        autoHeight: true
    });

    $owlRecall.on('changed.owl.carousel', function (e) {
        changeSlide(e);
    });

    $('body').on('click', __control, function (e) {
        var $curControl = $(this),
            $carousel = $curControl.parent(__carousel);

        $carousel = $carousel.length ? $carousel : $curControl.closest(__wrap).find(__carousel);

        if ($carousel.length) {
            if ($curControl.hasClass('js-carousel__control-next')) {
                $carousel.trigger('next.owl.carousel');
            } else {
                $carousel.trigger('prev.owl.carousel');
            }

        }

    })


    function initControls($carousel) {
        if (!$carousel.find(__control).length) {
            $('<div/>')
                .addClass('carousel__control carousel__control_prev js-carousel__control js-carousel__control-prev')
                .appendTo($carousel)
            $('<div/>')
                .addClass('carousel__control carousel__control_next js-carousel__control js-carousel__control-next')
                .appendTo($carousel)
        }
    }

    function changeSlide(e, isInit) {
        var $carousel = $(e.target),
            $wrap = $carousel.closest(__wrap),
            $counter = $wrap.find(__counter),
            $total = $wrap.find(__total),
            $items = null;

        var curCount = e.page.index == -1 ? 0 : e.page.index;
        $counter.text(curCount + 1)

        if (isInit) {
            $items = $carousel.children()
            $total.text($items.length)
        }
    }
});

$(function () {
    $('.js-diff-img').twentytwenty({
        default_offset_pct: 0.5,
        orientation: 'horizontal',
        no_overlay: true
    });
});


$(function () {
    var __header = '.js-header',
        __content = '.js-header__content',
        __toggle = '.js-header__toggle',
        c_open = 'state-open';

    $('.js-wrapper').on('click', __toggle, function (e) {
        e.preventDefault();

        var $toggle = $(this),
            $header = $toggle.closest(__header),
            $content = $header.find(__content);

        if ($header.hasClass(c_open)) {
            $header.removeClass(c_open);
        } else {
            $header.addClass(c_open);
        }
    })
});
$(function () {

    var imgPlacemark = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAzCAYAAADsBOpPAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAII0lEQVRo3rWabWwUxxnHf7t3nPFLbIJ5N8Ymdv2CsVMIcWwCJRFuiAN2RRFRgtS66oe2MqISpBSr/dAiRXwIRCJIlH5pBKmaClMUGtFWgpJiUQNVGru0CMcBbCuE2sbnN+COw7d30w++XdZzO3t3hj7SaGd2ZnZ++9x/Xm81UjQhRC3QCNQCpcDsWNYocBNoB04DFzRNE6k+/4mYEEITQmwTQlwTydvnhmF8r7W11QNotvB/hy0QQrSlACpb+9jYWBGgS+DTegHXCkKINcDHwNPmvYfh+3x+6ww3/3uBobHr3H/gByBz5mzmzSph6cLVlC95lZm+bPujxkOh0Jb09PTzgCkTWS5JyUcJLIRYBbQBGQCRaJhPu3/LP7qO8jB83/WhPm8mL5Q3UV32XTy6z3rXQCDQkJWV9TcHaDusK7gjsBBiPvBPYDHA/Qd+Pvr7LvpHribjBMvy5jzL5hffJWOm2S8Z6+npqS4qKuqLgZkhaWgV8F+B9QDB0Ai/++T7jN77MiVY03Iy8/jONz8gI21SVZFI5GJxcfH6vr6+qATt5nXLdAfYLSZsVET4qP2tacMCjAduc6r9J0SjBgAej2d1Z2fnG7G25Y6Iw9UdGPi5Gfnsiw+57b8ybVjTvhrqpPNGq5XOzs7+KeCxQdvBXaGnAAshVgIrACaMIJevve+CobGsoJ5NNW/TULuPZQWvoWm6svSlrveZMIKTjep6+dWrV6tj0E7gSmiv9NxvmZHe/nYeTIwrATa+sJeKwk1WunzJq5Qsfpk/XtyDENG48sHQCN23zlK5dLKJ/Pz8BqBDKha1ASbUsAasNhM3brcpYYsWrZ0Ca1rJ4vWU5b+irNfT327F09LSnrd5WCWPOC/Lv2GpGRkc61Y2/LW8l6342NjYqeHh4ZNm+plFa5T1Bka6rLjX6y1OFRYeScK8OcfMMGcwJ7NrNRwOTxiGEbY84KLjQGjIiuu6/jTxkoRHUjDlYaY1QMgVrNYehu8pG+7pb6dyaSMAc+fOfd2e1ztwWVkvEhvaJl9amxHzrn3ctQe7ly09yxq+YybMgd7Jum+d5YuvzsXd7xu4zLW+PynrpafNegQfidxlqiTMq9PiyJLFFA8LIe5ompYPMPupAgKhYWXjH19s4dmib1O0aC2g0dPfzr9u/oGoiCjrzM5aYsXD4XC/DdT0qqlj2cuWh732t4hGo//2eDzPARQuqOXWUIey8aiI0HnjBJ03TpCsLZlfbcXv3r17zQarS8E+vJkeFoDVQzSAiYmJP5slKgo3omuepGESmabplu4BOjo6zjqAaqjXzBpIw9rhw4f/IoQYAMjOWEDVM5ufGHDl0kZyMhcBYBjGSHNz8+UYhB1WpWHLpnS63bt3h4PB4LvmjReX/xCfN/OxYX3eDNYsb7bSXV1dR3t7ew0Xj8pysNLyfksLBoP/2bBhwzZN03J83gxmeNPpHbj4WMDfqNrB0gU1AITD4eH6+vqfDQ4OGkzqMqq4OoX41drBgwcf+v3+X5rp50reoMDWWVK1JfOeZ1XJNit9/vz5d65cuRKSPGi/uprdu6aWPIAnFAr9Pi0tbRPAveAgR8+8yYOHYynBpvtyaHrlQ7IzFwIwMjJyITc398cxL0aBSCwYsRCOBTNt5pvlhXIePX78+FtCiFGApzLm01Czz3X5GOcJTaehdp8FaxjG+K5du952qZLUJlRXVWxqahq8fv16s5kuXFDDuqodSQOvq9pB4YJa65mnT5/+xbFjx4bkdojfHqn2eXHAcQVLS0vP+v3+98wC1WVNrCjemhB2RfFWqsuarHR3d/dvNm/efCEBoP2+6oWUHrbCqlWr3gkEAmfNzLqVeyjOW6eELc5bR93KPVba7/e3VVZW/joJUOXIYDdzWMN2nTKIj4+PawMDA+fr6+vrPB7PHE3TKM57iS8HP+Xeg8EpD1uYu5wta9/Do88AIBgM3mhsbNzZ29s7ITnEHLoiUogidbLY1fK4HdiElkcOrbOzcyIzM/OTmpqaBl3Xszy6l9L8OvoGLhEITa6b580q4fWXfkXajCwAwuHw4Pbt23906tSpUQevRR2CE3Scpz0SqBO0Bmjnzp0LFBYWtlVVVTXoup7u9fgoza/j1p3PyEqfw9Z1h8mILR8Nwxjdu3fvDw4dOnTbQQYqUNnTUdS6trzpYXIF5wPSgSwgB8gF5gN5ra2tGyORyKjq5M8wjLsHDhx4E1gZCyuArwNVwHJgGZNbsSKgAMiLPTs31lZWrG1fjMW+Tp7iYaS4o8dPnDgxVFZWdqmiouI1TdNm2jUciUTuHzlypHnnzp1dtttOUrD/9E7elbUbJwlZFipNA2gnT54cysnJOVNeXp7n8/nmCyGM4eHhtv3797e0tLRcl54hy8AJVu5sSjmoPBs3Vdvk4nWI23e+8l5M9qw5FdunZPs0rNKwNUqoPOr0EijyzbjTOKsawpwAnTwLCknIAMmAOq2w7NB2PaYC6zpxyKalEFcBOw1hqXrYcT3hdJDh5i0TMqIoa+rYbjK007LRcZJwMtUu020xnWihnYqH5Ukk4WrNbVusuaTlPFVnU027blOw6wl8on18qtBu06/TOOy0j1PCQmINy0BRHh102CGdDqTlek7BDpuUJdPpNOlqP12UvSuf2Mh5dviU/owxLdl/Ip3GX/nQw+m432lMll8gadhUgFXQrsdKOHfGaXl2OsBu0E55dnPaHqUMOx1guU5S/60lAEzpE4Xpfg6gJZlO9Ad4yt9TPO73C9OtP+0PP570BxeJJPHY9j8rHTPFG9UXvAAAAABJRU5ErkJggg=='

    var mapContainer = $('.js-map');

    if (mapContainer.length) {
        ymaps.ready(initMap);
    }


    function initMap() {
        var mapCenter = mapContainer.data('center');
        var mapTextBalloon = mapCenter;

        mapCenter = ymaps.geocode(mapCenter);

        mapCenter.then(
            function (res) {
                var CenterGeoObject = res.geoObjects.get(0),
                    centerCoords = CenterGeoObject.geometry.getCoordinates();

                var myMap = new ymaps.Map(mapContainer[0], {
                    center: centerCoords,
                    zoom: mapContainer.data('zoom') || 5,
                    controls: []
                });

                var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    balloonContent: mapTextBalloon
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: imgPlacemark,
                    // Размеры метки.
                    iconImageSize: [44, 51],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-20, -40]
                });

                var zoomControl = new ymaps.control.ZoomControl({
                    options: {
                        position: {
                            top: 300,
                            left: 'auto',
                            right: 10
                        },
                        size: 'auto'
                    }
                });

                myMap.controls.add(zoomControl);
                myMap.geoObjects.add(myPlacemark);
                myMap.behaviors.disable('scrollZoom');
            }
        );
    }

});

$(document).ready(function () {
    var $buttonClose = $('.js-popup__close');

    $(document).on('click', ".js-popup__close", function () {
        $.magnificPopup.close();
    })
});

$(function () {
    var __item = '.js-table-m__item',
        __header = '.js-table-m__item-header',
        __content = '.js-table-m__item-content';

    var c_open = 'state-open';

    $('.js-wrapper').on('click', __header, function (e) {
        e.preventDefault();

        var $header = $(this),
            $item = $header.closest(__item),
            $content = $item.find(__content);

        if ($item.hasClass(c_open)) {
            $content.slideUp(300, function () {
                $item.removeClass(c_open)
            })
        } else {
            $content.slideDown(300, function () {
                $item.addClass(c_open)
            })
        }
    });

    var $tables = $('.content table');

    $tables.each(function (ind, item) {
        var $table = $(item),
            $th = $table.find('thead th'),
            $tr = $table.find('tbody tr');

        var $tableWrap = $table.wrap('<div class="table-wrap js-table-wrap"/>').parent(),
            $tableM = $('<div class="table-m"/>'),
            $tableMitems = $('<div class="table-m__items"/>');

        $tr.each(function (indTr, itemTr) {
            var $curTr = $(itemTr),
                $curTd = $curTr.find('td');

            var $tableMitem = $('<div class="table-m__item js-table-m__item"><div class="table-m__item-header js-table-m__item-header"><div class="table-m__item-header-title">' + $curTd.first().html() + '</div></div></div>');

            var $tableMitemContent = $('<div class="table-m__item-content js-table-m__item-content"/>');

            $curTd.each(function (indTd, itemTd) {
                if (indTd === 0) return;

                var $cell = $(itemTd);

                var $tableMitemCell = $('<div class="table-m__item-cell"><div class="table-m__item-cell-th">' + $th.eq(indTd).html() + '</div><div class="table-m__item-cell-td">' + $cell.html() + '</div></div>');

                $tableMitemContent.append($tableMitemCell);
            });

            $tableMitem.append($tableMitemContent);
            $tableMitems.append($tableMitem);



        });

        $tableM.append($tableMitems);
        $tableWrap.append($tableM);
    })
});

$(function () {
    $(".js-work__desc").nanoScroller({
        alwaysVisible: true,
        preventPageScrolling: false
    });
});

$(function () {
    var __scrollButton = '.section-scroll__content';

    $('body').on('click', __scrollButton, function (e) {
        var $button = $(this),
            $curSection = $button.closest('.section'),
            curScroll = $('body').scrollTop(),
            curSectionCoords = $curSection[0].getBoundingClientRect(),
            newScroll = 0;

        newScroll = curScroll + curSectionCoords.bottom;

        $('body, html').animate({
            scrollTop: newScroll
        }, 500)

    })
});

$(document).ready(function () {

    $('.js-mask-phone').mask("+79999999999", {
        completed: function () {
            this.trigger('validation.validForm');
            this.closest('.js-valid-form').trigger('validation.vilidForm');
        }
    }, {
        autoclear: false
    }, {
        placeholder: "+7__________"
    });


    //Просто попап для форм и любого контента
    $('.js-popup-open').magnificPopup({
        type: 'ajax',
        preloader: true,
        showCloseBtn: false,

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
                window.send_counter_event('callback_mail-button');
            },
            ajaxContentAdded: function () {
                setTimeout(function () {
                    $('.mfp-content .js-mask-phone').mask("+79999999999", {}, {
                        autoclear: false
                    }, {
                        placeholder: "+7__________"
                    });
                }, 1000);
            }
        }
    });

    //Попап с галереей
    $('.js-popup-gallery').magnificPopup({
        delegate: 'a.js-gallery-link',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        closeBtnInside: false,
        fixedContentPos: true,
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function (item) {
                return item.el.attr('title');
            }
        }
    });



});

window.send_counter_event = function (target) {
    if (typeof Ya != "undefined" && typeof Object.keys != "undefined") {
        var counter = Ya._metrika.counters[Object.keys(Ya._metrika.counters)[0]];
        counter.reachGoal(target, function () {
            console.log('sended ' + target);
        });
    }
};
// callback_mail-button - нажатие на кнопку обратного звонка
// submit_ + name формы - попытка отправки формы
// success_send_ + name формы - успешная отправка формы

function openSuccessPopup() {
    $.magnificPopup.close();
    $.magnificPopup.open({
        type: 'inline',
        preloader: true,
        showCloseBtn: false,
        items: {
            src: $("#popup-message-succes")
        },
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    });
}
$(function () {

    $(document).on("submit", 'form[name="callback_mail"], form[name="message_mail"]', function (e) {
        e.stopPropagation();
        var form_obj = $(this);
        var path = form_obj.data("ajax_valid_path");
        var form_name = $(this).attr('name');

        form_obj.find(".js_wrap_input").removeClass("error").find(".input__error").html("");
        window.send_counter_event('submit_' + form_name);
        $.ajax({
            url: path,
            type: 'POST',
            data: form_obj.serialize()
        }).then(function (response) {
            var isError = false;
            var inputs = form_obj.find('input,textarea');
            if (response.hasOwnProperty('error')) {
                for (var name in response.error) {
                    inputs.each(function (key, obj) {
                        if ($(obj).attr("name") && $(obj).attr("name").indexOf(name) != -1) {
                            $(obj).closest(".js_wrap_input").addClass("error").find(".input__error").html(response.error[name][0]);
                            isError = true;
                        }
                    });
                }
            }
            if (!isError && response.hasOwnProperty('ok')) {
                form_obj.find(".js_wrap_input input").val("");
                window.send_counter_event('success_send_' + form_name);
                openSuccessPopup();
            }
            return false;

        });
        return false;
    });



    // разворачивание текста
    $(".js_open_text").on("click", function () {
        var wrap = $(this).closest(".js_wrap");
        var set_class = false;
        wrap.find(".js_toggle_text").slideToggle("slow", function () {
            if (!set_class) {
                set_class = true;
                if (wrap.hasClass("js_open")) {
                    wrap.removeClass("js_open").addClass("js_close");
                } else {
                    wrap.removeClass("js_close").addClass("js_open");
                }
            }
            return false;
        });
        return false;
    });

});
// форматирование денег
function numberFormat(item) {
    var n = 0,
        x = 3,
        s = ' ',
        c = ',';
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = parseInt(item).toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
}