/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */ ! function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, (function (e, t) {
    "use strict";
    var i = [],
        n = e.document,
        r = Object.getPrototypeOf,
        s = i.slice,
        a = i.concat,
        o = i.push,
        l = i.indexOf,
        d = {},
        c = d.toString,
        u = d.hasOwnProperty,
        h = u.toString,
        p = h.call(Object),
        f = {},
        g = function (e) {
            return "function" == typeof e && "number" != typeof e.nodeType
        },
        m = function (e) {
            return null != e && e === e.window
        },
        v = {
            type: !0,
            src: !0,
            noModule: !0
        };

    function y(e, t, i) {
        var r, s = (t = t || n).createElement("script");
        if (s.text = e, i)
            for (r in v) i[r] && (s[r] = i[r]);
        t.head.appendChild(s).parentNode.removeChild(s)
    }

    function b(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? d[c.call(e)] || "object" : typeof e
    }
    var w = function (e, t) {
            return new w.fn.init(e, t)
        },
        x = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

    function C(e) {
        var t = !!e && "length" in e && e.length,
            i = b(e);
        return !g(e) && !m(e) && ("array" === i || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }
    w.fn = w.prototype = {
        jquery: "3.3.1",
        constructor: w,
        length: 0,
        toArray: function () {
            return s.call(this)
        },
        get: function (e) {
            return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e]
        },
        pushStack: function (e) {
            var t = w.merge(this.constructor(), e);
            return t.prevObject = this, t
        },
        each: function (e) {
            return w.each(this, e)
        },
        map: function (e) {
            return this.pushStack(w.map(this, (function (t, i) {
                return e.call(t, i, t)
            })))
        },
        slice: function () {
            return this.pushStack(s.apply(this, arguments))
        },
        first: function () {
            return this.eq(0)
        },
        last: function () {
            return this.eq(-1)
        },
        eq: function (e) {
            var t = this.length,
                i = +e + (e < 0 ? t : 0);
            return this.pushStack(i >= 0 && i < t ? [this[i]] : [])
        },
        end: function () {
            return this.prevObject || this.constructor()
        },
        push: o,
        sort: i.sort,
        splice: i.splice
    }, w.extend = w.fn.extend = function () {
        var e, t, i, n, r, s, a = arguments[0] || {},
            o = 1,
            l = arguments.length,
            d = !1;
        for ("boolean" == typeof a && (d = a, a = arguments[o] || {}, o++), "object" == typeof a || g(a) || (a = {}), o === l && (a = this, o--); o < l; o++)
            if (null != (e = arguments[o]))
                for (t in e) i = a[t], a !== (n = e[t]) && (d && n && (w.isPlainObject(n) || (r = Array.isArray(n))) ? (r ? (r = !1, s = i && Array.isArray(i) ? i : []) : s = i && w.isPlainObject(i) ? i : {}, a[t] = w.extend(d, s, n)) : void 0 !== n && (a[t] = n));
        return a
    }, w.extend({
        expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function (e) {
            throw new Error(e)
        },
        noop: function () {},
        isPlainObject: function (e) {
            var t, i;
            return !(!e || "[object Object]" !== c.call(e) || (t = r(e)) && ("function" != typeof (i = u.call(t, "constructor") && t.constructor) || h.call(i) !== p))
        },
        isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        globalEval: function (e) {
            y(e)
        },
        each: function (e, t) {
            var i, n = 0;
            if (C(e))
                for (i = e.length; n < i && !1 !== t.call(e[n], n, e[n]); n++);
            else
                for (n in e)
                    if (!1 === t.call(e[n], n, e[n])) break;
            return e
        },
        trim: function (e) {
            return null == e ? "" : (e + "").replace(x, "")
        },
        makeArray: function (e, t) {
            var i = t || [];
            return null != e && (C(Object(e)) ? w.merge(i, "string" == typeof e ? [e] : e) : o.call(i, e)), i
        },
        inArray: function (e, t, i) {
            return null == t ? -1 : l.call(t, e, i)
        },
        merge: function (e, t) {
            for (var i = +t.length, n = 0, r = e.length; n < i; n++) e[r++] = t[n];
            return e.length = r, e
        },
        grep: function (e, t, i) {
            for (var n = [], r = 0, s = e.length, a = !i; r < s; r++) !t(e[r], r) !== a && n.push(e[r]);
            return n
        },
        map: function (e, t, i) {
            var n, r, s = 0,
                o = [];
            if (C(e))
                for (n = e.length; s < n; s++) null != (r = t(e[s], s, i)) && o.push(r);
            else
                for (s in e) null != (r = t(e[s], s, i)) && o.push(r);
            return a.apply([], o)
        },
        guid: 1,
        support: f
    }), "function" == typeof Symbol && (w.fn[Symbol.iterator] = i[Symbol.iterator]), w.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), (function (e, t) {
        d["[object " + t + "]"] = t.toLowerCase()
    }));
    var T = function (e) {
        var t, i, n, r, s, a, o, l, d, c, u, h, p, f, g, m, v, y, b, w = "sizzle" + 1 * new Date,
            x = e.document,
            C = 0,
            T = 0,
            E = ae(),
            S = ae(),
            _ = ae(),
            k = function (e, t) {
                return e === t && (u = !0), 0
            },
            $ = {}.hasOwnProperty,
            I = [],
            D = I.pop,
            A = I.push,
            M = I.push,
            P = I.slice,
            O = function (e, t) {
                for (var i = 0, n = e.length; i < n; i++)
                    if (e[i] === t) return i;
                return -1
            },
            N = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            L = "[\\x20\\t\\r\\n\\f]",
            z = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            j = "\\[" + L + "*(" + z + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + z + "))|)" + L + "*\\]",
            H = ":(" + z + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + j + ")*)|.*)\\)|)",
            q = new RegExp(L + "+", "g"),
            R = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"),
            B = new RegExp("^" + L + "*," + L + "*"),
            W = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"),
            F = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"),
            G = new RegExp(H),
            X = new RegExp("^" + z + "$"),
            V = {
                ID: new RegExp("^#(" + z + ")"),
                CLASS: new RegExp("^\\.(" + z + ")"),
                TAG: new RegExp("^(" + z + "|[*])"),
                ATTR: new RegExp("^" + j),
                PSEUDO: new RegExp("^" + H),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + N + ")$", "i"),
                needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
            },
            Y = /^(?:input|select|textarea|button)$/i,
            U = /^h\d$/i,
            K = /^[^{]+\{\s*\[native \w/,
            Q = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            Z = /[+~]/,
            J = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"),
            ee = function (e, t, i) {
                var n = "0x" + t - 65536;
                return n != n || i ? t : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            },
            te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ie = function (e, t) {
                return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            ne = function () {
                h()
            },
            re = ye((function (e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }), {
                dir: "parentNode",
                next: "legend"
            });
        try {
            M.apply(I = P.call(x.childNodes), x.childNodes), I[x.childNodes.length].nodeType
        } catch (e) {
            M = {
                apply: I.length ? function (e, t) {
                    A.apply(e, P.call(t))
                } : function (e, t) {
                    for (var i = e.length, n = 0; e[i++] = t[n++];);
                    e.length = i - 1
                }
            }
        }

        function se(e, t, n, r) {
            var s, o, d, c, u, f, v, y = t && t.ownerDocument,
                C = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== C && 9 !== C && 11 !== C) return n;
            if (!r && ((t ? t.ownerDocument || t : x) !== p && h(t), t = t || p, g)) {
                if (11 !== C && (u = Q.exec(e)))
                    if (s = u[1]) {
                        if (9 === C) {
                            if (!(d = t.getElementById(s))) return n;
                            if (d.id === s) return n.push(d), n
                        } else if (y && (d = y.getElementById(s)) && b(t, d) && d.id === s) return n.push(d), n
                    } else {
                        if (u[2]) return M.apply(n, t.getElementsByTagName(e)), n;
                        if ((s = u[3]) && i.getElementsByClassName && t.getElementsByClassName) return M.apply(n, t.getElementsByClassName(s)), n
                    } if (i.qsa && !_[e + " "] && (!m || !m.test(e))) {
                    if (1 !== C) y = t, v = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((c = t.getAttribute("id")) ? c = c.replace(te, ie) : t.setAttribute("id", c = w), o = (f = a(e)).length; o--;) f[o] = "#" + c + " " + ve(f[o]);
                        v = f.join(","), y = Z.test(e) && ge(t.parentNode) || t
                    }
                    if (v) try {
                        return M.apply(n, y.querySelectorAll(v)), n
                    } catch (e) {} finally {
                        c === w && t.removeAttribute("id")
                    }
                }
            }
            return l(e.replace(R, "$1"), t, n, r)
        }

        function ae() {
            var e = [];
            return function t(i, r) {
                return e.push(i + " ") > n.cacheLength && delete t[e.shift()], t[i + " "] = r
            }
        }

        function oe(e) {
            return e[w] = !0, e
        }

        function le(e) {
            var t = p.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function de(e, t) {
            for (var i = e.split("|"), r = i.length; r--;) n.attrHandle[i[r]] = t
        }

        function ce(e, t) {
            var i = t && e,
                n = i && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (n) return n;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === t) return -1;
            return e ? 1 : -1
        }

        function ue(e) {
            return function (t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function he(e) {
            return function (t) {
                var i = t.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && t.type === e
            }
        }

        function pe(e) {
            return function (t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && re(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function fe(e) {
            return oe((function (t) {
                return t = +t, oe((function (i, n) {
                    for (var r, s = e([], i.length, t), a = s.length; a--;) i[r = s[a]] && (i[r] = !(n[r] = i[r]))
                }))
            }))
        }

        function ge(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }
        for (t in i = se.support = {}, s = se.isXML = function (e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return !!t && "HTML" !== t.nodeName
            }, h = se.setDocument = function (e) {
                var t, r, a = e ? e.ownerDocument || e : x;
                return a !== p && 9 === a.nodeType && a.documentElement ? (f = (p = a).documentElement, g = !s(p), x !== p && (r = p.defaultView) && r.top !== r && (r.addEventListener ? r.addEventListener("unload", ne, !1) : r.attachEvent && r.attachEvent("onunload", ne)), i.attributes = le((function (e) {
                    return e.className = "i", !e.getAttribute("className")
                })), i.getElementsByTagName = le((function (e) {
                    return e.appendChild(p.createComment("")), !e.getElementsByTagName("*").length
                })), i.getElementsByClassName = K.test(p.getElementsByClassName), i.getById = le((function (e) {
                    return f.appendChild(e).id = w, !p.getElementsByName || !p.getElementsByName(w).length
                })), i.getById ? (n.filter.ID = function (e) {
                    var t = e.replace(J, ee);
                    return function (e) {
                        return e.getAttribute("id") === t
                    }
                }, n.find.ID = function (e, t) {
                    if (void 0 !== t.getElementById && g) {
                        var i = t.getElementById(e);
                        return i ? [i] : []
                    }
                }) : (n.filter.ID = function (e) {
                    var t = e.replace(J, ee);
                    return function (e) {
                        var i = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                        return i && i.value === t
                    }
                }, n.find.ID = function (e, t) {
                    if (void 0 !== t.getElementById && g) {
                        var i, n, r, s = t.getElementById(e);
                        if (s) {
                            if ((i = s.getAttributeNode("id")) && i.value === e) return [s];
                            for (r = t.getElementsByName(e), n = 0; s = r[n++];)
                                if ((i = s.getAttributeNode("id")) && i.value === e) return [s]
                        }
                        return []
                    }
                }), n.find.TAG = i.getElementsByTagName ? function (e, t) {
                    return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : i.qsa ? t.querySelectorAll(e) : void 0
                } : function (e, t) {
                    var i, n = [],
                        r = 0,
                        s = t.getElementsByTagName(e);
                    if ("*" === e) {
                        for (; i = s[r++];) 1 === i.nodeType && n.push(i);
                        return n
                    }
                    return s
                }, n.find.CLASS = i.getElementsByClassName && function (e, t) {
                    if (void 0 !== t.getElementsByClassName && g) return t.getElementsByClassName(e)
                }, v = [], m = [], (i.qsa = K.test(p.querySelectorAll)) && (le((function (e) {
                    f.appendChild(e).innerHTML = "<a id='" + w + "'></a><select id='" + w + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + L + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || m.push("\\[" + L + "*(?:value|" + N + ")"), e.querySelectorAll("[id~=" + w + "-]").length || m.push("~="), e.querySelectorAll(":checked").length || m.push(":checked"), e.querySelectorAll("a#" + w + "+*").length || m.push(".#.+[+~]")
                })), le((function (e) {
                    e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                    var t = p.createElement("input");
                    t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && m.push("name" + L + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && m.push(":enabled", ":disabled"), f.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && m.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), m.push(",.*:")
                }))), (i.matchesSelector = K.test(y = f.matches || f.webkitMatchesSelector || f.mozMatchesSelector || f.oMatchesSelector || f.msMatchesSelector)) && le((function (e) {
                    i.disconnectedMatch = y.call(e, "*"), y.call(e, "[s!='']:x"), v.push("!=", H)
                })), m = m.length && new RegExp(m.join("|")), v = v.length && new RegExp(v.join("|")), t = K.test(f.compareDocumentPosition), b = t || K.test(f.contains) ? function (e, t) {
                    var i = 9 === e.nodeType ? e.documentElement : e,
                        n = t && t.parentNode;
                    return e === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(n)))
                } : function (e, t) {
                    if (t)
                        for (; t = t.parentNode;)
                            if (t === e) return !0;
                    return !1
                }, k = t ? function (e, t) {
                    if (e === t) return u = !0, 0;
                    var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                    return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !i.sortDetached && t.compareDocumentPosition(e) === n ? e === p || e.ownerDocument === x && b(x, e) ? -1 : t === p || t.ownerDocument === x && b(x, t) ? 1 : c ? O(c, e) - O(c, t) : 0 : 4 & n ? -1 : 1)
                } : function (e, t) {
                    if (e === t) return u = !0, 0;
                    var i, n = 0,
                        r = e.parentNode,
                        s = t.parentNode,
                        a = [e],
                        o = [t];
                    if (!r || !s) return e === p ? -1 : t === p ? 1 : r ? -1 : s ? 1 : c ? O(c, e) - O(c, t) : 0;
                    if (r === s) return ce(e, t);
                    for (i = e; i = i.parentNode;) a.unshift(i);
                    for (i = t; i = i.parentNode;) o.unshift(i);
                    for (; a[n] === o[n];) n++;
                    return n ? ce(a[n], o[n]) : a[n] === x ? -1 : o[n] === x ? 1 : 0
                }, p) : p
            }, se.matches = function (e, t) {
                return se(e, null, null, t)
            }, se.matchesSelector = function (e, t) {
                if ((e.ownerDocument || e) !== p && h(e), t = t.replace(F, "='$1']"), i.matchesSelector && g && !_[t + " "] && (!v || !v.test(t)) && (!m || !m.test(t))) try {
                    var n = y.call(e, t);
                    if (n || i.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                } catch (e) {}
                return se(t, p, null, [e]).length > 0
            }, se.contains = function (e, t) {
                return (e.ownerDocument || e) !== p && h(e), b(e, t)
            }, se.attr = function (e, t) {
                (e.ownerDocument || e) !== p && h(e);
                var r = n.attrHandle[t.toLowerCase()],
                    s = r && $.call(n.attrHandle, t.toLowerCase()) ? r(e, t, !g) : void 0;
                return void 0 !== s ? s : i.attributes || !g ? e.getAttribute(t) : (s = e.getAttributeNode(t)) && s.specified ? s.value : null
            }, se.escape = function (e) {
                return (e + "").replace(te, ie)
            }, se.error = function (e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            }, se.uniqueSort = function (e) {
                var t, n = [],
                    r = 0,
                    s = 0;
                if (u = !i.detectDuplicates, c = !i.sortStable && e.slice(0), e.sort(k), u) {
                    for (; t = e[s++];) t === e[s] && (r = n.push(s));
                    for (; r--;) e.splice(n[r], 1)
                }
                return c = null, e
            }, r = se.getText = function (e) {
                var t, i = "",
                    n = 0,
                    s = e.nodeType;
                if (s) {
                    if (1 === s || 9 === s || 11 === s) {
                        if ("string" == typeof e.textContent) return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) i += r(e)
                    } else if (3 === s || 4 === s) return e.nodeValue
                } else
                    for (; t = e[n++];) i += r(t);
                return i
            }, (n = se.selectors = {
                cacheLength: 50,
                createPseudo: oe,
                match: V,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function (e) {
                        return e[1] = e[1].replace(J, ee), e[3] = (e[3] || e[4] || e[5] || "").replace(J, ee), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function (e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e
                    },
                    PSEUDO: function (e) {
                        var t, i = !e[6] && e[2];
                        return V.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : i && G.test(i) && (t = a(i, !0)) && (t = i.indexOf(")", i.length - t) - i.length) && (e[0] = e[0].slice(0, t), e[2] = i.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function (e) {
                        var t = e.replace(J, ee).toLowerCase();
                        return "*" === e ? function () {
                            return !0
                        } : function (e) {
                            return e.nodeName && e.nodeName.toLowerCase() === t
                        }
                    },
                    CLASS: function (e) {
                        var t = E[e + " "];
                        return t || (t = new RegExp("(^|" + L + ")" + e + "(" + L + "|$)")) && E(e, (function (e) {
                            return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                        }))
                    },
                    ATTR: function (e, t, i) {
                        return function (n) {
                            var r = se.attr(n, e);
                            return null == r ? "!=" === t : !t || (r += "", "=" === t ? r === i : "!=" === t ? r !== i : "^=" === t ? i && 0 === r.indexOf(i) : "*=" === t ? i && r.indexOf(i) > -1 : "$=" === t ? i && r.slice(-i.length) === i : "~=" === t ? (" " + r.replace(q, " ") + " ").indexOf(i) > -1 : "|=" === t && (r === i || r.slice(0, i.length + 1) === i + "-"))
                        }
                    },
                    CHILD: function (e, t, i, n, r) {
                        var s = "nth" !== e.slice(0, 3),
                            a = "last" !== e.slice(-4),
                            o = "of-type" === t;
                        return 1 === n && 0 === r ? function (e) {
                            return !!e.parentNode
                        } : function (t, i, l) {
                            var d, c, u, h, p, f, g = s !== a ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                v = o && t.nodeName.toLowerCase(),
                                y = !l && !o,
                                b = !1;
                            if (m) {
                                if (s) {
                                    for (; g;) {
                                        for (h = t; h = h[g];)
                                            if (o ? h.nodeName.toLowerCase() === v : 1 === h.nodeType) return !1;
                                        f = g = "only" === e && !f && "nextSibling"
                                    }
                                    return !0
                                }
                                if (f = [a ? m.firstChild : m.lastChild], a && y) {
                                    for (b = (p = (d = (c = (u = (h = m)[w] || (h[w] = {}))[h.uniqueID] || (u[h.uniqueID] = {}))[e] || [])[0] === C && d[1]) && d[2], h = p && m.childNodes[p]; h = ++p && h && h[g] || (b = p = 0) || f.pop();)
                                        if (1 === h.nodeType && ++b && h === t) {
                                            c[e] = [C, p, b];
                                            break
                                        }
                                } else if (y && (b = p = (d = (c = (u = (h = t)[w] || (h[w] = {}))[h.uniqueID] || (u[h.uniqueID] = {}))[e] || [])[0] === C && d[1]), !1 === b)
                                    for (;
                                        (h = ++p && h && h[g] || (b = p = 0) || f.pop()) && ((o ? h.nodeName.toLowerCase() !== v : 1 !== h.nodeType) || !++b || (y && ((c = (u = h[w] || (h[w] = {}))[h.uniqueID] || (u[h.uniqueID] = {}))[e] = [C, b]), h !== t)););
                                return (b -= r) === n || b % n == 0 && b / n >= 0
                            }
                        }
                    },
                    PSEUDO: function (e, t) {
                        var i, r = n.pseudos[e] || n.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
                        return r[w] ? r(t) : r.length > 1 ? (i = [e, e, "", t], n.setFilters.hasOwnProperty(e.toLowerCase()) ? oe((function (e, i) {
                            for (var n, s = r(e, t), a = s.length; a--;) e[n = O(e, s[a])] = !(i[n] = s[a])
                        })) : function (e) {
                            return r(e, 0, i)
                        }) : r
                    }
                },
                pseudos: {
                    not: oe((function (e) {
                        var t = [],
                            i = [],
                            n = o(e.replace(R, "$1"));
                        return n[w] ? oe((function (e, t, i, r) {
                            for (var s, a = n(e, null, r, []), o = e.length; o--;)(s = a[o]) && (e[o] = !(t[o] = s))
                        })) : function (e, r, s) {
                            return t[0] = e, n(t, null, s, i), t[0] = null, !i.pop()
                        }
                    })),
                    has: oe((function (e) {
                        return function (t) {
                            return se(e, t).length > 0
                        }
                    })),
                    contains: oe((function (e) {
                        return e = e.replace(J, ee),
                            function (t) {
                                return (t.textContent || t.innerText || r(t)).indexOf(e) > -1
                            }
                    })),
                    lang: oe((function (e) {
                        return X.test(e || "") || se.error("unsupported lang: " + e), e = e.replace(J, ee).toLowerCase(),
                            function (t) {
                                var i;
                                do {
                                    if (i = g ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (i = i.toLowerCase()) === e || 0 === i.indexOf(e + "-")
                                } while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    })),
                    target: function (t) {
                        var i = e.location && e.location.hash;
                        return i && i.slice(1) === t.id
                    },
                    root: function (e) {
                        return e === f
                    },
                    focus: function (e) {
                        return e === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: pe(!1),
                    disabled: pe(!0),
                    checked: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function (e) {
                        return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                    },
                    empty: function (e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeType < 6) return !1;
                        return !0
                    },
                    parent: function (e) {
                        return !n.pseudos.empty(e)
                    },
                    header: function (e) {
                        return U.test(e.nodeName)
                    },
                    input: function (e) {
                        return Y.test(e.nodeName)
                    },
                    button: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function (e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                    },
                    first: fe((function () {
                        return [0]
                    })),
                    last: fe((function (e, t) {
                        return [t - 1]
                    })),
                    eq: fe((function (e, t, i) {
                        return [i < 0 ? i + t : i]
                    })),
                    even: fe((function (e, t) {
                        for (var i = 0; i < t; i += 2) e.push(i);
                        return e
                    })),
                    odd: fe((function (e, t) {
                        for (var i = 1; i < t; i += 2) e.push(i);
                        return e
                    })),
                    lt: fe((function (e, t, i) {
                        for (var n = i < 0 ? i + t : i; --n >= 0;) e.push(n);
                        return e
                    })),
                    gt: fe((function (e, t, i) {
                        for (var n = i < 0 ? i + t : i; ++n < t;) e.push(n);
                        return e
                    }))
                }
            }).pseudos.nth = n.pseudos.eq, {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) n.pseudos[t] = ue(t);
        for (t in {
                submit: !0,
                reset: !0
            }) n.pseudos[t] = he(t);

        function me() {}

        function ve(e) {
            for (var t = 0, i = e.length, n = ""; t < i; t++) n += e[t].value;
            return n
        }

        function ye(e, t, i) {
            var n = t.dir,
                r = t.next,
                s = r || n,
                a = i && "parentNode" === s,
                o = T++;
            return t.first ? function (t, i, r) {
                for (; t = t[n];)
                    if (1 === t.nodeType || a) return e(t, i, r);
                return !1
            } : function (t, i, l) {
                var d, c, u, h = [C, o];
                if (l) {
                    for (; t = t[n];)
                        if ((1 === t.nodeType || a) && e(t, i, l)) return !0
                } else
                    for (; t = t[n];)
                        if (1 === t.nodeType || a)
                            if (c = (u = t[w] || (t[w] = {}))[t.uniqueID] || (u[t.uniqueID] = {}), r && r === t.nodeName.toLowerCase()) t = t[n] || t;
                            else {
                                if ((d = c[s]) && d[0] === C && d[1] === o) return h[2] = d[2];
                                if (c[s] = h, h[2] = e(t, i, l)) return !0
                            } return !1
            }
        }

        function be(e) {
            return e.length > 1 ? function (t, i, n) {
                for (var r = e.length; r--;)
                    if (!e[r](t, i, n)) return !1;
                return !0
            } : e[0]
        }

        function we(e, t, i, n, r) {
            for (var s, a = [], o = 0, l = e.length, d = null != t; o < l; o++)(s = e[o]) && (i && !i(s, n, r) || (a.push(s), d && t.push(o)));
            return a
        }

        function xe(e, t, i, n, r, s) {
            return n && !n[w] && (n = xe(n)), r && !r[w] && (r = xe(r, s)), oe((function (s, a, o, l) {
                var d, c, u, h = [],
                    p = [],
                    f = a.length,
                    g = s || function (e, t, i) {
                        for (var n = 0, r = t.length; n < r; n++) se(e, t[n], i);
                        return i
                    }(t || "*", o.nodeType ? [o] : o, []),
                    m = !e || !s && t ? g : we(g, h, e, o, l),
                    v = i ? r || (s ? e : f || n) ? [] : a : m;
                if (i && i(m, v, o, l), n)
                    for (d = we(v, p), n(d, [], o, l), c = d.length; c--;)(u = d[c]) && (v[p[c]] = !(m[p[c]] = u));
                if (s) {
                    if (r || e) {
                        if (r) {
                            for (d = [], c = v.length; c--;)(u = v[c]) && d.push(m[c] = u);
                            r(null, v = [], d, l)
                        }
                        for (c = v.length; c--;)(u = v[c]) && (d = r ? O(s, u) : h[c]) > -1 && (s[d] = !(a[d] = u))
                    }
                } else v = we(v === a ? v.splice(f, v.length) : v), r ? r(null, a, v, l) : M.apply(a, v)
            }))
        }

        function Ce(e) {
            for (var t, i, r, s = e.length, a = n.relative[e[0].type], o = a || n.relative[" "], l = a ? 1 : 0, c = ye((function (e) {
                    return e === t
                }), o, !0), u = ye((function (e) {
                    return O(t, e) > -1
                }), o, !0), h = [function (e, i, n) {
                    var r = !a && (n || i !== d) || ((t = i).nodeType ? c(e, i, n) : u(e, i, n));
                    return t = null, r
                }]; l < s; l++)
                if (i = n.relative[e[l].type]) h = [ye(be(h), i)];
                else {
                    if ((i = n.filter[e[l].type].apply(null, e[l].matches))[w]) {
                        for (r = ++l; r < s && !n.relative[e[r].type]; r++);
                        return xe(l > 1 && be(h), l > 1 && ve(e.slice(0, l - 1).concat({
                            value: " " === e[l - 2].type ? "*" : ""
                        })).replace(R, "$1"), i, l < r && Ce(e.slice(l, r)), r < s && Ce(e = e.slice(r)), r < s && ve(e))
                    }
                    h.push(i)
                } return be(h)
        }

        function Te(e, t) {
            var i = t.length > 0,
                r = e.length > 0,
                s = function (s, a, o, l, c) {
                    var u, f, m, v = 0,
                        y = "0",
                        b = s && [],
                        w = [],
                        x = d,
                        T = s || r && n.find.TAG("*", c),
                        E = C += null == x ? 1 : Math.random() || .1,
                        S = T.length;
                    for (c && (d = a === p || a || c); y !== S && null != (u = T[y]); y++) {
                        if (r && u) {
                            for (f = 0, a || u.ownerDocument === p || (h(u), o = !g); m = e[f++];)
                                if (m(u, a || p, o)) {
                                    l.push(u);
                                    break
                                } c && (C = E)
                        }
                        i && ((u = !m && u) && v--, s && b.push(u))
                    }
                    if (v += y, i && y !== v) {
                        for (f = 0; m = t[f++];) m(b, w, a, o);
                        if (s) {
                            if (v > 0)
                                for (; y--;) b[y] || w[y] || (w[y] = D.call(l));
                            w = we(w)
                        }
                        M.apply(l, w), c && !s && w.length > 0 && v + t.length > 1 && se.uniqueSort(l)
                    }
                    return c && (C = E, d = x), b
                };
            return i ? oe(s) : s
        }
        return me.prototype = n.filters = n.pseudos, n.setFilters = new me, a = se.tokenize = function (e, t) {
            var i, r, s, a, o, l, d, c = S[e + " "];
            if (c) return t ? 0 : c.slice(0);
            for (o = e, l = [], d = n.preFilter; o;) {
                for (a in i && !(r = B.exec(o)) || (r && (o = o.slice(r[0].length) || o), l.push(s = [])), i = !1, (r = W.exec(o)) && (i = r.shift(), s.push({
                        value: i,
                        type: r[0].replace(R, " ")
                    }), o = o.slice(i.length)), n.filter) !(r = V[a].exec(o)) || d[a] && !(r = d[a](r)) || (i = r.shift(), s.push({
                    value: i,
                    type: a,
                    matches: r
                }), o = o.slice(i.length));
                if (!i) break
            }
            return t ? o.length : o ? se.error(e) : S(e, l).slice(0)
        }, o = se.compile = function (e, t) {
            var i, n = [],
                r = [],
                s = _[e + " "];
            if (!s) {
                for (t || (t = a(e)), i = t.length; i--;)(s = Ce(t[i]))[w] ? n.push(s) : r.push(s);
                (s = _(e, Te(r, n))).selector = e
            }
            return s
        }, l = se.select = function (e, t, i, r) {
            var s, l, d, c, u, h = "function" == typeof e && e,
                p = !r && a(e = h.selector || e);
            if (i = i || [], 1 === p.length) {
                if ((l = p[0] = p[0].slice(0)).length > 2 && "ID" === (d = l[0]).type && 9 === t.nodeType && g && n.relative[l[1].type]) {
                    if (!(t = (n.find.ID(d.matches[0].replace(J, ee), t) || [])[0])) return i;
                    h && (t = t.parentNode), e = e.slice(l.shift().value.length)
                }
                for (s = V.needsContext.test(e) ? 0 : l.length; s-- && (d = l[s], !n.relative[c = d.type]);)
                    if ((u = n.find[c]) && (r = u(d.matches[0].replace(J, ee), Z.test(l[0].type) && ge(t.parentNode) || t))) {
                        if (l.splice(s, 1), !(e = r.length && ve(l))) return M.apply(i, r), i;
                        break
                    }
            }
            return (h || o(e, p))(r, t, !g, i, !t || Z.test(e) && ge(t.parentNode) || t), i
        }, i.sortStable = w.split("").sort(k).join("") === w, i.detectDuplicates = !!u, h(), i.sortDetached = le((function (e) {
            return 1 & e.compareDocumentPosition(p.createElement("fieldset"))
        })), le((function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        })) || de("type|href|height|width", (function (e, t, i) {
            if (!i) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        })), i.attributes && le((function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        })) || de("value", (function (e, t, i) {
            if (!i && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        })), le((function (e) {
            return null == e.getAttribute("disabled")
        })) || de(N, (function (e, t, i) {
            var n;
            if (!i) return !0 === e[t] ? t.toLowerCase() : (n = e.getAttributeNode(t)) && n.specified ? n.value : null
        })), se
    }(e);
    w.find = T, w.expr = T.selectors, w.expr[":"] = w.expr.pseudos, w.uniqueSort = w.unique = T.uniqueSort, w.text = T.getText, w.isXMLDoc = T.isXML, w.contains = T.contains, w.escapeSelector = T.escape;
    var E = function (e, t, i) {
            for (var n = [], r = void 0 !== i;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (r && w(e).is(i)) break;
                    n.push(e)
                } return n
        },
        S = function (e, t) {
            for (var i = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && i.push(e);
            return i
        },
        _ = w.expr.match.needsContext;

    function k(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }
    var $ = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

    function I(e, t, i) {
        return g(t) ? w.grep(e, (function (e, n) {
            return !!t.call(e, n, e) !== i
        })) : t.nodeType ? w.grep(e, (function (e) {
            return e === t !== i
        })) : "string" != typeof t ? w.grep(e, (function (e) {
            return l.call(t, e) > -1 !== i
        })) : w.filter(t, e, i)
    }
    w.filter = function (e, t, i) {
        var n = t[0];
        return i && (e = ":not(" + e + ")"), 1 === t.length && 1 === n.nodeType ? w.find.matchesSelector(n, e) ? [n] : [] : w.find.matches(e, w.grep(t, (function (e) {
            return 1 === e.nodeType
        })))
    }, w.fn.extend({
        find: function (e) {
            var t, i, n = this.length,
                r = this;
            if ("string" != typeof e) return this.pushStack(w(e).filter((function () {
                for (t = 0; t < n; t++)
                    if (w.contains(r[t], this)) return !0
            })));
            for (i = this.pushStack([]), t = 0; t < n; t++) w.find(e, r[t], i);
            return n > 1 ? w.uniqueSort(i) : i
        },
        filter: function (e) {
            return this.pushStack(I(this, e || [], !1))
        },
        not: function (e) {
            return this.pushStack(I(this, e || [], !0))
        },
        is: function (e) {
            return !!I(this, "string" == typeof e && _.test(e) ? w(e) : e || [], !1).length
        }
    });
    var D, A = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (w.fn.init = function (e, t, i) {
        var r, s;
        if (!e) return this;
        if (i = i || D, "string" == typeof e) {
            if (!(r = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : A.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || i).find(e) : this.constructor(t).find(e);
            if (r[1]) {
                if (t = t instanceof w ? t[0] : t, w.merge(this, w.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : n, !0)), $.test(r[1]) && w.isPlainObject(t))
                    for (r in t) g(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                return this
            }
            return (s = n.getElementById(r[2])) && (this[0] = s, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : g(e) ? void 0 !== i.ready ? i.ready(e) : e(w) : w.makeArray(e, this)
    }).prototype = w.fn, D = w(n);
    var M = /^(?:parents|prev(?:Until|All))/,
        P = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };

    function O(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e
    }
    w.fn.extend({
        has: function (e) {
            var t = w(e, this),
                i = t.length;
            return this.filter((function () {
                for (var e = 0; e < i; e++)
                    if (w.contains(this, t[e])) return !0
            }))
        },
        closest: function (e, t) {
            var i, n = 0,
                r = this.length,
                s = [],
                a = "string" != typeof e && w(e);
            if (!_.test(e))
                for (; n < r; n++)
                    for (i = this[n]; i && i !== t; i = i.parentNode)
                        if (i.nodeType < 11 && (a ? a.index(i) > -1 : 1 === i.nodeType && w.find.matchesSelector(i, e))) {
                            s.push(i);
                            break
                        } return this.pushStack(s.length > 1 ? w.uniqueSort(s) : s)
        },
        index: function (e) {
            return e ? "string" == typeof e ? l.call(w(e), this[0]) : l.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function (e, t) {
            return this.pushStack(w.uniqueSort(w.merge(this.get(), w(e, t))))
        },
        addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), w.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function (e) {
            return E(e, "parentNode")
        },
        parentsUntil: function (e, t, i) {
            return E(e, "parentNode", i)
        },
        next: function (e) {
            return O(e, "nextSibling")
        },
        prev: function (e) {
            return O(e, "previousSibling")
        },
        nextAll: function (e) {
            return E(e, "nextSibling")
        },
        prevAll: function (e) {
            return E(e, "previousSibling")
        },
        nextUntil: function (e, t, i) {
            return E(e, "nextSibling", i)
        },
        prevUntil: function (e, t, i) {
            return E(e, "previousSibling", i)
        },
        siblings: function (e) {
            return S((e.parentNode || {}).firstChild, e)
        },
        children: function (e) {
            return S(e.firstChild)
        },
        contents: function (e) {
            return k(e, "iframe") ? e.contentDocument : (k(e, "template") && (e = e.content || e), w.merge([], e.childNodes))
        }
    }, (function (e, t) {
        w.fn[e] = function (i, n) {
            var r = w.map(this, t, i);
            return "Until" !== e.slice(-5) && (n = i), n && "string" == typeof n && (r = w.filter(n, r)), this.length > 1 && (P[e] || w.uniqueSort(r), M.test(e) && r.reverse()), this.pushStack(r)
        }
    }));
    var N = /[^\x20\t\r\n\f]+/g;

    function L(e) {
        return e
    }

    function z(e) {
        throw e
    }

    function j(e, t, i, n) {
        var r;
        try {
            e && g(r = e.promise) ? r.call(e).done(t).fail(i) : e && g(r = e.then) ? r.call(e, t, i) : t.apply(void 0, [e].slice(n))
        } catch (e) {
            i.apply(void 0, [e])
        }
    }
    w.Callbacks = function (e) {
        e = "string" == typeof e ? function (e) {
            var t = {};
            return w.each(e.match(N) || [], (function (e, i) {
                t[i] = !0
            })), t
        }(e) : w.extend({}, e);
        var t, i, n, r, s = [],
            a = [],
            o = -1,
            l = function () {
                for (r = r || e.once, n = t = !0; a.length; o = -1)
                    for (i = a.shift(); ++o < s.length;) !1 === s[o].apply(i[0], i[1]) && e.stopOnFalse && (o = s.length, i = !1);
                e.memory || (i = !1), t = !1, r && (s = i ? [] : "")
            },
            d = {
                add: function () {
                    return s && (i && !t && (o = s.length - 1, a.push(i)), function t(i) {
                        w.each(i, (function (i, n) {
                            g(n) ? e.unique && d.has(n) || s.push(n) : n && n.length && "string" !== b(n) && t(n)
                        }))
                    }(arguments), i && !t && l()), this
                },
                remove: function () {
                    return w.each(arguments, (function (e, t) {
                        for (var i;
                            (i = w.inArray(t, s, i)) > -1;) s.splice(i, 1), i <= o && o--
                    })), this
                },
                has: function (e) {
                    return e ? w.inArray(e, s) > -1 : s.length > 0
                },
                empty: function () {
                    return s && (s = []), this
                },
                disable: function () {
                    return r = a = [], s = i = "", this
                },
                disabled: function () {
                    return !s
                },
                lock: function () {
                    return r = a = [], i || t || (s = i = ""), this
                },
                locked: function () {
                    return !!r
                },
                fireWith: function (e, i) {
                    return r || (i = [e, (i = i || []).slice ? i.slice() : i], a.push(i), t || l()), this
                },
                fire: function () {
                    return d.fireWith(this, arguments), this
                },
                fired: function () {
                    return !!n
                }
            };
        return d
    }, w.extend({
        Deferred: function (t) {
            var i = [
                    ["notify", "progress", w.Callbacks("memory"), w.Callbacks("memory"), 2],
                    ["resolve", "done", w.Callbacks("once memory"), w.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", w.Callbacks("once memory"), w.Callbacks("once memory"), 1, "rejected"]
                ],
                n = "pending",
                r = {
                    state: function () {
                        return n
                    },
                    always: function () {
                        return s.done(arguments).fail(arguments), this
                    },
                    catch: function (e) {
                        return r.then(null, e)
                    },
                    pipe: function () {
                        var e = arguments;
                        return w.Deferred((function (t) {
                            w.each(i, (function (i, n) {
                                var r = g(e[n[4]]) && e[n[4]];
                                s[n[1]]((function () {
                                    var e = r && r.apply(this, arguments);
                                    e && g(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[n[0] + "With"](this, r ? [e] : arguments)
                                }))
                            })), e = null
                        })).promise()
                    },
                    then: function (t, n, r) {
                        var s = 0;

                        function a(t, i, n, r) {
                            return function () {
                                var o = this,
                                    l = arguments,
                                    d = function () {
                                        var e, d;
                                        if (!(t < s)) {
                                            if ((e = n.apply(o, l)) === i.promise()) throw new TypeError("Thenable self-resolution");
                                            d = e && ("object" == typeof e || "function" == typeof e) && e.then, g(d) ? r ? d.call(e, a(s, i, L, r), a(s, i, z, r)) : (s++, d.call(e, a(s, i, L, r), a(s, i, z, r), a(s, i, L, i.notifyWith))) : (n !== L && (o = void 0, l = [e]), (r || i.resolveWith)(o, l))
                                        }
                                    },
                                    c = r ? d : function () {
                                        try {
                                            d()
                                        } catch (e) {
                                            w.Deferred.exceptionHook && w.Deferred.exceptionHook(e, c.stackTrace), t + 1 >= s && (n !== z && (o = void 0, l = [e]), i.rejectWith(o, l))
                                        }
                                    };
                                t ? c() : (w.Deferred.getStackHook && (c.stackTrace = w.Deferred.getStackHook()), e.setTimeout(c))
                            }
                        }
                        return w.Deferred((function (e) {
                            i[0][3].add(a(0, e, g(r) ? r : L, e.notifyWith)), i[1][3].add(a(0, e, g(t) ? t : L)), i[2][3].add(a(0, e, g(n) ? n : z))
                        })).promise()
                    },
                    promise: function (e) {
                        return null != e ? w.extend(e, r) : r
                    }
                },
                s = {};
            return w.each(i, (function (e, t) {
                var a = t[2],
                    o = t[5];
                r[t[1]] = a.add, o && a.add((function () {
                    n = o
                }), i[3 - e][2].disable, i[3 - e][3].disable, i[0][2].lock, i[0][3].lock), a.add(t[3].fire), s[t[0]] = function () {
                    return s[t[0] + "With"](this === s ? void 0 : this, arguments), this
                }, s[t[0] + "With"] = a.fireWith
            })), r.promise(s), t && t.call(s, s), s
        },
        when: function (e) {
            var t = arguments.length,
                i = t,
                n = Array(i),
                r = s.call(arguments),
                a = w.Deferred(),
                o = function (e) {
                    return function (i) {
                        n[e] = this, r[e] = arguments.length > 1 ? s.call(arguments) : i, --t || a.resolveWith(n, r)
                    }
                };
            if (t <= 1 && (j(e, a.done(o(i)).resolve, a.reject, !t), "pending" === a.state() || g(r[i] && r[i].then))) return a.then();
            for (; i--;) j(r[i], o(i), a.reject);
            return a.promise()
        }
    });
    var H = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    w.Deferred.exceptionHook = function (t, i) {
        e.console && e.console.warn && t && H.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, i)
    }, w.readyException = function (t) {
        e.setTimeout((function () {
            throw t
        }))
    };
    var q = w.Deferred();

    function R() {
        n.removeEventListener("DOMContentLoaded", R), e.removeEventListener("load", R), w.ready()
    }
    w.fn.ready = function (e) {
        return q.then(e).catch((function (e) {
            w.readyException(e)
        })), this
    }, w.extend({
        isReady: !1,
        readyWait: 1,
        ready: function (e) {
            (!0 === e ? --w.readyWait : w.isReady) || (w.isReady = !0, !0 !== e && --w.readyWait > 0 || q.resolveWith(n, [w]))
        }
    }), w.ready.then = q.then, "complete" === n.readyState || "loading" !== n.readyState && !n.documentElement.doScroll ? e.setTimeout(w.ready) : (n.addEventListener("DOMContentLoaded", R), e.addEventListener("load", R));
    var B = function (e, t, i, n, r, s, a) {
            var o = 0,
                l = e.length,
                d = null == i;
            if ("object" === b(i))
                for (o in r = !0, i) B(e, t, o, i[o], !0, s, a);
            else if (void 0 !== n && (r = !0, g(n) || (a = !0), d && (a ? (t.call(e, n), t = null) : (d = t, t = function (e, t, i) {
                    return d.call(w(e), i)
                })), t))
                for (; o < l; o++) t(e[o], i, a ? n : n.call(e[o], o, t(e[o], i)));
            return r ? e : d ? t.call(e) : l ? t(e[0], i) : s
        },
        W = /^-ms-/,
        F = /-([a-z])/g;

    function G(e, t) {
        return t.toUpperCase()
    }

    function X(e) {
        return e.replace(W, "ms-").replace(F, G)
    }
    var V = function (e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    };

    function Y() {
        this.expando = w.expando + Y.uid++
    }
    Y.uid = 1, Y.prototype = {
        cache: function (e) {
            var t = e[this.expando];
            return t || (t = {}, V(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function (e, t, i) {
            var n, r = this.cache(e);
            if ("string" == typeof t) r[X(t)] = i;
            else
                for (n in t) r[X(n)] = t[n];
            return r
        },
        get: function (e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][X(t)]
        },
        access: function (e, t, i) {
            return void 0 === t || t && "string" == typeof t && void 0 === i ? this.get(e, t) : (this.set(e, t, i), void 0 !== i ? i : t)
        },
        remove: function (e, t) {
            var i, n = e[this.expando];
            if (void 0 !== n) {
                if (void 0 !== t) {
                    i = (t = Array.isArray(t) ? t.map(X) : (t = X(t)) in n ? [t] : t.match(N) || []).length;
                    for (; i--;) delete n[t[i]]
                }(void 0 === t || w.isEmptyObject(n)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        },
        hasData: function (e) {
            var t = e[this.expando];
            return void 0 !== t && !w.isEmptyObject(t)
        }
    };
    var U = new Y,
        K = new Y,
        Q = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Z = /[A-Z]/g;

    function J(e, t, i) {
        var n;
        if (void 0 === i && 1 === e.nodeType)
            if (n = "data-" + t.replace(Z, "-$&").toLowerCase(), "string" == typeof (i = e.getAttribute(n))) {
                try {
                    i = function (e) {
                        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Q.test(e) ? JSON.parse(e) : e)
                    }(i)
                } catch (e) {}
                K.set(e, t, i)
            } else i = void 0;
        return i
    }
    w.extend({
        hasData: function (e) {
            return K.hasData(e) || U.hasData(e)
        },
        data: function (e, t, i) {
            return K.access(e, t, i)
        },
        removeData: function (e, t) {
            K.remove(e, t)
        },
        _data: function (e, t, i) {
            return U.access(e, t, i)
        },
        _removeData: function (e, t) {
            U.remove(e, t)
        }
    }), w.fn.extend({
        data: function (e, t) {
            var i, n, r, s = this[0],
                a = s && s.attributes;
            if (void 0 === e) {
                if (this.length && (r = K.get(s), 1 === s.nodeType && !U.get(s, "hasDataAttrs"))) {
                    for (i = a.length; i--;) a[i] && 0 === (n = a[i].name).indexOf("data-") && (n = X(n.slice(5)), J(s, n, r[n]));
                    U.set(s, "hasDataAttrs", !0)
                }
                return r
            }
            return "object" == typeof e ? this.each((function () {
                K.set(this, e)
            })) : B(this, (function (t) {
                var i;
                if (s && void 0 === t) {
                    if (void 0 !== (i = K.get(s, e))) return i;
                    if (void 0 !== (i = J(s, e))) return i
                } else this.each((function () {
                    K.set(this, e, t)
                }))
            }), null, t, arguments.length > 1, null, !0)
        },
        removeData: function (e) {
            return this.each((function () {
                K.remove(this, e)
            }))
        }
    }), w.extend({
        queue: function (e, t, i) {
            var n;
            if (e) return t = (t || "fx") + "queue", n = U.get(e, t), i && (!n || Array.isArray(i) ? n = U.access(e, t, w.makeArray(i)) : n.push(i)), n || []
        },
        dequeue: function (e, t) {
            t = t || "fx";
            var i = w.queue(e, t),
                n = i.length,
                r = i.shift(),
                s = w._queueHooks(e, t);
            "inprogress" === r && (r = i.shift(), n--), r && ("fx" === t && i.unshift("inprogress"), delete s.stop, r.call(e, (function () {
                w.dequeue(e, t)
            }), s)), !n && s && s.empty.fire()
        },
        _queueHooks: function (e, t) {
            var i = t + "queueHooks";
            return U.get(e, i) || U.access(e, i, {
                empty: w.Callbacks("once memory").add((function () {
                    U.remove(e, [t + "queue", i])
                }))
            })
        }
    }), w.fn.extend({
        queue: function (e, t) {
            var i = 2;
            return "string" != typeof e && (t = e, e = "fx", i--), arguments.length < i ? w.queue(this[0], e) : void 0 === t ? this : this.each((function () {
                var i = w.queue(this, e, t);
                w._queueHooks(this, e), "fx" === e && "inprogress" !== i[0] && w.dequeue(this, e)
            }))
        },
        dequeue: function (e) {
            return this.each((function () {
                w.dequeue(this, e)
            }))
        },
        clearQueue: function (e) {
            return this.queue(e || "fx", [])
        },
        promise: function (e, t) {
            var i, n = 1,
                r = w.Deferred(),
                s = this,
                a = this.length,
                o = function () {
                    --n || r.resolveWith(s, [s])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;)(i = U.get(s[a], e + "queueHooks")) && i.empty && (n++, i.empty.add(o));
            return o(), r.promise(t)
        }
    });
    var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
        ie = ["Top", "Right", "Bottom", "Left"],
        ne = function (e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && w.contains(e.ownerDocument, e) && "none" === w.css(e, "display")
        },
        re = function (e, t, i, n) {
            var r, s, a = {};
            for (s in t) a[s] = e.style[s], e.style[s] = t[s];
            for (s in r = i.apply(e, n || []), t) e.style[s] = a[s];
            return r
        };

    function se(e, t, i, n) {
        var r, s, a = 20,
            o = n ? function () {
                return n.cur()
            } : function () {
                return w.css(e, t, "")
            },
            l = o(),
            d = i && i[3] || (w.cssNumber[t] ? "" : "px"),
            c = (w.cssNumber[t] || "px" !== d && +l) && te.exec(w.css(e, t));
        if (c && c[3] !== d) {
            for (l /= 2, d = d || c[3], c = +l || 1; a--;) w.style(e, t, c + d), (1 - s) * (1 - (s = o() / l || .5)) <= 0 && (a = 0), c /= s;
            c *= 2, w.style(e, t, c + d), i = i || []
        }
        return i && (c = +c || +l || 0, r = i[1] ? c + (i[1] + 1) * i[2] : +i[2], n && (n.unit = d, n.start = c, n.end = r)), r
    }
    var ae = {};

    function oe(e) {
        var t, i = e.ownerDocument,
            n = e.nodeName,
            r = ae[n];
        return r || (t = i.body.appendChild(i.createElement(n)), r = w.css(t, "display"), t.parentNode.removeChild(t), "none" === r && (r = "block"), ae[n] = r, r)
    }

    function le(e, t) {
        for (var i, n, r = [], s = 0, a = e.length; s < a; s++)(n = e[s]).style && (i = n.style.display, t ? ("none" === i && (r[s] = U.get(n, "display") || null, r[s] || (n.style.display = "")), "" === n.style.display && ne(n) && (r[s] = oe(n))) : "none" !== i && (r[s] = "none", U.set(n, "display", i)));
        for (s = 0; s < a; s++) null != r[s] && (e[s].style.display = r[s]);
        return e
    }
    w.fn.extend({
        show: function () {
            return le(this, !0)
        },
        hide: function () {
            return le(this)
        },
        toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each((function () {
                ne(this) ? w(this).show() : w(this).hide()
            }))
        }
    });
    var de = /^(?:checkbox|radio)$/i,
        ce = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        ue = /^$|^module$|\/(?:java|ecma)script/i,
        he = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };

    function pe(e, t) {
        var i;
        return i = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && k(e, t) ? w.merge([e], i) : i
    }

    function fe(e, t) {
        for (var i = 0, n = e.length; i < n; i++) U.set(e[i], "globalEval", !t || U.get(t[i], "globalEval"))
    }
    he.optgroup = he.option, he.tbody = he.tfoot = he.colgroup = he.caption = he.thead, he.th = he.td;
    var ge = /<|&#?\w+;/;

    function me(e, t, i, n, r) {
        for (var s, a, o, l, d, c, u = t.createDocumentFragment(), h = [], p = 0, f = e.length; p < f; p++)
            if ((s = e[p]) || 0 === s)
                if ("object" === b(s)) w.merge(h, s.nodeType ? [s] : s);
                else if (ge.test(s)) {
            for (a = a || u.appendChild(t.createElement("div")), o = (ce.exec(s) || ["", ""])[1].toLowerCase(), l = he[o] || he._default, a.innerHTML = l[1] + w.htmlPrefilter(s) + l[2], c = l[0]; c--;) a = a.lastChild;
            w.merge(h, a.childNodes), (a = u.firstChild).textContent = ""
        } else h.push(t.createTextNode(s));
        for (u.textContent = "", p = 0; s = h[p++];)
            if (n && w.inArray(s, n) > -1) r && r.push(s);
            else if (d = w.contains(s.ownerDocument, s), a = pe(u.appendChild(s), "script"), d && fe(a), i)
            for (c = 0; s = a[c++];) ue.test(s.type || "") && i.push(s);
        return u
    }! function () {
        var e = n.createDocumentFragment().appendChild(n.createElement("div")),
            t = n.createElement("input");
        t.setAttribute("type", "radio"), t.setAttribute("checked", "checked"), t.setAttribute("name", "t"), e.appendChild(t), f.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", f.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var ve = n.documentElement,
        ye = /^key/,
        be = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        we = /^([^.]*)(?:\.(.+)|)/;

    function xe() {
        return !0
    }

    function Ce() {
        return !1
    }

    function Te() {
        try {
            return n.activeElement
        } catch (e) {}
    }

    function Ee(e, t, i, n, r, s) {
        var a, o;
        if ("object" == typeof t) {
            for (o in "string" != typeof i && (n = n || i, i = void 0), t) Ee(e, o, i, n, t[o], s);
            return e
        }
        if (null == n && null == r ? (r = i, n = i = void 0) : null == r && ("string" == typeof i ? (r = n, n = void 0) : (r = n, n = i, i = void 0)), !1 === r) r = Ce;
        else if (!r) return e;
        return 1 === s && (a = r, (r = function (e) {
            return w().off(e), a.apply(this, arguments)
        }).guid = a.guid || (a.guid = w.guid++)), e.each((function () {
            w.event.add(this, t, r, n, i)
        }))
    }
    w.event = {
        global: {},
        add: function (e, t, i, n, r) {
            var s, a, o, l, d, c, u, h, p, f, g, m = U.get(e);
            if (m)
                for (i.handler && (i = (s = i).handler, r = s.selector), r && w.find.matchesSelector(ve, r), i.guid || (i.guid = w.guid++), (l = m.events) || (l = m.events = {}), (a = m.handle) || (a = m.handle = function (t) {
                        return void 0 !== w && w.event.triggered !== t.type ? w.event.dispatch.apply(e, arguments) : void 0
                    }), d = (t = (t || "").match(N) || [""]).length; d--;) p = g = (o = we.exec(t[d]) || [])[1], f = (o[2] || "").split(".").sort(), p && (u = w.event.special[p] || {}, p = (r ? u.delegateType : u.bindType) || p, u = w.event.special[p] || {}, c = w.extend({
                    type: p,
                    origType: g,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: r,
                    needsContext: r && w.expr.match.needsContext.test(r),
                    namespace: f.join(".")
                }, s), (h = l[p]) || ((h = l[p] = []).delegateCount = 0, u.setup && !1 !== u.setup.call(e, n, f, a) || e.addEventListener && e.addEventListener(p, a)), u.add && (u.add.call(e, c), c.handler.guid || (c.handler.guid = i.guid)), r ? h.splice(h.delegateCount++, 0, c) : h.push(c), w.event.global[p] = !0)
        },
        remove: function (e, t, i, n, r) {
            var s, a, o, l, d, c, u, h, p, f, g, m = U.hasData(e) && U.get(e);
            if (m && (l = m.events)) {
                for (d = (t = (t || "").match(N) || [""]).length; d--;)
                    if (p = g = (o = we.exec(t[d]) || [])[1], f = (o[2] || "").split(".").sort(), p) {
                        for (u = w.event.special[p] || {}, h = l[p = (n ? u.delegateType : u.bindType) || p] || [], o = o[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = s = h.length; s--;) c = h[s], !r && g !== c.origType || i && i.guid !== c.guid || o && !o.test(c.namespace) || n && n !== c.selector && ("**" !== n || !c.selector) || (h.splice(s, 1), c.selector && h.delegateCount--, u.remove && u.remove.call(e, c));
                        a && !h.length && (u.teardown && !1 !== u.teardown.call(e, f, m.handle) || w.removeEvent(e, p, m.handle), delete l[p])
                    } else
                        for (p in l) w.event.remove(e, p + t[d], i, n, !0);
                w.isEmptyObject(l) && U.remove(e, "handle events")
            }
        },
        dispatch: function (e) {
            var t, i, n, r, s, a, o = w.event.fix(e),
                l = new Array(arguments.length),
                d = (U.get(this, "events") || {})[o.type] || [],
                c = w.event.special[o.type] || {};
            for (l[0] = o, t = 1; t < arguments.length; t++) l[t] = arguments[t];
            if (o.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, o)) {
                for (a = w.event.handlers.call(this, o, d), t = 0;
                    (r = a[t++]) && !o.isPropagationStopped();)
                    for (o.currentTarget = r.elem, i = 0;
                        (s = r.handlers[i++]) && !o.isImmediatePropagationStopped();) o.rnamespace && !o.rnamespace.test(s.namespace) || (o.handleObj = s, o.data = s.data, void 0 !== (n = ((w.event.special[s.origType] || {}).handle || s.handler).apply(r.elem, l)) && !1 === (o.result = n) && (o.preventDefault(), o.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, o), o.result
            }
        },
        handlers: function (e, t) {
            var i, n, r, s, a, o = [],
                l = t.delegateCount,
                d = e.target;
            if (l && d.nodeType && !("click" === e.type && e.button >= 1))
                for (; d !== this; d = d.parentNode || this)
                    if (1 === d.nodeType && ("click" !== e.type || !0 !== d.disabled)) {
                        for (s = [], a = {}, i = 0; i < l; i++) void 0 === a[r = (n = t[i]).selector + " "] && (a[r] = n.needsContext ? w(r, this).index(d) > -1 : w.find(r, this, null, [d]).length), a[r] && s.push(n);
                        s.length && o.push({
                            elem: d,
                            handlers: s
                        })
                    } return d = this, l < t.length && o.push({
                elem: d,
                handlers: t.slice(l)
            }), o
        },
        addProp: function (e, t) {
            Object.defineProperty(w.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: g(t) ? function () {
                    if (this.originalEvent) return t(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[e]
                },
                set: function (t) {
                    Object.defineProperty(this, e, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: t
                    })
                }
            })
        },
        fix: function (e) {
            return e[w.expando] ? e : new w.Event(e)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function () {
                    if (this !== Te() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function () {
                    if (this === Te() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && k(this, "input")) return this.click(), !1
                },
                _default: function (e) {
                    return k(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, w.removeEvent = function (e, t, i) {
        e.removeEventListener && e.removeEventListener(t, i)
    }, w.Event = function (e, t) {
        if (!(this instanceof w.Event)) return new w.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? xe : Ce, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && w.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[w.expando] = !0
    }, w.Event.prototype = {
        constructor: w.Event,
        isDefaultPrevented: Ce,
        isPropagationStopped: Ce,
        isImmediatePropagationStopped: Ce,
        isSimulated: !1,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = xe, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = xe, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = xe, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, w.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (e) {
            var t = e.button;
            return null == e.which && ye.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && be.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, w.event.addProp), w.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, (function (e, t) {
        w.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function (e) {
                var i, n = this,
                    r = e.relatedTarget,
                    s = e.handleObj;
                return r && (r === n || w.contains(n, r)) || (e.type = s.origType, i = s.handler.apply(this, arguments), e.type = t), i
            }
        }
    })), w.fn.extend({
        on: function (e, t, i, n) {
            return Ee(this, e, t, i, n)
        },
        one: function (e, t, i, n) {
            return Ee(this, e, t, i, n, 1)
        },
        off: function (e, t, i) {
            var n, r;
            if (e && e.preventDefault && e.handleObj) return n = e.handleObj, w(e.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" == typeof e) {
                for (r in e) this.off(r, t, e[r]);
                return this
            }
            return !1 !== t && "function" != typeof t || (i = t, t = void 0), !1 === i && (i = Ce), this.each((function () {
                w.event.remove(this, e, i, t)
            }))
        }
    });
    var Se = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        _e = /<script|<style|<link/i,
        ke = /checked\s*(?:[^=]|=\s*.checked.)/i,
        $e = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Ie(e, t) {
        return k(e, "table") && k(11 !== t.nodeType ? t : t.firstChild, "tr") && w(e).children("tbody")[0] || e
    }

    function De(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function Ae(e) {
        return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
    }

    function Me(e, t) {
        var i, n, r, s, a, o, l, d;
        if (1 === t.nodeType) {
            if (U.hasData(e) && (s = U.access(e), a = U.set(t, s), d = s.events))
                for (r in delete a.handle, a.events = {}, d)
                    for (i = 0, n = d[r].length; i < n; i++) w.event.add(t, r, d[r][i]);
            K.hasData(e) && (o = K.access(e), l = w.extend({}, o), K.set(t, l))
        }
    }

    function Pe(e, t) {
        var i = t.nodeName.toLowerCase();
        "input" === i && de.test(e.type) ? t.checked = e.checked : "input" !== i && "textarea" !== i || (t.defaultValue = e.defaultValue)
    }

    function Oe(e, t, i, n) {
        t = a.apply([], t);
        var r, s, o, l, d, c, u = 0,
            h = e.length,
            p = h - 1,
            m = t[0],
            v = g(m);
        if (v || h > 1 && "string" == typeof m && !f.checkClone && ke.test(m)) return e.each((function (r) {
            var s = e.eq(r);
            v && (t[0] = m.call(this, r, s.html())), Oe(s, t, i, n)
        }));
        if (h && (s = (r = me(t, e[0].ownerDocument, !1, e, n)).firstChild, 1 === r.childNodes.length && (r = s), s || n)) {
            for (l = (o = w.map(pe(r, "script"), De)).length; u < h; u++) d = r, u !== p && (d = w.clone(d, !0, !0), l && w.merge(o, pe(d, "script"))), i.call(e[u], d, u);
            if (l)
                for (c = o[o.length - 1].ownerDocument, w.map(o, Ae), u = 0; u < l; u++) d = o[u], ue.test(d.type || "") && !U.access(d, "globalEval") && w.contains(c, d) && (d.src && "module" !== (d.type || "").toLowerCase() ? w._evalUrl && w._evalUrl(d.src) : y(d.textContent.replace($e, ""), c, d))
        }
        return e
    }

    function Ne(e, t, i) {
        for (var n, r = t ? w.filter(t, e) : e, s = 0; null != (n = r[s]); s++) i || 1 !== n.nodeType || w.cleanData(pe(n)), n.parentNode && (i && w.contains(n.ownerDocument, n) && fe(pe(n, "script")), n.parentNode.removeChild(n));
        return e
    }
    w.extend({
        htmlPrefilter: function (e) {
            return e.replace(Se, "<$1></$2>")
        },
        clone: function (e, t, i) {
            var n, r, s, a, o = e.cloneNode(!0),
                l = w.contains(e.ownerDocument, e);
            if (!(f.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || w.isXMLDoc(e)))
                for (a = pe(o), n = 0, r = (s = pe(e)).length; n < r; n++) Pe(s[n], a[n]);
            if (t)
                if (i)
                    for (s = s || pe(e), a = a || pe(o), n = 0, r = s.length; n < r; n++) Me(s[n], a[n]);
                else Me(e, o);
            return (a = pe(o, "script")).length > 0 && fe(a, !l && pe(e, "script")), o
        },
        cleanData: function (e) {
            for (var t, i, n, r = w.event.special, s = 0; void 0 !== (i = e[s]); s++)
                if (V(i)) {
                    if (t = i[U.expando]) {
                        if (t.events)
                            for (n in t.events) r[n] ? w.event.remove(i, n) : w.removeEvent(i, n, t.handle);
                        i[U.expando] = void 0
                    }
                    i[K.expando] && (i[K.expando] = void 0)
                }
        }
    }), w.fn.extend({
        detach: function (e) {
            return Ne(this, e, !0)
        },
        remove: function (e) {
            return Ne(this, e)
        },
        text: function (e) {
            return B(this, (function (e) {
                return void 0 === e ? w.text(this) : this.empty().each((function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                }))
            }), null, e, arguments.length)
        },
        append: function () {
            return Oe(this, arguments, (function (e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Ie(this, e).appendChild(e)
            }))
        },
        prepend: function () {
            return Oe(this, arguments, (function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = Ie(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            }))
        },
        before: function () {
            return Oe(this, arguments, (function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            }))
        },
        after: function () {
            return Oe(this, arguments, (function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            }))
        },
        empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (w.cleanData(pe(e, !1)), e.textContent = "");
            return this
        },
        clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map((function () {
                return w.clone(this, e, t)
            }))
        },
        html: function (e) {
            return B(this, (function (e) {
                var t = this[0] || {},
                    i = 0,
                    n = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !_e.test(e) && !he[(ce.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = w.htmlPrefilter(e);
                    try {
                        for (; i < n; i++) 1 === (t = this[i] || {}).nodeType && (w.cleanData(pe(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {}
                }
                t && this.empty().append(e)
            }), null, e, arguments.length)
        },
        replaceWith: function () {
            var e = [];
            return Oe(this, arguments, (function (t) {
                var i = this.parentNode;
                w.inArray(this, e) < 0 && (w.cleanData(pe(this)), i && i.replaceChild(t, this))
            }), e)
        }
    }), w.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, (function (e, t) {
        w.fn[e] = function (e) {
            for (var i, n = [], r = w(e), s = r.length - 1, a = 0; a <= s; a++) i = a === s ? this : this.clone(!0), w(r[a])[t](i), o.apply(n, i.get());
            return this.pushStack(n)
        }
    }));
    var Le = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
        ze = function (t) {
            var i = t.ownerDocument.defaultView;
            return i && i.opener || (i = e), i.getComputedStyle(t)
        },
        je = new RegExp(ie.join("|"), "i");

    function He(e, t, i) {
        var n, r, s, a, o = e.style;
        return (i = i || ze(e)) && ("" !== (a = i.getPropertyValue(t) || i[t]) || w.contains(e.ownerDocument, e) || (a = w.style(e, t)), !f.pixelBoxStyles() && Le.test(a) && je.test(t) && (n = o.width, r = o.minWidth, s = o.maxWidth, o.minWidth = o.maxWidth = o.width = a, a = i.width, o.width = n, o.minWidth = r, o.maxWidth = s)), void 0 !== a ? a + "" : a
    }

    function qe(e, t) {
        return {
            get: function () {
                if (!e()) return (this.get = t).apply(this, arguments);
                delete this.get
            }
        }
    }! function () {
        function t() {
            if (c) {
                d.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", c.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", ve.appendChild(d).appendChild(c);
                var t = e.getComputedStyle(c);
                r = "1%" !== t.top, l = 12 === i(t.marginLeft), c.style.right = "60%", o = 36 === i(t.right), s = 36 === i(t.width), c.style.position = "absolute", a = 36 === c.offsetWidth || "absolute", ve.removeChild(d), c = null
            }
        }

        function i(e) {
            return Math.round(parseFloat(e))
        }
        var r, s, a, o, l, d = n.createElement("div"),
            c = n.createElement("div");
        c.style && (c.style.backgroundClip = "content-box", c.cloneNode(!0).style.backgroundClip = "", f.clearCloneStyle = "content-box" === c.style.backgroundClip, w.extend(f, {
            boxSizingReliable: function () {
                return t(), s
            },
            pixelBoxStyles: function () {
                return t(), o
            },
            pixelPosition: function () {
                return t(), r
            },
            reliableMarginLeft: function () {
                return t(), l
            },
            scrollboxSize: function () {
                return t(), a
            }
        }))
    }();
    var Re = /^(none|table(?!-c[ea]).+)/,
        Be = /^--/,
        We = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Fe = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        Ge = ["Webkit", "Moz", "ms"],
        Xe = n.createElement("div").style;

    function Ve(e) {
        var t = w.cssProps[e];
        return t || (t = w.cssProps[e] = function (e) {
            if (e in Xe) return e;
            for (var t = e[0].toUpperCase() + e.slice(1), i = Ge.length; i--;)
                if ((e = Ge[i] + t) in Xe) return e
        }(e) || e), t
    }

    function Ye(e, t, i) {
        var n = te.exec(t);
        return n ? Math.max(0, n[2] - (i || 0)) + (n[3] || "px") : t
    }

    function Ue(e, t, i, n, r, s) {
        var a = "width" === t ? 1 : 0,
            o = 0,
            l = 0;
        if (i === (n ? "border" : "content")) return 0;
        for (; a < 4; a += 2) "margin" === i && (l += w.css(e, i + ie[a], !0, r)), n ? ("content" === i && (l -= w.css(e, "padding" + ie[a], !0, r)), "margin" !== i && (l -= w.css(e, "border" + ie[a] + "Width", !0, r))) : (l += w.css(e, "padding" + ie[a], !0, r), "padding" !== i ? l += w.css(e, "border" + ie[a] + "Width", !0, r) : o += w.css(e, "border" + ie[a] + "Width", !0, r));
        return !n && s >= 0 && (l += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - s - l - o - .5))), l
    }

    function Ke(e, t, i) {
        var n = ze(e),
            r = He(e, t, n),
            s = "border-box" === w.css(e, "boxSizing", !1, n),
            a = s;
        if (Le.test(r)) {
            if (!i) return r;
            r = "auto"
        }
        return a = a && (f.boxSizingReliable() || r === e.style[t]), ("auto" === r || !parseFloat(r) && "inline" === w.css(e, "display", !1, n)) && (r = e["offset" + t[0].toUpperCase() + t.slice(1)], a = !0), (r = parseFloat(r) || 0) + Ue(e, t, i || (s ? "border" : "content"), a, n, r) + "px"
    }

    function Qe(e, t, i, n, r) {
        return new Qe.prototype.init(e, t, i, n, r)
    }
    w.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var i = He(e, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function (e, t, i, n) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var r, s, a, o = X(t),
                    l = Be.test(t),
                    d = e.style;
                if (l || (t = Ve(o)), a = w.cssHooks[t] || w.cssHooks[o], void 0 === i) return a && "get" in a && void 0 !== (r = a.get(e, !1, n)) ? r : d[t];
                "string" == (s = typeof i) && (r = te.exec(i)) && r[1] && (i = se(e, t, r), s = "number"), null != i && i == i && ("number" === s && (i += r && r[3] || (w.cssNumber[o] ? "" : "px")), f.clearCloneStyle || "" !== i || 0 !== t.indexOf("background") || (d[t] = "inherit"), a && "set" in a && void 0 === (i = a.set(e, i, n)) || (l ? d.setProperty(t, i) : d[t] = i))
            }
        },
        css: function (e, t, i, n) {
            var r, s, a, o = X(t);
            return Be.test(t) || (t = Ve(o)), (a = w.cssHooks[t] || w.cssHooks[o]) && "get" in a && (r = a.get(e, !0, i)), void 0 === r && (r = He(e, t, n)), "normal" === r && t in Fe && (r = Fe[t]), "" === i || i ? (s = parseFloat(r), !0 === i || isFinite(s) ? s || 0 : r) : r
        }
    }), w.each(["height", "width"], (function (e, t) {
        w.cssHooks[t] = {
            get: function (e, i, n) {
                if (i) return !Re.test(w.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Ke(e, t, n) : re(e, We, (function () {
                    return Ke(e, t, n)
                }))
            },
            set: function (e, i, n) {
                var r, s = ze(e),
                    a = "border-box" === w.css(e, "boxSizing", !1, s),
                    o = n && Ue(e, t, n, a, s);
                return a && f.scrollboxSize() === s.position && (o -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(s[t]) - Ue(e, t, "border", !1, s) - .5)), o && (r = te.exec(i)) && "px" !== (r[3] || "px") && (e.style[t] = i, i = w.css(e, t)), Ye(0, i, o)
            }
        }
    })), w.cssHooks.marginLeft = qe(f.reliableMarginLeft, (function (e, t) {
        if (t) return (parseFloat(He(e, "marginLeft")) || e.getBoundingClientRect().left - re(e, {
            marginLeft: 0
        }, (function () {
            return e.getBoundingClientRect().left
        }))) + "px"
    })), w.each({
        margin: "",
        padding: "",
        border: "Width"
    }, (function (e, t) {
        w.cssHooks[e + t] = {
            expand: function (i) {
                for (var n = 0, r = {}, s = "string" == typeof i ? i.split(" ") : [i]; n < 4; n++) r[e + ie[n] + t] = s[n] || s[n - 2] || s[0];
                return r
            }
        }, "margin" !== e && (w.cssHooks[e + t].set = Ye)
    })), w.fn.extend({
        css: function (e, t) {
            return B(this, (function (e, t, i) {
                var n, r, s = {},
                    a = 0;
                if (Array.isArray(t)) {
                    for (n = ze(e), r = t.length; a < r; a++) s[t[a]] = w.css(e, t[a], !1, n);
                    return s
                }
                return void 0 !== i ? w.style(e, t, i) : w.css(e, t)
            }), e, t, arguments.length > 1)
        }
    }), w.Tween = Qe, Qe.prototype = {
        constructor: Qe,
        init: function (e, t, i, n, r, s) {
            this.elem = e, this.prop = i, this.easing = r || w.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = n, this.unit = s || (w.cssNumber[i] ? "" : "px")
        },
        cur: function () {
            var e = Qe.propHooks[this.prop];
            return e && e.get ? e.get(this) : Qe.propHooks._default.get(this)
        },
        run: function (e) {
            var t, i = Qe.propHooks[this.prop];
            return this.options.duration ? this.pos = t = w.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : Qe.propHooks._default.set(this), this
        }
    }, Qe.prototype.init.prototype = Qe.prototype, Qe.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = w.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            },
            set: function (e) {
                w.fx.step[e.prop] ? w.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[w.cssProps[e.prop]] && !w.cssHooks[e.prop] ? e.elem[e.prop] = e.now : w.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, Qe.propHooks.scrollTop = Qe.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, w.easing = {
        linear: function (e) {
            return e
        },
        swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, w.fx = Qe.prototype.init, w.fx.step = {};
    var Ze, Je, et = /^(?:toggle|show|hide)$/,
        tt = /queueHooks$/;

    function it() {
        Je && (!1 === n.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(it) : e.setTimeout(it, w.fx.interval), w.fx.tick())
    }

    function nt() {
        return e.setTimeout((function () {
            Ze = void 0
        })), Ze = Date.now()
    }

    function rt(e, t) {
        var i, n = 0,
            r = {
                height: e
            };
        for (t = t ? 1 : 0; n < 4; n += 2 - t) r["margin" + (i = ie[n])] = r["padding" + i] = e;
        return t && (r.opacity = r.width = e), r
    }

    function st(e, t, i) {
        for (var n, r = (at.tweeners[t] || []).concat(at.tweeners["*"]), s = 0, a = r.length; s < a; s++)
            if (n = r[s].call(i, t, e)) return n
    }

    function at(e, t, i) {
        var n, r, s = 0,
            a = at.prefilters.length,
            o = w.Deferred().always((function () {
                delete l.elem
            })),
            l = function () {
                if (r) return !1;
                for (var t = Ze || nt(), i = Math.max(0, d.startTime + d.duration - t), n = 1 - (i / d.duration || 0), s = 0, a = d.tweens.length; s < a; s++) d.tweens[s].run(n);
                return o.notifyWith(e, [d, n, i]), n < 1 && a ? i : (a || o.notifyWith(e, [d, 1, 0]), o.resolveWith(e, [d]), !1)
            },
            d = o.promise({
                elem: e,
                props: w.extend({}, t),
                opts: w.extend(!0, {
                    specialEasing: {},
                    easing: w.easing._default
                }, i),
                originalProperties: t,
                originalOptions: i,
                startTime: Ze || nt(),
                duration: i.duration,
                tweens: [],
                createTween: function (t, i) {
                    var n = w.Tween(e, d.opts, t, i, d.opts.specialEasing[t] || d.opts.easing);
                    return d.tweens.push(n), n
                },
                stop: function (t) {
                    var i = 0,
                        n = t ? d.tweens.length : 0;
                    if (r) return this;
                    for (r = !0; i < n; i++) d.tweens[i].run(1);
                    return t ? (o.notifyWith(e, [d, 1, 0]), o.resolveWith(e, [d, t])) : o.rejectWith(e, [d, t]), this
                }
            }),
            c = d.props;
        for (function (e, t) {
                var i, n, r, s, a;
                for (i in e)
                    if (r = t[n = X(i)], s = e[i], Array.isArray(s) && (r = s[1], s = e[i] = s[0]), i !== n && (e[n] = s, delete e[i]), (a = w.cssHooks[n]) && "expand" in a)
                        for (i in s = a.expand(s), delete e[n], s) i in e || (e[i] = s[i], t[i] = r);
                    else t[n] = r
            }(c, d.opts.specialEasing); s < a; s++)
            if (n = at.prefilters[s].call(d, e, c, d.opts)) return g(n.stop) && (w._queueHooks(d.elem, d.opts.queue).stop = n.stop.bind(n)), n;
        return w.map(c, st, d), g(d.opts.start) && d.opts.start.call(e, d), d.progress(d.opts.progress).done(d.opts.done, d.opts.complete).fail(d.opts.fail).always(d.opts.always), w.fx.timer(w.extend(l, {
            elem: e,
            anim: d,
            queue: d.opts.queue
        })), d
    }
    w.Animation = w.extend(at, {
            tweeners: {
                "*": [function (e, t) {
                    var i = this.createTween(e, t);
                    return se(i.elem, e, te.exec(t), i), i
                }]
            },
            tweener: function (e, t) {
                g(e) ? (t = e, e = ["*"]) : e = e.match(N);
                for (var i, n = 0, r = e.length; n < r; n++) i = e[n], at.tweeners[i] = at.tweeners[i] || [], at.tweeners[i].unshift(t)
            },
            prefilters: [function (e, t, i) {
                var n, r, s, a, o, l, d, c, u = "width" in t || "height" in t,
                    h = this,
                    p = {},
                    f = e.style,
                    g = e.nodeType && ne(e),
                    m = U.get(e, "fxshow");
                for (n in i.queue || (null == (a = w._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, o = a.empty.fire, a.empty.fire = function () {
                        a.unqueued || o()
                    }), a.unqueued++, h.always((function () {
                        h.always((function () {
                            a.unqueued--, w.queue(e, "fx").length || a.empty.fire()
                        }))
                    }))), t)
                    if (r = t[n], et.test(r)) {
                        if (delete t[n], s = s || "toggle" === r, r === (g ? "hide" : "show")) {
                            if ("show" !== r || !m || void 0 === m[n]) continue;
                            g = !0
                        }
                        p[n] = m && m[n] || w.style(e, n)
                    } if ((l = !w.isEmptyObject(t)) || !w.isEmptyObject(p))
                    for (n in u && 1 === e.nodeType && (i.overflow = [f.overflow, f.overflowX, f.overflowY], null == (d = m && m.display) && (d = U.get(e, "display")), "none" === (c = w.css(e, "display")) && (d ? c = d : (le([e], !0), d = e.style.display || d, c = w.css(e, "display"), le([e]))), ("inline" === c || "inline-block" === c && null != d) && "none" === w.css(e, "float") && (l || (h.done((function () {
                            f.display = d
                        })), null == d && (c = f.display, d = "none" === c ? "" : c)), f.display = "inline-block")), i.overflow && (f.overflow = "hidden", h.always((function () {
                            f.overflow = i.overflow[0], f.overflowX = i.overflow[1], f.overflowY = i.overflow[2]
                        }))), l = !1, p) l || (m ? "hidden" in m && (g = m.hidden) : m = U.access(e, "fxshow", {
                        display: d
                    }), s && (m.hidden = !g), g && le([e], !0), h.done((function () {
                        for (n in g || le([e]), U.remove(e, "fxshow"), p) w.style(e, n, p[n])
                    }))), l = st(g ? m[n] : 0, n, h), n in m || (m[n] = l.start, g && (l.end = l.start, l.start = 0))
            }],
            prefilter: function (e, t) {
                t ? at.prefilters.unshift(e) : at.prefilters.push(e)
            }
        }), w.speed = function (e, t, i) {
            var n = e && "object" == typeof e ? w.extend({}, e) : {
                complete: i || !i && t || g(e) && e,
                duration: e,
                easing: i && t || t && !g(t) && t
            };
            return w.fx.off ? n.duration = 0 : "number" != typeof n.duration && (n.duration in w.fx.speeds ? n.duration = w.fx.speeds[n.duration] : n.duration = w.fx.speeds._default), null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function () {
                g(n.old) && n.old.call(this), n.queue && w.dequeue(this, n.queue)
            }, n
        }, w.fn.extend({
            fadeTo: function (e, t, i, n) {
                return this.filter(ne).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, i, n)
            },
            animate: function (e, t, i, n) {
                var r = w.isEmptyObject(e),
                    s = w.speed(t, i, n),
                    a = function () {
                        var t = at(this, w.extend({}, e), s);
                        (r || U.get(this, "finish")) && t.stop(!0)
                    };
                return a.finish = a, r || !1 === s.queue ? this.each(a) : this.queue(s.queue, a)
            },
            stop: function (e, t, i) {
                var n = function (e) {
                    var t = e.stop;
                    delete e.stop, t(i)
                };
                return "string" != typeof e && (i = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each((function () {
                    var t = !0,
                        r = null != e && e + "queueHooks",
                        s = w.timers,
                        a = U.get(this);
                    if (r) a[r] && a[r].stop && n(a[r]);
                    else
                        for (r in a) a[r] && a[r].stop && tt.test(r) && n(a[r]);
                    for (r = s.length; r--;) s[r].elem !== this || null != e && s[r].queue !== e || (s[r].anim.stop(i), t = !1, s.splice(r, 1));
                    !t && i || w.dequeue(this, e)
                }))
            },
            finish: function (e) {
                return !1 !== e && (e = e || "fx"), this.each((function () {
                    var t, i = U.get(this),
                        n = i[e + "queue"],
                        r = i[e + "queueHooks"],
                        s = w.timers,
                        a = n ? n.length : 0;
                    for (i.finish = !0, w.queue(this, e, []), r && r.stop && r.stop.call(this, !0), t = s.length; t--;) s[t].elem === this && s[t].queue === e && (s[t].anim.stop(!0), s.splice(t, 1));
                    for (t = 0; t < a; t++) n[t] && n[t].finish && n[t].finish.call(this);
                    delete i.finish
                }))
            }
        }), w.each(["toggle", "show", "hide"], (function (e, t) {
            var i = w.fn[t];
            w.fn[t] = function (e, n, r) {
                return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(rt(t, !0), e, n, r)
            }
        })), w.each({
            slideDown: rt("show"),
            slideUp: rt("hide"),
            slideToggle: rt("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, (function (e, t) {
            w.fn[e] = function (e, i, n) {
                return this.animate(t, e, i, n)
            }
        })), w.timers = [], w.fx.tick = function () {
            var e, t = 0,
                i = w.timers;
            for (Ze = Date.now(); t < i.length; t++)(e = i[t])() || i[t] !== e || i.splice(t--, 1);
            i.length || w.fx.stop(), Ze = void 0
        }, w.fx.timer = function (e) {
            w.timers.push(e), w.fx.start()
        }, w.fx.interval = 13, w.fx.start = function () {
            Je || (Je = !0, it())
        }, w.fx.stop = function () {
            Je = null
        }, w.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, w.fn.delay = function (t, i) {
            return t = w.fx && w.fx.speeds[t] || t, i = i || "fx", this.queue(i, (function (i, n) {
                var r = e.setTimeout(i, t);
                n.stop = function () {
                    e.clearTimeout(r)
                }
            }))
        },
        function () {
            var e = n.createElement("input"),
                t = n.createElement("select").appendChild(n.createElement("option"));
            e.type = "checkbox", f.checkOn = "" !== e.value, f.optSelected = t.selected, (e = n.createElement("input")).value = "t", e.type = "radio", f.radioValue = "t" === e.value
        }();
    var ot, lt = w.expr.attrHandle;
    w.fn.extend({
        attr: function (e, t) {
            return B(this, w.attr, e, t, arguments.length > 1)
        },
        removeAttr: function (e) {
            return this.each((function () {
                w.removeAttr(this, e)
            }))
        }
    }), w.extend({
        attr: function (e, t, i) {
            var n, r, s = e.nodeType;
            if (3 !== s && 8 !== s && 2 !== s) return void 0 === e.getAttribute ? w.prop(e, t, i) : (1 === s && w.isXMLDoc(e) || (r = w.attrHooks[t.toLowerCase()] || (w.expr.match.bool.test(t) ? ot : void 0)), void 0 !== i ? null === i ? void w.removeAttr(e, t) : r && "set" in r && void 0 !== (n = r.set(e, i, t)) ? n : (e.setAttribute(t, i + ""), i) : r && "get" in r && null !== (n = r.get(e, t)) ? n : null == (n = w.find.attr(e, t)) ? void 0 : n)
        },
        attrHooks: {
            type: {
                set: function (e, t) {
                    if (!f.radioValue && "radio" === t && k(e, "input")) {
                        var i = e.value;
                        return e.setAttribute("type", t), i && (e.value = i), t
                    }
                }
            }
        },
        removeAttr: function (e, t) {
            var i, n = 0,
                r = t && t.match(N);
            if (r && 1 === e.nodeType)
                for (; i = r[n++];) e.removeAttribute(i)
        }
    }), ot = {
        set: function (e, t, i) {
            return !1 === t ? w.removeAttr(e, i) : e.setAttribute(i, i), i
        }
    }, w.each(w.expr.match.bool.source.match(/\w+/g), (function (e, t) {
        var i = lt[t] || w.find.attr;
        lt[t] = function (e, t, n) {
            var r, s, a = t.toLowerCase();
            return n || (s = lt[a], lt[a] = r, r = null != i(e, t, n) ? a : null, lt[a] = s), r
        }
    }));
    var dt = /^(?:input|select|textarea|button)$/i,
        ct = /^(?:a|area)$/i;

    function ut(e) {
        return (e.match(N) || []).join(" ")
    }

    function ht(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function pt(e) {
        return Array.isArray(e) ? e : "string" == typeof e && e.match(N) || []
    }
    w.fn.extend({
        prop: function (e, t) {
            return B(this, w.prop, e, t, arguments.length > 1)
        },
        removeProp: function (e) {
            return this.each((function () {
                delete this[w.propFix[e] || e]
            }))
        }
    }), w.extend({
        prop: function (e, t, i) {
            var n, r, s = e.nodeType;
            if (3 !== s && 8 !== s && 2 !== s) return 1 === s && w.isXMLDoc(e) || (t = w.propFix[t] || t, r = w.propHooks[t]), void 0 !== i ? r && "set" in r && void 0 !== (n = r.set(e, i, t)) ? n : e[t] = i : r && "get" in r && null !== (n = r.get(e, t)) ? n : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = w.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : dt.test(e.nodeName) || ct.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), f.optSelected || (w.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function (e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), w.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], (function () {
        w.propFix[this.toLowerCase()] = this
    })), w.fn.extend({
        addClass: function (e) {
            var t, i, n, r, s, a, o, l = 0;
            if (g(e)) return this.each((function (t) {
                w(this).addClass(e.call(this, t, ht(this)))
            }));
            if ((t = pt(e)).length)
                for (; i = this[l++];)
                    if (r = ht(i), n = 1 === i.nodeType && " " + ut(r) + " ") {
                        for (a = 0; s = t[a++];) n.indexOf(" " + s + " ") < 0 && (n += s + " ");
                        r !== (o = ut(n)) && i.setAttribute("class", o)
                    } return this
        },
        removeClass: function (e) {
            var t, i, n, r, s, a, o, l = 0;
            if (g(e)) return this.each((function (t) {
                w(this).removeClass(e.call(this, t, ht(this)))
            }));
            if (!arguments.length) return this.attr("class", "");
            if ((t = pt(e)).length)
                for (; i = this[l++];)
                    if (r = ht(i), n = 1 === i.nodeType && " " + ut(r) + " ") {
                        for (a = 0; s = t[a++];)
                            for (; n.indexOf(" " + s + " ") > -1;) n = n.replace(" " + s + " ", " ");
                        r !== (o = ut(n)) && i.setAttribute("class", o)
                    } return this
        },
        toggleClass: function (e, t) {
            var i = typeof e,
                n = "string" === i || Array.isArray(e);
            return "boolean" == typeof t && n ? t ? this.addClass(e) : this.removeClass(e) : g(e) ? this.each((function (i) {
                w(this).toggleClass(e.call(this, i, ht(this), t), t)
            })) : this.each((function () {
                var t, r, s, a;
                if (n)
                    for (r = 0, s = w(this), a = pt(e); t = a[r++];) s.hasClass(t) ? s.removeClass(t) : s.addClass(t);
                else void 0 !== e && "boolean" !== i || ((t = ht(this)) && U.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : U.get(this, "__className__") || ""))
            }))
        },
        hasClass: function (e) {
            var t, i, n = 0;
            for (t = " " + e + " "; i = this[n++];)
                if (1 === i.nodeType && (" " + ut(ht(i)) + " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var ft = /\r/g;
    w.fn.extend({
        val: function (e) {
            var t, i, n, r = this[0];
            return arguments.length ? (n = g(e), this.each((function (i) {
                var r;
                1 === this.nodeType && (null == (r = n ? e.call(this, i, w(this).val()) : e) ? r = "" : "number" == typeof r ? r += "" : Array.isArray(r) && (r = w.map(r, (function (e) {
                    return null == e ? "" : e + ""
                }))), (t = w.valHooks[this.type] || w.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, r, "value") || (this.value = r))
            }))) : r ? (t = w.valHooks[r.type] || w.valHooks[r.nodeName.toLowerCase()]) && "get" in t && void 0 !== (i = t.get(r, "value")) ? i : "string" == typeof (i = r.value) ? i.replace(ft, "") : null == i ? "" : i : void 0
        }
    }), w.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = w.find.attr(e, "value");
                    return null != t ? t : ut(w.text(e))
                }
            },
            select: {
                get: function (e) {
                    var t, i, n, r = e.options,
                        s = e.selectedIndex,
                        a = "select-one" === e.type,
                        o = a ? null : [],
                        l = a ? s + 1 : r.length;
                    for (n = s < 0 ? l : a ? s : 0; n < l; n++)
                        if (((i = r[n]).selected || n === s) && !i.disabled && (!i.parentNode.disabled || !k(i.parentNode, "optgroup"))) {
                            if (t = w(i).val(), a) return t;
                            o.push(t)
                        } return o
                },
                set: function (e, t) {
                    for (var i, n, r = e.options, s = w.makeArray(t), a = r.length; a--;)((n = r[a]).selected = w.inArray(w.valHooks.option.get(n), s) > -1) && (i = !0);
                    return i || (e.selectedIndex = -1), s
                }
            }
        }
    }), w.each(["radio", "checkbox"], (function () {
        w.valHooks[this] = {
            set: function (e, t) {
                if (Array.isArray(t)) return e.checked = w.inArray(w(e).val(), t) > -1
            }
        }, f.checkOn || (w.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    })), f.focusin = "onfocusin" in e;
    var gt = /^(?:focusinfocus|focusoutblur)$/,
        mt = function (e) {
            e.stopPropagation()
        };
    w.extend(w.event, {
        trigger: function (t, i, r, s) {
            var a, o, l, d, c, h, p, f, v = [r || n],
                y = u.call(t, "type") ? t.type : t,
                b = u.call(t, "namespace") ? t.namespace.split(".") : [];
            if (o = f = l = r = r || n, 3 !== r.nodeType && 8 !== r.nodeType && !gt.test(y + w.event.triggered) && (y.indexOf(".") > -1 && (y = (b = y.split(".")).shift(), b.sort()), c = y.indexOf(":") < 0 && "on" + y, (t = t[w.expando] ? t : new w.Event(y, "object" == typeof t && t)).isTrigger = s ? 2 : 3, t.namespace = b.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + b.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), i = null == i ? [t] : w.makeArray(i, [t]), p = w.event.special[y] || {}, s || !p.trigger || !1 !== p.trigger.apply(r, i))) {
                if (!s && !p.noBubble && !m(r)) {
                    for (d = p.delegateType || y, gt.test(d + y) || (o = o.parentNode); o; o = o.parentNode) v.push(o), l = o;
                    l === (r.ownerDocument || n) && v.push(l.defaultView || l.parentWindow || e)
                }
                for (a = 0;
                    (o = v[a++]) && !t.isPropagationStopped();) f = o, t.type = a > 1 ? d : p.bindType || y, (h = (U.get(o, "events") || {})[t.type] && U.get(o, "handle")) && h.apply(o, i), (h = c && o[c]) && h.apply && V(o) && (t.result = h.apply(o, i), !1 === t.result && t.preventDefault());
                return t.type = y, s || t.isDefaultPrevented() || p._default && !1 !== p._default.apply(v.pop(), i) || !V(r) || c && g(r[y]) && !m(r) && ((l = r[c]) && (r[c] = null), w.event.triggered = y, t.isPropagationStopped() && f.addEventListener(y, mt), r[y](), t.isPropagationStopped() && f.removeEventListener(y, mt), w.event.triggered = void 0, l && (r[c] = l)), t.result
            }
        },
        simulate: function (e, t, i) {
            var n = w.extend(new w.Event, i, {
                type: e,
                isSimulated: !0
            });
            w.event.trigger(n, null, t)
        }
    }), w.fn.extend({
        trigger: function (e, t) {
            return this.each((function () {
                w.event.trigger(e, t, this)
            }))
        },
        triggerHandler: function (e, t) {
            var i = this[0];
            if (i) return w.event.trigger(e, t, i, !0)
        }
    }), f.focusin || w.each({
        focus: "focusin",
        blur: "focusout"
    }, (function (e, t) {
        var i = function (e) {
            w.event.simulate(t, e.target, w.event.fix(e))
        };
        w.event.special[t] = {
            setup: function () {
                var n = this.ownerDocument || this,
                    r = U.access(n, t);
                r || n.addEventListener(e, i, !0), U.access(n, t, (r || 0) + 1)
            },
            teardown: function () {
                var n = this.ownerDocument || this,
                    r = U.access(n, t) - 1;
                r ? U.access(n, t, r) : (n.removeEventListener(e, i, !0), U.remove(n, t))
            }
        }
    }));
    var vt = e.location,
        yt = Date.now(),
        bt = /\?/;
    w.parseXML = function (t) {
        var i;
        if (!t || "string" != typeof t) return null;
        try {
            i = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            i = void 0
        }
        return i && !i.getElementsByTagName("parsererror").length || w.error("Invalid XML: " + t), i
    };
    var wt = /\[\]$/,
        xt = /\r?\n/g,
        Ct = /^(?:submit|button|image|reset|file)$/i,
        Tt = /^(?:input|select|textarea|keygen)/i;

    function Et(e, t, i, n) {
        var r;
        if (Array.isArray(t)) w.each(t, (function (t, r) {
            i || wt.test(e) ? n(e, r) : Et(e + "[" + ("object" == typeof r && null != r ? t : "") + "]", r, i, n)
        }));
        else if (i || "object" !== b(t)) n(e, t);
        else
            for (r in t) Et(e + "[" + r + "]", t[r], i, n)
    }
    w.param = function (e, t) {
        var i, n = [],
            r = function (e, t) {
                var i = g(t) ? t() : t;
                n[n.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == i ? "" : i)
            };
        if (Array.isArray(e) || e.jquery && !w.isPlainObject(e)) w.each(e, (function () {
            r(this.name, this.value)
        }));
        else
            for (i in e) Et(i, e[i], t, r);
        return n.join("&")
    }, w.fn.extend({
        serialize: function () {
            return w.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map((function () {
                var e = w.prop(this, "elements");
                return e ? w.makeArray(e) : this
            })).filter((function () {
                var e = this.type;
                return this.name && !w(this).is(":disabled") && Tt.test(this.nodeName) && !Ct.test(e) && (this.checked || !de.test(e))
            })).map((function (e, t) {
                var i = w(this).val();
                return null == i ? null : Array.isArray(i) ? w.map(i, (function (e) {
                    return {
                        name: t.name,
                        value: e.replace(xt, "\r\n")
                    }
                })) : {
                    name: t.name,
                    value: i.replace(xt, "\r\n")
                }
            })).get()
        }
    });
    var St = /%20/g,
        _t = /#.*$/,
        kt = /([?&])_=[^&]*/,
        $t = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        It = /^(?:GET|HEAD)$/,
        Dt = /^\/\//,
        At = {},
        Mt = {},
        Pt = "*/".concat("*"),
        Ot = n.createElement("a");

    function Nt(e) {
        return function (t, i) {
            "string" != typeof t && (i = t, t = "*");
            var n, r = 0,
                s = t.toLowerCase().match(N) || [];
            if (g(i))
                for (; n = s[r++];) "+" === n[0] ? (n = n.slice(1) || "*", (e[n] = e[n] || []).unshift(i)) : (e[n] = e[n] || []).push(i)
        }
    }

    function Lt(e, t, i, n) {
        var r = {},
            s = e === Mt;

        function a(o) {
            var l;
            return r[o] = !0, w.each(e[o] || [], (function (e, o) {
                var d = o(t, i, n);
                return "string" != typeof d || s || r[d] ? s ? !(l = d) : void 0 : (t.dataTypes.unshift(d), a(d), !1)
            })), l
        }
        return a(t.dataTypes[0]) || !r["*"] && a("*")
    }

    function zt(e, t) {
        var i, n, r = w.ajaxSettings.flatOptions || {};
        for (i in t) void 0 !== t[i] && ((r[i] ? e : n || (n = {}))[i] = t[i]);
        return n && w.extend(!0, e, n), e
    }
    Ot.href = vt.href, w.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: vt.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(vt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Pt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": w.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function (e, t) {
            return t ? zt(zt(e, w.ajaxSettings), t) : zt(w.ajaxSettings, e)
        },
        ajaxPrefilter: Nt(At),
        ajaxTransport: Nt(Mt),
        ajax: function (t, i) {
            "object" == typeof t && (i = t, t = void 0), i = i || {};
            var r, s, a, o, l, d, c, u, h, p, f = w.ajaxSetup({}, i),
                g = f.context || f,
                m = f.context && (g.nodeType || g.jquery) ? w(g) : w.event,
                v = w.Deferred(),
                y = w.Callbacks("once memory"),
                b = f.statusCode || {},
                x = {},
                C = {},
                T = "canceled",
                E = {
                    readyState: 0,
                    getResponseHeader: function (e) {
                        var t;
                        if (c) {
                            if (!o)
                                for (o = {}; t = $t.exec(a);) o[t[1].toLowerCase()] = t[2];
                            t = o[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function () {
                        return c ? a : null
                    },
                    setRequestHeader: function (e, t) {
                        return null == c && (e = C[e.toLowerCase()] = C[e.toLowerCase()] || e, x[e] = t), this
                    },
                    overrideMimeType: function (e) {
                        return null == c && (f.mimeType = e), this
                    },
                    statusCode: function (e) {
                        var t;
                        if (e)
                            if (c) E.always(e[E.status]);
                            else
                                for (t in e) b[t] = [b[t], e[t]];
                        return this
                    },
                    abort: function (e) {
                        var t = e || T;
                        return r && r.abort(t), S(0, t), this
                    }
                };
            if (v.promise(E), f.url = ((t || f.url || vt.href) + "").replace(Dt, vt.protocol + "//"), f.type = i.method || i.type || f.method || f.type, f.dataTypes = (f.dataType || "*").toLowerCase().match(N) || [""], null == f.crossDomain) {
                d = n.createElement("a");
                try {
                    d.href = f.url, d.href = d.href, f.crossDomain = Ot.protocol + "//" + Ot.host != d.protocol + "//" + d.host
                } catch (e) {
                    f.crossDomain = !0
                }
            }
            if (f.data && f.processData && "string" != typeof f.data && (f.data = w.param(f.data, f.traditional)), Lt(At, f, i, E), c) return E;
            for (h in (u = w.event && f.global) && 0 == w.active++ && w.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !It.test(f.type), s = f.url.replace(_t, ""), f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(St, "+")) : (p = f.url.slice(s.length), f.data && (f.processData || "string" == typeof f.data) && (s += (bt.test(s) ? "&" : "?") + f.data, delete f.data), !1 === f.cache && (s = s.replace(kt, "$1"), p = (bt.test(s) ? "&" : "?") + "_=" + yt++ + p), f.url = s + p), f.ifModified && (w.lastModified[s] && E.setRequestHeader("If-Modified-Since", w.lastModified[s]), w.etag[s] && E.setRequestHeader("If-None-Match", w.etag[s])), (f.data && f.hasContent && !1 !== f.contentType || i.contentType) && E.setRequestHeader("Content-Type", f.contentType), E.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + Pt + "; q=0.01" : "") : f.accepts["*"]), f.headers) E.setRequestHeader(h, f.headers[h]);
            if (f.beforeSend && (!1 === f.beforeSend.call(g, E, f) || c)) return E.abort();
            if (T = "abort", y.add(f.complete), E.done(f.success), E.fail(f.error), r = Lt(Mt, f, i, E)) {
                if (E.readyState = 1, u && m.trigger("ajaxSend", [E, f]), c) return E;
                f.async && f.timeout > 0 && (l = e.setTimeout((function () {
                    E.abort("timeout")
                }), f.timeout));
                try {
                    c = !1, r.send(x, S)
                } catch (e) {
                    if (c) throw e;
                    S(-1, e)
                }
            } else S(-1, "No Transport");

            function S(t, i, n, o) {
                var d, h, p, x, C, T = i;
                c || (c = !0, l && e.clearTimeout(l), r = void 0, a = o || "", E.readyState = t > 0 ? 4 : 0, d = t >= 200 && t < 300 || 304 === t, n && (x = function (e, t, i) {
                    for (var n, r, s, a, o = e.contents, l = e.dataTypes;
                        "*" === l[0];) l.shift(), void 0 === n && (n = e.mimeType || t.getResponseHeader("Content-Type"));
                    if (n)
                        for (r in o)
                            if (o[r] && o[r].test(n)) {
                                l.unshift(r);
                                break
                            } if (l[0] in i) s = l[0];
                    else {
                        for (r in i) {
                            if (!l[0] || e.converters[r + " " + l[0]]) {
                                s = r;
                                break
                            }
                            a || (a = r)
                        }
                        s = s || a
                    }
                    if (s) return s !== l[0] && l.unshift(s), i[s]
                }(f, E, n)), x = function (e, t, i, n) {
                    var r, s, a, o, l, d = {},
                        c = e.dataTypes.slice();
                    if (c[1])
                        for (a in e.converters) d[a.toLowerCase()] = e.converters[a];
                    for (s = c.shift(); s;)
                        if (e.responseFields[s] && (i[e.responseFields[s]] = t), !l && n && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = s, s = c.shift())
                            if ("*" === s) s = l;
                            else if ("*" !== l && l !== s) {
                        if (!(a = d[l + " " + s] || d["* " + s]))
                            for (r in d)
                                if ((o = r.split(" "))[1] === s && (a = d[l + " " + o[0]] || d["* " + o[0]])) {
                                    !0 === a ? a = d[r] : !0 !== d[r] && (s = o[0], c.unshift(o[1]));
                                    break
                                } if (!0 !== a)
                            if (a && e.throws) t = a(t);
                            else try {
                                t = a(t)
                            } catch (e) {
                                return {
                                    state: "parsererror",
                                    error: a ? e : "No conversion from " + l + " to " + s
                                }
                            }
                    }
                    return {
                        state: "success",
                        data: t
                    }
                }(f, x, E, d), d ? (f.ifModified && ((C = E.getResponseHeader("Last-Modified")) && (w.lastModified[s] = C), (C = E.getResponseHeader("etag")) && (w.etag[s] = C)), 204 === t || "HEAD" === f.type ? T = "nocontent" : 304 === t ? T = "notmodified" : (T = x.state, h = x.data, d = !(p = x.error))) : (p = T, !t && T || (T = "error", t < 0 && (t = 0))), E.status = t, E.statusText = (i || T) + "", d ? v.resolveWith(g, [h, T, E]) : v.rejectWith(g, [E, T, p]), E.statusCode(b), b = void 0, u && m.trigger(d ? "ajaxSuccess" : "ajaxError", [E, f, d ? h : p]), y.fireWith(g, [E, T]), u && (m.trigger("ajaxComplete", [E, f]), --w.active || w.event.trigger("ajaxStop")))
            }
            return E
        },
        getJSON: function (e, t, i) {
            return w.get(e, t, i, "json")
        },
        getScript: function (e, t) {
            return w.get(e, void 0, t, "script")
        }
    }), w.each(["get", "post"], (function (e, t) {
        w[t] = function (e, i, n, r) {
            return g(i) && (r = r || n, n = i, i = void 0), w.ajax(w.extend({
                url: e,
                type: t,
                dataType: r,
                data: i,
                success: n
            }, w.isPlainObject(e) && e))
        }
    })), w._evalUrl = function (e) {
        return w.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            throws: !0
        })
    }, w.fn.extend({
        wrapAll: function (e) {
            var t;
            return this[0] && (g(e) && (e = e.call(this[0])), t = w(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map((function () {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            })).append(this)), this
        },
        wrapInner: function (e) {
            return g(e) ? this.each((function (t) {
                w(this).wrapInner(e.call(this, t))
            })) : this.each((function () {
                var t = w(this),
                    i = t.contents();
                i.length ? i.wrapAll(e) : t.append(e)
            }))
        },
        wrap: function (e) {
            var t = g(e);
            return this.each((function (i) {
                w(this).wrapAll(t ? e.call(this, i) : e)
            }))
        },
        unwrap: function (e) {
            return this.parent(e).not("body").each((function () {
                w(this).replaceWith(this.childNodes)
            })), this
        }
    }), w.expr.pseudos.hidden = function (e) {
        return !w.expr.pseudos.visible(e)
    }, w.expr.pseudos.visible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, w.ajaxSettings.xhr = function () {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    };
    var jt = {
            0: 200,
            1223: 204
        },
        Ht = w.ajaxSettings.xhr();
    f.cors = !!Ht && "withCredentials" in Ht, f.ajax = Ht = !!Ht, w.ajaxTransport((function (t) {
        var i, n;
        if (f.cors || Ht && !t.crossDomain) return {
            send: function (r, s) {
                var a, o = t.xhr();
                if (o.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                    for (a in t.xhrFields) o[a] = t.xhrFields[a];
                for (a in t.mimeType && o.overrideMimeType && o.overrideMimeType(t.mimeType), t.crossDomain || r["X-Requested-With"] || (r["X-Requested-With"] = "XMLHttpRequest"), r) o.setRequestHeader(a, r[a]);
                i = function (e) {
                    return function () {
                        i && (i = n = o.onload = o.onerror = o.onabort = o.ontimeout = o.onreadystatechange = null, "abort" === e ? o.abort() : "error" === e ? "number" != typeof o.status ? s(0, "error") : s(o.status, o.statusText) : s(jt[o.status] || o.status, o.statusText, "text" !== (o.responseType || "text") || "string" != typeof o.responseText ? {
                            binary: o.response
                        } : {
                            text: o.responseText
                        }, o.getAllResponseHeaders()))
                    }
                }, o.onload = i(), n = o.onerror = o.ontimeout = i("error"), void 0 !== o.onabort ? o.onabort = n : o.onreadystatechange = function () {
                    4 === o.readyState && e.setTimeout((function () {
                        i && n()
                    }))
                }, i = i("abort");
                try {
                    o.send(t.hasContent && t.data || null)
                } catch (e) {
                    if (i) throw e
                }
            },
            abort: function () {
                i && i()
            }
        }
    })), w.ajaxPrefilter((function (e) {
        e.crossDomain && (e.contents.script = !1)
    })), w.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function (e) {
                return w.globalEval(e), e
            }
        }
    }), w.ajaxPrefilter("script", (function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    })), w.ajaxTransport("script", (function (e) {
        var t, i;
        if (e.crossDomain) return {
            send: function (r, s) {
                t = w("<script>").prop({
                    charset: e.scriptCharset,
                    src: e.url
                }).on("load error", i = function (e) {
                    t.remove(), i = null, e && s("error" === e.type ? 404 : 200, e.type)
                }), n.head.appendChild(t[0])
            },
            abort: function () {
                i && i()
            }
        }
    }));
    var qt = [],
        Rt = /(=)\?(?=&|$)|\?\?/;
    w.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var e = qt.pop() || w.expando + "_" + yt++;
            return this[e] = !0, e
        }
    }), w.ajaxPrefilter("json jsonp", (function (t, i, n) {
        var r, s, a, o = !1 !== t.jsonp && (Rt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Rt.test(t.data) && "data");
        if (o || "jsonp" === t.dataTypes[0]) return r = t.jsonpCallback = g(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, o ? t[o] = t[o].replace(Rt, "$1" + r) : !1 !== t.jsonp && (t.url += (bt.test(t.url) ? "&" : "?") + t.jsonp + "=" + r), t.converters["script json"] = function () {
            return a || w.error(r + " was not called"), a[0]
        }, t.dataTypes[0] = "json", s = e[r], e[r] = function () {
            a = arguments
        }, n.always((function () {
            void 0 === s ? w(e).removeProp(r) : e[r] = s, t[r] && (t.jsonpCallback = i.jsonpCallback, qt.push(r)), a && g(s) && s(a[0]), a = s = void 0
        })), "script"
    })), f.createHTMLDocument = function () {
        var e = n.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
    }(), w.parseHTML = function (e, t, i) {
        return "string" != typeof e ? [] : ("boolean" == typeof t && (i = t, t = !1), t || (f.createHTMLDocument ? ((r = (t = n.implementation.createHTMLDocument("")).createElement("base")).href = n.location.href, t.head.appendChild(r)) : t = n), a = !i && [], (s = $.exec(e)) ? [t.createElement(s[1])] : (s = me([e], t, a), a && a.length && w(a).remove(), w.merge([], s.childNodes)));
        var r, s, a
    }, w.fn.load = function (e, t, i) {
        var n, r, s, a = this,
            o = e.indexOf(" ");
        return o > -1 && (n = ut(e.slice(o)), e = e.slice(0, o)), g(t) ? (i = t, t = void 0) : t && "object" == typeof t && (r = "POST"), a.length > 0 && w.ajax({
            url: e,
            type: r || "GET",
            dataType: "html",
            data: t
        }).done((function (e) {
            s = arguments, a.html(n ? w("<div>").append(w.parseHTML(e)).find(n) : e)
        })).always(i && function (e, t) {
            a.each((function () {
                i.apply(this, s || [e.responseText, t, e])
            }))
        }), this
    }, w.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], (function (e, t) {
        w.fn[t] = function (e) {
            return this.on(t, e)
        }
    })), w.expr.pseudos.animated = function (e) {
        return w.grep(w.timers, (function (t) {
            return e === t.elem
        })).length
    }, w.offset = {
        setOffset: function (e, t, i) {
            var n, r, s, a, o, l, d = w.css(e, "position"),
                c = w(e),
                u = {};
            "static" === d && (e.style.position = "relative"), o = c.offset(), s = w.css(e, "top"), l = w.css(e, "left"), ("absolute" === d || "fixed" === d) && (s + l).indexOf("auto") > -1 ? (a = (n = c.position()).top, r = n.left) : (a = parseFloat(s) || 0, r = parseFloat(l) || 0), g(t) && (t = t.call(e, i, w.extend({}, o))), null != t.top && (u.top = t.top - o.top + a), null != t.left && (u.left = t.left - o.left + r), "using" in t ? t.using.call(e, u) : c.css(u)
        }
    }, w.fn.extend({
        offset: function (e) {
            if (arguments.length) return void 0 === e ? this : this.each((function (t) {
                w.offset.setOffset(this, e, t)
            }));
            var t, i, n = this[0];
            return n ? n.getClientRects().length ? (t = n.getBoundingClientRect(), i = n.ownerDocument.defaultView, {
                top: t.top + i.pageYOffset,
                left: t.left + i.pageXOffset
            }) : {
                top: 0,
                left: 0
            } : void 0
        },
        position: function () {
            if (this[0]) {
                var e, t, i, n = this[0],
                    r = {
                        top: 0,
                        left: 0
                    };
                if ("fixed" === w.css(n, "position")) t = n.getBoundingClientRect();
                else {
                    for (t = this.offset(), i = n.ownerDocument, e = n.offsetParent || i.documentElement; e && (e === i.body || e === i.documentElement) && "static" === w.css(e, "position");) e = e.parentNode;
                    e && e !== n && 1 === e.nodeType && ((r = w(e).offset()).top += w.css(e, "borderTopWidth", !0), r.left += w.css(e, "borderLeftWidth", !0))
                }
                return {
                    top: t.top - r.top - w.css(n, "marginTop", !0),
                    left: t.left - r.left - w.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function () {
            return this.map((function () {
                for (var e = this.offsetParent; e && "static" === w.css(e, "position");) e = e.offsetParent;
                return e || ve
            }))
        }
    }), w.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, (function (e, t) {
        var i = "pageYOffset" === t;
        w.fn[e] = function (n) {
            return B(this, (function (e, n, r) {
                var s;
                if (m(e) ? s = e : 9 === e.nodeType && (s = e.defaultView), void 0 === r) return s ? s[t] : e[n];
                s ? s.scrollTo(i ? s.pageXOffset : r, i ? r : s.pageYOffset) : e[n] = r
            }), e, n, arguments.length)
        }
    })), w.each(["top", "left"], (function (e, t) {
        w.cssHooks[t] = qe(f.pixelPosition, (function (e, i) {
            if (i) return i = He(e, t), Le.test(i) ? w(e).position()[t] + "px" : i
        }))
    })), w.each({
        Height: "height",
        Width: "width"
    }, (function (e, t) {
        w.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, (function (i, n) {
            w.fn[n] = function (r, s) {
                var a = arguments.length && (i || "boolean" != typeof r),
                    o = i || (!0 === r || !0 === s ? "margin" : "border");
                return B(this, (function (t, i, r) {
                    var s;
                    return m(t) ? 0 === n.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (s = t.documentElement, Math.max(t.body["scroll" + e], s["scroll" + e], t.body["offset" + e], s["offset" + e], s["client" + e])) : void 0 === r ? w.css(t, i, o) : w.style(t, i, r, o)
                }), t, a ? r : void 0, a)
            }
        }))
    })), w.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), (function (e, t) {
        w.fn[t] = function (e, i) {
            return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
        }
    })), w.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), w.fn.extend({
        bind: function (e, t, i) {
            return this.on(e, null, t, i)
        },
        unbind: function (e, t) {
            return this.off(e, null, t)
        },
        delegate: function (e, t, i, n) {
            return this.on(t, e, i, n)
        },
        undelegate: function (e, t, i) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
        }
    }), w.proxy = function (e, t) {
        var i, n, r;
        if ("string" == typeof t && (i = e[t], t = e, e = i), g(e)) return n = s.call(arguments, 2), (r = function () {
            return e.apply(t || this, n.concat(s.call(arguments)))
        }).guid = e.guid = e.guid || w.guid++, r
    }, w.holdReady = function (e) {
        e ? w.readyWait++ : w.ready(!0)
    }, w.isArray = Array.isArray, w.parseJSON = JSON.parse, w.nodeName = k, w.isFunction = g, w.isWindow = m, w.camelCase = X, w.type = b, w.now = Date.now, w.isNumeric = function (e) {
        var t = w.type(e);
        return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
    }, "function" == typeof define && define.amd && define("jquery", [], (function () {
        return w
    }));
    var Bt = e.jQuery,
        Wt = e.$;
    return w.noConflict = function (t) {
        return e.$ === w && (e.$ = Wt), t && e.jQuery === w && (e.jQuery = Bt), w
    }, t || (e.jQuery = e.$ = w), w
}));
/*!
 * prettify.js
 */
var q = null;
window.PR_SHOULD_USE_CONTINUATION = !0,
    function () {
        function e(e, t, i, n) {
            t && (i(e = {
                a: t,
                d: e
            }), n.push.apply(n, e.e))
        }

        function t(t, i) {
            var n, r = {};
            ! function () {
                for (var e = t.concat(i), s = [], a = {}, o = 0, l = e.length; o < l; ++o) {
                    var d = e[o],
                        c = d[3];
                    if (c)
                        for (var u = c.length; --u >= 0;) r[c.charAt(u)] = d;
                    c = "" + (d = d[1]), a.hasOwnProperty(c) || (s.push(d), a[c] = q)
                }
                s.push(/[\S\s]/), n = function (e) {
                    function t(e) {
                        var t = e.charCodeAt(0);
                        if (92 !== t) return t;
                        var i = e.charAt(1);
                        return (t = u[i]) ? t : "0" <= i && i <= "7" ? parseInt(e.substring(1), 8) : "u" === i || "x" === i ? parseInt(e.substring(2), 16) : e.charCodeAt(1)
                    }

                    function i(e) {
                        return e < 32 ? (e < 16 ? "\\x0" : "\\x") + e.toString(16) : ("\\" !== (e = String.fromCharCode(e)) && "-" !== e && "[" !== e && "]" !== e || (e = "\\" + e), e)
                    }

                    function n(e) {
                        for (var n = e.substring(1, e.length - 1).match(/\\u[\dA-Fa-f]{4}|\\x[\dA-Fa-f]{2}|\\[0-3][0-7]{0,2}|\\[0-7]{1,2}|\\[\S\s]|[^\\]/g), r = (e = [], []), s = "^" === n[0], a = s ? 1 : 0, o = n.length; a < o; ++a) {
                            var l = n[a];
                            if (/\\[bdsw]/i.test(l)) e.push(l);
                            else {
                                var d;
                                l = t(l);
                                a + 2 < o && "-" === n[a + 1] ? (d = t(n[a + 2]), a += 2) : d = l, r.push([l, d]), d < 65 || l > 122 || (d < 65 || l > 90 || r.push([32 | Math.max(65, l), 32 | Math.min(d, 90)]), d < 97 || l > 122 || r.push([-33 & Math.max(97, l), -33 & Math.min(d, 122)]))
                            }
                        }
                        for (r.sort((function (e, t) {
                                return e[0] - t[0] || t[1] - e[1]
                            })), n = [], l = [NaN, NaN], a = 0; a < r.length; ++a)(o = r[a])[0] <= l[1] + 1 ? l[1] = Math.max(l[1], o[1]) : n.push(l = o);
                        for (r = ["["], s && r.push("^"), r.push.apply(r, e), a = 0; a < n.length; ++a) o = n[a], r.push(i(o[0])), o[1] > o[0] && (o[1] + 1 > o[0] && r.push("-"), r.push(i(o[1])));
                        return r.push("]"), r.join("")
                    }

                    function r(e) {
                        for (var t = e.source.match(/\[(?:[^\\\]]|\\[\S\s])*]|\\u[\dA-Fa-f]{4}|\\x[\dA-Fa-f]{2}|\\\d+|\\[^\dux]|\(\?[!:=]|[()^]|[^()[\\^]+/g), i = t.length, r = [], o = 0, l = 0; o < i; ++o) {
                            var d = t[o];
                            "(" === d ? ++l : "\\" === d.charAt(0) && (d = +d.substring(1)) && d <= l && (r[d] = -1)
                        }
                        for (o = 1; o < r.length; ++o) - 1 === r[o] && (r[o] = ++s);
                        for (l = o = 0; o < i; ++o) "(" === (d = t[o]) ? void 0 === r[++l] && (t[o] = "(?:") : "\\" === d.charAt(0) && (d = +d.substring(1)) && d <= l && (t[o] = "\\" + r[l]);
                        for (l = o = 0; o < i; ++o) "^" === t[o] && "^" !== t[o + 1] && (t[o] = "");
                        if (e.ignoreCase && a)
                            for (o = 0; o < i; ++o) e = (d = t[o]).charAt(0), d.length >= 2 && "[" === e ? t[o] = n(d) : "\\" !== e && (t[o] = d.replace(/[A-Za-z]/g, (function (e) {
                                return e = e.charCodeAt(0), "[" + String.fromCharCode(-33 & e, 32 | e) + "]"
                            })));
                        return t.join("")
                    }
                    for (var s = 0, a = !1, o = !1, l = 0, d = e.length; l < d; ++l) {
                        var c = e[l];
                        if (c.ignoreCase) o = !0;
                        else if (/[a-z]/i.test(c.source.replace(/\\u[\da-f]{4}|\\x[\da-f]{2}|\\[^UXux]/gi, ""))) {
                            a = !0, o = !1;
                            break
                        }
                    }
                    var u = {
                            b: 8,
                            t: 9,
                            n: 10,
                            v: 11,
                            f: 12,
                            r: 13
                        },
                        h = [];
                    for (l = 0, d = e.length; l < d; ++l) {
                        if ((c = e[l]).global || c.multiline) throw Error("" + c);
                        h.push("(?:" + r(c) + ")")
                    }
                    return RegExp(h.join("|"), o ? "gi" : "g")
                }(s)
            }();
            var a = i.length;
            return function t(o) {
                for (var l = o.d, d = [l, "pln"], c = 0, u = o.a.match(n) || [], h = {}, p = 0, f = u.length; p < f; ++p) {
                    var g, m = u[p],
                        v = h[m],
                        y = void 0;
                    if ("string" == typeof v) g = !1;
                    else {
                        var b = r[m.charAt(0)];
                        if (b) y = m.match(b[1]), v = b[0];
                        else {
                            for (g = 0; g < a; ++g)
                                if (b = i[g], y = m.match(b[1])) {
                                    v = b[0];
                                    break
                                } y || (v = "pln")
                        }!(g = v.length >= 5 && "lang-" === v.substring(0, 5)) || y && "string" == typeof y[1] || (g = !1, v = "src"), g || (h[m] = v)
                    }
                    if (b = c, c += m.length, g) {
                        g = y[1];
                        var w = m.indexOf(g),
                            x = w + g.length;
                        y[2] && (w = (x = m.length - y[2].length) - g.length), v = v.substring(5), e(l + b, m.substring(0, w), t, d), e(l + b + w, g, s(v, g), d), e(l + b + x, m.substring(x), t, d)
                    } else d.push(l + b, v)
                }
                o.e = d
            }
        }

        function i(e) {
            var i = [],
                n = [];
            e.tripleQuotedStrings ? i.push(["str", /^(?:'''(?:[^'\\]|\\[\S\s]|''?(?=[^']))*(?:'''|$)|"""(?:[^"\\]|\\[\S\s]|""?(?=[^"]))*(?:"""|$)|'(?:[^'\\]|\\[\S\s])*(?:'|$)|"(?:[^"\\]|\\[\S\s])*(?:"|$))/, q, "'\""]) : e.multiLineStrings ? i.push(["str", /^(?:'(?:[^'\\]|\\[\S\s])*(?:'|$)|"(?:[^"\\]|\\[\S\s])*(?:"|$)|`(?:[^\\`]|\\[\S\s])*(?:`|$))/, q, "'\"`"]) : i.push(["str", /^(?:'(?:[^\n\r'\\]|\\.)*(?:'|$)|"(?:[^\n\r"\\]|\\.)*(?:"|$))/, q, "\"'"]), e.verbatimStrings && n.push(["str", /^@"(?:[^"]|"")*(?:"|$)/, q]);
            var r = e.hashComments;
            return r && (e.cStyleComments ? (r > 1 ? i.push(["com", /^#(?:##(?:[^#]|#(?!##))*(?:###|$)|.*)/, q, "#"]) : i.push(["com", /^#(?:(?:define|elif|else|endif|error|ifdef|include|ifndef|line|pragma|undef|warning)\b|[^\n\r]*)/, q, "#"]), n.push(["str", /^<(?:(?:(?:\.\.\/)*|\/?)(?:[\w-]+(?:\/[\w-]+)+)?[\w-]+\.h|[a-z]\w*)>/, q])) : i.push(["com", /^#[^\n\r]*/, q, "#"])), e.cStyleComments && (n.push(["com", /^\/\/[^\n\r]*/, q]), n.push(["com", /^\/\*[\S\s]*?(?:\*\/|$)/, q])), e.regexLiterals && n.push(["lang-regex", /^(?:^^\.?|[!+-]|!=|!==|#|%|%=|&|&&|&&=|&=|\(|\*|\*=|\+=|,|-=|->|\/|\/=|:|::|;|<|<<|<<=|<=|=|==|===|>|>=|>>|>>=|>>>|>>>=|[?@[^]|\^=|\^\^|\^\^=|{|\||\|=|\|\||\|\|=|~|break|case|continue|delete|do|else|finally|instanceof|return|throw|try|typeof)\s*(\/(?=[^*/])(?:[^/[\\]|\\[\S\s]|\[(?:[^\\\]]|\\[\S\s])*(?:]|$))+\/)/]), (r = e.types) && n.push(["typ", r]), (e = ("" + e.keywords).replace(/^ | $/g, "")).length && n.push(["kwd", RegExp("^(?:" + e.replace(/[\s,]+/g, "|") + ")\\b"), q]), i.push(["pln", /^\s+/, q, " \r\n\t "]), n.push(["lit", /^@[$_a-z][\w$@]*/i, q], ["typ", /^(?:[@_]?[A-Z]+[a-z][\w$@]*|\w+_t\b)/, q], ["pln", /^[$_a-z][\w$@]*/i, q], ["lit", /^(?:0x[\da-f]+|(?:\d(?:_\d+)*\d*(?:\.\d*)?|\.\d\+)(?:e[+-]?\d+)?)[a-z]*/i, q, "0123456789"], ["pln", /^\\[\S\s]?/, q], ["pun", /^.[^\s\w"-$'./@\\`]*/, q]), t(i, n)
        }

        function n(e, t) {
            function i(e) {
                switch (e.nodeType) {
                    case 1:
                        if (s.test(e.className)) break;
                        if ("BR" === e.nodeName) n(e), e.parentNode && e.parentNode.removeChild(e);
                        else
                            for (e = e.firstChild; e; e = e.nextSibling) i(e);
                        break;
                    case 3:
                    case 4:
                        if (l) {
                            var t = e.nodeValue,
                                r = t.match(a);
                            if (r) {
                                var d = t.substring(0, r.index);
                                e.nodeValue = d, (t = t.substring(r.index + r[0].length)) && e.parentNode.insertBefore(o.createTextNode(t), e.nextSibling), n(e), d || e.parentNode.removeChild(e)
                            }
                        }
                }
            }

            function n(e) {
                for (; !e.nextSibling;)
                    if (!(e = e.parentNode)) return;
                var t;
                for (e = function e(t, i) {
                        var n = i ? t.cloneNode(!1) : t;
                        if (r = t.parentNode) {
                            var r = e(r, 1),
                                s = t.nextSibling;
                            r.appendChild(n);
                            for (var a = s; a; a = s) s = a.nextSibling, r.appendChild(a)
                        }
                        return n
                    }(e.nextSibling, 0);
                    (t = e.parentNode) && 1 === t.nodeType;) e = t;
                d.push(e)
            }
            var r, s = /(?:^|\s)nocode(?:\s|$)/,
                a = /\r\n?|\n/,
                o = e.ownerDocument;
            e.currentStyle ? r = e.currentStyle.whiteSpace : window.getComputedStyle && (r = o.defaultView.getComputedStyle(e, q).getPropertyValue("white-space"));
            var l = r && "pre" === r.substring(0, 3);
            for (r = o.createElement("LI"); e.firstChild;) r.appendChild(e.firstChild);
            for (var d = [r], c = 0; c < d.length; ++c) i(d[c]);
            t === (0 | t) && d[0].setAttribute("value", t);
            var u = o.createElement("OL");
            u.className = "linenums";
            for (var h = Math.max(0, t - 1 | 0) || 0, p = (c = 0, d.length); c < p; ++c)(r = d[c]).className = "L" + (c + h) % 10, r.firstChild || r.appendChild(o.createTextNode(" ")), u.appendChild(r);
            e.appendChild(u)
        }

        function r(e, t) {
            for (var i = t.length; --i >= 0;) {
                var n = t[i];
                v.hasOwnProperty(n) ? window.console && console.warn("cannot override language handler %s", n) : v[n] = e
            }
        }

        function s(e, t) {
            return e && v.hasOwnProperty(e) || (e = /^\s*</.test(t) ? "default-markup" : "default-code"), v[e]
        }

        function a(e) {
            var t = e.g;
            try {
                var i = (d = function (e) {
                    var t, i = /(?:^|\s)nocode(?:\s|$)/,
                        n = [],
                        r = 0,
                        s = [],
                        a = 0;
                    e.currentStyle ? t = e.currentStyle.whiteSpace : window.getComputedStyle && (t = document.defaultView.getComputedStyle(e, q).getPropertyValue("white-space"));
                    var o = t && "pre" === t.substring(0, 3);
                    return function e(t) {
                        switch (t.nodeType) {
                            case 1:
                                if (i.test(t.className)) break;
                                for (var l = t.firstChild; l; l = l.nextSibling) e(l);
                                "BR" !== (l = t.nodeName) && "LI" !== l || (n[a] = "\n", s[a << 1] = r++, s[a++ << 1 | 1] = t);
                                break;
                            case 3:
                            case 4:
                                (l = t.nodeValue).length && (l = o ? l.replace(/\r\n?/g, "\n") : l.replace(/[\t\n\r ]+/g, " "), n[a] = l, s[a << 1] = r, r += l.length, s[a++ << 1 | 1] = t)
                        }
                    }(e), {
                        a: n.join("").replace(/\n$/, ""),
                        c: s
                    }
                }(e.h)).a;
                e.a = i, e.c = d.c, e.d = 0, s(t, i)(e);
                var n, r, a = /\bMSIE\b/.test(navigator.userAgent),
                    o = (t = /\n/g, e.a),
                    l = o.length,
                    d = 0,
                    c = e.c,
                    u = c.length,
                    h = (i = 0, e.e),
                    p = h.length;
                e = 0;
                for (h[p] = l, r = n = 0; r < p;) h[r] !== h[r + 2] ? (h[n++] = h[r++], h[n++] = h[r++]) : r += 2;
                for (p = n, r = n = 0; r < p;) {
                    for (var f = h[r], g = h[r + 1], m = r + 2; m + 2 <= p && h[m + 1] === g;) m += 2;
                    h[n++] = f, h[n++] = g, r = m
                }
                for (h.length = n; i < u;) {
                    var v, y = c[i + 2] || l,
                        b = h[e + 2] || l,
                        w = (m = Math.min(y, b), c[i + 1]);
                    if (1 !== w.nodeType && (v = o.substring(d, m))) {
                        a && (v = v.replace(t, "\r")), w.nodeValue = v;
                        var x = w.ownerDocument,
                            C = x.createElement("SPAN");
                        C.className = h[e + 1];
                        var T = w.parentNode;
                        T.replaceChild(C, w), C.appendChild(w), d < y && (c[i + 1] = w = x.createTextNode(o.substring(m, y)), T.insertBefore(w, C.nextSibling))
                    }(d = m) >= y && (i += 2), d >= b && (e += 2)
                }
            } catch (e) {
                "console" in window && console.log(e && e.stack ? e.stack : e)
            }
        }
        var o, l, d = [o = [
                [l = ["break,continue,do,else,for,if,return,while"], "auto,case,char,const,default,double,enum,extern,float,goto,int,long,register,short,signed,sizeof,static,struct,switch,typedef,union,unsigned,void,volatile"], "catch,class,delete,false,import,new,operator,private,protected,public,this,throw,true,try,typeof"
            ], "alignof,align_union,asm,axiom,bool,concept,concept_map,const_cast,constexpr,decltype,dynamic_cast,explicit,export,friend,inline,late_check,mutable,namespace,nullptr,reinterpret_cast,static_assert,static_cast,template,typeid,typename,using,virtual,where"],
            c = [o, "abstract,boolean,byte,extends,final,finally,implements,import,instanceof,null,native,package,strictfp,super,synchronized,throws,transient"],
            u = [c, "as,base,by,checked,decimal,delegate,descending,dynamic,event,fixed,foreach,from,group,implicit,in,interface,internal,into,is,lock,object,out,override,orderby,params,partial,readonly,ref,sbyte,sealed,stackalloc,string,select,uint,ulong,unchecked,unsafe,ushort,var"],
            h = [l, "and,as,assert,class,def,del,elif,except,exec,finally,from,global,import,in,is,lambda,nonlocal,not,or,pass,print,raise,try,with,yield,False,True,None"],
            p = [l, "alias,and,begin,case,class,def,defined,elsif,end,ensure,false,in,module,next,nil,not,or,redo,rescue,retry,self,super,then,true,undef,unless,until,when,yield,BEGIN,END"],
            f = /^(DIR|FILE|vector|(de|priority_)?queue|list|stack|(const_)?iterator|(multi)?(set|map)|bitset|u?(int|float)\d*)/,
            g = /\S/,
            m = i({
                keywords: [d, u, o = [o, "debugger,eval,export,function,get,null,set,undefined,var,with,Infinity,NaN"], "caller,delete,die,do,dump,elsif,eval,exit,foreach,for,goto,if,import,last,local,my,next,no,our,print,package,redo,require,sub,undef,unless,until,use,wantarray,while,BEGIN,END" + h, p, l = [l, "case,done,elif,esac,eval,fi,function,in,local,set,then,until"]],
                hashComments: !0,
                cStyleComments: !0,
                multiLineStrings: !0,
                regexLiterals: !0
            }),
            v = {};
        r(m, ["default-code"]), r(t([], [
            ["pln", /^[^<?]+/],
            ["dec", /^<!\w[^>]*(?:>|$)/],
            ["com", /^<\!--[\S\s]*?(?:--\>|$)/],
            ["lang-", /^<\?([\S\s]+?)(?:\?>|$)/],
            ["lang-", /^<%([\S\s]+?)(?:%>|$)/],
            ["pun", /^(?:<[%?]|[%?]>)/],
            ["lang-", /^<xmp\b[^>]*>([\S\s]+?)<\/xmp\b[^>]*>/i],
            ["lang-js", /^<script\b[^>]*>([\S\s]*?)(<\/script\b[^>]*>)/i],
            ["lang-css", /^<style\b[^>]*>([\S\s]*?)(<\/style\b[^>]*>)/i],
            ["lang-in.tag", /^(<\/?[a-z][^<>]*>)/i]
        ]), ["default-markup", "htm", "html", "mxml", "xhtml", "xml", "xsl"]), r(t([
            ["pln", /^\s+/, q, " \t\r\n"],
            ["atv", /^(?:"[^"]*"?|'[^']*'?)/, q, "\"'"]
        ], [
            ["tag", /^^<\/?[a-z](?:[\w-.:]*\w)?|\/?>$/i],
            ["atn", /^(?!style[\s=]|on)[a-z](?:[\w:-]*\w)?/i],
            ["lang-uq.val", /^=\s*([^\s"'>]*(?:[^\s"'/>]|\/(?=\s)))/],
            ["pun", /^[/<->]+/],
            ["lang-js", /^on\w+\s*=\s*"([^"]+)"/i],
            ["lang-js", /^on\w+\s*=\s*'([^']+)'/i],
            ["lang-js", /^on\w+\s*=\s*([^\s"'>]+)/i],
            ["lang-css", /^style\s*=\s*"([^"]+)"/i],
            ["lang-css", /^style\s*=\s*'([^']+)'/i],
            ["lang-css", /^style\s*=\s*([^\s"'>]+)/i]
        ]), ["in.tag"]), r(t([], [
            ["atv", /^[\S\s]+/]
        ]), ["uq.val"]), r(i({
            keywords: d,
            hashComments: !0,
            cStyleComments: !0,
            types: f
        }), ["c", "cc", "cpp", "cxx", "cyc", "m"]), r(i({
            keywords: "null,true,false"
        }), ["json"]), r(i({
            keywords: u,
            hashComments: !0,
            cStyleComments: !0,
            verbatimStrings: !0,
            types: f
        }), ["cs"]), r(i({
            keywords: c,
            cStyleComments: !0
        }), ["java"]), r(i({
            keywords: l,
            hashComments: !0,
            multiLineStrings: !0
        }), ["bsh", "csh", "sh"]), r(i({
            keywords: h,
            hashComments: !0,
            multiLineStrings: !0,
            tripleQuotedStrings: !0
        }), ["cv", "py"]), r(i({
            keywords: "caller,delete,die,do,dump,elsif,eval,exit,foreach,for,goto,if,import,last,local,my,next,no,our,print,package,redo,require,sub,undef,unless,until,use,wantarray,while,BEGIN,END",
            hashComments: !0,
            multiLineStrings: !0,
            regexLiterals: !0
        }), ["perl", "pl", "pm"]), r(i({
            keywords: p,
            hashComments: !0,
            multiLineStrings: !0,
            regexLiterals: !0
        }), ["rb"]), r(i({
            keywords: o,
            cStyleComments: !0,
            regexLiterals: !0
        }), ["js"]), r(i({
            keywords: "all,and,by,catch,class,else,extends,false,finally,for,if,in,is,isnt,loop,new,no,not,null,of,off,on,or,return,super,then,true,try,unless,until,when,while,yes",
            hashComments: 3,
            cStyleComments: !0,
            multilineStrings: !0,
            tripleQuotedStrings: !0,
            regexLiterals: !0
        }), ["coffee"]), r(t([], [
            ["str", /^[\S\s]+/]
        ]), ["regex"]), window.prettyPrintOne = function (e, t, i) {
            var r = document.createElement("PRE");
            return r.innerHTML = e, i && n(r, i), a({
                g: t,
                i: i,
                h: r
            }), r.innerHTML
        }, window.prettyPrint = function (e) {
            for (var t = [document.getElementsByTagName("pre"), document.getElementsByTagName("code"), document.getElementsByTagName("xmp")], i = [], r = 0; r < t.length; ++r)
                for (var s = 0, o = t[r].length; s < o; ++s) i.push(t[r][s]);
            t = q;
            var l = Date;
            l.now || (l = {
                now: function () {
                    return +new Date
                }
            });
            var d = 0,
                c = /\blang(?:uage)?-([\w.]+)(?!\S)/;
            ! function t() {
                for (var r = window.PR_SHOULD_USE_CONTINUATION ? l.now() + 250 : 1 / 0; d < i.length && l.now() < r; d++) {
                    var s = i[d];
                    if ((o = s.className).indexOf("prettyprint") >= 0) {
                        var o, u, h;
                        if (h = !(o = o.match(c))) {
                            for (var p = void 0, f = (h = s).firstChild; f; f = f.nextSibling) {
                                var m = f.nodeType;
                                p = 1 === m ? p ? h : f : 3 === m && g.test(f.nodeValue) ? h : p
                            }
                            h = (u = p === h ? void 0 : p) && "CODE" === u.tagName
                        }
                        for (h && (o = u.className.match(c)), o && (o = o[1]), h = !1, p = s.parentNode; p; p = p.parentNode)
                            if (("pre" === p.tagName || "code" === p.tagName || "xmp" === p.tagName) && p.className && p.className.indexOf("prettyprint") >= 0) {
                                h = !0;
                                break
                            } h || ((h = !!(h = s.className.match(/\blinenums\b(?::(\d+))?/)) && (!h[1] || !h[1].length || +h[1])) && n(s, h), a({
                            g: o,
                            h: s,
                            i: h
                        }))
                    }
                }
                d < i.length ? setTimeout(t, 250) : e && e()
            }()
        }, window.PR = {
            createSimpleLexer: t,
            registerLangHandler: r,
            sourceDecorator: i,
            PR_ATTRIB_NAME: "atn",
            PR_ATTRIB_VALUE: "atv",
            PR_COMMENT: "com",
            PR_DECLARATION: "dec",
            PR_KEYWORD: "kwd",
            PR_LITERAL: "lit",
            PR_NOCODE: "nocode",
            PR_PLAIN: "pln",
            PR_PUNCTUATION: "pun",
            PR_SOURCE: "src",
            PR_STRING: "str",
            PR_TAG: "tag",
            PR_TYPE: "typ"
        }
    }(),
    function (e) {
        e.fn.justifiedGallery = function (t) {
            function i(e, t) {
                return -1 !== e.indexOf(t, e.length - t.length)
            }

            function n(e, t) {
                var n = !1;
                for (var r in t.settings.sizeRangeSuffixes)
                    if (0 !== t.settings.sizeRangeSuffixes[r].length) {
                        if (i(e, t.settings.sizeRangeSuffixes[r])) return t.settings.sizeRangeSuffixes[r]
                    } else n = !0;
                if (n) return "";
                throw "unknown suffix for " + e
            }

            function r(e, t, i, r) {
                var s = e.match(r.settings.extension),
                    a = null != s ? s[0] : "",
                    o = e.replace(r.settings.extension, "");
                return (o = function (e, t) {
                    return e.substring(0, e.length - t.length)
                }(o, n(o, r))) + (function (e, t, i) {
                    var n;
                    return 100 >= (n = e > t ? e : t) ? i.settings.sizeRangeSuffixes.lt100 : 240 >= n ? i.settings.sizeRangeSuffixes.lt240 : 320 >= n ? i.settings.sizeRangeSuffixes.lt320 : 500 >= n ? i.settings.sizeRangeSuffixes.lt500 : 640 >= n ? i.settings.sizeRangeSuffixes.lt640 : i.settings.sizeRangeSuffixes.lt1024
                }(t, i, r) + a)
            }

            function s(t) {
                var i = e(t.currentTarget).find(".caption");
                t.data.settings.cssAnimation ? i.addClass("caption-visible").removeClass("caption-hidden") : i.stop().fadeTo(t.data.settings.captionSettings.animationDuration, t.data.settings.captionSettings.visibleOpacity)
            }

            function a(t) {
                var i = e(t.currentTarget).find(".caption");
                t.data.settings.cssAnimation ? i.removeClass("caption-visible").removeClass("caption-hidden") : i.stop().fadeTo(t.data.settings.captionSettings.animationDuration, t.data.settings.captionSettings.nonVisibleOpacity)
            }

            function o(e, t, i) {
                i.settings.cssAnimation ? (e.addClass("entry-visible"), t()) : e.stop().fadeTo(i.settings.imagesAnimationDuration, 1, t)
            }

            function l(e, t) {
                t.settings.cssAnimation ? e.removeClass("entry-visible") : e.stop().fadeTo(0, 0)
            }

            function d(e) {
                var t = e.find("> img");
                return 0 === t.length && (t = e.find("> a > img")), t
            }

            function c(t, i, n, l, c, u, h) {
                function p() {
                    g !== v && f.attr("src", v)
                }
                var f = d(t);
                f.css("width", l), f.css("height", c), f.css("margin-left", -l / 2), f.css("margin-top", -c / 2), t.width(l), t.height(u), t.css("top", n), t.css("left", i);
                var g = f.attr("src"),
                    v = r(g, l, c, h);
                f.one("error", (function () {
                    f.attr("src", f.data("jg.originalSrc"))
                })), "skipped" === f.data("jg.loaded") ? m(g, (function () {
                    o(t, p, h), f.data("jg.loaded", !0)
                })) : o(t, p, h);
                var y = t.data("jg.captionMouseEvents");
                if (!0 === h.settings.captions) {
                    var b = t.find(".caption");
                    if (0 === b.length) {
                        var w = f.attr("alt");
                        void 0 === w && (w = t.attr("title")), void 0 !== w && (b = e('<div class="caption">' + w + "</div>"), t.append(b))
                    }
                    0 !== b.length && (h.settings.cssAnimation || b.stop().fadeTo(h.settings.imagesAnimationDuration, h.settings.captionSettings.nonVisibleOpacity), void 0 === y && (y = {
                        mouseenter: s,
                        mouseleave: a
                    }, t.on("mouseenter", void 0, h, y.mouseenter), t.on("mouseleave", void 0, h, y.mouseleave), t.data("jg.captionMouseEvents", y)))
                } else void 0 !== y && (t.off("mouseenter", void 0, h, y.mouseenter), t.off("mouseleave", void 0, h, y.mouseleave), t.removeData("jg.captionMouseEvents"))
            }

            function u(e) {
                e.lastAnalyzedIndex = -1, e.buildingRow.entriesBuff = [], e.buildingRow.aspectRatio = 0, e.buildingRow.width = 0, e.offY = e.border
            }

            function h(e, t) {
                var i, n, r, s, a = e.settings,
                    o = e.border;
                if (s = function (e, t) {
                        var i, n, r, s, a, o, l = e.settings,
                            c = !0,
                            u = 0,
                            h = e.galleryWidth - 2 * e.border - (e.buildingRow.entriesBuff.length - 1) * l.margins,
                            p = h / e.buildingRow.aspectRatio,
                            f = e.buildingRow.width / h > l.justifyThreshold;
                        if (t && "hide" === l.lastRow && !f) {
                            for (i = 0; i < e.buildingRow.entriesBuff.length; i++) n = e.buildingRow.entriesBuff[i], l.cssAnimation ? n.removeClass("entry-visible") : n.stop().fadeTo(0, 0);
                            return -1
                        }
                        for (t && !f && "nojustify" === l.lastRow && (c = !1), i = 0; i < e.buildingRow.entriesBuff.length; i++) s = (r = d(e.buildingRow.entriesBuff[i])).data("jg.imgw") / r.data("jg.imgh"), c ? (a = i === e.buildingRow.entriesBuff.length - 1 ? h : p * s, o = p) : (a = l.rowHeight * s, o = l.rowHeight), h -= Math.round(a), r.data("jg.jimgw", Math.round(a)), r.data("jg.jimgh", Math.ceil(o)), (0 === i || u > o) && (u = o);
                        return l.fixedHeight && u > l.rowHeight && (u = l.rowHeight), {
                            minHeight: u,
                            justify: c
                        }
                    }(e, t), r = s.minHeight, t && "hide" === a.lastRow && -1 === r) return e.buildingRow.entriesBuff = [], e.buildingRow.aspectRatio = 0, void(e.buildingRow.width = 0);
                a.maxRowHeight > 0 && a.maxRowHeight < r ? r = a.maxRowHeight : 0 === a.maxRowHeight && 1.5 * a.rowHeight < r && (r = 1.5 * a.rowHeight);
                for (var l = 0; l < e.buildingRow.entriesBuff.length; l++) n = d(i = e.buildingRow.entriesBuff[l]), c(i, o, e.offY, n.data("jg.jimgw"), n.data("jg.jimgh"), r, e), o += n.data("jg.jimgw") + a.margins;
                e.$gallery.height(e.offY + r + e.border + (e.spinner.active ? e.spinner.$el.innerHeight() : 0)), (!t || r <= e.settings.rowHeight && s.justify) && (e.offY += r + e.settings.margins, e.buildingRow.entriesBuff = [], e.buildingRow.aspectRatio = 0, e.buildingRow.width = 0, e.$gallery.trigger("jg.rowflush"))
            }

            function p(e) {
                e.yield.flushed = 0, null !== e.imgAnalyzerTimeout && clearTimeout(e.imgAnalyzerTimeout)
            }

            function f(e, t) {
                p(e), e.imgAnalyzerTimeout = setTimeout((function () {
                    g(e, t)
                }), .001), g(e, t)
            }

            function g(t, i) {
                for (var n, r = t.settings, s = t.lastAnalyzedIndex + 1; s < t.entries.length; s++) {
                    var a = e(t.entries[s]),
                        o = d(a);
                    if (!0 === o.data("jg.loaded") || "skipped" === o.data("jg.loaded")) {
                        n = s >= t.entries.length - 1;
                        var l = t.galleryWidth - 2 * t.border - (t.buildingRow.entriesBuff.length - 1) * r.margins,
                            c = o.data("jg.imgw") / o.data("jg.imgh");
                        if (l / (t.buildingRow.aspectRatio + c) < r.rowHeight && (h(t, n), ++t.yield.flushed >= t.yield.every)) return void f(t, i);
                        t.buildingRow.entriesBuff.push(a), t.buildingRow.aspectRatio += c, t.buildingRow.width += c * r.rowHeight, t.lastAnalyzedIndex = s
                    } else if ("error" !== o.data("jg.loaded")) return
                }
                t.buildingRow.entriesBuff.length > 0 && h(t, !0), t.spinner.active && (t.spinner.active = !1, t.$gallery.height(t.$gallery.height() - t.spinner.$el.innerHeight()), t.spinner.$el.detach(), function (e) {
                    clearInterval(e.intervalId), e.intervalId = null
                }(t.spinner)), p(t), t.$gallery.trigger(i ? "jg.resize" : "jg.complete")
            }

            function m(t, i, n) {
                if (i || n) {
                    var r = new Image,
                        s = e(r);
                    i && s.one("load", (function () {
                        s.off("load error"), i(r)
                    })), n && s.one("error", (function () {
                        s.off("load error"), n(r)
                    })), r.src = t
                }
            }
            var v = {
                sizeRangeSuffixes: {
                    lt100: "",
                    lt240: "",
                    lt320: "",
                    lt500: "",
                    lt640: "",
                    lt1024: ""
                },
                rowHeight: 120,
                maxRowHeight: 0,
                margins: 1,
                border: -1,
                lastRow: "nojustify",
                justifyThreshold: .75,
                fixedHeight: !1,
                waitThumbnailsLoad: !0,
                captions: !0,
                cssAnimation: !1,
                imagesAnimationDuration: 500,
                captionSettings: {
                    animationDuration: 500,
                    visibleOpacity: .7,
                    nonVisibleOpacity: 0
                },
                rel: null,
                target: null,
                extension: /\.[^.\\/]+$/,
                refreshTime: 100,
                randomize: !1
            };
            return this.each((function (i, n) {
                var r = e(n);
                r.addClass("justified-gallery");
                var s = r.data("jg.context");
                if (void 0 === s) {
                    if (null != t && "object" != typeof t) throw "The argument must be an object";
                    var a = e('<div class="spinner"><span></span><span></span><span></span></div>'),
                        o = e.extend({}, v, t),
                        c = o.border >= 0 ? o.border : o.margins;
                    s = {
                        settings: o,
                        imgAnalyzerTimeout: null,
                        entries: null,
                        buildingRow: {
                            entriesBuff: [],
                            width: 0,
                            aspectRatio: 0
                        },
                        lastAnalyzedIndex: -1,
                        yield: {
                            every: 2,
                            flushed: 0
                        },
                        border: c,
                        offY: c,
                        spinner: {
                            active: !1,
                            phase: 0,
                            timeslot: 150,
                            $el: a,
                            $points: a.find("span"),
                            intervalId: null
                        },
                        checkWidthIntervalId: null,
                        galleryWidth: r.width(),
                        $gallery: r
                    }, r.data("jg.context", s)
                } else if ("norewind" === t)
                    for (var h = 0; h < s.buildingRow.entriesBuff.length; h++) l(s.buildingRow.entriesBuff[h], s);
                else s.settings = e.extend({}, s.settings, t), s.border = s.settings.border >= 0 ? s.settings.border : s.settings.margins, u(s);
                if (function (e) {
                        function t(e) {
                            if ("string" != typeof n.sizeRangeSuffixes[e]) throw "sizeRangeSuffixes." + e + " must be a string"
                        }

                        function i(e, t) {
                            if ("string" == typeof e[t]) {
                                if (e[t] = parseFloat(e[t], 10), isNaN(e[t])) throw "invalid number for " + t
                            } else {
                                if ("number" != typeof e[t]) throw t + " must be a number";
                                if (isNaN(e[t])) throw "invalid number for " + t
                            }
                        }
                        var n = e.settings;
                        if ("object" != typeof n.sizeRangeSuffixes) throw "sizeRangeSuffixes must be defined and must be an object";
                        if (t("lt100"), t("lt240"), t("lt320"), t("lt500"), t("lt640"), t("lt1024"), i(n, "rowHeight"), i(n, "maxRowHeight"), n.maxRowHeight > 0 && n.maxRowHeight < n.rowHeight && (n.maxRowHeight = n.rowHeight), i(n, "margins"), i(n, "border"), "nojustify" !== n.lastRow && "justify" !== n.lastRow && "hide" !== n.lastRow) throw 'lastRow must be "nojustify", "justify" or "hide"';
                        if (i(n, "justifyThreshold"), n.justifyThreshold < 0 || n.justifyThreshold > 1) throw "justifyThreshold must be in the interval [0,1]";
                        if ("boolean" != typeof n.cssAnimation) throw "cssAnimation must be a boolean";
                        if (i(n.captionSettings, "animationDuration"), i(n, "imagesAnimationDuration"), i(n.captionSettings, "visibleOpacity"), n.captionSettings.visibleOpacity < 0 || n.captionSettings.visibleOpacity > 1) throw "captionSettings.visibleOpacity must be in the interval [0, 1]";
                        if (i(n.captionSettings, "nonVisibleOpacity"), n.captionSettings.visibleOpacity < 0 || n.captionSettings.visibleOpacity > 1) throw "captionSettings.nonVisibleOpacity must be in the interval [0, 1]";
                        if ("boolean" != typeof n.fixedHeight) throw "fixedHeight must be a boolean";
                        if ("boolean" != typeof n.captions) throw "captions must be a boolean";
                        if (i(n, "refreshTime"), "boolean" != typeof n.randomize) throw "randomize must be a boolean"
                    }(s), s.entries = r.find("> a, > div:not(.spinner)").toArray(), 0 !== s.entries.length) {
                    s.settings.randomize && (s.entries.sort((function () {
                        return 2 * Math.random() - 1
                    })), e.each(s.entries, (function () {
                        e(this).appendTo(r)
                    })));
                    var p = !1,
                        g = !1;
                    e.each(s.entries, (function (t, i) {
                            var n = e(i),
                                a = d(n);
                            if (n.addClass("jg-entry"), !0 !== a.data("jg.loaded") && "skipped" !== a.data("jg.loaded")) {
                                null !== s.settings.rel && n.attr("rel", s.settings.rel), null !== s.settings.target && n.attr("target", s.settings.target);
                                var o = void 0 !== a.data("safe-src") ? a.data("safe-src") : a.attr("src");
                                a.data("jg.originalSrc", o), a.attr("src", o);
                                var l = parseInt(a.attr("width"), 10),
                                    c = parseInt(a.attr("height"), 10);
                                if (!0 !== s.settings.waitThumbnailsLoad && !isNaN(l) && !isNaN(c)) return a.data("jg.imgw", l), a.data("jg.imgh", c), a.data("jg.loaded", "skipped"), g = !0, f(s, !1), !0;
                                a.data("jg.loaded", !1), p = !0, !1 === s.spinner.active && (s.spinner.active = !0, r.append(s.spinner.$el), r.height(s.offY + s.spinner.$el.innerHeight()), function (e) {
                                    clearInterval(e.intervalId), e.intervalId = setInterval((function () {
                                        e.phase < e.$points.length ? e.$points.eq(e.phase).fadeTo(e.timeslot, 1) : e.$points.eq(e.phase - e.$points.length).fadeTo(e.timeslot, 0), e.phase = (e.phase + 1) % (2 * e.$points.length)
                                    }), e.timeslot)
                                }(s.spinner)), m(o, (function (e) {
                                    a.data("jg.imgw", e.width), a.data("jg.imgh", e.height), a.data("jg.loaded", !0), f(s, !1)
                                }), (function () {
                                    a.data("jg.loaded", "error"), f(s, !1)
                                }))
                            }
                        })), p || g || f(s, !1),
                        function (e) {
                            e.checkWidthIntervalId = setInterval((function () {
                                var t = parseInt(e.$gallery.width(), 10);
                                e.galleryWidth !== t && (e.galleryWidth = t, u(e), f(e, !0))
                            }), e.settings.refreshTime)
                        }(s)
                }
            }))
        }
    }(jQuery),
    function (e) {
        "use strict";
        e.fn.emulateTransitionEnd = function (t) {
            var i = !1,
                n = this;
            e(this).one("bsTransitionEnd", (function () {
                i = !0
            }));
            return setTimeout((function () {
                i || e(n).trigger(e.support.transition.end)
            }), t), this
        }, e((function () {
            e.support.transition = function () {
                var e = document.createElement("bootstrap"),
                    t = {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd otransitionend",
                        transition: "transitionend"
                    };
                for (var i in t)
                    if (void 0 !== e.style[i]) return {
                        end: t[i]
                    };
                return !1
            }(), e.support.transition && (e.event.special.bsTransitionEnd = {
                bindType: e.support.transition.end,
                delegateType: e.support.transition.end,
                handle: function (t) {
                    return e(t.target).is(this) ? t.handleObj.handler.apply(this, arguments) : void 0
                }
            })
        }))
    }(jQuery),
    function (e) {
        "use strict";

        function t(t) {
            var i, n = t.attr("data-target") || (i = t.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
            return e(n)
        }

        function i(t) {
            return this.each((function () {
                var i = e(this),
                    r = i.data("bs.collapse"),
                    s = e.extend({}, n.DEFAULTS, i.data(), "object" == typeof t && t);
                !r && s.toggle && /show|hide/.test(t) && (s.toggle = !1), r || i.data("bs.collapse", r = new n(this, s)), "string" == typeof t && r[t]()
            }))
        }
        var n = function (t, i) {
            this.$element = e(t), this.options = e.extend({}, n.DEFAULTS, i), this.$trigger = e('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
        };
        n.VERSION = "3.3.4", n.TRANSITION_DURATION = 350, n.DEFAULTS = {
            toggle: !0
        }, n.prototype.dimension = function () {
            return this.$element.hasClass("width") ? "width" : "height"
        }, n.prototype.show = function () {
            if (!this.transitioning && !this.$element.hasClass("in")) {
                var t, r = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
                if (!(r && r.length && (t = r.data("bs.collapse"), t && t.transitioning))) {
                    var s = e.Event("show.bs.collapse");
                    if (this.$element.trigger(s), !s.isDefaultPrevented()) {
                        r && r.length && (i.call(r, "hide"), t || r.data("bs.collapse", null));
                        var a = this.dimension();
                        this.$element.removeClass("collapse").addClass("collapsing")[a](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                        var o = function () {
                            this.$element.removeClass("collapsing").addClass("collapse in")[a](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                        };
                        if (!e.support.transition) return o.call(this);
                        var l = e.camelCase(["scroll", a].join("-"));
                        this.$element.one("bsTransitionEnd", e.proxy(o, this)).emulateTransitionEnd(n.TRANSITION_DURATION)[a](this.$element[0][l])
                    }
                }
            }
        }, n.prototype.hide = function () {
            if (!this.transitioning && this.$element.hasClass("in")) {
                var t = e.Event("hide.bs.collapse");
                if (this.$element.trigger(t), !t.isDefaultPrevented()) {
                    var i = this.dimension();
                    this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                    var r = function () {
                        this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                    };
                    return e.support.transition ? void this.$element[i](0).one("bsTransitionEnd", e.proxy(r, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : r.call(this)
                }
            }
        }, n.prototype.toggle = function () {
            this[this.$element.hasClass("in") ? "hide" : "show"]()
        }, n.prototype.getParent = function () {
            return e(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(e.proxy((function (i, n) {
                var r = e(n);
                this.addAriaAndCollapsedClass(t(r), r)
            }), this)).end()
        }, n.prototype.addAriaAndCollapsedClass = function (e, t) {
            var i = e.hasClass("in");
            e.attr("aria-expanded", i), t.toggleClass("collapsed", !i).attr("aria-expanded", i)
        };
        var r = e.fn.collapse;
        e.fn.collapse = i, e.fn.collapse.Constructor = n, e.fn.collapse.noConflict = function () {
            return e.fn.collapse = r, this
        }, e(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', (function (n) {
            var r = e(this);
            r.attr("data-target") || n.preventDefault();
            var s = t(r),
                a = s.data("bs.collapse") ? "toggle" : r.data();
            i.call(s, a)
        }))
    }(jQuery),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(e.jQuery)
    }(this, (function (e) {
        ! function () {
            "use strict";

            function t(t, n) {
                if (this.el = t, this.$el = e(t), this.s = e.extend({}, i, n), this.s.dynamic && "undefined" !== this.s.dynamicEl && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) throw "When using dynamic mode, you must also define dynamicEl as an Array.";
                return this.modules = {}, this.lGalleryOn = !1, this.lgBusy = !1, this.hideBartimeout = !1, this.isTouch = "ontouchstart" in document.documentElement, this.s.slideEndAnimatoin && (this.s.hideControlOnEnd = !1), this.s.dynamic ? this.$items = this.s.dynamicEl : "this" === this.s.selector ? this.$items = this.$el : "" !== this.s.selector ? this.s.selectWithin ? this.$items = e(this.s.selectWithin).find(this.s.selector) : this.$items = this.$el.find(e(this.s.selector)) : this.$items = this.$el.children(), this.$slide = "", this.$outer = "", this.init(), this
            }
            var i = {
                mode: "lg-slide",
                cssEasing: "ease",
                easing: "linear",
                speed: 600,
                height: "100%",
                width: "100%",
                addClass: "",
                startClass: "lg-start-zoom",
                backdropDuration: 150,
                hideBarsDelay: 6e3,
                useLeft: !1,
                closable: !0,
                loop: !0,
                escKey: !0,
                keyPress: !0,
                controls: !0,
                slideEndAnimatoin: !0,
                hideControlOnEnd: !1,
                mousewheel: !0,
                getCaptionFromTitleOrAlt: !0,
                appendSubHtmlTo: ".lg-sub-html",
                subHtmlSelectorRelative: !1,
                preload: 1,
                showAfterLoad: !0,
                selector: "",
                selectWithin: "",
                nextHtml: "",
                prevHtml: "",
                index: !1,
                iframeMaxWidth: "100%",
                download: !0,
                counter: !0,
                appendCounterTo: ".lg-toolbar",
                swipeThreshold: 50,
                enableSwipe: !0,
                enableDrag: !0,
                dynamic: !1,
                dynamicEl: [],
                galleryId: 1
            };
            t.prototype.init = function () {
                var t = this;
                t.s.preload > t.$items.length && (t.s.preload = t.$items.length);
                var i = window.location.hash;
                i.indexOf("lg=" + this.s.galleryId) > 0 && (t.index = parseInt(i.split("&slide=")[1], 10), e("body").addClass("lg-from-hash"), e("body").hasClass("lg-on") || (setTimeout((function () {
                    t.build(t.index)
                })), e("body").addClass("lg-on"))), t.s.dynamic ? (t.$el.trigger("onBeforeOpen.lg"), t.index = t.s.index || 0, e("body").hasClass("lg-on") || setTimeout((function () {
                    t.build(t.index), e("body").addClass("lg-on")
                }))) : t.$items.on("click.lgcustom", (function (i) {
                    try {
                        i.preventDefault(), i.preventDefault()
                    } catch (e) {
                        i.returnValue = !1
                    }
                    t.$el.trigger("onBeforeOpen.lg"), t.index = t.s.index || t.$items.index(this), e("body").hasClass("lg-on") || (t.build(t.index), e("body").addClass("lg-on"))
                }))
            }, t.prototype.build = function (t) {
                var i = this;
                i.structure(), e.each(e.fn.lightGallery.modules, (function (t) {
                    i.modules[t] = new e.fn.lightGallery.modules[t](i.el)
                })), i.slide(t, !1, !1, !1), i.s.keyPress && i.keyPress(), i.$items.length > 1 ? (i.arrow(), setTimeout((function () {
                    i.enableDrag(), i.enableSwipe()
                }), 50), i.s.mousewheel && i.mousewheel()) : i.$slide.on("click.lg", (function () {
                    i.$el.trigger("onSlideClick.lg")
                })), i.counter(), i.closeGallery(), i.$el.trigger("onAfterOpen.lg"), i.$outer.on("mousemove.lg click.lg touchstart.lg", (function () {
                    i.$outer.removeClass("lg-hide-items"), clearTimeout(i.hideBartimeout), i.hideBartimeout = setTimeout((function () {
                        i.$outer.addClass("lg-hide-items")
                    }), i.s.hideBarsDelay)
                })), i.$outer.trigger("mousemove.lg")
            }, t.prototype.structure = function () {
                var t, i = "",
                    n = "",
                    r = 0,
                    s = "",
                    a = this;
                for (e("body").append('<div class="lg-backdrop"></div>'), e(".lg-backdrop").css("transition-duration", this.s.backdropDuration + "ms"), r = 0; r < this.$items.length; r++) i += '<div class="lg-item"></div>';
                if (this.s.controls && this.$items.length > 1 && (n = '<div class="lg-actions"><button class="lg-prev lg-icon">' + this.s.prevHtml + '</button><button class="lg-next lg-icon">' + this.s.nextHtml + "</button></div>"), ".lg-sub-html" === this.s.appendSubHtmlTo && (s = '<div class="lg-sub-html"></div>'), t = '<div class="lg-outer ' + this.s.addClass + " " + this.s.startClass + '"><div class="lg" style="width:' + this.s.width + "; height:" + this.s.height + '"><div class="lg-inner">' + i + '</div><div class="lg-toolbar lg-group"><span class="lg-close lg-icon"></span></div>' + n + s + "</div></div>", e("body").append(t), this.$outer = e(".lg-outer"), this.$slide = this.$outer.find(".lg-item"), this.s.useLeft ? (this.$outer.addClass("lg-use-left"), this.s.mode = "lg-slide") : this.$outer.addClass("lg-use-css3"), a.setTop(), e(window).on("resize.lg orientationchange.lg", (function () {
                        setTimeout((function () {
                            a.setTop()
                        }), 100)
                    })), this.$slide.eq(this.index).addClass("lg-current"), this.doCss() ? this.$outer.addClass("lg-css3") : (this.$outer.addClass("lg-css"), this.s.speed = 0), this.$outer.addClass(this.s.mode), this.s.enableDrag && this.$items.length > 1 && this.$outer.addClass("lg-grab"), this.s.showAfterLoad && this.$outer.addClass("lg-show-after-load"), this.doCss()) {
                    var o = this.$outer.find(".lg-inner");
                    o.css("transition-timing-function", this.s.cssEasing), o.css("transition-duration", this.s.speed + "ms")
                }
                setTimeout((function () {
                    e(".lg-backdrop").addClass("in")
                })), setTimeout((function () {
                    a.$outer.addClass("lg-visible")
                }), this.s.backdropDuration), this.s.download && this.$outer.find(".lg-toolbar").append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>'), this.prevScrollTop = e(window).scrollTop()
            }, t.prototype.setTop = function () {
                if ("100%" !== this.s.height) {
                    var t = e(window).height(),
                        i = (t - parseInt(this.s.height, 10)) / 2,
                        n = this.$outer.find(".lg");
                    t >= parseInt(this.s.height, 10) ? n.css("top", i + "px") : n.css("top", "0px")
                }
            }, t.prototype.doCss = function () {
                return !! function () {
                    var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"],
                        t = document.documentElement,
                        i = 0;
                    for (i = 0; i < e.length; i++)
                        if (e[i] in t.style) return !0
                }()
            }, t.prototype.isVideo = function (e, t) {
                var i;
                if (i = this.s.dynamic ? this.s.dynamicEl[t].html : this.$items.eq(t).attr("data-html"), !e) return i ? {
                    html5: !0
                } : (console.error("lightGallery :- data-src is not pvovided on slide item " + (t + 1) + ". Please make sure the selector property is properly configured. More info - http://sachinchoolur.github.io/lightGallery/demos/html-markup.html"), !1);
                var n = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i),
                    r = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
                    s = e.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i),
                    a = e.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);
                return n ? {
                    youtube: n
                } : r ? {
                    vimeo: r
                } : s ? {
                    dailymotion: s
                } : a ? {
                    vk: a
                } : void 0
            }, t.prototype.counter = function () {
                this.s.counter && e(this.s.appendCounterTo).append('<div id="lg-counter"><span id="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span id="lg-counter-all">' + this.$items.length + "</span></div>")
            }, t.prototype.addHtml = function (t) {
                var i, n, r = null;
                if (this.s.dynamic ? this.s.dynamicEl[t].subHtmlUrl ? i = this.s.dynamicEl[t].subHtmlUrl : r = this.s.dynamicEl[t].subHtml : (n = this.$items.eq(t)).attr("data-sub-html-url") ? i = n.attr("data-sub-html-url") : (r = n.attr("data-sub-html"), this.s.getCaptionFromTitleOrAlt && !r && (r = n.attr("title") || n.find("img").first().attr("alt"))), !i)
                    if (null != r) {
                        var s = r.substring(0, 1);
                        "." !== s && "#" !== s || (r = this.s.subHtmlSelectorRelative && !this.s.dynamic ? n.find(r).html() : e(r).html())
                    } else r = "";
                ".lg-sub-html" === this.s.appendSubHtmlTo ? i ? this.$outer.find(this.s.appendSubHtmlTo).load(i) : this.$outer.find(this.s.appendSubHtmlTo).html(r) : i ? this.$slide.eq(t).load(i) : this.$slide.eq(t).append(r), null != r && ("" === r ? this.$outer.find(this.s.appendSubHtmlTo).addClass("lg-empty-html") : this.$outer.find(this.s.appendSubHtmlTo).removeClass("lg-empty-html")), this.$el.trigger("onAfterAppendSubHtml.lg", [t])
            }, t.prototype.preload = function (e) {
                var t = 1,
                    i = 1;
                for (t = 1; t <= this.s.preload && !(t >= this.$items.length - e); t++) this.loadContent(e + t, !1, 0);
                for (i = 1; i <= this.s.preload && !(e - i < 0); i++) this.loadContent(e - i, !1, 0)
            }, t.prototype.loadContent = function (t, i, n) {
                var r, s, a, o, l, d, c = this,
                    u = !1,
                    h = function (t) {
                        for (var i = [], n = [], r = 0; r < t.length; r++) {
                            var a = t[r].split(" ");
                            "" === a[0] && a.splice(0, 1), n.push(a[0]), i.push(a[1])
                        }
                        for (var o = e(window).width(), l = 0; l < i.length; l++)
                            if (parseInt(i[l], 10) > o) {
                                s = n[l];
                                break
                            }
                    };
                if (c.s.dynamic) {
                    if (c.s.dynamicEl[t].poster && (u = !0, a = c.s.dynamicEl[t].poster), d = c.s.dynamicEl[t].html, s = c.s.dynamicEl[t].src, c.s.dynamicEl[t].responsive) h(c.s.dynamicEl[t].responsive.split(","));
                    o = c.s.dynamicEl[t].srcset, l = c.s.dynamicEl[t].sizes
                } else {
                    if (c.$items.eq(t).attr("data-poster") && (u = !0, a = c.$items.eq(t).attr("data-poster")), d = c.$items.eq(t).attr("data-html"), s = c.$items.eq(t).attr("href") || c.$items.eq(t).attr("data-src"), c.$items.eq(t).attr("data-responsive")) h(c.$items.eq(t).attr("data-responsive").split(","));
                    o = c.$items.eq(t).attr("data-srcset"), l = c.$items.eq(t).attr("data-sizes")
                }
                var p = !1;
                c.s.dynamic ? c.s.dynamicEl[t].iframe && (p = !0) : "true" === c.$items.eq(t).attr("data-iframe") && (p = !0);
                var f = c.isVideo(s, t);
                if (!c.$slide.eq(t).hasClass("lg-loaded")) {
                    if (p) c.$slide.eq(t).prepend('<div class="lg-video-cont lg-has-iframe" style="max-width:' + c.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + s + '"  allowfullscreen="true"></iframe></div></div>');
                    else if (u) {
                        var g;
                        g = f && f.youtube ? "lg-has-youtube" : f && f.vimeo ? "lg-has-vimeo" : "lg-has-html5", c.$slide.eq(t).prepend('<div class="lg-video-cont ' + g + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + a + '" /></div></div>')
                    } else f ? (c.$slide.eq(t).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>'), c.$el.trigger("hasVideo.lg", [t, s, d])) : c.$slide.eq(t).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + s + '" /></div>');
                    if (c.$el.trigger("onAferAppendSlide.lg", [t]), r = c.$slide.eq(t).find(".lg-object"), l && r.attr("sizes", l), o) {
                        r.attr("srcset", o);
                        try {
                            picturefill({
                                elements: [r[0]]
                            })
                        } catch (e) {
                            console.warn("lightGallery :- If you want srcset to be supported for older browser please include picturefil version 2 javascript library in your document.")
                        }
                    }
                    ".lg-sub-html" !== this.s.appendSubHtmlTo && c.addHtml(t), c.$slide.eq(t).addClass("lg-loaded")
                }
                c.$slide.eq(t).find(".lg-object").on("load.lg error.lg", (function () {
                    var i = 0;
                    n && !e("body").hasClass("lg-from-hash") && (i = n), setTimeout((function () {
                        c.$slide.eq(t).addClass("lg-complete"), c.$el.trigger("onSlideItemLoad.lg", [t, n || 0])
                    }), i)
                })), f && f.html5 && !u && c.$slide.eq(t).addClass("lg-complete"), !0 === i && (c.$slide.eq(t).hasClass("lg-complete") ? c.preload(t) : c.$slide.eq(t).find(".lg-object").on("load.lg error.lg", (function () {
                    c.preload(t)
                })))
            }, t.prototype.slide = function (t, i, n, r) {
                var s = this.$outer.find(".lg-current").index(),
                    a = this;
                if (!a.lGalleryOn || s !== t) {
                    var o = this.$slide.length,
                        l = a.lGalleryOn ? this.s.speed : 0;
                    if (!a.lgBusy) {
                        var d, c, u;
                        if (this.s.download)(d = a.s.dynamic ? !1 !== a.s.dynamicEl[t].downloadUrl && (a.s.dynamicEl[t].downloadUrl || a.s.dynamicEl[t].src) : "false" !== a.$items.eq(t).attr("data-download-url") && (a.$items.eq(t).attr("data-download-url") || a.$items.eq(t).attr("href") || a.$items.eq(t).attr("data-src"))) ? (e("#lg-download").attr("href", d), a.$outer.removeClass("lg-hide-download")) : a.$outer.addClass("lg-hide-download");
                        if (this.$el.trigger("onBeforeSlide.lg", [s, t, i, n]), a.lgBusy = !0, clearTimeout(a.hideBartimeout), ".lg-sub-html" === this.s.appendSubHtmlTo && setTimeout((function () {
                                a.addHtml(t)
                            }), l), this.arrowDisable(t), r || (t < s ? r = "prev" : t > s && (r = "next")), i) this.$slide.removeClass("lg-prev-slide lg-current lg-next-slide"), o > 2 ? (c = t - 1, u = t + 1, (0 === t && s === o - 1 || t === o - 1 && 0 === s) && (u = 0, c = o - 1)) : (c = 0, u = 1), "prev" === r ? a.$slide.eq(u).addClass("lg-next-slide") : a.$slide.eq(c).addClass("lg-prev-slide"), a.$slide.eq(t).addClass("lg-current");
                        else a.$outer.addClass("lg-no-trans"), this.$slide.removeClass("lg-prev-slide lg-next-slide"), "prev" === r ? (this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(s).addClass("lg-next-slide")) : (this.$slide.eq(t).addClass("lg-next-slide"), this.$slide.eq(s).addClass("lg-prev-slide")), setTimeout((function () {
                            a.$slide.removeClass("lg-current"), a.$slide.eq(t).addClass("lg-current"), a.$outer.removeClass("lg-no-trans")
                        }), 50);
                        a.lGalleryOn ? (setTimeout((function () {
                            a.loadContent(t, !0, 0)
                        }), this.s.speed + 50), setTimeout((function () {
                            a.lgBusy = !1, a.$el.trigger("onAfterSlide.lg", [s, t, i, n])
                        }), this.s.speed)) : (a.loadContent(t, !0, a.s.backdropDuration), a.lgBusy = !1, a.$el.trigger("onAfterSlide.lg", [s, t, i, n])), a.lGalleryOn = !0, this.s.counter && e("#lg-counter-current").text(t + 1)
                    }
                }
            }, t.prototype.goToNextSlide = function (e) {
                var t = this,
                    i = t.s.loop;
                e && t.$slide.length < 3 && (i = !1), t.lgBusy || (t.index + 1 < t.$slide.length ? (t.index++, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : i ? (t.index = 0, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-right-end"), setTimeout((function () {
                    t.$outer.removeClass("lg-right-end")
                }), 400)))
            }, t.prototype.goToPrevSlide = function (e) {
                var t = this,
                    i = t.s.loop;
                e && t.$slide.length < 3 && (i = !1), t.lgBusy || (t.index > 0 ? (t.index--, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : i ? (t.index = t.$items.length - 1, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-left-end"), setTimeout((function () {
                    t.$outer.removeClass("lg-left-end")
                }), 400)))
            }, t.prototype.keyPress = function () {
                var t = this;
                this.$items.length > 1 && e(window).on("keyup.lg", (function (e) {
                    t.$items.length > 1 && (37 === e.keyCode && (e.preventDefault(), t.goToPrevSlide()), 39 === e.keyCode && (e.preventDefault(), t.goToNextSlide()))
                })), e(window).on("keydown.lg", (function (e) {
                    !0 === t.s.escKey && 27 === e.keyCode && (e.preventDefault(), t.$outer.hasClass("lg-thumb-open") ? t.$outer.removeClass("lg-thumb-open") : t.destroy())
                }))
            }, t.prototype.arrow = function () {
                var e = this;
                this.$outer.find(".lg-prev").on("click.lg", (function () {
                    e.goToPrevSlide()
                })), this.$outer.find(".lg-next").on("click.lg", (function () {
                    e.goToNextSlide()
                }))
            }, t.prototype.arrowDisable = function (e) {
                !this.s.loop && this.s.hideControlOnEnd && (e + 1 < this.$slide.length ? this.$outer.find(".lg-next").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-next").attr("disabled", "disabled").addClass("disabled"), e > 0 ? this.$outer.find(".lg-prev").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-prev").attr("disabled", "disabled").addClass("disabled"))
            }, t.prototype.setTranslate = function (e, t, i) {
                this.s.useLeft ? e.css("left", t) : e.css({
                    transform: "translate3d(" + t + "px, " + i + "px, 0px)"
                })
            }, t.prototype.touchMove = function (t, i) {
                var n = i - t;
                Math.abs(n) > 15 && (this.$outer.addClass("lg-dragging"), this.setTranslate(this.$slide.eq(this.index), n, 0), this.setTranslate(e(".lg-prev-slide"), -this.$slide.eq(this.index).width() + n, 0), this.setTranslate(e(".lg-next-slide"), this.$slide.eq(this.index).width() + n, 0))
            }, t.prototype.touchEnd = function (e) {
                var t = this;
                "lg-slide" !== t.s.mode && t.$outer.addClass("lg-slide"), this.$slide.not(".lg-current, .lg-prev-slide, .lg-next-slide").css("opacity", "0"), setTimeout((function () {
                    t.$outer.removeClass("lg-dragging"), e < 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToNextSlide(!0) : e > 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToPrevSlide(!0) : Math.abs(e) < 5 && t.$el.trigger("onSlideClick.lg"), t.$slide.removeAttr("style")
                })), setTimeout((function () {
                    t.$outer.hasClass("lg-dragging") || "lg-slide" === t.s.mode || t.$outer.removeClass("lg-slide")
                }), t.s.speed + 100)
            }, t.prototype.enableSwipe = function () {
                var e = this,
                    t = 0,
                    i = 0,
                    n = !1;
                e.s.enableSwipe && e.doCss() && (e.$slide.on("touchstart.lg", (function (i) {
                    e.$outer.hasClass("lg-zoomed") || e.lgBusy || (i.preventDefault(), e.manageSwipeClass(), t = i.originalEvent.targetTouches[0].pageX)
                })), e.$slide.on("touchmove.lg", (function (r) {
                    e.$outer.hasClass("lg-zoomed") || (r.preventDefault(), i = r.originalEvent.targetTouches[0].pageX, e.touchMove(t, i), n = !0)
                })), e.$slide.on("touchend.lg", (function () {
                    e.$outer.hasClass("lg-zoomed") || (n ? (n = !1, e.touchEnd(i - t)) : e.$el.trigger("onSlideClick.lg"))
                })))
            }, t.prototype.enableDrag = function () {
                var t = this,
                    i = 0,
                    n = 0,
                    r = !1,
                    s = !1;
                t.s.enableDrag && t.doCss() && (t.$slide.on("mousedown.lg", (function (n) {
                    t.$outer.hasClass("lg-zoomed") || (e(n.target).hasClass("lg-object") || e(n.target).hasClass("lg-video-play")) && (n.preventDefault(), t.lgBusy || (t.manageSwipeClass(), i = n.pageX, r = !0, t.$outer.scrollLeft += 1, t.$outer.scrollLeft -= 1, t.$outer.removeClass("lg-grab").addClass("lg-grabbing"), t.$el.trigger("onDragstart.lg")))
                })), e(window).on("mousemove.lg", (function (e) {
                    r && (s = !0, n = e.pageX, t.touchMove(i, n), t.$el.trigger("onDragmove.lg"))
                })), e(window).on("mouseup.lg", (function (a) {
                    s ? (s = !1, t.touchEnd(n - i), t.$el.trigger("onDragend.lg")) : (e(a.target).hasClass("lg-object") || e(a.target).hasClass("lg-video-play")) && t.$el.trigger("onSlideClick.lg"), r && (r = !1, t.$outer.removeClass("lg-grabbing").addClass("lg-grab"))
                })))
            }, t.prototype.manageSwipeClass = function () {
                var e = this.index + 1,
                    t = this.index - 1;
                this.s.loop && this.$slide.length > 2 && (0 === this.index ? t = this.$slide.length - 1 : this.index === this.$slide.length - 1 && (e = 0)), this.$slide.removeClass("lg-next-slide lg-prev-slide"), t > -1 && this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(e).addClass("lg-next-slide")
            }, t.prototype.mousewheel = function () {
                var e = this;
                e.$outer.on("mousewheel.lg", (function (t) {
                    t.deltaY && (t.deltaY > 0 ? e.goToPrevSlide() : e.goToNextSlide(), t.preventDefault())
                }))
            }, t.prototype.closeGallery = function () {
                var t = this,
                    i = !1;
                this.$outer.find(".lg-close").on("click.lg", (function () {
                    t.destroy()
                })), t.s.closable && (t.$outer.on("mousedown.lg", (function (t) {
                    i = !!(e(t.target).is(".lg-outer") || e(t.target).is(".lg-item ") || e(t.target).is(".lg-img-wrap"))
                })), t.$outer.on("mouseup.lg", (function (n) {
                    (e(n.target).is(".lg-outer") || e(n.target).is(".lg-item ") || e(n.target).is(".lg-img-wrap") && i) && (t.$outer.hasClass("lg-dragging") || t.destroy())
                })))
            }, t.prototype.destroy = function (t) {
                var i = this;
                t || (i.$el.trigger("onBeforeClose.lg"), e(window).scrollTop(i.prevScrollTop)), t && (i.s.dynamic || this.$items.off("click.lg click.lgcustom"), e.removeData(i.el, "lightGallery")), this.$el.off(".lg.tm"), e.each(e.fn.lightGallery.modules, (function (e) {
                    i.modules[e] && i.modules[e].destroy()
                })), this.lGalleryOn = !1, clearTimeout(i.hideBartimeout), this.hideBartimeout = !1, e(window).off(".lg"), e("body").removeClass("lg-on lg-from-hash"), i.$outer && i.$outer.removeClass("lg-visible"), e(".lg-backdrop").removeClass("in"), setTimeout((function () {
                    i.$outer && i.$outer.remove(), e(".lg-backdrop").remove(), t || i.$el.trigger("onCloseAfter.lg")
                }), i.s.backdropDuration + 50)
            }, e.fn.lightGallery = function (i) {
                return this.each((function () {
                    if (e.data(this, "lightGallery")) try {
                        e(this).data("lightGallery").init()
                    } catch (e) {
                        console.error("lightGallery has not initiated properly")
                    } else e.data(this, "lightGallery", new t(this, i))
                }))
            }, e.fn.lightGallery.modules = {}
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    autoplay: !1,
                    pause: 5e3,
                    progressBar: !0,
                    fourceAutoplay: !1,
                    autoplayControls: !0,
                    appendAutoplayControlsTo: ".lg-toolbar"
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.$el = e(i), !(this.core.$items.length < 2) && (this.core.s = e.extend({}, t, this.core.s), this.interval = !1, this.fromAuto = !0, this.canceledOnTouch = !1, this.fourceAutoplayTemp = this.core.s.fourceAutoplay, this.core.doCss() || (this.core.s.progressBar = !1), this.init(), this)
                };
            i.prototype.init = function () {
                var e = this;
                e.core.s.autoplayControls && e.controls(), e.core.s.progressBar && e.core.$outer.find(".lg").append('<div class="lg-progress-bar"><div class="lg-progress"></div></div>'), e.progress(), e.core.s.autoplay && e.$el.one("onSlideItemLoad.lg.tm", (function () {
                    e.startlAuto()
                })), e.$el.on("onDragstart.lg.tm touchstart.lg.tm", (function () {
                    e.interval && (e.cancelAuto(), e.canceledOnTouch = !0)
                })), e.$el.on("onDragend.lg.tm touchend.lg.tm onSlideClick.lg.tm", (function () {
                    !e.interval && e.canceledOnTouch && (e.startlAuto(), e.canceledOnTouch = !1)
                }))
            }, i.prototype.progress = function () {
                var e, t, i = this;
                i.$el.on("onBeforeSlide.lg.tm", (function () {
                    i.core.s.progressBar && i.fromAuto && (e = i.core.$outer.find(".lg-progress-bar"), t = i.core.$outer.find(".lg-progress"), i.interval && (t.removeAttr("style"), e.removeClass("lg-start"), setTimeout((function () {
                        t.css("transition", "width " + (i.core.s.speed + i.core.s.pause) + "ms ease 0s"), e.addClass("lg-start")
                    }), 20))), i.fromAuto || i.core.s.fourceAutoplay || i.cancelAuto(), i.fromAuto = !1
                }))
            }, i.prototype.controls = function () {
                var t = this;
                e(this.core.s.appendAutoplayControlsTo).append('<span class="lg-autoplay-button lg-icon"></span>'), t.core.$outer.find(".lg-autoplay-button").on("click.lg", (function () {
                    e(t.core.$outer).hasClass("lg-show-autoplay") ? (t.cancelAuto(), t.core.s.fourceAutoplay = !1) : t.interval || (t.startlAuto(), t.core.s.fourceAutoplay = t.fourceAutoplayTemp)
                }))
            }, i.prototype.startlAuto = function () {
                var e = this;
                e.core.$outer.find(".lg-progress").css("transition", "width " + (e.core.s.speed + e.core.s.pause) + "ms ease 0s"), e.core.$outer.addClass("lg-show-autoplay"), e.core.$outer.find(".lg-progress-bar").addClass("lg-start"), e.interval = setInterval((function () {
                    e.core.index + 1 < e.core.$items.length ? e.core.index++ : e.core.index = 0, e.fromAuto = !0, e.core.slide(e.core.index, !1, !1, "next")
                }), e.core.s.speed + e.core.s.pause)
            }, i.prototype.cancelAuto = function () {
                clearInterval(this.interval), this.interval = !1, this.core.$outer.find(".lg-progress").removeAttr("style"), this.core.$outer.removeClass("lg-show-autoplay"), this.core.$outer.find(".lg-progress-bar").removeClass("lg-start")
            }, i.prototype.destroy = function () {
                this.cancelAuto(), this.core.$outer.find(".lg-progress-bar").remove()
            }, e.fn.lightGallery.modules.autoplay = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    fullScreen: !0
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.$el = e(i), this.core.s = e.extend({}, t, this.core.s), this.init(), this
                };
            i.prototype.init = function () {
                var e = "";
                if (this.core.s.fullScreen) {
                    if (!(document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled)) return;
                    e = '<span class="lg-fullscreen lg-icon"></span>', this.core.$outer.find(".lg-toolbar").append(e), this.fullScreen()
                }
            }, i.prototype.requestFullscreen = function () {
                var e = document.documentElement;
                e.requestFullscreen ? e.requestFullscreen() : e.msRequestFullscreen ? e.msRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.webkitRequestFullscreen && e.webkitRequestFullscreen()
            }, i.prototype.exitFullscreen = function () {
                document.exitFullscreen ? document.exitFullscreen() : document.msExitFullscreen ? document.msExitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen()
            }, i.prototype.fullScreen = function () {
                var t = this;
                e(document).on("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg", (function () {
                    t.core.$outer.toggleClass("lg-fullscreen-on")
                })), this.core.$outer.find(".lg-fullscreen").on("click.lg", (function () {
                    document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement ? t.exitFullscreen() : t.requestFullscreen()
                }))
            }, i.prototype.destroy = function () {
                this.exitFullscreen(), e(document).off("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg")
            }, e.fn.lightGallery.modules.fullscreen = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    pager: !1
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.$el = e(i), this.core.s = e.extend({}, t, this.core.s), this.core.s.pager && this.core.$items.length > 1 && this.init(), this
                };
            i.prototype.init = function () {
                var t, i, n, r = this,
                    s = "";
                if (r.core.$outer.find(".lg").append('<div class="lg-pager-outer"></div>'), r.core.s.dynamic)
                    for (var a = 0; a < r.core.s.dynamicEl.length; a++) s += '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + r.core.s.dynamicEl[a].thumb + '" /></div></span>';
                else r.core.$items.each((function () {
                    s += r.core.s.exThumbImage ? '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + e(this).attr(r.core.s.exThumbImage) + '" /></div></span>' : '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + e(this).find("img").attr("src") + '" /></div></span>'
                }));
                (i = r.core.$outer.find(".lg-pager-outer")).html(s), (t = r.core.$outer.find(".lg-pager-cont")).on("click.lg touchend.lg", (function () {
                    var t = e(this);
                    r.core.index = t.index(), r.core.slide(r.core.index, !1, !0, !1)
                })), i.on("mouseover.lg", (function () {
                    clearTimeout(n), i.addClass("lg-pager-hover")
                })), i.on("mouseout.lg", (function () {
                    n = setTimeout((function () {
                        i.removeClass("lg-pager-hover")
                    }))
                })), r.core.$el.on("onBeforeSlide.lg.tm", (function (e, i, n) {
                    t.removeClass("lg-pager-active"), t.eq(n).addClass("lg-pager-active")
                }))
            }, i.prototype.destroy = function () {}, e.fn.lightGallery.modules.pager = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    thumbnail: !0,
                    animateThumb: !0,
                    currentPagerPosition: "middle",
                    thumbWidth: 100,
                    thumbHeight: "80px",
                    thumbContHeight: 100,
                    thumbMargin: 5,
                    exThumbImage: !1,
                    showThumbByDefault: !0,
                    toogleThumb: !0,
                    pullCaptionUp: !0,
                    enableThumbDrag: !0,
                    enableThumbSwipe: !0,
                    swipeThreshold: 50,
                    loadYoutubeThumbnail: !0,
                    youtubeThumbSize: 1,
                    loadVimeoThumbnail: !0,
                    vimeoThumbSize: "thumbnail_small",
                    loadDailymotionThumbnail: !0
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.$el = e(i), this.$thumbOuter = null, this.thumbOuterWidth = 0, this.thumbTotalWidth = this.core.$items.length * (this.core.s.thumbWidth + this.core.s.thumbMargin), this.thumbIndex = this.core.index, this.core.s.animateThumb && (this.core.s.thumbHeight = "100%"), this.left = 0, this.init(), this
                };
            i.prototype.init = function () {
                var e = this;
                this.core.s.thumbnail && this.core.$items.length > 1 && (this.core.s.showThumbByDefault && setTimeout((function () {
                    e.core.$outer.addClass("lg-thumb-open")
                }), 700), this.core.s.pullCaptionUp && this.core.$outer.addClass("lg-pull-caption-up"), this.build(), this.core.s.animateThumb && this.core.doCss() ? (this.core.s.enableThumbDrag && this.enableThumbDrag(), this.core.s.enableThumbSwipe && this.enableThumbSwipe(), this.thumbClickable = !1) : this.thumbClickable = !0, this.toogle(), this.thumbkeyPress())
            }, i.prototype.build = function () {
                function t(e, t, i) {
                    var a, o = n.core.isVideo(e, i) || {},
                        l = "";
                    o.youtube || o.vimeo || o.dailymotion ? o.youtube ? a = n.core.s.loadYoutubeThumbnail ? "//img.youtube.com/vi/" + o.youtube[1] + "/" + n.core.s.youtubeThumbSize + ".jpg" : t : o.vimeo ? n.core.s.loadVimeoThumbnail ? (a = "//i.vimeocdn.com/video/error_" + s + ".jpg", l = o.vimeo[1]) : a = t : o.dailymotion && (a = n.core.s.loadDailymotionThumbnail ? "//www.dailymotion.com/thumbnail/video/" + o.dailymotion[1] : t) : a = t, r += '<div data-vimeo-id="' + l + '" class="lg-thumb-item" style="width:' + n.core.s.thumbWidth + "px; height: " + n.core.s.thumbHeight + "; margin-right: " + n.core.s.thumbMargin + 'px"><img src="' + a + '" /></div>', l = ""
                }
                var i, n = this,
                    r = "",
                    s = "";
                switch (this.core.s.vimeoThumbSize) {
                    case "thumbnail_large":
                        s = "640";
                        break;
                    case "thumbnail_medium":
                        s = "200x150";
                        break;
                    case "thumbnail_small":
                        s = "100x75"
                }
                if (n.core.$outer.addClass("lg-has-thumb"), n.core.$outer.find(".lg").append('<div class="lg-thumb-outer"><div class="lg-thumb lg-group"></div></div>'), n.$thumbOuter = n.core.$outer.find(".lg-thumb-outer"), n.thumbOuterWidth = n.$thumbOuter.width(), n.core.s.animateThumb && n.core.$outer.find(".lg-thumb").css({
                        width: n.thumbTotalWidth + "px",
                        position: "relative"
                    }), this.core.s.animateThumb && n.$thumbOuter.css("height", n.core.s.thumbContHeight + "px"), n.core.s.dynamic)
                    for (var a = 0; a < n.core.s.dynamicEl.length; a++) t(n.core.s.dynamicEl[a].src, n.core.s.dynamicEl[a].thumb, a);
                else n.core.$items.each((function (i) {
                    n.core.s.exThumbImage ? t(e(this).attr("href") || e(this).attr("data-src"), e(this).attr(n.core.s.exThumbImage), i) : t(e(this).attr("href") || e(this).attr("data-src"), e(this).find("img").attr("src"), i)
                }));
                n.core.$outer.find(".lg-thumb").html(r), (i = n.core.$outer.find(".lg-thumb-item")).each((function () {
                    var t = e(this),
                        i = t.attr("data-vimeo-id");
                    i && e.getJSON("//www.vimeo.com/api/v2/video/" + i + ".json?callback=?", {
                        format: "json"
                    }, (function (e) {
                        t.find("img").attr("src", e[0][n.core.s.vimeoThumbSize])
                    }))
                })), i.eq(n.core.index).addClass("active"), n.core.$el.on("onBeforeSlide.lg.tm", (function () {
                    i.removeClass("active"), i.eq(n.core.index).addClass("active")
                })), i.on("click.lg touchend.lg", (function () {
                    var t = e(this);
                    setTimeout((function () {
                        (n.thumbClickable && !n.core.lgBusy || !n.core.doCss()) && (n.core.index = t.index(), n.core.slide(n.core.index, !1, !0, !1))
                    }), 50)
                })), n.core.$el.on("onBeforeSlide.lg.tm", (function () {
                    n.animateThumb(n.core.index)
                })), e(window).on("resize.lg.thumb orientationchange.lg.thumb", (function () {
                    setTimeout((function () {
                        n.animateThumb(n.core.index), n.thumbOuterWidth = n.$thumbOuter.width()
                    }), 200)
                }))
            }, i.prototype.setTranslate = function (e) {
                this.core.$outer.find(".lg-thumb").css({
                    transform: "translate3d(-" + e + "px, 0px, 0px)"
                })
            }, i.prototype.animateThumb = function (e) {
                var t = this.core.$outer.find(".lg-thumb");
                if (this.core.s.animateThumb) {
                    var i;
                    switch (this.core.s.currentPagerPosition) {
                        case "left":
                            i = 0;
                            break;
                        case "middle":
                            i = this.thumbOuterWidth / 2 - this.core.s.thumbWidth / 2;
                            break;
                        case "right":
                            i = this.thumbOuterWidth - this.core.s.thumbWidth
                    }
                    this.left = (this.core.s.thumbWidth + this.core.s.thumbMargin) * e - 1 - i, this.left > this.thumbTotalWidth - this.thumbOuterWidth && (this.left = this.thumbTotalWidth - this.thumbOuterWidth), this.left < 0 && (this.left = 0), this.core.lGalleryOn ? (t.hasClass("on") || this.core.$outer.find(".lg-thumb").css("transition-duration", this.core.s.speed + "ms"), this.core.doCss() || t.animate({
                        left: -this.left + "px"
                    }, this.core.s.speed)) : this.core.doCss() || t.css("left", -this.left + "px"), this.setTranslate(this.left)
                }
            }, i.prototype.enableThumbDrag = function () {
                var t = this,
                    i = 0,
                    n = 0,
                    r = !1,
                    s = !1,
                    a = 0;
                t.$thumbOuter.addClass("lg-grab"), t.core.$outer.find(".lg-thumb").on("mousedown.lg.thumb", (function (e) {
                    t.thumbTotalWidth > t.thumbOuterWidth && (e.preventDefault(), i = e.pageX, r = !0, t.core.$outer.scrollLeft += 1, t.core.$outer.scrollLeft -= 1, t.thumbClickable = !1, t.$thumbOuter.removeClass("lg-grab").addClass("lg-grabbing"))
                })), e(window).on("mousemove.lg.thumb", (function (e) {
                    r && (a = t.left, s = !0, n = e.pageX, t.$thumbOuter.addClass("lg-dragging"), (a -= n - i) > t.thumbTotalWidth - t.thumbOuterWidth && (a = t.thumbTotalWidth - t.thumbOuterWidth), a < 0 && (a = 0), t.setTranslate(a))
                })), e(window).on("mouseup.lg.thumb", (function () {
                    s ? (s = !1, t.$thumbOuter.removeClass("lg-dragging"), t.left = a, Math.abs(n - i) < t.core.s.swipeThreshold && (t.thumbClickable = !0)) : t.thumbClickable = !0, r && (r = !1, t.$thumbOuter.removeClass("lg-grabbing").addClass("lg-grab"))
                }))
            }, i.prototype.enableThumbSwipe = function () {
                var e = this,
                    t = 0,
                    i = 0,
                    n = !1,
                    r = 0;
                e.core.$outer.find(".lg-thumb").on("touchstart.lg", (function (i) {
                    e.thumbTotalWidth > e.thumbOuterWidth && (i.preventDefault(), t = i.originalEvent.targetTouches[0].pageX, e.thumbClickable = !1)
                })), e.core.$outer.find(".lg-thumb").on("touchmove.lg", (function (s) {
                    e.thumbTotalWidth > e.thumbOuterWidth && (s.preventDefault(), i = s.originalEvent.targetTouches[0].pageX, n = !0, e.$thumbOuter.addClass("lg-dragging"), r = e.left, (r -= i - t) > e.thumbTotalWidth - e.thumbOuterWidth && (r = e.thumbTotalWidth - e.thumbOuterWidth), r < 0 && (r = 0), e.setTranslate(r))
                })), e.core.$outer.find(".lg-thumb").on("touchend.lg", (function () {
                    e.thumbTotalWidth > e.thumbOuterWidth && n ? (n = !1, e.$thumbOuter.removeClass("lg-dragging"), Math.abs(i - t) < e.core.s.swipeThreshold && (e.thumbClickable = !0), e.left = r) : e.thumbClickable = !0
                }))
            }, i.prototype.toogle = function () {
                var e = this;
                e.core.s.toogleThumb && (e.core.$outer.addClass("lg-can-toggle"), e.$thumbOuter.append('<span class="lg-toogle-thumb lg-icon"></span>'), e.core.$outer.find(".lg-toogle-thumb").on("click.lg", (function () {
                    e.core.$outer.toggleClass("lg-thumb-open")
                })))
            }, i.prototype.thumbkeyPress = function () {
                var t = this;
                e(window).on("keydown.lg.thumb", (function (e) {
                    38 === e.keyCode ? (e.preventDefault(), t.core.$outer.addClass("lg-thumb-open")) : 40 === e.keyCode && (e.preventDefault(), t.core.$outer.removeClass("lg-thumb-open"))
                }))
            }, i.prototype.destroy = function () {
                this.core.s.thumbnail && this.core.$items.length > 1 && (e(window).off("resize.lg.thumb orientationchange.lg.thumb keydown.lg.thumb"), this.$thumbOuter.remove(), this.core.$outer.removeClass("lg-has-thumb"))
            }, e.fn.lightGallery.modules.Thumbnail = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    videoMaxWidth: "855px",
                    youtubePlayerParams: !1,
                    vimeoPlayerParams: !1,
                    dailymotionPlayerParams: !1,
                    vkPlayerParams: !1,
                    videojs: !1,
                    videojsOptions: {}
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.$el = e(i), this.core.s = e.extend({}, t, this.core.s), this.videoLoaded = !1, this.init(), this
                };
            i.prototype.init = function () {
                var t = this;
                t.core.$el.on("hasVideo.lg.tm", (function (e, i, n, r) {
                    if (t.core.$slide.eq(i).find(".lg-video").append(t.loadVideo(n, "lg-object", !0, i, r)), r)
                        if (t.core.s.videojs) try {
                            videojs(t.core.$slide.eq(i).find(".lg-html5").get(0), t.core.s.videojsOptions, (function () {
                                t.videoLoaded || this.play()
                            }))
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else t.videoLoaded || t.core.$slide.eq(i).find(".lg-html5").get(0).play()
                })), t.core.$el.on("onAferAppendSlide.lg.tm", (function (e, i) {
                    var n = t.core.$slide.eq(i).find(".lg-video-cont");
                    n.hasClass("lg-has-iframe") || (n.css("max-width", t.core.s.videoMaxWidth), t.videoLoaded = !0)
                }));
                var i = function (e) {
                    if (e.find(".lg-object").hasClass("lg-has-poster") && e.find(".lg-object").is(":visible"))
                        if (e.hasClass("lg-has-video")) {
                            var i = e.find(".lg-youtube").get(0),
                                n = e.find(".lg-vimeo").get(0),
                                r = e.find(".lg-dailymotion").get(0),
                                s = e.find(".lg-html5").get(0);
                            if (i) i.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*");
                            else if (n) try {
                                    $f(n).api("play")
                                } catch (e) {
                                    console.error("Make sure you have included froogaloop2 js")
                                } else if (r) r.contentWindow.postMessage("play", "*");
                                else if (s)
                                if (t.core.s.videojs) try {
                                    videojs(s).play()
                                } catch (e) {
                                    console.error("Make sure you have included videojs")
                                } else s.play();
                            e.addClass("lg-video-playing")
                        } else {
                            e.addClass("lg-video-playing lg-has-video");
                            var a = function (i, n) {
                                if (e.find(".lg-video").append(t.loadVideo(i, "", !1, t.core.index, n)), n)
                                    if (t.core.s.videojs) try {
                                        videojs(t.core.$slide.eq(t.core.index).find(".lg-html5").get(0), t.core.s.videojsOptions, (function () {
                                            this.play()
                                        }))
                                    } catch (e) {
                                        console.error("Make sure you have included videojs")
                                    } else t.core.$slide.eq(t.core.index).find(".lg-html5").get(0).play()
                            };
                            t.core.s.dynamic ? a(t.core.s.dynamicEl[t.core.index].src, t.core.s.dynamicEl[t.core.index].html) : a(t.core.$items.eq(t.core.index).attr("href") || t.core.$items.eq(t.core.index).attr("data-src"), t.core.$items.eq(t.core.index).attr("data-html"));
                            var o = e.find(".lg-object");
                            e.find(".lg-video").append(o), e.find(".lg-video-object").hasClass("lg-html5") || (e.removeClass("lg-complete"), e.find(".lg-video-object").on("load.lg error.lg", (function () {
                                e.addClass("lg-complete")
                            })))
                        }
                };
                t.core.doCss() && t.core.$items.length > 1 && (t.core.s.enableSwipe || t.core.s.enableDrag) ? t.core.$el.on("onSlideClick.lg.tm", (function () {
                    var e = t.core.$slide.eq(t.core.index);
                    i(e)
                })) : t.core.$slide.on("click.lg", (function () {
                    i(e(this))
                })), t.core.$el.on("onBeforeSlide.lg.tm", (function (i, n, r) {
                    var s, a = t.core.$slide.eq(n),
                        o = a.find(".lg-youtube").get(0),
                        l = a.find(".lg-vimeo").get(0),
                        d = a.find(".lg-dailymotion").get(0),
                        c = a.find(".lg-vk").get(0),
                        u = a.find(".lg-html5").get(0);
                    if (o) o.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*");
                    else if (l) try {
                            $f(l).api("pause")
                        } catch (e) {
                            console.error("Make sure you have included froogaloop2 js")
                        } else if (d) d.contentWindow.postMessage("pause", "*");
                        else if (u)
                        if (t.core.s.videojs) try {
                            videojs(u).pause()
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else u.pause();
                    c && e(c).attr("src", e(c).attr("src").replace("&autoplay", "&noplay")), s = t.core.s.dynamic ? t.core.s.dynamicEl[r].src : t.core.$items.eq(r).attr("href") || t.core.$items.eq(r).attr("data-src");
                    var h = t.core.isVideo(s, r) || {};
                    (h.youtube || h.vimeo || h.dailymotion || h.vk) && t.core.$outer.addClass("lg-hide-download")
                })), t.core.$el.on("onAfterSlide.lg.tm", (function (e, i) {
                    t.core.$slide.eq(i).removeClass("lg-video-playing")
                }))
            }, i.prototype.loadVideo = function (t, i, n, r, s) {
                var a = "",
                    o = 1,
                    l = "",
                    d = this.core.isVideo(t, r) || {};
                if (n && (o = this.videoLoaded ? 0 : 1), d.youtube) l = "?wmode=opaque&autoplay=" + o + "&enablejsapi=1", this.core.s.youtubePlayerParams && (l = l + "&" + e.param(this.core.s.youtubePlayerParams)), a = '<iframe class="lg-video-object lg-youtube ' + i + '" width="560" height="315" src="//www.youtube.com/embed/' + d.youtube[1] + l + '" frameborder="0" allowfullscreen></iframe>';
                else if (d.vimeo) l = "?autoplay=" + o + "&api=1", this.core.s.vimeoPlayerParams && (l = l + "&" + e.param(this.core.s.vimeoPlayerParams)), a = '<iframe class="lg-video-object lg-vimeo ' + i + '" width="560" height="315"  src="//player.vimeo.com/video/' + d.vimeo[1] + l + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                else if (d.dailymotion) l = "?wmode=opaque&autoplay=" + o + "&api=postMessage", this.core.s.dailymotionPlayerParams && (l = l + "&" + e.param(this.core.s.dailymotionPlayerParams)), a = '<iframe class="lg-video-object lg-dailymotion ' + i + '" width="560" height="315" src="//www.dailymotion.com/embed/video/' + d.dailymotion[1] + l + '" frameborder="0" allowfullscreen></iframe>';
                else if (d.html5) {
                    var c = s.substring(0, 1);
                    "." !== c && "#" !== c || (s = e(s).html()), a = s
                } else d.vk && (l = "&autoplay=" + o, this.core.s.vkPlayerParams && (l = l + "&" + e.param(this.core.s.vkPlayerParams)), a = '<iframe class="lg-video-object lg-vk ' + i + '" width="560" height="315" src="http://vk.com/video_ext.php?' + d.vk[1] + l + '" frameborder="0" allowfullscreen></iframe>');
                return a
            }, i.prototype.destroy = function () {
                this.videoLoaded = !1
            }, e.fn.lightGallery.modules.video = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    scale: 1,
                    zoom: !0,
                    actualSize: !0,
                    enableZoomAfter: 300,
                    useLeftForZoom: function () {
                        var e = !1,
                            t = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
                        return t && parseInt(t[2], 10) < 54 && (e = !0), e
                    }()
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.zoom && this.core.doCss() && (this.init(), this.zoomabletimeout = !1, this.pageX = e(window).width() / 2, this.pageY = e(window).height() / 2 + e(window).scrollTop()), this
                };
            i.prototype.init = function () {
                var t = this,
                    i = '<span id="lg-zoom-in" class="lg-icon"></span><span id="lg-zoom-out" class="lg-icon"></span>';
                t.core.s.actualSize && (i += '<span id="lg-actual-size" class="lg-icon"></span>'), t.core.s.useLeftForZoom ? t.core.$outer.addClass("lg-use-left-for-zoom") : t.core.$outer.addClass("lg-use-transition-for-zoom"), this.core.$outer.find(".lg-toolbar").append(i), t.core.$el.on("onSlideItemLoad.lg.tm.zoom", (function (i, n, r) {
                    var s = t.core.s.enableZoomAfter + r;
                    e("body").hasClass("lg-from-hash") && r ? s = 0 : e("body").removeClass("lg-from-hash"), t.zoomabletimeout = setTimeout((function () {
                        t.core.$slide.eq(n).addClass("lg-zoomable")
                    }), s + 30)
                }));
                var n = 1,
                    r = function (i) {
                        var n = t.core.$outer.find(".lg-current .lg-image"),
                            r = (e(window).width() - n.prop("offsetWidth")) / 2,
                            s = (e(window).height() - n.prop("offsetHeight")) / 2 + e(window).scrollTop(),
                            a = (i - 1) * (t.pageX - r),
                            o = (i - 1) * (t.pageY - s);
                        n.css("transform", "scale3d(" + i + ", " + i + ", 1)").attr("data-scale", i), t.core.s.useLeftForZoom ? n.parent().css({
                            left: -a + "px",
                            top: -o + "px"
                        }).attr("data-x", a).attr("data-y", o) : n.parent().css("transform", "translate3d(-" + a + "px, -" + o + "px, 0)").attr("data-x", a).attr("data-y", o)
                    },
                    s = function () {
                        n > 1 ? t.core.$outer.addClass("lg-zoomed") : t.resetZoom(), n < 1 && (n = 1), r(n)
                    },
                    a = function (i, r, a, o) {
                        var l, d = r.prop("offsetWidth");
                        l = t.core.s.dynamic ? t.core.s.dynamicEl[a].width || r[0].naturalWidth || d : t.core.$items.eq(a).attr("data-width") || r[0].naturalWidth || d, t.core.$outer.hasClass("lg-zoomed") ? n = 1 : l > d && (n = l / d || 2), o ? (t.pageX = e(window).width() / 2, t.pageY = e(window).height() / 2 + e(window).scrollTop()) : (t.pageX = i.pageX || i.originalEvent.targetTouches[0].pageX, t.pageY = i.pageY || i.originalEvent.targetTouches[0].pageY), s(), setTimeout((function () {
                            t.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                        }), 10)
                    },
                    o = !1;
                t.core.$el.on("onAferAppendSlide.lg.tm.zoom", (function (e, i) {
                    var n = t.core.$slide.eq(i).find(".lg-image");
                    n.on("dblclick", (function (e) {
                        a(e, n, i)
                    })), n.on("touchstart", (function (e) {
                        o ? (clearTimeout(o), o = null, a(e, n, i)) : o = setTimeout((function () {
                            o = null
                        }), 300), e.preventDefault()
                    }))
                })), e(window).on("resize.lg.zoom scroll.lg.zoom orientationchange.lg.zoom", (function () {
                    t.pageX = e(window).width() / 2, t.pageY = e(window).height() / 2 + e(window).scrollTop(), r(n)
                })), e("#lg-zoom-out").on("click.lg", (function () {
                    t.core.$outer.find(".lg-current .lg-image").length && (n -= t.core.s.scale, s())
                })), e("#lg-zoom-in").on("click.lg", (function () {
                    t.core.$outer.find(".lg-current .lg-image").length && (n += t.core.s.scale, s())
                })), e("#lg-actual-size").on("click.lg", (function (e) {
                    a(e, t.core.$slide.eq(t.core.index).find(".lg-image"), t.core.index, !0)
                })), t.core.$el.on("onBeforeSlide.lg.tm", (function () {
                    n = 1, t.resetZoom()
                })), t.zoomDrag(), t.zoomSwipe()
            }, i.prototype.resetZoom = function () {
                this.core.$outer.removeClass("lg-zoomed"), this.core.$slide.find(".lg-img-wrap").removeAttr("style data-x data-y"), this.core.$slide.find(".lg-image").removeAttr("style data-scale"), this.pageX = e(window).width() / 2, this.pageY = e(window).height() / 2 + e(window).scrollTop()
            }, i.prototype.zoomSwipe = function () {
                var e = this,
                    t = {},
                    i = {},
                    n = !1,
                    r = !1,
                    s = !1;
                e.core.$slide.on("touchstart.lg", (function (i) {
                    if (e.core.$outer.hasClass("lg-zoomed")) {
                        var n = e.core.$slide.eq(e.core.index).find(".lg-object");
                        s = n.prop("offsetHeight") * n.attr("data-scale") > e.core.$outer.find(".lg").height(), ((r = n.prop("offsetWidth") * n.attr("data-scale") > e.core.$outer.find(".lg").width()) || s) && (i.preventDefault(), t = {
                            x: i.originalEvent.targetTouches[0].pageX,
                            y: i.originalEvent.targetTouches[0].pageY
                        })
                    }
                })), e.core.$slide.on("touchmove.lg", (function (a) {
                    if (e.core.$outer.hasClass("lg-zoomed")) {
                        var o, l, d = e.core.$slide.eq(e.core.index).find(".lg-img-wrap");
                        a.preventDefault(), n = !0, i = {
                            x: a.originalEvent.targetTouches[0].pageX,
                            y: a.originalEvent.targetTouches[0].pageY
                        }, e.core.$outer.addClass("lg-zoom-dragging"), l = s ? -Math.abs(d.attr("data-y")) + (i.y - t.y) : -Math.abs(d.attr("data-y")), o = r ? -Math.abs(d.attr("data-x")) + (i.x - t.x) : -Math.abs(d.attr("data-x")), (Math.abs(i.x - t.x) > 15 || Math.abs(i.y - t.y) > 15) && (e.core.s.useLeftForZoom ? d.css({
                            left: o + "px",
                            top: l + "px"
                        }) : d.css("transform", "translate3d(" + o + "px, " + l + "px, 0)"))
                    }
                })), e.core.$slide.on("touchend.lg", (function () {
                    e.core.$outer.hasClass("lg-zoomed") && n && (n = !1, e.core.$outer.removeClass("lg-zoom-dragging"), e.touchendZoom(t, i, r, s))
                }))
            }, i.prototype.zoomDrag = function () {
                var t = this,
                    i = {},
                    n = {},
                    r = !1,
                    s = !1,
                    a = !1,
                    o = !1;
                t.core.$slide.on("mousedown.lg.zoom", (function (n) {
                    var s = t.core.$slide.eq(t.core.index).find(".lg-object");
                    o = s.prop("offsetHeight") * s.attr("data-scale") > t.core.$outer.find(".lg").height(), a = s.prop("offsetWidth") * s.attr("data-scale") > t.core.$outer.find(".lg").width(), t.core.$outer.hasClass("lg-zoomed") && e(n.target).hasClass("lg-object") && (a || o) && (n.preventDefault(), i = {
                        x: n.pageX,
                        y: n.pageY
                    }, r = !0, t.core.$outer.scrollLeft += 1, t.core.$outer.scrollLeft -= 1, t.core.$outer.removeClass("lg-grab").addClass("lg-grabbing"))
                })), e(window).on("mousemove.lg.zoom", (function (e) {
                    if (r) {
                        var l, d, c = t.core.$slide.eq(t.core.index).find(".lg-img-wrap");
                        s = !0, n = {
                            x: e.pageX,
                            y: e.pageY
                        }, t.core.$outer.addClass("lg-zoom-dragging"), d = o ? -Math.abs(c.attr("data-y")) + (n.y - i.y) : -Math.abs(c.attr("data-y")), l = a ? -Math.abs(c.attr("data-x")) + (n.x - i.x) : -Math.abs(c.attr("data-x")), t.core.s.useLeftForZoom ? c.css({
                            left: l + "px",
                            top: d + "px"
                        }) : c.css("transform", "translate3d(" + l + "px, " + d + "px, 0)")
                    }
                })), e(window).on("mouseup.lg.zoom", (function (e) {
                    r && (r = !1, t.core.$outer.removeClass("lg-zoom-dragging"), !s || i.x === n.x && i.y === n.y || (n = {
                        x: e.pageX,
                        y: e.pageY
                    }, t.touchendZoom(i, n, a, o)), s = !1), t.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                }))
            }, i.prototype.touchendZoom = function (e, t, i, n) {
                var r = this,
                    s = r.core.$slide.eq(r.core.index).find(".lg-img-wrap"),
                    a = r.core.$slide.eq(r.core.index).find(".lg-object"),
                    o = -Math.abs(s.attr("data-x")) + (t.x - e.x),
                    l = -Math.abs(s.attr("data-y")) + (t.y - e.y),
                    d = (r.core.$outer.find(".lg").height() - a.prop("offsetHeight")) / 2,
                    c = Math.abs(a.prop("offsetHeight") * Math.abs(a.attr("data-scale")) - r.core.$outer.find(".lg").height() + d),
                    u = (r.core.$outer.find(".lg").width() - a.prop("offsetWidth")) / 2,
                    h = Math.abs(a.prop("offsetWidth") * Math.abs(a.attr("data-scale")) - r.core.$outer.find(".lg").width() + u);
                (Math.abs(t.x - e.x) > 15 || Math.abs(t.y - e.y) > 15) && (n && (l <= -c ? l = -c : l >= -d && (l = -d)), i && (o <= -h ? o = -h : o >= -u && (o = -u)), n ? s.attr("data-y", Math.abs(l)) : l = -Math.abs(s.attr("data-y")), i ? s.attr("data-x", Math.abs(o)) : o = -Math.abs(s.attr("data-x")), r.core.s.useLeftForZoom ? s.css({
                    left: o + "px",
                    top: l + "px"
                }) : s.css("transform", "translate3d(" + o + "px, " + l + "px, 0)"))
            }, i.prototype.destroy = function () {
                var t = this;
                t.core.$el.off(".lg.zoom"), e(window).off(".lg.zoom"), t.core.$slide.off(".lg.zoom"), t.core.$el.off(".lg.tm.zoom"), t.resetZoom(), clearTimeout(t.zoomabletimeout), t.zoomabletimeout = !1
            }, e.fn.lightGallery.modules.zoom = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    hash: !0
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.hash && (this.oldHash = window.location.hash, this.init()), this
                };
            i.prototype.init = function () {
                var t, i = this;
                i.core.$el.on("onAfterSlide.lg.tm", (function (e, t, n) {
                    history.replaceState ? history.replaceState(null, null, "#lg=" + i.core.s.galleryId + "&slide=" + n) : window.location.hash = "lg=" + i.core.s.galleryId + "&slide=" + n
                })), e(window).on("hashchange.lg.hash", (function () {
                    t = window.location.hash;
                    var e = parseInt(t.split("&slide=")[1], 10);
                    t.indexOf("lg=" + i.core.s.galleryId) > -1 ? i.core.slide(e, !1, !1) : i.core.lGalleryOn && i.core.destroy()
                }))
            }, i.prototype.destroy = function () {
                this.core.s.hash && (this.oldHash && this.oldHash.indexOf("lg=" + this.core.s.galleryId) < 0 ? history.replaceState ? history.replaceState(null, null, this.oldHash) : window.location.hash = this.oldHash : history.replaceState ? history.replaceState(null, document.title, window.location.pathname + window.location.search) : window.location.hash = "", this.core.$el.off(".lg.hash"))
            }, e.fn.lightGallery.modules.hash = i
        }()
    })),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], (function (e) {
            return t(e)
        })) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, (function (e) {
        ! function () {
            "use strict";
            var t = {
                    share: !0,
                    facebook: !0,
                    facebookDropdownText: "Facebook",
                    twitter: !0,
                    twitterDropdownText: "Twitter",
                    googlePlus: !0,
                    googlePlusDropdownText: "GooglePlus",
                    pinterest: !0,
                    pinterestDropdownText: "Pinterest"
                },
                i = function (i) {
                    return this.core = e(i).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.share && this.init(), this
                };
            i.prototype.init = function () {
                var t = this,
                    i = '<span id="lg-share" class="lg-icon"><ul class="lg-dropdown" style="position: absolute;">';
                i += t.core.s.facebook ? '<li><a id="lg-share-facebook" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.facebookDropdownText + "</span></a></li>" : "", i += t.core.s.twitter ? '<li><a id="lg-share-twitter" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.twitterDropdownText + "</span></a></li>" : "", i += t.core.s.googlePlus ? '<li><a id="lg-share-googleplus" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.googlePlusDropdownText + "</span></a></li>" : "", i += t.core.s.pinterest ? '<li><a id="lg-share-pinterest" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.pinterestDropdownText + "</span></a></li>" : "", i += "</ul></span>", this.core.$outer.find(".lg-toolbar").append(i), this.core.$outer.find(".lg").append('<div id="lg-dropdown-overlay"></div>'), e("#lg-share").on("click.lg", (function () {
                    t.core.$outer.toggleClass("lg-dropdown-active")
                })), e("#lg-dropdown-overlay").on("click.lg", (function () {
                    t.core.$outer.removeClass("lg-dropdown-active")
                })), t.core.$el.on("onAfterSlide.lg.tm", (function (i, n, r) {
                    setTimeout((function () {
                        e("#lg-share-facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(t.core.$items.eq(r).attr("data-facebook-share-url") || window.location.href)), e("#lg-share-twitter").attr("href", "https://twitter.com/intent/tweet?text=" + t.core.$items.eq(r).attr("data-tweet-text") + "&url=" + encodeURIComponent(t.core.$items.eq(r).attr("data-twitter-share-url") || window.location.href)), e("#lg-share-googleplus").attr("href", "https://plus.google.com/share?url=" + encodeURIComponent(t.core.$items.eq(r).attr("data-googleplus-share-url") || window.location.href)), e("#lg-share-pinterest").attr("href", "http://www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(t.core.$items.eq(r).attr("data-pinterest-share-url") || window.location.href) + "&media=" + encodeURIComponent(t.core.$items.eq(r).attr("href") || t.core.$items.eq(r).attr("data-src")) + "&description=" + t.core.$items.eq(r).attr("data-pinterest-text"))
                    }), 100)
                }))
            }, i.prototype.destroy = function () {}, e.fn.lightGallery.modules.share = i
        }()
    })),
    function (e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }((function (e) {
        function t(t) {
            var a = t || window.event,
                o = l.call(arguments, 1),
                d = 0,
                u = 0,
                h = 0,
                p = 0,
                f = 0,
                g = 0;
            if ((t = e.event.fix(a)).type = "mousewheel", "detail" in a && (h = -1 * a.detail), "wheelDelta" in a && (h = a.wheelDelta), "wheelDeltaY" in a && (h = a.wheelDeltaY), "wheelDeltaX" in a && (u = -1 * a.wheelDeltaX), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (u = -1 * h, h = 0), d = 0 === h ? u : h, "deltaY" in a && (d = h = -1 * a.deltaY), "deltaX" in a && (u = a.deltaX, 0 === h && (d = -1 * u)), 0 !== h || 0 !== u) {
                if (1 === a.deltaMode) {
                    var m = e.data(this, "mousewheel-line-height");
                    d *= m, h *= m, u *= m
                } else if (2 === a.deltaMode) {
                    var v = e.data(this, "mousewheel-page-height");
                    d *= v, h *= v, u *= v
                }
                if (p = Math.max(Math.abs(h), Math.abs(u)), (!s || s > p) && (s = p, n(a, p) && (s /= 40)), n(a, p) && (d /= 40, u /= 40, h /= 40), d = Math[d >= 1 ? "floor" : "ceil"](d / s), u = Math[u >= 1 ? "floor" : "ceil"](u / s), h = Math[h >= 1 ? "floor" : "ceil"](h / s), c.settings.normalizeOffset && this.getBoundingClientRect) {
                    var y = this.getBoundingClientRect();
                    f = t.clientX - y.left, g = t.clientY - y.top
                }
                return t.deltaX = u, t.deltaY = h, t.deltaFactor = s, t.offsetX = f, t.offsetY = g, t.deltaMode = 0, o.unshift(t, d, u, h), r && clearTimeout(r), r = setTimeout(i, 200), (e.event.dispatch || e.event.handle).apply(this, o)
            }
        }

        function i() {
            s = null
        }

        function n(e, t) {
            return c.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
        }
        var r, s, a = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            o = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            l = Array.prototype.slice;
        if (e.event.fixHooks)
            for (var d = a.length; d;) e.event.fixHooks[a[--d]] = e.event.mouseHooks;
        var c = e.event.special.mousewheel = {
            version: "3.1.12",
            setup: function () {
                if (this.addEventListener)
                    for (var i = o.length; i;) this.addEventListener(o[--i], t, !1);
                else this.onmousewheel = t;
                e.data(this, "mousewheel-line-height", c.getLineHeight(this)), e.data(this, "mousewheel-page-height", c.getPageHeight(this))
            },
            teardown: function () {
                if (this.removeEventListener)
                    for (var i = o.length; i;) this.removeEventListener(o[--i], t, !1);
                else this.onmousewheel = null;
                e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height")
            },
            getLineHeight: function (t) {
                var i = e(t),
                    n = i["offsetParent" in e.fn ? "offsetParent" : "parent"]();
                return n.length || (n = e("body")), parseInt(n.css("fontSize"), 10) || parseInt(i.css("fontSize"), 10) || 16
            },
            getPageHeight: function (t) {
                return e(t).height()
            },
            settings: {
                adjustOldDeltas: !0,
                normalizeOffset: !0
            }
        };
        e.fn.extend({
            mousewheel: function (e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function (e) {
                return this.unbind("mousewheel", e)
            }
        })
    })),
    function (e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
    }((function (e) {
        var t, i, n, r, s, a, o = "Close",
            l = "BeforeClose",
            d = "MarkupParse",
            c = "Open",
            u = "Change",
            h = "mfp",
            p = ".mfp",
            f = "mfp-ready",
            g = "mfp-removing",
            m = "mfp-prevent-close",
            v = function () {},
            y = !!window.jQuery,
            b = e(window),
            w = function (e, i) {
                t.ev.on(h + e + p, i)
            },
            x = function (t, i, n, r) {
                var s = document.createElement("div");
                return s.className = "mfp-" + t, n && (s.innerHTML = n), r ? i && i.appendChild(s) : (s = e(s), i && s.appendTo(i)), s
            },
            C = function (i, n) {
                t.ev.triggerHandler(h + i, n), t.st.callbacks && (i = i.charAt(0).toLowerCase() + i.slice(1), t.st.callbacks[i] && t.st.callbacks[i].apply(t, e.isArray(n) ? n : [n]))
            },
            T = function (i) {
                return i === a && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)), a = i), t.currTemplate.closeBtn
            },
            E = function () {
                e.magnificPopup.instance || ((t = new v).init(), e.magnificPopup.instance = t)
            };
        v.prototype = {
            constructor: v,
            init: function () {
                var i = navigator.appVersion;
                t.isLowIE = t.isIE8 = document.all && !document.addEventListener, t.isAndroid = /android/gi.test(i), t.isIOS = /iphone|ipad|ipod/gi.test(i), t.supportsTransition = function () {
                    var e = document.createElement("p").style,
                        t = ["ms", "O", "Moz", "Webkit"];
                    if (void 0 !== e.transition) return !0;
                    for (; t.length;)
                        if (t.pop() + "Transition" in e) return !0;
                    return !1
                }(), t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), n = e(document), t.popupsCache = {}
            },
            open: function (i) {
                var r;
                if (!1 === i.isObj) {
                    t.items = i.items.toArray(), t.index = 0;
                    var a, o = i.items;
                    for (r = 0; r < o.length; r++)
                        if ((a = o[r]).parsed && (a = a.el[0]), a === i.el[0]) {
                            t.index = r;
                            break
                        }
                } else t.items = e.isArray(i.items) ? i.items : [i.items], t.index = i.index || 0;
                if (!t.isOpen) {
                    t.types = [], s = "", i.mainEl && i.mainEl.length ? t.ev = i.mainEl.eq(0) : t.ev = n, i.key ? (t.popupsCache[i.key] || (t.popupsCache[i.key] = {}), t.currTemplate = t.popupsCache[i.key]) : t.currTemplate = {}, t.st = e.extend(!0, {}, e.magnificPopup.defaults, i), t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = x("bg").on("click" + p, (function () {
                        t.close()
                    })), t.wrap = x("wrap").attr("tabindex", -1).on("click" + p, (function (e) {
                        t._checkIfClose(e.target) && t.close()
                    })), t.container = x("container", t.wrap)), t.contentContainer = x("content"), t.st.preloader && (t.preloader = x("preloader", t.container, t.st.tLoading));
                    var l = e.magnificPopup.modules;
                    for (r = 0; r < l.length; r++) {
                        var u = l[r];
                        u = u.charAt(0).toUpperCase() + u.slice(1), t["init" + u].call(t)
                    }
                    C("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (w(d, (function (e, t, i, n) {
                        i.close_replaceWith = T(n.type)
                    })), s += " mfp-close-btn-in") : t.wrap.append(T())), t.st.alignTop && (s += " mfp-align-top"), t.fixedContentPos ? t.wrap.css({
                        overflow: t.st.overflowY,
                        overflowX: "hidden",
                        overflowY: t.st.overflowY
                    }) : t.wrap.css({
                        top: b.scrollTop(),
                        position: "absolute"
                    }), (!1 === t.st.fixedBgPos || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                        height: n.height(),
                        position: "absolute"
                    }), t.st.enableEscapeKey && n.on("keyup" + p, (function (e) {
                        27 === e.keyCode && t.close()
                    })), b.on("resize" + p, (function () {
                        t.updateSize()
                    })), t.st.closeOnContentClick || (s += " mfp-auto-cursor"), s && t.wrap.addClass(s);
                    var h = t.wH = b.height(),
                        g = {};
                    if (t.fixedContentPos && t._hasScrollBar(h)) {
                        var m = t._getScrollbarSize();
                        m && (g.marginRight = m)
                    }
                    t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : g.overflow = "hidden");
                    var v = t.st.mainClass;
                    return t.isIE7 && (v += " mfp-ie7"), v && t._addClassToMFP(v), t.updateItemHTML(), C("BuildControls"), e("html").css(g), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || e(document.body)), t._lastFocusedEl = document.activeElement, setTimeout((function () {
                        t.content ? (t._addClassToMFP(f), t._setFocus()) : t.bgOverlay.addClass(f), n.on("focusin" + p, t._onFocusIn)
                    }), 16), t.isOpen = !0, t.updateSize(h), C(c), i
                }
                t.updateItemHTML()
            },
            close: function () {
                t.isOpen && (C(l), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(g), setTimeout((function () {
                    t._close()
                }), t.st.removalDelay)) : t._close())
            },
            _close: function () {
                C(o);
                var i = g + " " + f + " ";
                if (t.bgOverlay.detach(), t.wrap.detach(), t.container.empty(), t.st.mainClass && (i += t.st.mainClass + " "), t._removeClassFromMFP(i), t.fixedContentPos) {
                    var r = {
                        marginRight: ""
                    };
                    t.isIE7 ? e("body, html").css("overflow", "") : r.overflow = "", e("html").css(r)
                }
                n.off("keyup.mfp focusin" + p), t.ev.off(p), t.wrap.attr("class", "mfp-wrap").removeAttr("style"), t.bgOverlay.attr("class", "mfp-bg"), t.container.attr("class", "mfp-container"), !t.st.showCloseBtn || t.st.closeBtnInside && !0 !== t.currTemplate[t.currItem.type] || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(), t.st.autoFocusLast && t._lastFocusedEl && e(t._lastFocusedEl).focus(), t.currItem = null, t.content = null, t.currTemplate = null, t.prevHeight = 0, C("AfterClose")
            },
            updateSize: function (e) {
                if (t.isIOS) {
                    var i = document.documentElement.clientWidth / window.innerWidth,
                        n = window.innerHeight * i;
                    t.wrap.css("height", n), t.wH = n
                } else t.wH = e || b.height();
                t.fixedContentPos || t.wrap.css("height", t.wH), C("Resize")
            },
            updateItemHTML: function () {
                var i = t.items[t.index];
                t.contentContainer.detach(), t.content && t.content.detach(), i.parsed || (i = t.parseEl(t.index));
                var n = i.type;
                if (C("BeforeChange", [t.currItem ? t.currItem.type : "", n]), t.currItem = i, !t.currTemplate[n]) {
                    var s = !!t.st[n] && t.st[n].markup;
                    C("FirstMarkupParse", s), t.currTemplate[n] = !s || e(s)
                }
                r && r !== i.type && t.container.removeClass("mfp-" + r + "-holder");
                var a = t["get" + n.charAt(0).toUpperCase() + n.slice(1)](i, t.currTemplate[n]);
                t.appendContent(a, n), i.preloaded = !0, C(u, i), r = i.type, t.container.prepend(t.contentContainer), C("AfterChange")
            },
            appendContent: function (e, i) {
                t.content = e, e ? t.st.showCloseBtn && t.st.closeBtnInside && !0 === t.currTemplate[i] ? t.content.find(".mfp-close").length || t.content.append(T()) : t.content = e : t.content = "", C("BeforeAppend"), t.container.addClass("mfp-" + i + "-holder"), t.contentContainer.append(t.content)
            },
            parseEl: function (i) {
                var n, r = t.items[i];
                if (r.tagName ? r = {
                        el: e(r)
                    } : (n = r.type, r = {
                        data: r,
                        src: r.src
                    }), r.el) {
                    for (var s = t.types, a = 0; a < s.length; a++)
                        if (r.el.hasClass("mfp-" + s[a])) {
                            n = s[a];
                            break
                        } r.src = r.el.attr("data-mfp-src"), r.src || (r.src = r.el.attr("href"))
                }
                return r.type = n || t.st.type || "inline", r.index = i, r.parsed = !0, t.items[i] = r, C("ElementParse", r), t.items[i]
            },
            addGroup: function (e, i) {
                var n = function (n) {
                    n.mfpEl = this, t._openClick(n, e, i)
                };
                i || (i = {});
                var r = "click.magnificPopup";
                i.mainEl = e, i.items ? (i.isObj = !0, e.off(r).on(r, n)) : (i.isObj = !1, i.delegate ? e.off(r).on(r, i.delegate, n) : (i.items = e, e.off(r).on(r, n)))
            },
            _openClick: function (i, n, r) {
                if ((void 0 !== r.midClick ? r.midClick : e.magnificPopup.defaults.midClick) || !(2 === i.which || i.ctrlKey || i.metaKey || i.altKey || i.shiftKey)) {
                    var s = void 0 !== r.disableOn ? r.disableOn : e.magnificPopup.defaults.disableOn;
                    if (s)
                        if (e.isFunction(s)) {
                            if (!s.call(t)) return !0
                        } else if (b.width() < s) return !0;
                    i.type && (i.preventDefault(), t.isOpen && i.stopPropagation()), r.el = e(i.mfpEl), r.delegate && (r.items = n.find(r.delegate)), t.open(r)
                }
            },
            updateStatus: function (e, n) {
                if (t.preloader) {
                    i !== e && t.container.removeClass("mfp-s-" + i), n || "loading" !== e || (n = t.st.tLoading);
                    var r = {
                        status: e,
                        text: n
                    };
                    C("UpdateStatus", r), e = r.status, n = r.text, t.preloader.html(n), t.preloader.find("a").on("click", (function (e) {
                        e.stopImmediatePropagation()
                    })), t.container.addClass("mfp-s-" + e), i = e
                }
            },
            _checkIfClose: function (i) {
                if (!e(i).hasClass(m)) {
                    var n = t.st.closeOnContentClick,
                        r = t.st.closeOnBgClick;
                    if (n && r) return !0;
                    if (!t.content || e(i).hasClass("mfp-close") || t.preloader && i === t.preloader[0]) return !0;
                    if (i === t.content[0] || e.contains(t.content[0], i)) {
                        if (n) return !0
                    } else if (r && e.contains(document, i)) return !0;
                    return !1
                }
            },
            _addClassToMFP: function (e) {
                t.bgOverlay.addClass(e), t.wrap.addClass(e)
            },
            _removeClassFromMFP: function (e) {
                this.bgOverlay.removeClass(e), t.wrap.removeClass(e)
            },
            _hasScrollBar: function (e) {
                return (t.isIE7 ? n.height() : document.body.scrollHeight) > (e || b.height())
            },
            _setFocus: function () {
                (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
            },
            _onFocusIn: function (i) {
                return i.target === t.wrap[0] || e.contains(t.wrap[0], i.target) ? void 0 : (t._setFocus(), !1)
            },
            _parseMarkup: function (t, i, n) {
                var r;
                n.data && (i = e.extend(n.data, i)), C(d, [t, i, n]), e.each(i, (function (i, n) {
                    if (void 0 === n || !1 === n) return !0;
                    if ((r = i.split("_")).length > 1) {
                        var s = t.find(p + "-" + r[0]);
                        if (s.length > 0) {
                            var a = r[1];
                            "replaceWith" === a ? s[0] !== n[0] && s.replaceWith(n) : "img" === a ? s.is("img") ? s.attr("src", n) : s.replaceWith(e("<img>").attr("src", n).attr("class", s.attr("class"))) : s.attr(r[1], n)
                        }
                    } else t.find(p + "-" + i).html(n)
                }))
            },
            _getScrollbarSize: function () {
                if (void 0 === t.scrollbarSize) {
                    var e = document.createElement("div");
                    e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(e), t.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e)
                }
                return t.scrollbarSize
            }
        }, e.magnificPopup = {
            instance: null,
            proto: v.prototype,
            modules: [],
            open: function (t, i) {
                return E(), (t = t ? e.extend(!0, {}, t) : {}).isObj = !0, t.index = i || 0, this.instance.open(t)
            },
            close: function () {
                return e.magnificPopup.instance && e.magnificPopup.instance.close()
            },
            registerModule: function (t, i) {
                i.options && (e.magnificPopup.defaults[t] = i.options), e.extend(this.proto, i.proto), this.modules.push(t)
            },
            defaults: {
                disableOn: 0,
                key: null,
                midClick: !1,
                mainClass: "",
                preloader: !0,
                focus: "",
                closeOnContentClick: !1,
                closeOnBgClick: !0,
                closeBtnInside: !0,
                showCloseBtn: !0,
                enableEscapeKey: !0,
                modal: !1,
                alignTop: !1,
                removalDelay: 0,
                prependTo: null,
                fixedContentPos: "auto",
                fixedBgPos: "auto",
                overflowY: "auto",
                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
                tClose: "Close (Esc)",
                tLoading: "Loading...",
                autoFocusLast: !0
            }
        }, e.fn.magnificPopup = function (i) {
            E();
            var n = e(this);
            if ("string" == typeof i)
                if ("open" === i) {
                    var r, s = y ? n.data("magnificPopup") : n[0].magnificPopup,
                        a = parseInt(arguments[1], 10) || 0;
                    s.items ? r = s.items[a] : (r = n, s.delegate && (r = r.find(s.delegate)), r = r.eq(a)), t._openClick({
                        mfpEl: r
                    }, n, s)
                } else t.isOpen && t[i].apply(t, Array.prototype.slice.call(arguments, 1));
            else i = e.extend(!0, {}, i), y ? n.data("magnificPopup", i) : n[0].magnificPopup = i, t.addGroup(n, i);
            return n
        };
        var S, _, k, $ = "inline",
            I = function () {
                k && (_.after(k.addClass(S)).detach(), k = null)
            };
        e.magnificPopup.registerModule($, {
            options: {
                hiddenClass: "hide",
                markup: "",
                tNotFound: "Content not found"
            },
            proto: {
                initInline: function () {
                    t.types.push($), w(o + "." + $, (function () {
                        I()
                    }))
                },
                getInline: function (i, n) {
                    if (I(), i.src) {
                        var r = t.st.inline,
                            s = e(i.src);
                        if (s.length) {
                            var a = s[0].parentNode;
                            a && a.tagName && (_ || (S = r.hiddenClass, _ = x(S), S = "mfp-" + S), k = s.after(_).detach().removeClass(S)), t.updateStatus("ready")
                        } else t.updateStatus("error", r.tNotFound), s = e("<div>");
                        return i.inlineElement = s, s
                    }
                    return t.updateStatus("ready"), t._parseMarkup(n, {}, i), n
                }
            }
        });
        var D, A = "ajax",
            M = function () {
                D && e(document.body).removeClass(D)
            },
            P = function () {
                M(), t.req && t.req.abort()
            };
        e.magnificPopup.registerModule(A, {
            options: {
                settings: null,
                cursor: "mfp-ajax-cur",
                tError: '<a href="%url%">The content</a> could not be loaded.'
            },
            proto: {
                initAjax: function () {
                    t.types.push(A), D = t.st.ajax.cursor, w(o + "." + A, P), w("BeforeChange." + A, P)
                },
                getAjax: function (i) {
                    D && e(document.body).addClass(D), t.updateStatus("loading");
                    var n = e.extend({
                        url: i.src,
                        success: function (n, r, s) {
                            var a = {
                                data: n,
                                xhr: s
                            };
                            C("ParseAjax", a), t.appendContent(e(a.data), A), i.finished = !0, M(), t._setFocus(), setTimeout((function () {
                                t.wrap.addClass(f)
                            }), 16), t.updateStatus("ready"), C("AjaxContentAdded")
                        },
                        error: function () {
                            M(), i.finished = i.loadError = !0, t.updateStatus("error", t.st.ajax.tError.replace("%url%", i.src))
                        }
                    }, t.st.ajax.settings);
                    return t.req = e.ajax(n), ""
                }
            }
        });
        var O, N = function (i) {
            if (i.data && void 0 !== i.data.title) return i.data.title;
            var n = t.st.image.titleSrc;
            if (n) {
                if (e.isFunction(n)) return n.call(t, i);
                if (i.el) return i.el.attr(n) || ""
            }
            return ""
        };
        e.magnificPopup.registerModule("image", {
            options: {
                markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                cursor: "mfp-zoom-out-cur",
                titleSrc: "title",
                verticalFit: !0,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            proto: {
                initImage: function () {
                    var i = t.st.image,
                        n = ".image";
                    t.types.push("image"), w(c + n, (function () {
                        "image" === t.currItem.type && i.cursor && e(document.body).addClass(i.cursor)
                    })), w(o + n, (function () {
                        i.cursor && e(document.body).removeClass(i.cursor), b.off("resize" + p)
                    })), w("Resize" + n, t.resizeImage), t.isLowIE && w("AfterChange", t.resizeImage)
                },
                resizeImage: function () {
                    var e = t.currItem;
                    if (e && e.img && t.st.image.verticalFit) {
                        var i = 0;
                        t.isLowIE && (i = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", t.wH - i)
                    }
                },
                _onImageHasSize: function (e) {
                    e.img && (e.hasSize = !0, O && clearInterval(O), e.isCheckingImgSize = !1, C("ImageHasSize", e), e.imgHidden && (t.content && t.content.removeClass("mfp-loading"), e.imgHidden = !1))
                },
                findImageSize: function (e) {
                    var i = 0,
                        n = e.img[0],
                        r = function (s) {
                            O && clearInterval(O), O = setInterval((function () {
                                return n.naturalWidth > 0 ? void t._onImageHasSize(e) : (i > 200 && clearInterval(O), void(3 === ++i ? r(10) : 40 === i ? r(50) : 100 === i && r(500)))
                            }), s)
                        };
                    r(1)
                },
                getImage: function (i, n) {
                    var r = 0,
                        s = function () {
                            i && (i.img[0].complete ? (i.img.off(".mfploader"), i === t.currItem && (t._onImageHasSize(i), t.updateStatus("ready")), i.hasSize = !0, i.loaded = !0, C("ImageLoadComplete")) : 200 > ++r ? setTimeout(s, 100) : a())
                        },
                        a = function () {
                            i && (i.img.off(".mfploader"), i === t.currItem && (t._onImageHasSize(i), t.updateStatus("error", o.tError.replace("%url%", i.src))), i.hasSize = !0, i.loaded = !0, i.loadError = !0)
                        },
                        o = t.st.image,
                        l = n.find(".mfp-img");
                    if (l.length) {
                        var d = document.createElement("img");
                        d.className = "mfp-img", i.el && i.el.find("img").length && (d.alt = i.el.find("img").attr("alt")), i.img = e(d).on("load.mfploader", s).on("error.mfploader", a), d.src = i.src, l.is("img") && (i.img = i.img.clone()), (d = i.img[0]).naturalWidth > 0 ? i.hasSize = !0 : d.width || (i.hasSize = !1)
                    }
                    return t._parseMarkup(n, {
                        title: N(i),
                        img_replaceWith: i.img
                    }, i), t.resizeImage(), i.hasSize ? (O && clearInterval(O), i.loadError ? (n.addClass("mfp-loading"), t.updateStatus("error", o.tError.replace("%url%", i.src))) : (n.removeClass("mfp-loading"), t.updateStatus("ready")), n) : (t.updateStatus("loading"), i.loading = !0, i.hasSize || (i.imgHidden = !0, n.addClass("mfp-loading"), t.findImageSize(i)), n)
                }
            }
        });
        var L;
        e.magnificPopup.registerModule("zoom", {
            options: {
                enabled: !1,
                easing: "ease-in-out",
                duration: 300,
                opener: function (e) {
                    return e.is("img") ? e : e.find("img")
                }
            },
            proto: {
                initZoom: function () {
                    var e, i = t.st.zoom,
                        n = ".zoom";
                    if (i.enabled && t.supportsTransition) {
                        var r, s, a = i.duration,
                            d = function (e) {
                                var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                    n = "all " + i.duration / 1e3 + "s " + i.easing,
                                    r = {
                                        position: "fixed",
                                        zIndex: 9999,
                                        left: 0,
                                        top: 0,
                                        "-webkit-backface-visibility": "hidden"
                                    },
                                    s = "transition";
                                return r["-webkit-" + s] = r["-moz-" + s] = r["-o-" + s] = r[s] = n, t.css(r), t
                            },
                            c = function () {
                                t.content.css("visibility", "visible")
                            };
                        w("BuildControls" + n, (function () {
                            if (t._allowZoom()) {
                                if (clearTimeout(r), t.content.css("visibility", "hidden"), !(e = t._getItemToZoom())) return void c();
                                (s = d(e)).css(t._getOffset()), t.wrap.append(s), r = setTimeout((function () {
                                    s.css(t._getOffset(!0)), r = setTimeout((function () {
                                        c(), setTimeout((function () {
                                            s.remove(), e = s = null, C("ZoomAnimationEnded")
                                        }), 16)
                                    }), a)
                                }), 16)
                            }
                        })), w(l + n, (function () {
                            if (t._allowZoom()) {
                                if (clearTimeout(r), t.st.removalDelay = a, !e) {
                                    if (!(e = t._getItemToZoom())) return;
                                    s = d(e)
                                }
                                s.css(t._getOffset(!0)), t.wrap.append(s), t.content.css("visibility", "hidden"), setTimeout((function () {
                                    s.css(t._getOffset())
                                }), 16)
                            }
                        })), w(o + n, (function () {
                            t._allowZoom() && (c(), s && s.remove(), e = null)
                        }))
                    }
                },
                _allowZoom: function () {
                    return "image" === t.currItem.type
                },
                _getItemToZoom: function () {
                    return !!t.currItem.hasSize && t.currItem.img
                },
                _getOffset: function (i) {
                    var n, r = (n = i ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem)).offset(),
                        s = parseInt(n.css("padding-top"), 10),
                        a = parseInt(n.css("padding-bottom"), 10);
                    r.top -= e(window).scrollTop() - s;
                    var o = {
                        width: n.width(),
                        height: (y ? n.innerHeight() : n[0].offsetHeight) - a - s
                    };
                    return void 0 === L && (L = void 0 !== document.createElement("p").style.MozTransform), L ? o["-moz-transform"] = o.transform = "translate(" + r.left + "px," + r.top + "px)" : (o.left = r.left, o.top = r.top), o
                }
            }
        });
        var z = "iframe",
            j = function (e) {
                if (t.currTemplate[z]) {
                    var i = t.currTemplate[z].find("iframe");
                    i.length && (e || (i[0].src = "//about:blank"), t.isIE8 && i.css("display", e ? "block" : "none"))
                }
            };
        e.magnificPopup.registerModule(z, {
            options: {
                markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                srcAction: "iframe_src",
                patterns: {
                    youtube: {
                        index: "youtube.com",
                        id: "v=",
                        src: "//www.youtube.com/embed/%id%?autoplay=1"
                    },
                    vimeo: {
                        index: "vimeo.com/",
                        id: "/",
                        src: "//player.vimeo.com/video/%id%?autoplay=1"
                    },
                    gmaps: {
                        index: "//maps.google.",
                        src: "%id%&output=embed"
                    }
                }
            },
            proto: {
                initIframe: function () {
                    t.types.push(z), w("BeforeChange", (function (e, t, i) {
                        t !== i && (t === z ? j() : i === z && j(!0))
                    })), w(o + "." + z, (function () {
                        j()
                    }))
                },
                getIframe: function (i, n) {
                    var r = i.src,
                        s = t.st.iframe;
                    e.each(s.patterns, (function () {
                        return r.indexOf(this.index) > -1 ? (this.id && (r = "string" == typeof this.id ? r.substr(r.lastIndexOf(this.id) + this.id.length, r.length) : this.id.call(this, r)), r = this.src.replace("%id%", r), !1) : void 0
                    }));
                    var a = {};
                    return s.srcAction && (a[s.srcAction] = r), t._parseMarkup(n, a, i), t.updateStatus("ready"), n
                }
            }
        });
        var H = function (e) {
                var i = t.items.length;
                return e > i - 1 ? e - i : 0 > e ? i + e : e
            },
            q = function (e, t, i) {
                return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, i)
            };
        e.magnificPopup.registerModule("gallery", {
            options: {
                enabled: !1,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0, 2],
                navigateByImgClick: !0,
                arrows: !0,
                tPrev: "Previous (Left arrow key)",
                tNext: "Next (Right arrow key)",
                tCounter: "%curr% of %total%"
            },
            proto: {
                initGallery: function () {
                    var i = t.st.gallery,
                        r = ".mfp-gallery";
                    return t.direction = !0, !(!i || !i.enabled) && (s += " mfp-gallery", w(c + r, (function () {
                        i.navigateByImgClick && t.wrap.on("click" + r, ".mfp-img", (function () {
                            return t.items.length > 1 ? (t.next(), !1) : void 0
                        })), n.on("keydown" + r, (function (e) {
                            37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                        }))
                    })), w("UpdateStatus" + r, (function (e, i) {
                        i.text && (i.text = q(i.text, t.currItem.index, t.items.length))
                    })), w(d + r, (function (e, n, r, s) {
                        var a = t.items.length;
                        r.counter = a > 1 ? q(i.tCounter, s.index, a) : ""
                    })), w("BuildControls" + r, (function () {
                        if (t.items.length > 1 && i.arrows && !t.arrowLeft) {
                            var n = i.arrowMarkup,
                                r = t.arrowLeft = e(n.replace(/%title%/gi, i.tPrev).replace(/%dir%/gi, "left")).addClass(m),
                                s = t.arrowRight = e(n.replace(/%title%/gi, i.tNext).replace(/%dir%/gi, "right")).addClass(m);
                            r.click((function () {
                                t.prev()
                            })), s.click((function () {
                                t.next()
                            })), t.container.append(r.add(s))
                        }
                    })), w(u + r, (function () {
                        t._preloadTimeout && clearTimeout(t._preloadTimeout), t._preloadTimeout = setTimeout((function () {
                            t.preloadNearbyImages(), t._preloadTimeout = null
                        }), 16)
                    })), void w(o + r, (function () {
                        n.off(r), t.wrap.off("click" + r), t.arrowRight = t.arrowLeft = null
                    })))
                },
                next: function () {
                    t.direction = !0, t.index = H(t.index + 1), t.updateItemHTML()
                },
                prev: function () {
                    t.direction = !1, t.index = H(t.index - 1), t.updateItemHTML()
                },
                goTo: function (e) {
                    t.direction = e >= t.index, t.index = e, t.updateItemHTML()
                },
                preloadNearbyImages: function () {
                    var e, i = t.st.gallery.preload,
                        n = Math.min(i[0], t.items.length),
                        r = Math.min(i[1], t.items.length);
                    for (e = 1; e <= (t.direction ? r : n); e++) t._preloadItem(t.index + e);
                    for (e = 1; e <= (t.direction ? n : r); e++) t._preloadItem(t.index - e)
                },
                _preloadItem: function (i) {
                    if (i = H(i), !t.items[i].preloaded) {
                        var n = t.items[i];
                        n.parsed || (n = t.parseEl(i)), C("LazyLoad", n), "image" === n.type && (n.img = e('<img class="mfp-img" />').on("load.mfploader", (function () {
                            n.hasSize = !0
                        })).on("error.mfploader", (function () {
                            n.hasSize = !0, n.loadError = !0, C("LazyLoadError", n)
                        })).attr("src", n.src)), n.preloaded = !0
                    }
                }
            }
        });
        var R = "retina";
        e.magnificPopup.registerModule(R, {
            options: {
                replaceSrc: function (e) {
                    return e.src.replace(/\.\w+$/, (function (e) {
                        return "@2x" + e
                    }))
                },
                ratio: 1
            },
            proto: {
                initRetina: function () {
                    if (window.devicePixelRatio > 1) {
                        var e = t.st.retina,
                            i = e.ratio;
                        (i = isNaN(i) ? i() : i) > 1 && (w("ImageHasSize." + R, (function (e, t) {
                            t.img.css({
                                "max-width": t.img[0].naturalWidth / i,
                                width: "100%"
                            })
                        })), w("ElementParse." + R, (function (t, n) {
                            n.src = e.replaceSrc(n, i)
                        })))
                    }
                }
            }
        }), E()
    })),
    function (e, t) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Swiper = t()
    }(this, (function () {
        "use strict";
        var e = "undefined" == typeof document ? {
                body: {},
                addEventListener: function () {},
                removeEventListener: function () {},
                activeElement: {
                    blur: function () {},
                    nodeName: ""
                },
                querySelector: function () {
                    return null
                },
                querySelectorAll: function () {
                    return []
                },
                getElementById: function () {
                    return null
                },
                createEvent: function () {
                    return {
                        initEvent: function () {}
                    }
                },
                createElement: function () {
                    return {
                        children: [],
                        childNodes: [],
                        style: {},
                        setAttribute: function () {},
                        getElementsByTagName: function () {
                            return []
                        }
                    }
                },
                location: {
                    hash: ""
                }
            } : document,
            t = "undefined" == typeof window ? {
                document: e,
                navigator: {
                    userAgent: ""
                },
                location: {},
                history: {},
                CustomEvent: function () {
                    return this
                },
                addEventListener: function () {},
                removeEventListener: function () {},
                getComputedStyle: function () {
                    return {
                        getPropertyValue: function () {
                            return ""
                        }
                    }
                },
                Image: function () {},
                Date: function () {},
                screen: {},
                setTimeout: function () {},
                clearTimeout: function () {}
            } : window,
            i = function (e) {
                for (var t = 0; t < e.length; t += 1) this[t] = e[t];
                return this.length = e.length, this
            };

        function n(n, r) {
            var s = [],
                a = 0;
            if (n && !r && n instanceof i) return n;
            if (n)
                if ("string" == typeof n) {
                    var o, l, d = n.trim();
                    if (0 <= d.indexOf("<") && 0 <= d.indexOf(">")) {
                        var c = "div";
                        for (0 === d.indexOf("<li") && (c = "ul"), 0 === d.indexOf("<tr") && (c = "tbody"), 0 !== d.indexOf("<td") && 0 !== d.indexOf("<th") || (c = "tr"), 0 === d.indexOf("<tbody") && (c = "table"), 0 === d.indexOf("<option") && (c = "select"), (l = e.createElement(c)).innerHTML = d, a = 0; a < l.childNodes.length; a += 1) s.push(l.childNodes[a])
                    } else
                        for (o = r || "#" !== n[0] || n.match(/[ .<>:~]/) ? (r || e).querySelectorAll(n.trim()) : [e.getElementById(n.trim().split("#")[1])], a = 0; a < o.length; a += 1) o[a] && s.push(o[a])
                } else if (n.nodeType || n === t || n === e) s.push(n);
            else if (0 < n.length && n[0].nodeType)
                for (a = 0; a < n.length; a += 1) s.push(n[a]);
            return new i(s)
        }

        function r(e) {
            for (var t = [], i = 0; i < e.length; i += 1) - 1 === t.indexOf(e[i]) && t.push(e[i]);
            return t
        }
        n.fn = i.prototype, n.Class = i, n.Dom7 = i;
        var s = {
            addClass: function (e) {
                if (void 0 === e) return this;
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.add(t[i]);
                return this
            },
            removeClass: function (e) {
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.remove(t[i]);
                return this
            },
            hasClass: function (e) {
                return !!this[0] && this[0].classList.contains(e)
            },
            toggleClass: function (e) {
                for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n] && void 0 !== this[n].classList && this[n].classList.toggle(t[i]);
                return this
            },
            attr: function (e, t) {
                var i = arguments;
                if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
                for (var n = 0; n < this.length; n += 1)
                    if (2 === i.length) this[n].setAttribute(e, t);
                    else
                        for (var r in e) this[n][r] = e[r], this[n].setAttribute(r, e[r]);
                return this
            },
            removeAttr: function (e) {
                for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e);
                return this
            },
            data: function (e, t) {
                var i;
                if (void 0 !== t) {
                    for (var n = 0; n < this.length; n += 1)(i = this[n]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[e] = t;
                    return this
                }
                if (i = this[0]) return i.dom7ElementDataStorage && e in i.dom7ElementDataStorage ? i.dom7ElementDataStorage[e] : i.getAttribute("data-" + e) || void 0
            },
            transform: function (e) {
                for (var t = 0; t < this.length; t += 1) {
                    var i = this[t].style;
                    i.webkitTransform = e, i.transform = e
                }
                return this
            },
            transition: function (e) {
                "string" != typeof e && (e += "ms");
                for (var t = 0; t < this.length; t += 1) {
                    var i = this[t].style;
                    i.webkitTransitionDuration = e, i.transitionDuration = e
                }
                return this
            },
            on: function () {
                for (var e, t = [], i = arguments.length; i--;) t[i] = arguments[i];
                var r = t[0],
                    s = t[1],
                    a = t[2],
                    o = t[3];

                function l(e) {
                    var t = e.target;
                    if (t) {
                        var i = e.target.dom7EventData || [];
                        if (i.indexOf(e) < 0 && i.unshift(e), n(t).is(s)) a.apply(t, i);
                        else
                            for (var r = n(t).parents(), o = 0; o < r.length; o += 1) n(r[o]).is(s) && a.apply(r[o], i)
                    }
                }

                function d(e) {
                    var t = e && e.target && e.target.dom7EventData || [];
                    t.indexOf(e) < 0 && t.unshift(e), a.apply(this, t)
                }
                "function" == typeof t[1] && (r = (e = t)[0], a = e[1], o = e[2], s = void 0), o || (o = !1);
                for (var c, u = r.split(" "), h = 0; h < this.length; h += 1) {
                    var p = this[h];
                    if (s)
                        for (c = 0; c < u.length; c += 1) {
                            var f = u[c];
                            p.dom7LiveListeners || (p.dom7LiveListeners = {}), p.dom7LiveListeners[f] || (p.dom7LiveListeners[f] = []), p.dom7LiveListeners[f].push({
                                listener: a,
                                proxyListener: l
                            }), p.addEventListener(f, l, o)
                        } else
                            for (c = 0; c < u.length; c += 1) {
                                var g = u[c];
                                p.dom7Listeners || (p.dom7Listeners = {}), p.dom7Listeners[g] || (p.dom7Listeners[g] = []), p.dom7Listeners[g].push({
                                    listener: a,
                                    proxyListener: d
                                }), p.addEventListener(g, d, o)
                            }
                }
                return this
            },
            off: function () {
                for (var e, t = [], i = arguments.length; i--;) t[i] = arguments[i];
                var n = t[0],
                    r = t[1],
                    s = t[2],
                    a = t[3];
                "function" == typeof t[1] && (n = (e = t)[0], s = e[1], a = e[2], r = void 0), a || (a = !1);
                for (var o = n.split(" "), l = 0; l < o.length; l += 1)
                    for (var d = o[l], c = 0; c < this.length; c += 1) {
                        var u = this[c],
                            h = void 0;
                        if (!r && u.dom7Listeners ? h = u.dom7Listeners[d] : r && u.dom7LiveListeners && (h = u.dom7LiveListeners[d]), h && h.length)
                            for (var p = h.length - 1; 0 <= p; p -= 1) {
                                var f = h[p];
                                s && f.listener === s ? (u.removeEventListener(d, f.proxyListener, a), h.splice(p, 1)) : s || (u.removeEventListener(d, f.proxyListener, a), h.splice(p, 1))
                            }
                    }
                return this
            },
            trigger: function () {
                for (var i = [], n = arguments.length; n--;) i[n] = arguments[n];
                for (var r = i[0].split(" "), s = i[1], a = 0; a < r.length; a += 1)
                    for (var o = r[a], l = 0; l < this.length; l += 1) {
                        var d = this[l],
                            c = void 0;
                        try {
                            c = new t.CustomEvent(o, {
                                detail: s,
                                bubbles: !0,
                                cancelable: !0
                            })
                        } catch (i) {
                            (c = e.createEvent("Event")).initEvent(o, !0, !0), c.detail = s
                        }
                        d.dom7EventData = i.filter((function (e, t) {
                            return 0 < t
                        })), d.dispatchEvent(c), d.dom7EventData = [], delete d.dom7EventData
                    }
                return this
            },
            transitionEnd: function (e) {
                var t, i = ["webkitTransitionEnd", "transitionend"],
                    n = this;

                function r(s) {
                    if (s.target === this)
                        for (e.call(this, s), t = 0; t < i.length; t += 1) n.off(i[t], r)
                }
                if (e)
                    for (t = 0; t < i.length; t += 1) n.on(i[t], r);
                return this
            },
            outerWidth: function (e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                    }
                    return this[0].offsetWidth
                }
                return null
            },
            outerHeight: function (e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                    }
                    return this[0].offsetHeight
                }
                return null
            },
            offset: function () {
                if (0 < this.length) {
                    var i = this[0],
                        n = i.getBoundingClientRect(),
                        r = e.body,
                        s = i.clientTop || r.clientTop || 0,
                        a = i.clientLeft || r.clientLeft || 0,
                        o = i === t ? t.scrollY : i.scrollTop,
                        l = i === t ? t.scrollX : i.scrollLeft;
                    return {
                        top: n.top + o - s,
                        left: n.left + l - a
                    }
                }
                return null
            },
            css: function (e, i) {
                var n;
                if (1 === arguments.length) {
                    if ("string" != typeof e) {
                        for (n = 0; n < this.length; n += 1)
                            for (var r in e) this[n].style[r] = e[r];
                        return this
                    }
                    if (this[0]) return t.getComputedStyle(this[0], null).getPropertyValue(e)
                }
                if (2 === arguments.length && "string" == typeof e) {
                    for (n = 0; n < this.length; n += 1) this[n].style[e] = i;
                    return this
                }
                return this
            },
            each: function (e) {
                if (!e) return this;
                for (var t = 0; t < this.length; t += 1)
                    if (!1 === e.call(this[t], t, this[t])) return this;
                return this
            },
            html: function (e) {
                if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;
                for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e;
                return this
            },
            text: function (e) {
                if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;
                for (var t = 0; t < this.length; t += 1) this[t].textContent = e;
                return this
            },
            is: function (r) {
                var s, a, o = this[0];
                if (!o || void 0 === r) return !1;
                if ("string" == typeof r) {
                    if (o.matches) return o.matches(r);
                    if (o.webkitMatchesSelector) return o.webkitMatchesSelector(r);
                    if (o.msMatchesSelector) return o.msMatchesSelector(r);
                    for (s = n(r), a = 0; a < s.length; a += 1)
                        if (s[a] === o) return !0;
                    return !1
                }
                if (r === e) return o === e;
                if (r === t) return o === t;
                if (r.nodeType || r instanceof i) {
                    for (s = r.nodeType ? [r] : r, a = 0; a < s.length; a += 1)
                        if (s[a] === o) return !0;
                    return !1
                }
                return !1
            },
            index: function () {
                var e, t = this[0];
                if (t) {
                    for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1);
                    return e
                }
            },
            eq: function (e) {
                if (void 0 === e) return this;
                var t, n = this.length;
                return new i(n - 1 < e ? [] : e < 0 ? (t = n + e) < 0 ? [] : [this[t]] : [this[e]])
            },
            append: function () {
                for (var t, n = [], r = arguments.length; r--;) n[r] = arguments[r];
                for (var s = 0; s < n.length; s += 1) {
                    t = n[s];
                    for (var a = 0; a < this.length; a += 1)
                        if ("string" == typeof t) {
                            var o = e.createElement("div");
                            for (o.innerHTML = t; o.firstChild;) this[a].appendChild(o.firstChild)
                        } else if (t instanceof i)
                        for (var l = 0; l < t.length; l += 1) this[a].appendChild(t[l]);
                    else this[a].appendChild(t)
                }
                return this
            },
            prepend: function (t) {
                var n, r, s = this;
                for (n = 0; n < this.length; n += 1)
                    if ("string" == typeof t) {
                        var a = e.createElement("div");
                        for (a.innerHTML = t, r = a.childNodes.length - 1; 0 <= r; r -= 1) s[n].insertBefore(a.childNodes[r], s[n].childNodes[0])
                    } else if (t instanceof i)
                    for (r = 0; r < t.length; r += 1) s[n].insertBefore(t[r], s[n].childNodes[0]);
                else s[n].insertBefore(t, s[n].childNodes[0]);
                return this
            },
            next: function (e) {
                return 0 < this.length ? e ? this[0].nextElementSibling && n(this[0].nextElementSibling).is(e) ? new i([this[0].nextElementSibling]) : new i([]) : this[0].nextElementSibling ? new i([this[0].nextElementSibling]) : new i([]) : new i([])
            },
            nextAll: function (e) {
                var t = [],
                    r = this[0];
                if (!r) return new i([]);
                for (; r.nextElementSibling;) {
                    var s = r.nextElementSibling;
                    e ? n(s).is(e) && t.push(s) : t.push(s), r = s
                }
                return new i(t)
            },
            prev: function (e) {
                if (0 < this.length) {
                    var t = this[0];
                    return e ? t.previousElementSibling && n(t.previousElementSibling).is(e) ? new i([t.previousElementSibling]) : new i([]) : t.previousElementSibling ? new i([t.previousElementSibling]) : new i([])
                }
                return new i([])
            },
            prevAll: function (e) {
                var t = [],
                    r = this[0];
                if (!r) return new i([]);
                for (; r.previousElementSibling;) {
                    var s = r.previousElementSibling;
                    e ? n(s).is(e) && t.push(s) : t.push(s), r = s
                }
                return new i(t)
            },
            parent: function (e) {
                for (var t = [], i = 0; i < this.length; i += 1) null !== this[i].parentNode && (e ? n(this[i].parentNode).is(e) && t.push(this[i].parentNode) : t.push(this[i].parentNode));
                return n(r(t))
            },
            parents: function (e) {
                for (var t = [], i = 0; i < this.length; i += 1)
                    for (var s = this[i].parentNode; s;) e ? n(s).is(e) && t.push(s) : t.push(s), s = s.parentNode;
                return n(r(t))
            },
            closest: function (e) {
                var t = this;
                return void 0 === e ? new i([]) : (t.is(e) || (t = t.parents(e).eq(0)), t)
            },
            find: function (e) {
                for (var t = [], n = 0; n < this.length; n += 1)
                    for (var r = this[n].querySelectorAll(e), s = 0; s < r.length; s += 1) t.push(r[s]);
                return new i(t)
            },
            children: function (e) {
                for (var t = [], s = 0; s < this.length; s += 1)
                    for (var a = this[s].childNodes, o = 0; o < a.length; o += 1) e ? 1 === a[o].nodeType && n(a[o]).is(e) && t.push(a[o]) : 1 === a[o].nodeType && t.push(a[o]);
                return new i(r(t))
            },
            remove: function () {
                for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]);
                return this
            },
            add: function () {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                var i, r;
                for (i = 0; i < e.length; i += 1) {
                    var s = n(e[i]);
                    for (r = 0; r < s.length; r += 1) this[this.length] = s[r], this.length += 1
                }
                return this
            },
            styles: function () {
                return this[0] ? t.getComputedStyle(this[0], null) : {}
            }
        };
        Object.keys(s).forEach((function (e) {
            n.fn[e] = s[e]
        }));
        var a, o, l, d = {
                deleteProps: function (e) {
                    var t = e;
                    Object.keys(t).forEach((function (e) {
                        try {
                            t[e] = null
                        } catch (e) {}
                        try {
                            delete t[e]
                        } catch (e) {}
                    }))
                },
                nextTick: function (e, t) {
                    return void 0 === t && (t = 0), setTimeout(e, t)
                },
                now: function () {
                    return Date.now()
                },
                getTranslate: function (e, i) {
                    var n, r, s;
                    void 0 === i && (i = "x");
                    var a = t.getComputedStyle(e, null);
                    return t.WebKitCSSMatrix ? (6 < (r = a.transform || a.webkitTransform).split(",").length && (r = r.split(", ").map((function (e) {
                        return e.replace(",", ".")
                    })).join(", ")), s = new t.WebKitCSSMatrix("none" === r ? "" : r)) : n = (s = a.MozTransform || a.OTransform || a.MsTransform || a.msTransform || a.transform || a.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === i && (r = t.WebKitCSSMatrix ? s.m41 : 16 === n.length ? parseFloat(n[12]) : parseFloat(n[4])), "y" === i && (r = t.WebKitCSSMatrix ? s.m42 : 16 === n.length ? parseFloat(n[13]) : parseFloat(n[5])), r || 0
                },
                parseUrlQuery: function (e) {
                    var i, n, r, s, a = {},
                        o = e || t.location.href;
                    if ("string" == typeof o && o.length)
                        for (s = (n = (o = -1 < o.indexOf("?") ? o.replace(/\S*\?/, "") : "").split("&").filter((function (e) {
                                return "" !== e
                            }))).length, i = 0; i < s; i += 1) r = n[i].replace(/#\S+/g, "").split("="), a[decodeURIComponent(r[0])] = void 0 === r[1] ? void 0 : decodeURIComponent(r[1]) || "";
                    return a
                },
                isObject: function (e) {
                    return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
                },
                extend: function () {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    for (var i = Object(e[0]), n = 1; n < e.length; n += 1) {
                        var r = e[n];
                        if (null != r)
                            for (var s = Object.keys(Object(r)), a = 0, o = s.length; a < o; a += 1) {
                                var l = s[a],
                                    c = Object.getOwnPropertyDescriptor(r, l);
                                void 0 !== c && c.enumerable && (d.isObject(i[l]) && d.isObject(r[l]) ? d.extend(i[l], r[l]) : !d.isObject(i[l]) && d.isObject(r[l]) ? (i[l] = {}, d.extend(i[l], r[l])) : i[l] = r[l])
                            }
                    }
                    return i
                }
            },
            c = (l = e.createElement("div"), {
                touch: t.Modernizr && !0 === t.Modernizr.touch || !!("ontouchstart" in t || t.DocumentTouch && e instanceof t.DocumentTouch),
                pointerEvents: !(!t.navigator.pointerEnabled && !t.PointerEvent),
                prefixedPointerEvents: !!t.navigator.msPointerEnabled,
                transition: (o = l.style, "transition" in o || "webkitTransition" in o || "MozTransition" in o),
                transforms3d: t.Modernizr && !0 === t.Modernizr.csstransforms3d || (a = l.style, "webkitPerspective" in a || "MozPerspective" in a || "OPerspective" in a || "MsPerspective" in a || "perspective" in a),
                flexbox: function () {
                    for (var e = l.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < t.length; i += 1)
                        if (t[i] in e) return !0;
                    return !1
                }(),
                observer: "MutationObserver" in t || "WebkitMutationObserver" in t,
                passiveListener: function () {
                    var e = !1;
                    try {
                        var i = Object.defineProperty({}, "passive", {
                            get: function () {
                                e = !0
                            }
                        });
                        t.addEventListener("testPassiveListener", null, i)
                    } catch (e) {}
                    return e
                }(),
                gestures: "ongesturestart" in t
            }),
            u = function (e) {
                void 0 === e && (e = {});
                var t = this;
                t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach((function (e) {
                    t.on(e, t.params.on[e])
                }))
            },
            h = {
                components: {
                    configurable: !0
                }
            };
        u.prototype.on = function (e, t, i) {
            var n = this;
            if ("function" != typeof t) return n;
            var r = i ? "unshift" : "push";
            return e.split(" ").forEach((function (e) {
                n.eventsListeners[e] || (n.eventsListeners[e] = []), n.eventsListeners[e][r](t)
            })), n
        }, u.prototype.once = function (e, t, i) {
            var n = this;
            return "function" != typeof t ? n : n.on(e, (function i() {
                for (var r = [], s = arguments.length; s--;) r[s] = arguments[s];
                t.apply(n, r), n.off(e, i)
            }), i)
        }, u.prototype.off = function (e, t) {
            var i = this;
            return i.eventsListeners && e.split(" ").forEach((function (e) {
                void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].length && i.eventsListeners[e].forEach((function (n, r) {
                    n === t && i.eventsListeners[e].splice(r, 1)
                }))
            })), i
        }, u.prototype.emit = function () {
            for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
            var i, n, r, s = this;
            return s.eventsListeners && ("string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], n = e.slice(1, e.length), r = s) : (i = e[0].events, n = e[0].data, r = e[0].context || s), (Array.isArray(i) ? i : i.split(" ")).forEach((function (e) {
                if (s.eventsListeners && s.eventsListeners[e]) {
                    var t = [];
                    s.eventsListeners[e].forEach((function (e) {
                        t.push(e)
                    })), t.forEach((function (e) {
                        e.apply(r, n)
                    }))
                }
            }))), s
        }, u.prototype.useModulesParams = function (e) {
            var t = this;
            t.modules && Object.keys(t.modules).forEach((function (i) {
                var n = t.modules[i];
                n.params && d.extend(e, n.params)
            }))
        }, u.prototype.useModules = function (e) {
            void 0 === e && (e = {});
            var t = this;
            t.modules && Object.keys(t.modules).forEach((function (i) {
                var n = t.modules[i],
                    r = e[i] || {};
                n.instance && Object.keys(n.instance).forEach((function (e) {
                    var i = n.instance[e];
                    t[e] = "function" == typeof i ? i.bind(t) : i
                })), n.on && t.on && Object.keys(n.on).forEach((function (e) {
                    t.on(e, n.on[e])
                })), n.create && n.create.bind(t)(r)
            }))
        }, h.components.set = function (e) {
            this.use && this.use(e)
        }, u.installModule = function (e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;) t[i] = arguments[i + 1];
            var n = this;
            n.prototype.modules || (n.prototype.modules = {});
            var r = e.name || Object.keys(n.prototype.modules).length + "_" + d.now();
            return (n.prototype.modules[r] = e).proto && Object.keys(e.proto).forEach((function (t) {
                n.prototype[t] = e.proto[t]
            })), e.static && Object.keys(e.static).forEach((function (t) {
                n[t] = e.static[t]
            })), e.install && e.install.apply(n, t), n
        }, u.use = function (e) {
            for (var t = [], i = arguments.length - 1; 0 < i--;) t[i] = arguments[i + 1];
            var n = this;
            return Array.isArray(e) ? (e.forEach((function (e) {
                return n.installModule(e)
            })), n) : n.installModule.apply(n, [e].concat(t))
        }, Object.defineProperties(u, h);
        var p = {
                updateSize: function () {
                    var e, t, i = this,
                        n = i.$el;
                    e = void 0 !== i.params.width ? i.params.width : n[0].clientWidth, t = void 0 !== i.params.height ? i.params.height : n[0].clientHeight, 0 === e && i.isHorizontal() || 0 === t && i.isVertical() || (e = e - parseInt(n.css("padding-left"), 10) - parseInt(n.css("padding-right"), 10), t = t - parseInt(n.css("padding-top"), 10) - parseInt(n.css("padding-bottom"), 10), d.extend(i, {
                        width: e,
                        height: t,
                        size: i.isHorizontal() ? e : t
                    }))
                },
                updateSlides: function () {
                    var e = this,
                        i = e.params,
                        n = e.$wrapperEl,
                        r = e.size,
                        s = e.rtlTranslate,
                        a = e.wrongRTL,
                        o = e.virtual && i.virtual.enabled,
                        l = o ? e.virtual.slides.length : e.slides.length,
                        u = n.children("." + e.params.slideClass),
                        h = o ? e.virtual.slides.length : u.length,
                        p = [],
                        f = [],
                        g = [],
                        m = i.slidesOffsetBefore;
                    "function" == typeof m && (m = i.slidesOffsetBefore.call(e));
                    var v = i.slidesOffsetAfter;
                    "function" == typeof v && (v = i.slidesOffsetAfter.call(e));
                    var y = e.snapGrid.length,
                        b = e.snapGrid.length,
                        w = i.spaceBetween,
                        x = -m,
                        C = 0,
                        T = 0;
                    if (void 0 !== r) {
                        var E, S;
                        "string" == typeof w && 0 <= w.indexOf("%") && (w = parseFloat(w.replace("%", "")) / 100 * r), e.virtualSize = -w, s ? u.css({
                            marginLeft: "",
                            marginTop: ""
                        }) : u.css({
                            marginRight: "",
                            marginBottom: ""
                        }), 1 < i.slidesPerColumn && (E = Math.floor(h / i.slidesPerColumn) === h / e.params.slidesPerColumn ? h : Math.ceil(h / i.slidesPerColumn) * i.slidesPerColumn, "auto" !== i.slidesPerView && "row" === i.slidesPerColumnFill && (E = Math.max(E, i.slidesPerView * i.slidesPerColumn)));
                        for (var _, k = i.slidesPerColumn, $ = E / k, I = $ - (i.slidesPerColumn * $ - h), D = 0; D < h; D += 1) {
                            S = 0;
                            var A = u.eq(D);
                            if (1 < i.slidesPerColumn) {
                                var M = void 0,
                                    P = void 0,
                                    O = void 0;
                                "column" === i.slidesPerColumnFill ? (O = D - (P = Math.floor(D / k)) * k, (I < P || P === I && O === k - 1) && k <= (O += 1) && (O = 0, P += 1), M = P + O * E / k, A.css({
                                    "-webkit-box-ordinal-group": M,
                                    "-moz-box-ordinal-group": M,
                                    "-ms-flex-order": M,
                                    "-webkit-order": M,
                                    order: M
                                })) : P = D - (O = Math.floor(D / $)) * $, A.css("margin-" + (e.isHorizontal() ? "top" : "left"), 0 !== O && i.spaceBetween && i.spaceBetween + "px").attr("data-swiper-column", P).attr("data-swiper-row", O)
                            }
                            if ("none" !== A.css("display")) {
                                if ("auto" === i.slidesPerView) {
                                    var N = t.getComputedStyle(A[0], null),
                                        L = A[0].style.transform,
                                        z = A[0].style.webkitTransform;
                                    L && (A[0].style.transform = "none"), z && (A[0].style.webkitTransform = "none"), S = i.roundLengths ? e.isHorizontal() ? A.outerWidth(!0) : A.outerHeight(!0) : e.isHorizontal() ? A[0].getBoundingClientRect().width + parseFloat(N.getPropertyValue("margin-left")) + parseFloat(N.getPropertyValue("margin-right")) : A[0].getBoundingClientRect().height + parseFloat(N.getPropertyValue("margin-top")) + parseFloat(N.getPropertyValue("margin-bottom")), L && (A[0].style.transform = L), z && (A[0].style.webkitTransform = z), i.roundLengths && (S = Math.floor(S))
                                } else S = (r - (i.slidesPerView - 1) * w) / i.slidesPerView, i.roundLengths && (S = Math.floor(S)), u[D] && (e.isHorizontal() ? u[D].style.width = S + "px" : u[D].style.height = S + "px");
                                u[D] && (u[D].swiperSlideSize = S), g.push(S), i.centeredSlides ? (x = x + S / 2 + C / 2 + w, 0 === C && 0 !== D && (x = x - r / 2 - w), 0 === D && (x = x - r / 2 - w), Math.abs(x) < .001 && (x = 0), i.roundLengths && (x = Math.floor(x)), T % i.slidesPerGroup == 0 && p.push(x), f.push(x)) : (i.roundLengths && (x = Math.floor(x)), T % i.slidesPerGroup == 0 && p.push(x), f.push(x), x = x + S + w), e.virtualSize += S + w, C = S, T += 1
                            }
                        }
                        if (e.virtualSize = Math.max(e.virtualSize, r) + v, s && a && ("slide" === i.effect || "coverflow" === i.effect) && n.css({
                                width: e.virtualSize + i.spaceBetween + "px"
                            }), c.flexbox && !i.setWrapperSize || (e.isHorizontal() ? n.css({
                                width: e.virtualSize + i.spaceBetween + "px"
                            }) : n.css({
                                height: e.virtualSize + i.spaceBetween + "px"
                            })), 1 < i.slidesPerColumn && (e.virtualSize = (S + i.spaceBetween) * E, e.virtualSize = Math.ceil(e.virtualSize / i.slidesPerColumn) - i.spaceBetween, e.isHorizontal() ? n.css({
                                width: e.virtualSize + i.spaceBetween + "px"
                            }) : n.css({
                                height: e.virtualSize + i.spaceBetween + "px"
                            }), i.centeredSlides)) {
                            _ = [];
                            for (var j = 0; j < p.length; j += 1) {
                                var H = p[j];
                                i.roundLengths && (H = Math.floor(H)), p[j] < e.virtualSize + p[0] && _.push(H)
                            }
                            p = _
                        }
                        if (!i.centeredSlides) {
                            _ = [];
                            for (var q = 0; q < p.length; q += 1) {
                                var R = p[q];
                                i.roundLengths && (R = Math.floor(R)), p[q] <= e.virtualSize - r && _.push(R)
                            }
                            p = _, 1 < Math.floor(e.virtualSize - r) - Math.floor(p[p.length - 1]) && p.push(e.virtualSize - r)
                        }
                        if (0 === p.length && (p = [0]), 0 !== i.spaceBetween && (e.isHorizontal() ? s ? u.css({
                                marginLeft: w + "px"
                            }) : u.css({
                                marginRight: w + "px"
                            }) : u.css({
                                marginBottom: w + "px"
                            })), i.centerInsufficientSlides) {
                            var B = 0;
                            if (g.forEach((function (e) {
                                    B += e + (i.spaceBetween ? i.spaceBetween : 0)
                                })), (B -= i.spaceBetween) < r) {
                                var W = (r - B) / 2;
                                p.forEach((function (e, t) {
                                    p[t] = e - W
                                })), f.forEach((function (e, t) {
                                    f[t] = e + W
                                }))
                            }
                        }
                        d.extend(e, {
                            slides: u,
                            snapGrid: p,
                            slidesGrid: f,
                            slidesSizesGrid: g
                        }), h !== l && e.emit("slidesLengthChange"), p.length !== y && (e.params.watchOverflow && e.checkOverflow(), e.emit("snapGridLengthChange")), f.length !== b && e.emit("slidesGridLengthChange"), (i.watchSlidesProgress || i.watchSlidesVisibility) && e.updateSlidesOffset()
                    }
                },
                updateAutoHeight: function (e) {
                    var t, i = this,
                        n = [],
                        r = 0;
                    if ("number" == typeof e ? i.setTransition(e) : !0 === e && i.setTransition(i.params.speed), "auto" !== i.params.slidesPerView && 1 < i.params.slidesPerView)
                        for (t = 0; t < Math.ceil(i.params.slidesPerView); t += 1) {
                            var s = i.activeIndex + t;
                            if (s > i.slides.length) break;
                            n.push(i.slides.eq(s)[0])
                        } else n.push(i.slides.eq(i.activeIndex)[0]);
                    for (t = 0; t < n.length; t += 1)
                        if (void 0 !== n[t]) {
                            var a = n[t].offsetHeight;
                            r = r < a ? a : r
                        } r && i.$wrapperEl.css("height", r + "px")
                },
                updateSlidesOffset: function () {
                    for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
                },
                updateSlidesProgress: function (e) {
                    void 0 === e && (e = this && this.translate || 0);
                    var t = this,
                        i = t.params,
                        r = t.slides,
                        s = t.rtlTranslate;
                    if (0 !== r.length) {
                        void 0 === r[0].swiperSlideOffset && t.updateSlidesOffset();
                        var a = -e;
                        s && (a = e), r.removeClass(i.slideVisibleClass), t.visibleSlidesIndexes = [], t.visibleSlides = [];
                        for (var o = 0; o < r.length; o += 1) {
                            var l = r[o],
                                d = (a + (i.centeredSlides ? t.minTranslate() : 0) - l.swiperSlideOffset) / (l.swiperSlideSize + i.spaceBetween);
                            if (i.watchSlidesVisibility) {
                                var c = -(a - l.swiperSlideOffset),
                                    u = c + t.slidesSizesGrid[o];
                                (0 <= c && c < t.size || 0 < u && u <= t.size || c <= 0 && u >= t.size) && (t.visibleSlides.push(l), t.visibleSlidesIndexes.push(o), r.eq(o).addClass(i.slideVisibleClass))
                            }
                            l.progress = s ? -d : d
                        }
                        t.visibleSlides = n(t.visibleSlides)
                    }
                },
                updateProgress: function (e) {
                    void 0 === e && (e = this && this.translate || 0);
                    var t = this,
                        i = t.params,
                        n = t.maxTranslate() - t.minTranslate(),
                        r = t.progress,
                        s = t.isBeginning,
                        a = t.isEnd,
                        o = s,
                        l = a;
                    0 === n ? a = s = !(r = 0) : (s = (r = (e - t.minTranslate()) / n) <= 0, a = 1 <= r), d.extend(t, {
                        progress: r,
                        isBeginning: s,
                        isEnd: a
                    }), (i.watchSlidesProgress || i.watchSlidesVisibility) && t.updateSlidesProgress(e), s && !o && t.emit("reachBeginning toEdge"), a && !l && t.emit("reachEnd toEdge"), (o && !s || l && !a) && t.emit("fromEdge"), t.emit("progress", r)
                },
                updateSlidesClasses: function () {
                    var e, t = this,
                        i = t.slides,
                        n = t.params,
                        r = t.$wrapperEl,
                        s = t.activeIndex,
                        a = t.realIndex,
                        o = t.virtual && n.virtual.enabled;
                    i.removeClass(n.slideActiveClass + " " + n.slideNextClass + " " + n.slidePrevClass + " " + n.slideDuplicateActiveClass + " " + n.slideDuplicateNextClass + " " + n.slideDuplicatePrevClass), (e = o ? t.$wrapperEl.find("." + n.slideClass + '[data-swiper-slide-index="' + s + '"]') : i.eq(s)).addClass(n.slideActiveClass), n.loop && (e.hasClass(n.slideDuplicateClass) ? r.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + a + '"]').addClass(n.slideDuplicateActiveClass) : r.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + a + '"]').addClass(n.slideDuplicateActiveClass));
                    var l = e.nextAll("." + n.slideClass).eq(0).addClass(n.slideNextClass);
                    n.loop && 0 === l.length && (l = i.eq(0)).addClass(n.slideNextClass);
                    var d = e.prevAll("." + n.slideClass).eq(0).addClass(n.slidePrevClass);
                    n.loop && 0 === d.length && (d = i.eq(-1)).addClass(n.slidePrevClass), n.loop && (l.hasClass(n.slideDuplicateClass) ? r.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass) : r.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass), d.hasClass(n.slideDuplicateClass) ? r.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass) : r.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass))
                },
                updateActiveIndex: function (e) {
                    var t, i = this,
                        n = i.rtlTranslate ? i.translate : -i.translate,
                        r = i.slidesGrid,
                        s = i.snapGrid,
                        a = i.params,
                        o = i.activeIndex,
                        l = i.realIndex,
                        c = i.snapIndex,
                        u = e;
                    if (void 0 === u) {
                        for (var h = 0; h < r.length; h += 1) void 0 !== r[h + 1] ? n >= r[h] && n < r[h + 1] - (r[h + 1] - r[h]) / 2 ? u = h : n >= r[h] && n < r[h + 1] && (u = h + 1) : n >= r[h] && (u = h);
                        a.normalizeSlideIndex && (u < 0 || void 0 === u) && (u = 0)
                    }
                    if ((t = 0 <= s.indexOf(n) ? s.indexOf(n) : Math.floor(u / a.slidesPerGroup)) >= s.length && (t = s.length - 1), u !== o) {
                        var p = parseInt(i.slides.eq(u).attr("data-swiper-slide-index") || u, 10);
                        d.extend(i, {
                            snapIndex: t,
                            realIndex: p,
                            previousIndex: o,
                            activeIndex: u
                        }), i.emit("activeIndexChange"), i.emit("snapIndexChange"), l !== p && i.emit("realIndexChange"), i.emit("slideChange")
                    } else t !== c && (i.snapIndex = t, i.emit("snapIndexChange"))
                },
                updateClickedSlide: function (e) {
                    var t = this,
                        i = t.params,
                        r = n(e.target).closest("." + i.slideClass)[0],
                        s = !1;
                    if (r)
                        for (var a = 0; a < t.slides.length; a += 1) t.slides[a] === r && (s = !0);
                    if (!r || !s) return t.clickedSlide = void 0, void(t.clickedIndex = void 0);
                    t.clickedSlide = r, t.virtual && t.params.virtual.enabled ? t.clickedIndex = parseInt(n(r).attr("data-swiper-slide-index"), 10) : t.clickedIndex = n(r).index(), i.slideToClickedSlide && void 0 !== t.clickedIndex && t.clickedIndex !== t.activeIndex && t.slideToClickedSlide()
                }
            },
            f = {
                getTranslate: function (e) {
                    void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                    var t = this.params,
                        i = this.rtlTranslate,
                        n = this.translate,
                        r = this.$wrapperEl;
                    if (t.virtualTranslate) return i ? -n : n;
                    var s = d.getTranslate(r[0], e);
                    return i && (s = -s), s || 0
                },
                setTranslate: function (e, t) {
                    var i = this,
                        n = i.rtlTranslate,
                        r = i.params,
                        s = i.$wrapperEl,
                        a = i.progress,
                        o = 0,
                        l = 0;
                    i.isHorizontal() ? o = n ? -e : e : l = e, r.roundLengths && (o = Math.floor(o), l = Math.floor(l)), r.virtualTranslate || (c.transforms3d ? s.transform("translate3d(" + o + "px, " + l + "px, 0px)") : s.transform("translate(" + o + "px, " + l + "px)")), i.previousTranslate = i.translate, i.translate = i.isHorizontal() ? o : l;
                    var d = i.maxTranslate() - i.minTranslate();
                    (0 === d ? 0 : (e - i.minTranslate()) / d) !== a && i.updateProgress(e), i.emit("setTranslate", i.translate, t)
                },
                minTranslate: function () {
                    return -this.snapGrid[0]
                },
                maxTranslate: function () {
                    return -this.snapGrid[this.snapGrid.length - 1]
                }
            },
            g = {
                slideTo: function (e, t, i, n) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                    var r = this,
                        s = e;
                    s < 0 && (s = 0);
                    var a = r.params,
                        o = r.snapGrid,
                        l = r.slidesGrid,
                        d = r.previousIndex,
                        u = r.activeIndex,
                        h = r.rtlTranslate;
                    if (r.animating && a.preventInteractionOnTransition) return !1;
                    var p = Math.floor(s / a.slidesPerGroup);
                    p >= o.length && (p = o.length - 1), (u || a.initialSlide || 0) === (d || 0) && i && r.emit("beforeSlideChangeStart");
                    var f, g = -o[p];
                    if (r.updateProgress(g), a.normalizeSlideIndex)
                        for (var m = 0; m < l.length; m += 1) - Math.floor(100 * g) >= Math.floor(100 * l[m]) && (s = m);
                    if (r.initialized && s !== u) {
                        if (!r.allowSlideNext && g < r.translate && g < r.minTranslate()) return !1;
                        if (!r.allowSlidePrev && g > r.translate && g > r.maxTranslate() && (u || 0) !== s) return !1
                    }
                    return f = u < s ? "next" : s < u ? "prev" : "reset", h && -g === r.translate || !h && g === r.translate ? (r.updateActiveIndex(s), a.autoHeight && r.updateAutoHeight(), r.updateSlidesClasses(), "slide" !== a.effect && r.setTranslate(g), "reset" !== f && (r.transitionStart(i, f), r.transitionEnd(i, f)), !1) : (0 !== t && c.transition ? (r.setTransition(t), r.setTranslate(g), r.updateActiveIndex(s), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, n), r.transitionStart(i, f), r.animating || (r.animating = !0, r.onSlideToWrapperTransitionEnd || (r.onSlideToWrapperTransitionEnd = function (e) {
                        r && !r.destroyed && e.target === this && (r.$wrapperEl[0].removeEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].removeEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd), r.onSlideToWrapperTransitionEnd = null, delete r.onSlideToWrapperTransitionEnd, r.transitionEnd(i, f))
                    }), r.$wrapperEl[0].addEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].addEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd))) : (r.setTransition(0), r.setTranslate(g), r.updateActiveIndex(s), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, n), r.transitionStart(i, f), r.transitionEnd(i, f)), !0)
                },
                slideToLoop: function (e, t, i, n) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                    var r = e;
                    return this.params.loop && (r += this.loopedSlides), this.slideTo(r, t, i, n)
                },
                slideNext: function (e, t, i) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var n = this,
                        r = n.params,
                        s = n.animating;
                    return r.loop ? !s && (n.loopFix(), n._clientLeft = n.$wrapperEl[0].clientLeft, n.slideTo(n.activeIndex + r.slidesPerGroup, e, t, i)) : n.slideTo(n.activeIndex + r.slidesPerGroup, e, t, i)
                },
                slidePrev: function (e, t, i) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var n = this,
                        r = n.params,
                        s = n.animating,
                        a = n.snapGrid,
                        o = n.slidesGrid,
                        l = n.rtlTranslate;
                    if (r.loop) {
                        if (s) return !1;
                        n.loopFix(), n._clientLeft = n.$wrapperEl[0].clientLeft
                    }

                    function d(e) {
                        return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                    }
                    var c, u = d(l ? n.translate : -n.translate),
                        h = a.map((function (e) {
                            return d(e)
                        })),
                        p = (o.map((function (e) {
                            return d(e)
                        })), a[h.indexOf(u)], a[h.indexOf(u) - 1]);
                    return void 0 !== p && (c = o.indexOf(p)) < 0 && (c = n.activeIndex - 1), n.slideTo(c, e, t, i)
                },
                slideReset: function (e, t, i) {
                    return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, i)
                },
                slideToClosest: function (e, t, i) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var n = this,
                        r = n.activeIndex,
                        s = Math.floor(r / n.params.slidesPerGroup);
                    if (s < n.snapGrid.length - 1) {
                        var a = n.rtlTranslate ? n.translate : -n.translate,
                            o = n.snapGrid[s];
                        (n.snapGrid[s + 1] - o) / 2 < a - o && (r = n.params.slidesPerGroup)
                    }
                    return n.slideTo(r, e, t, i)
                },
                slideToClickedSlide: function () {
                    var e, t = this,
                        i = t.params,
                        r = t.$wrapperEl,
                        s = "auto" === i.slidesPerView ? t.slidesPerViewDynamic() : i.slidesPerView,
                        a = t.clickedIndex;
                    if (i.loop) {
                        if (t.animating) return;
                        e = parseInt(n(t.clickedSlide).attr("data-swiper-slide-index"), 10), i.centeredSlides ? a < t.loopedSlides - s / 2 || a > t.slides.length - t.loopedSlides + s / 2 ? (t.loopFix(), a = r.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), d.nextTick((function () {
                            t.slideTo(a)
                        }))) : t.slideTo(a) : a > t.slides.length - s ? (t.loopFix(), a = r.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), d.nextTick((function () {
                            t.slideTo(a)
                        }))) : t.slideTo(a)
                    } else t.slideTo(a)
                }
            },
            m = {
                loopCreate: function () {
                    var t = this,
                        i = t.params,
                        r = t.$wrapperEl;
                    r.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                    var s = r.children("." + i.slideClass);
                    if (i.loopFillGroupWithBlank) {
                        var a = i.slidesPerGroup - s.length % i.slidesPerGroup;
                        if (a !== i.slidesPerGroup) {
                            for (var o = 0; o < a; o += 1) {
                                var l = n(e.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                                r.append(l)
                            }
                            s = r.children("." + i.slideClass)
                        }
                    }
                    "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = s.length), t.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10), t.loopedSlides += i.loopAdditionalSlides, t.loopedSlides > s.length && (t.loopedSlides = s.length);
                    var d = [],
                        c = [];
                    s.each((function (e, i) {
                        var r = n(i);
                        e < t.loopedSlides && c.push(i), e < s.length && e >= s.length - t.loopedSlides && d.push(i), r.attr("data-swiper-slide-index", e)
                    }));
                    for (var u = 0; u < c.length; u += 1) r.append(n(c[u].cloneNode(!0)).addClass(i.slideDuplicateClass));
                    for (var h = d.length - 1; 0 <= h; h -= 1) r.prepend(n(d[h].cloneNode(!0)).addClass(i.slideDuplicateClass))
                },
                loopFix: function () {
                    var e, t = this,
                        i = t.params,
                        n = t.activeIndex,
                        r = t.slides,
                        s = t.loopedSlides,
                        a = t.allowSlidePrev,
                        o = t.allowSlideNext,
                        l = t.snapGrid,
                        d = t.rtlTranslate;
                    t.allowSlidePrev = !0, t.allowSlideNext = !0;
                    var c = -l[n] - t.getTranslate();
                    n < s ? (e = r.length - 3 * s + n, e += s, t.slideTo(e, 0, !1, !0) && 0 !== c && t.setTranslate((d ? -t.translate : t.translate) - c)) : ("auto" === i.slidesPerView && 2 * s <= n || n >= r.length - s) && (e = -r.length + n + s, e += s, t.slideTo(e, 0, !1, !0) && 0 !== c && t.setTranslate((d ? -t.translate : t.translate) - c)), t.allowSlidePrev = a, t.allowSlideNext = o
                },
                loopDestroy: function () {
                    var e = this.$wrapperEl,
                        t = this.params,
                        i = this.slides;
                    e.children("." + t.slideClass + "." + t.slideDuplicateClass).remove(), i.removeAttr("data-swiper-slide-index")
                }
            },
            v = {
                setGrabCursor: function (e) {
                    if (!(c.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked)) {
                        var t = this.el;
                        t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                    }
                },
                unsetGrabCursor: function () {
                    c.touch || this.params.watchOverflow && this.isLocked || (this.el.style.cursor = "")
                }
            },
            y = {
                appendSlide: function (e) {
                    var t = this,
                        i = t.$wrapperEl,
                        n = t.params;
                    if (n.loop && t.loopDestroy(), "object" == typeof e && "length" in e)
                        for (var r = 0; r < e.length; r += 1) e[r] && i.append(e[r]);
                    else i.append(e);
                    n.loop && t.loopCreate(), n.observer && c.observer || t.update()
                },
                prependSlide: function (e) {
                    var t = this,
                        i = t.params,
                        n = t.$wrapperEl,
                        r = t.activeIndex;
                    i.loop && t.loopDestroy();
                    var s = r + 1;
                    if ("object" == typeof e && "length" in e) {
                        for (var a = 0; a < e.length; a += 1) e[a] && n.prepend(e[a]);
                        s = r + e.length
                    } else n.prepend(e);
                    i.loop && t.loopCreate(), i.observer && c.observer || t.update(), t.slideTo(s, 0, !1)
                },
                addSlide: function (e, t) {
                    var i = this,
                        n = i.$wrapperEl,
                        r = i.params,
                        s = i.activeIndex;
                    r.loop && (s -= i.loopedSlides, i.loopDestroy(), i.slides = n.children("." + r.slideClass));
                    var a = i.slides.length;
                    if (e <= 0) i.prependSlide(t);
                    else if (a <= e) i.appendSlide(t);
                    else {
                        for (var o = e < s ? s + 1 : s, l = [], d = a - 1; e <= d; d -= 1) {
                            var u = i.slides.eq(d);
                            u.remove(), l.unshift(u)
                        }
                        if ("object" == typeof t && "length" in t) {
                            for (var h = 0; h < t.length; h += 1) t[h] && n.append(t[h]);
                            o = e < s ? s + t.length : s
                        } else n.append(t);
                        for (var p = 0; p < l.length; p += 1) n.append(l[p]);
                        r.loop && i.loopCreate(), r.observer && c.observer || i.update(), r.loop ? i.slideTo(o + i.loopedSlides, 0, !1) : i.slideTo(o, 0, !1)
                    }
                },
                removeSlide: function (e) {
                    var t = this,
                        i = t.params,
                        n = t.$wrapperEl,
                        r = t.activeIndex;
                    i.loop && (r -= t.loopedSlides, t.loopDestroy(), t.slides = n.children("." + i.slideClass));
                    var s, a = r;
                    if ("object" == typeof e && "length" in e) {
                        for (var o = 0; o < e.length; o += 1) s = e[o], t.slides[s] && t.slides.eq(s).remove(), s < a && (a -= 1);
                        a = Math.max(a, 0)
                    } else s = e, t.slides[s] && t.slides.eq(s).remove(), s < a && (a -= 1), a = Math.max(a, 0);
                    i.loop && t.loopCreate(), i.observer && c.observer || t.update(), i.loop ? t.slideTo(a + t.loopedSlides, 0, !1) : t.slideTo(a, 0, !1)
                },
                removeAllSlides: function () {
                    for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                    this.removeSlide(e)
                }
            },
            b = function () {
                var i = t.navigator.userAgent,
                    n = {
                        ios: !1,
                        android: !1,
                        androidChrome: !1,
                        desktop: !1,
                        windows: !1,
                        iphone: !1,
                        ipod: !1,
                        ipad: !1,
                        cordova: t.cordova || t.phonegap,
                        phonegap: t.cordova || t.phonegap
                    },
                    r = i.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                    s = i.match(/(Android);?[\s\/]+([\d.]+)?/),
                    a = i.match(/(iPad).*OS\s([\d_]+)/),
                    o = i.match(/(iPod)(.*OS\s([\d_]+))?/),
                    l = !a && i.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                if (r && (n.os = "windows", n.osVersion = r[2], n.windows = !0), s && !r && (n.os = "android", n.osVersion = s[2], n.android = !0, n.androidChrome = 0 <= i.toLowerCase().indexOf("chrome")), (a || l || o) && (n.os = "ios", n.ios = !0), l && !o && (n.osVersion = l[2].replace(/_/g, "."), n.iphone = !0), a && (n.osVersion = a[2].replace(/_/g, "."), n.ipad = !0), o && (n.osVersion = o[3] ? o[3].replace(/_/g, ".") : null, n.iphone = !0), n.ios && n.osVersion && 0 <= i.indexOf("Version/") && "10" === n.osVersion.split(".")[0] && (n.osVersion = i.toLowerCase().split("version/")[1].split(" ")[0]), n.desktop = !(n.os || n.android || n.webView), n.webView = (l || a || o) && i.match(/.*AppleWebKit(?!.*Safari)/i), n.os && "ios" === n.os) {
                    var d = n.osVersion.split("."),
                        c = e.querySelector('meta[name="viewport"]');
                    n.minimalUi = !n.webView && (o || l) && (1 * d[0] == 7 ? 1 <= 1 * d[1] : 7 < 1 * d[0]) && c && 0 <= c.getAttribute("content").indexOf("minimal-ui")
                }
                return n.pixelRatio = t.devicePixelRatio || 1, n
            }();

        function w() {
            var e = this,
                t = e.params,
                i = e.el;
            if (!i || 0 !== i.offsetWidth) {
                t.breakpoints && e.setBreakpoint();
                var n = e.allowSlideNext,
                    r = e.allowSlidePrev,
                    s = e.snapGrid;
                if (e.allowSlideNext = !0, e.allowSlidePrev = !0, e.updateSize(), e.updateSlides(), t.freeMode) {
                    var a = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate());
                    e.setTranslate(a), e.updateActiveIndex(), e.updateSlidesClasses(), t.autoHeight && e.updateAutoHeight()
                } else e.updateSlidesClasses(), ("auto" === t.slidesPerView || 1 < t.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0);
                e.allowSlidePrev = r, e.allowSlideNext = n, e.params.watchOverflow && s !== e.snapGrid && e.checkOverflow()
            }
        }
        var x, C = {
                attachEvents: function () {
                    var i = this,
                        r = i.params,
                        s = i.touchEvents,
                        a = i.el,
                        o = i.wrapperEl;
                    i.onTouchStart = function (i) {
                        var r = this,
                            s = r.touchEventsData,
                            a = r.params,
                            o = r.touches;
                        if (!r.animating || !a.preventInteractionOnTransition) {
                            var l = i;
                            if (l.originalEvent && (l = l.originalEvent), s.isTouchEvent = "touchstart" === l.type, (s.isTouchEvent || !("which" in l) || 3 !== l.which) && !(!s.isTouchEvent && "button" in l && 0 < l.button || s.isTouched && s.isMoved))
                                if (a.noSwiping && n(l.target).closest(a.noSwipingSelector ? a.noSwipingSelector : "." + a.noSwipingClass)[0]) r.allowClick = !0;
                                else if (!a.swipeHandler || n(l).closest(a.swipeHandler)[0]) {
                                o.currentX = "touchstart" === l.type ? l.targetTouches[0].pageX : l.pageX, o.currentY = "touchstart" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                var c = o.currentX,
                                    u = o.currentY,
                                    h = a.edgeSwipeDetection || a.iOSEdgeSwipeDetection,
                                    p = a.edgeSwipeThreshold || a.iOSEdgeSwipeThreshold;
                                if (!h || !(c <= p || c >= t.screen.width - p)) {
                                    if (d.extend(s, {
                                            isTouched: !0,
                                            isMoved: !1,
                                            allowTouchCallbacks: !0,
                                            isScrolling: void 0,
                                            startMoving: void 0
                                        }), o.startX = c, o.startY = u, s.touchStartTime = d.now(), r.allowClick = !0, r.updateSize(), r.swipeDirection = void 0, 0 < a.threshold && (s.allowThresholdMove = !1), "touchstart" !== l.type) {
                                        var f = !0;
                                        n(l.target).is(s.formElements) && (f = !1), e.activeElement && n(e.activeElement).is(s.formElements) && e.activeElement !== l.target && e.activeElement.blur(), f && r.allowTouchMove && a.touchStartPreventDefault && l.preventDefault()
                                    }
                                    r.emit("touchStart", l)
                                }
                            }
                        }
                    }.bind(i), i.onTouchMove = function (t) {
                        var i = this,
                            r = i.touchEventsData,
                            s = i.params,
                            a = i.touches,
                            o = i.rtlTranslate,
                            l = t;
                        if (l.originalEvent && (l = l.originalEvent), r.isTouched) {
                            if (!r.isTouchEvent || "mousemove" !== l.type) {
                                var c = "touchmove" === l.type ? l.targetTouches[0].pageX : l.pageX,
                                    u = "touchmove" === l.type ? l.targetTouches[0].pageY : l.pageY;
                                if (l.preventedByNestedSwiper) return a.startX = c, void(a.startY = u);
                                if (!i.allowTouchMove) return i.allowClick = !1, void(r.isTouched && (d.extend(a, {
                                    startX: c,
                                    startY: u,
                                    currentX: c,
                                    currentY: u
                                }), r.touchStartTime = d.now()));
                                if (r.isTouchEvent && s.touchReleaseOnEdges && !s.loop)
                                    if (i.isVertical()) {
                                        if (u < a.startY && i.translate <= i.maxTranslate() || u > a.startY && i.translate >= i.minTranslate()) return r.isTouched = !1, void(r.isMoved = !1)
                                    } else if (c < a.startX && i.translate <= i.maxTranslate() || c > a.startX && i.translate >= i.minTranslate()) return;
                                if (r.isTouchEvent && e.activeElement && l.target === e.activeElement && n(l.target).is(r.formElements)) return r.isMoved = !0, void(i.allowClick = !1);
                                if (r.allowTouchCallbacks && i.emit("touchMove", l), !(l.targetTouches && 1 < l.targetTouches.length)) {
                                    a.currentX = c, a.currentY = u;
                                    var h, p = a.currentX - a.startX,
                                        f = a.currentY - a.startY;
                                    if (!(i.params.threshold && Math.sqrt(Math.pow(p, 2) + Math.pow(f, 2)) < i.params.threshold))
                                        if (void 0 === r.isScrolling && (i.isHorizontal() && a.currentY === a.startY || i.isVertical() && a.currentX === a.startX ? r.isScrolling = !1 : 25 <= p * p + f * f && (h = 180 * Math.atan2(Math.abs(f), Math.abs(p)) / Math.PI, r.isScrolling = i.isHorizontal() ? h > s.touchAngle : 90 - h > s.touchAngle)), r.isScrolling && i.emit("touchMoveOpposite", l), void 0 === r.startMoving && (a.currentX === a.startX && a.currentY === a.startY || (r.startMoving = !0)), r.isScrolling) r.isTouched = !1;
                                        else if (r.startMoving) {
                                        i.allowClick = !1, l.preventDefault(), s.touchMoveStopPropagation && !s.nested && l.stopPropagation(), r.isMoved || (s.loop && i.loopFix(), r.startTranslate = i.getTranslate(), i.setTransition(0), i.animating && i.$wrapperEl.trigger("webkitTransitionEnd transitionend"), r.allowMomentumBounce = !1, !s.grabCursor || !0 !== i.allowSlideNext && !0 !== i.allowSlidePrev || i.setGrabCursor(!0), i.emit("sliderFirstMove", l)), i.emit("sliderMove", l), r.isMoved = !0;
                                        var g = i.isHorizontal() ? p : f;
                                        a.diff = g, g *= s.touchRatio, o && (g = -g), i.swipeDirection = 0 < g ? "prev" : "next", r.currentTranslate = g + r.startTranslate;
                                        var m = !0,
                                            v = s.resistanceRatio;
                                        if (s.touchReleaseOnEdges && (v = 0), 0 < g && r.currentTranslate > i.minTranslate() ? (m = !1, s.resistance && (r.currentTranslate = i.minTranslate() - 1 + Math.pow(-i.minTranslate() + r.startTranslate + g, v))) : g < 0 && r.currentTranslate < i.maxTranslate() && (m = !1, s.resistance && (r.currentTranslate = i.maxTranslate() + 1 - Math.pow(i.maxTranslate() - r.startTranslate - g, v))), m && (l.preventedByNestedSwiper = !0), !i.allowSlideNext && "next" === i.swipeDirection && r.currentTranslate < r.startTranslate && (r.currentTranslate = r.startTranslate), !i.allowSlidePrev && "prev" === i.swipeDirection && r.currentTranslate > r.startTranslate && (r.currentTranslate = r.startTranslate), 0 < s.threshold) {
                                            if (!(Math.abs(g) > s.threshold || r.allowThresholdMove)) return void(r.currentTranslate = r.startTranslate);
                                            if (!r.allowThresholdMove) return r.allowThresholdMove = !0, a.startX = a.currentX, a.startY = a.currentY, r.currentTranslate = r.startTranslate, void(a.diff = i.isHorizontal() ? a.currentX - a.startX : a.currentY - a.startY)
                                        }
                                        s.followFinger && ((s.freeMode || s.watchSlidesProgress || s.watchSlidesVisibility) && (i.updateActiveIndex(), i.updateSlidesClasses()), s.freeMode && (0 === r.velocities.length && r.velocities.push({
                                            position: a[i.isHorizontal() ? "startX" : "startY"],
                                            time: r.touchStartTime
                                        }), r.velocities.push({
                                            position: a[i.isHorizontal() ? "currentX" : "currentY"],
                                            time: d.now()
                                        })), i.updateProgress(r.currentTranslate), i.setTranslate(r.currentTranslate))
                                    }
                                }
                            }
                        } else r.startMoving && r.isScrolling && i.emit("touchMoveOpposite", l)
                    }.bind(i), i.onTouchEnd = function (e) {
                        var t = this,
                            i = t.touchEventsData,
                            n = t.params,
                            r = t.touches,
                            s = t.rtlTranslate,
                            a = t.$wrapperEl,
                            o = t.slidesGrid,
                            l = t.snapGrid,
                            c = e;
                        if (c.originalEvent && (c = c.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", c), i.allowTouchCallbacks = !1, !i.isTouched) return i.isMoved && n.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void(i.startMoving = !1);
                        n.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                        var u, h = d.now(),
                            p = h - i.touchStartTime;
                        if (t.allowClick && (t.updateClickedSlide(c), t.emit("tap", c), p < 300 && 300 < h - i.lastClickTime && (i.clickTimeout && clearTimeout(i.clickTimeout), i.clickTimeout = d.nextTick((function () {
                                t && !t.destroyed && t.emit("click", c)
                            }), 300)), p < 300 && h - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), t.emit("doubleTap", c))), i.lastClickTime = d.now(), d.nextTick((function () {
                                t.destroyed || (t.allowClick = !0)
                            })), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === r.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, i.isMoved = !1, void(i.startMoving = !1);
                        if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, u = n.followFinger ? s ? t.translate : -t.translate : -i.currentTranslate, n.freeMode) {
                            if (u < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                            if (u > -t.maxTranslate()) return void(t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));
                            if (n.freeModeMomentum) {
                                if (1 < i.velocities.length) {
                                    var f = i.velocities.pop(),
                                        g = i.velocities.pop(),
                                        m = f.position - g.position,
                                        v = f.time - g.time;
                                    t.velocity = m / v, t.velocity /= 2, Math.abs(t.velocity) < n.freeModeMinimumVelocity && (t.velocity = 0), (150 < v || 300 < d.now() - f.time) && (t.velocity = 0)
                                } else t.velocity = 0;
                                t.velocity *= n.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                                var y = 1e3 * n.freeModeMomentumRatio,
                                    b = t.velocity * y,
                                    w = t.translate + b;
                                s && (w = -w);
                                var x, C, T = !1,
                                    E = 20 * Math.abs(t.velocity) * n.freeModeMomentumBounceRatio;
                                if (w < t.maxTranslate()) n.freeModeMomentumBounce ? (w + t.maxTranslate() < -E && (w = t.maxTranslate() - E), x = t.maxTranslate(), T = !0, i.allowMomentumBounce = !0) : w = t.maxTranslate(), n.loop && n.centeredSlides && (C = !0);
                                else if (w > t.minTranslate()) n.freeModeMomentumBounce ? (w - t.minTranslate() > E && (w = t.minTranslate() + E), x = t.minTranslate(), T = !0, i.allowMomentumBounce = !0) : w = t.minTranslate(), n.loop && n.centeredSlides && (C = !0);
                                else if (n.freeModeSticky) {
                                    for (var S, _ = 0; _ < l.length; _ += 1)
                                        if (l[_] > -w) {
                                            S = _;
                                            break
                                        } w = -(w = Math.abs(l[S] - w) < Math.abs(l[S - 1] - w) || "next" === t.swipeDirection ? l[S] : l[S - 1])
                                }
                                if (C && t.once("transitionEnd", (function () {
                                        t.loopFix()
                                    })), 0 !== t.velocity) y = s ? Math.abs((-w - t.translate) / t.velocity) : Math.abs((w - t.translate) / t.velocity);
                                else if (n.freeModeSticky) return void t.slideToClosest();
                                n.freeModeMomentumBounce && T ? (t.updateProgress(x), t.setTransition(y), t.setTranslate(w), t.transitionStart(!0, t.swipeDirection), t.animating = !0, a.transitionEnd((function () {
                                    t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(n.speed), t.setTranslate(x), a.transitionEnd((function () {
                                        t && !t.destroyed && t.transitionEnd()
                                    })))
                                }))) : t.velocity ? (t.updateProgress(w), t.setTransition(y), t.setTranslate(w), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, a.transitionEnd((function () {
                                    t && !t.destroyed && t.transitionEnd()
                                })))) : t.updateProgress(w), t.updateActiveIndex(), t.updateSlidesClasses()
                            } else if (n.freeModeSticky) return void t.slideToClosest();
                            (!n.freeModeMomentum || p >= n.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                        } else {
                            for (var k = 0, $ = t.slidesSizesGrid[0], I = 0; I < o.length; I += n.slidesPerGroup) void 0 !== o[I + n.slidesPerGroup] ? u >= o[I] && u < o[I + n.slidesPerGroup] && ($ = o[(k = I) + n.slidesPerGroup] - o[I]) : u >= o[I] && (k = I, $ = o[o.length - 1] - o[o.length - 2]);
                            var D = (u - o[k]) / $;
                            if (p > n.longSwipesMs) {
                                if (!n.longSwipes) return void t.slideTo(t.activeIndex);
                                "next" === t.swipeDirection && (D >= n.longSwipesRatio ? t.slideTo(k + n.slidesPerGroup) : t.slideTo(k)), "prev" === t.swipeDirection && (D > 1 - n.longSwipesRatio ? t.slideTo(k + n.slidesPerGroup) : t.slideTo(k))
                            } else {
                                if (!n.shortSwipes) return void t.slideTo(t.activeIndex);
                                "next" === t.swipeDirection && t.slideTo(k + n.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(k)
                            }
                        }
                    }.bind(i), i.onClick = function (e) {
                        this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                    }.bind(i);
                    var l = "container" === r.touchEventsTarget ? a : o,
                        u = !!r.nested;
                    if (c.touch || !c.pointerEvents && !c.prefixedPointerEvents) {
                        if (c.touch) {
                            var h = !("touchstart" !== s.start || !c.passiveListener || !r.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            l.addEventListener(s.start, i.onTouchStart, h), l.addEventListener(s.move, i.onTouchMove, c.passiveListener ? {
                                passive: !1,
                                capture: u
                            } : u), l.addEventListener(s.end, i.onTouchEnd, h)
                        }(r.simulateTouch && !b.ios && !b.android || r.simulateTouch && !c.touch && b.ios) && (l.addEventListener("mousedown", i.onTouchStart, !1), e.addEventListener("mousemove", i.onTouchMove, u), e.addEventListener("mouseup", i.onTouchEnd, !1))
                    } else l.addEventListener(s.start, i.onTouchStart, !1), e.addEventListener(s.move, i.onTouchMove, u), e.addEventListener(s.end, i.onTouchEnd, !1);
                    (r.preventClicks || r.preventClicksPropagation) && l.addEventListener("click", i.onClick, !0), i.on(b.ios || b.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", w, !0)
                },
                detachEvents: function () {
                    var t = this,
                        i = t.params,
                        n = t.touchEvents,
                        r = t.el,
                        s = t.wrapperEl,
                        a = "container" === i.touchEventsTarget ? r : s,
                        o = !!i.nested;
                    if (c.touch || !c.pointerEvents && !c.prefixedPointerEvents) {
                        if (c.touch) {
                            var l = !("onTouchStart" !== n.start || !c.passiveListener || !i.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            a.removeEventListener(n.start, t.onTouchStart, l), a.removeEventListener(n.move, t.onTouchMove, o), a.removeEventListener(n.end, t.onTouchEnd, l)
                        }(i.simulateTouch && !b.ios && !b.android || i.simulateTouch && !c.touch && b.ios) && (a.removeEventListener("mousedown", t.onTouchStart, !1), e.removeEventListener("mousemove", t.onTouchMove, o), e.removeEventListener("mouseup", t.onTouchEnd, !1))
                    } else a.removeEventListener(n.start, t.onTouchStart, !1), e.removeEventListener(n.move, t.onTouchMove, o), e.removeEventListener(n.end, t.onTouchEnd, !1);
                    (i.preventClicks || i.preventClicksPropagation) && a.removeEventListener("click", t.onClick, !0), t.off(b.ios || b.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", w)
                }
            },
            T = {
                setBreakpoint: function () {
                    var e = this,
                        t = e.activeIndex,
                        i = e.initialized,
                        n = e.loopedSlides;
                    void 0 === n && (n = 0);
                    var r = e.params,
                        s = r.breakpoints;
                    if (s && (!s || 0 !== Object.keys(s).length)) {
                        var a = e.getBreakpoint(s);
                        if (a && e.currentBreakpoint !== a) {
                            var o = a in s ? s[a] : e.originalParams,
                                l = r.loop && o.slidesPerView !== r.slidesPerView;
                            d.extend(e.params, o), d.extend(e, {
                                allowTouchMove: e.params.allowTouchMove,
                                allowSlideNext: e.params.allowSlideNext,
                                allowSlidePrev: e.params.allowSlidePrev
                            }), e.currentBreakpoint = a, l && i && (e.loopDestroy(), e.loopCreate(), e.updateSlides(), e.slideTo(t - n + e.loopedSlides, 0, !1)), e.emit("breakpoint", o)
                        }
                    }
                },
                getBreakpoint: function (e) {
                    if (e) {
                        var i = !1,
                            n = [];
                        Object.keys(e).forEach((function (e) {
                            n.push(e)
                        })), n.sort((function (e, t) {
                            return parseInt(e, 10) - parseInt(t, 10)
                        }));
                        for (var r = 0; r < n.length; r += 1) {
                            var s = n[r];
                            this.params.breakpointsInverse ? s <= t.innerWidth && (i = s) : s >= t.innerWidth && !i && (i = s)
                        }
                        return i || "max"
                    }
                }
            },
            E = {
                isIE: !!t.navigator.userAgent.match(/Trident/g) || !!t.navigator.userAgent.match(/MSIE/g),
                isEdge: !!t.navigator.userAgent.match(/Edge/g),
                isSafari: (x = t.navigator.userAgent.toLowerCase(), 0 <= x.indexOf("safari") && x.indexOf("chrome") < 0 && x.indexOf("android") < 0),
                isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)
            },
            S = {
                init: !0,
                direction: "horizontal",
                touchEventsTarget: "container",
                initialSlide: 0,
                speed: 300,
                preventInteractionOnTransition: !1,
                edgeSwipeDetection: !1,
                edgeSwipeThreshold: 20,
                freeMode: !1,
                freeModeMomentum: !0,
                freeModeMomentumRatio: 1,
                freeModeMomentumBounce: !0,
                freeModeMomentumBounceRatio: 1,
                freeModeMomentumVelocityRatio: 1,
                freeModeSticky: !1,
                freeModeMinimumVelocity: .02,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                breakpoints: void 0,
                breakpointsInverse: !1,
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerColumnFill: "column",
                slidesPerGroup: 1,
                centeredSlides: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                normalizeSlideIndex: !0,
                centerInsufficientSlides: !1,
                watchOverflow: !1,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                allowTouchMove: !0,
                threshold: 0,
                touchMoveStopPropagation: !0,
                touchStartPreventDefault: !0,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                resistance: !0,
                resistanceRatio: .85,
                watchSlidesProgress: !1,
                watchSlidesVisibility: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                preloadImages: !0,
                updateOnImagesReady: !0,
                loop: !1,
                loopAdditionalSlides: 0,
                loopedSlides: null,
                loopFillGroupWithBlank: !1,
                allowSlidePrev: !0,
                allowSlideNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                noSwipingSelector: null,
                passiveListeners: !0,
                containerModifierClass: "swiper-container-",
                slideClass: "swiper-slide",
                slideBlankClass: "swiper-slide-invisible-blank",
                slideActiveClass: "swiper-slide-active",
                slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                slideVisibleClass: "swiper-slide-visible",
                slideDuplicateClass: "swiper-slide-duplicate",
                slideNextClass: "swiper-slide-next",
                slideDuplicateNextClass: "swiper-slide-duplicate-next",
                slidePrevClass: "swiper-slide-prev",
                slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                wrapperClass: "swiper-wrapper",
                runCallbacksOnInit: !0
            },
            _ = {
                update: p,
                translate: f,
                transition: {
                    setTransition: function (e, t) {
                        this.$wrapperEl.transition(e), this.emit("setTransition", e, t)
                    },
                    transitionStart: function (e, t) {
                        void 0 === e && (e = !0);
                        var i = this,
                            n = i.activeIndex,
                            r = i.params,
                            s = i.previousIndex;
                        r.autoHeight && i.updateAutoHeight();
                        var a = t;
                        if (a || (a = s < n ? "next" : n < s ? "prev" : "reset"), i.emit("transitionStart"), e && n !== s) {
                            if ("reset" === a) return void i.emit("slideResetTransitionStart");
                            i.emit("slideChangeTransitionStart"), "next" === a ? i.emit("slideNextTransitionStart") : i.emit("slidePrevTransitionStart")
                        }
                    },
                    transitionEnd: function (e, t) {
                        void 0 === e && (e = !0);
                        var i = this,
                            n = i.activeIndex,
                            r = i.previousIndex;
                        i.animating = !1, i.setTransition(0);
                        var s = t;
                        if (s || (s = r < n ? "next" : n < r ? "prev" : "reset"), i.emit("transitionEnd"), e && n !== r) {
                            if ("reset" === s) return void i.emit("slideResetTransitionEnd");
                            i.emit("slideChangeTransitionEnd"), "next" === s ? i.emit("slideNextTransitionEnd") : i.emit("slidePrevTransitionEnd")
                        }
                    }
                },
                slide: g,
                loop: m,
                grabCursor: v,
                manipulation: y,
                events: C,
                breakpoints: T,
                checkOverflow: {
                    checkOverflow: function () {
                        var e = this,
                            t = e.isLocked;
                        e.isLocked = 1 === e.snapGrid.length, e.allowSlideNext = !e.isLocked, e.allowSlidePrev = !e.isLocked, t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock"), t && t !== e.isLocked && (e.isEnd = !1, e.navigation.update())
                    }
                },
                classes: {
                    addClasses: function () {
                        var e = this.classNames,
                            t = this.params,
                            i = this.rtl,
                            n = this.$el,
                            r = [];
                        r.push(t.direction), t.freeMode && r.push("free-mode"), c.flexbox || r.push("no-flexbox"), t.autoHeight && r.push("autoheight"), i && r.push("rtl"), 1 < t.slidesPerColumn && r.push("multirow"), b.android && r.push("android"), b.ios && r.push("ios"), (E.isIE || E.isEdge) && (c.pointerEvents || c.prefixedPointerEvents) && r.push("wp8-" + t.direction), r.forEach((function (i) {
                            e.push(t.containerModifierClass + i)
                        })), n.addClass(e.join(" "))
                    },
                    removeClasses: function () {
                        var e = this.$el,
                            t = this.classNames;
                        e.removeClass(t.join(" "))
                    }
                },
                images: {
                    loadImage: function (e, i, n, r, s, a) {
                        var o;

                        function l() {
                            a && a()
                        }
                        e.complete && s ? l() : i ? ((o = new t.Image).onload = l, o.onerror = l, r && (o.sizes = r), n && (o.srcset = n), i && (o.src = i)) : l()
                    },
                    preloadImages: function () {
                        var e = this;

                        function t() {
                            null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")))
                        }
                        e.imagesToLoad = e.$el.find("img");
                        for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                            var n = e.imagesToLoad[i];
                            e.loadImage(n, n.currentSrc || n.getAttribute("src"), n.srcset || n.getAttribute("srcset"), n.sizes || n.getAttribute("sizes"), !0, t)
                        }
                    }
                }
            },
            k = {},
            $ = function (e) {
                function t() {
                    for (var i, r, s, a = [], o = arguments.length; o--;) a[o] = arguments[o];
                    1 === a.length && a[0].constructor && a[0].constructor === Object ? s = a[0] : (r = (i = a)[0], s = i[1]), s || (s = {}), s = d.extend({}, s), r && !s.el && (s.el = r), e.call(this, s), Object.keys(_).forEach((function (e) {
                        Object.keys(_[e]).forEach((function (i) {
                            t.prototype[i] || (t.prototype[i] = _[e][i])
                        }))
                    }));
                    var l = this;
                    void 0 === l.modules && (l.modules = {}), Object.keys(l.modules).forEach((function (e) {
                        var t = l.modules[e];
                        if (t.params) {
                            var i = Object.keys(t.params)[0],
                                n = t.params[i];
                            if ("object" != typeof n || null === n) return;
                            if (!(i in s) || !("enabled" in n)) return;
                            !0 === s[i] && (s[i] = {
                                enabled: !0
                            }), "object" != typeof s[i] || "enabled" in s[i] || (s[i].enabled = !0), s[i] || (s[i] = {
                                enabled: !1
                            })
                        }
                    }));
                    var u = d.extend({}, S);
                    l.useModulesParams(u), l.params = d.extend({}, u, k, s), l.originalParams = d.extend({}, l.params), l.passedParams = d.extend({}, s);
                    var h = (l.$ = n)(l.params.el);
                    if (r = h[0]) {
                        if (1 < h.length) {
                            var p = [];
                            return h.each((function (e, i) {
                                var n = d.extend({}, s, {
                                    el: i
                                });
                                p.push(new t(n))
                            })), p
                        }
                        r.swiper = l, h.data("swiper", l);
                        var f, g, m = h.children("." + l.params.wrapperClass);
                        return d.extend(l, {
                            $el: h,
                            el: r,
                            $wrapperEl: m,
                            wrapperEl: m[0],
                            classNames: [],
                            slides: n(),
                            slidesGrid: [],
                            snapGrid: [],
                            slidesSizesGrid: [],
                            isHorizontal: function () {
                                return "horizontal" === l.params.direction
                            },
                            isVertical: function () {
                                return "vertical" === l.params.direction
                            },
                            rtl: "rtl" === r.dir.toLowerCase() || "rtl" === h.css("direction"),
                            rtlTranslate: "horizontal" === l.params.direction && ("rtl" === r.dir.toLowerCase() || "rtl" === h.css("direction")),
                            wrongRTL: "-webkit-box" === m.css("display"),
                            activeIndex: 0,
                            realIndex: 0,
                            isBeginning: !0,
                            isEnd: !1,
                            translate: 0,
                            previousTranslate: 0,
                            progress: 0,
                            velocity: 0,
                            animating: !1,
                            allowSlideNext: l.params.allowSlideNext,
                            allowSlidePrev: l.params.allowSlidePrev,
                            touchEvents: (f = ["touchstart", "touchmove", "touchend"], g = ["mousedown", "mousemove", "mouseup"], c.pointerEvents ? g = ["pointerdown", "pointermove", "pointerup"] : c.prefixedPointerEvents && (g = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), l.touchEventsTouch = {
                                start: f[0],
                                move: f[1],
                                end: f[2]
                            }, l.touchEventsDesktop = {
                                start: g[0],
                                move: g[1],
                                end: g[2]
                            }, c.touch || !l.params.simulateTouch ? l.touchEventsTouch : l.touchEventsDesktop),
                            touchEventsData: {
                                isTouched: void 0,
                                isMoved: void 0,
                                allowTouchCallbacks: void 0,
                                touchStartTime: void 0,
                                isScrolling: void 0,
                                currentTranslate: void 0,
                                startTranslate: void 0,
                                allowThresholdMove: void 0,
                                formElements: "input, select, option, textarea, button, video",
                                lastClickTime: d.now(),
                                clickTimeout: void 0,
                                velocities: [],
                                allowMomentumBounce: void 0,
                                isTouchEvent: void 0,
                                startMoving: void 0
                            },
                            allowClick: !0,
                            allowTouchMove: l.params.allowTouchMove,
                            touches: {
                                startX: 0,
                                startY: 0,
                                currentX: 0,
                                currentY: 0,
                                diff: 0
                            },
                            imagesToLoad: [],
                            imagesLoaded: 0
                        }), l.useModules(), l.params.init && l.init(), l
                    }
                }
                e && (t.__proto__ = e);
                var i = {
                    extendedDefaults: {
                        configurable: !0
                    },
                    defaults: {
                        configurable: !0
                    },
                    Class: {
                        configurable: !0
                    },
                    $: {
                        configurable: !0
                    }
                };
                return ((t.prototype = Object.create(e && e.prototype)).constructor = t).prototype.slidesPerViewDynamic = function () {
                    var e = this,
                        t = e.params,
                        i = e.slides,
                        n = e.slidesGrid,
                        r = e.size,
                        s = e.activeIndex,
                        a = 1;
                    if (t.centeredSlides) {
                        for (var o, l = i[s].swiperSlideSize, d = s + 1; d < i.length; d += 1) i[d] && !o && (a += 1, r < (l += i[d].swiperSlideSize) && (o = !0));
                        for (var c = s - 1; 0 <= c; c -= 1) i[c] && !o && (a += 1, r < (l += i[c].swiperSlideSize) && (o = !0))
                    } else
                        for (var u = s + 1; u < i.length; u += 1) n[u] - n[s] < r && (a += 1);
                    return a
                }, t.prototype.update = function () {
                    var e = this;
                    if (e && !e.destroyed) {
                        var t = e.snapGrid,
                            i = e.params;
                        i.breakpoints && e.setBreakpoint(), e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses(), e.params.freeMode ? (n(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || 1 < e.params.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || n(), i.watchOverflow && t !== e.snapGrid && e.checkOverflow(), e.emit("update")
                    }

                    function n() {
                        var t = e.rtlTranslate ? -1 * e.translate : e.translate,
                            i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
                        e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses()
                    }
                }, t.prototype.init = function () {
                    var e = this;
                    e.initialized || (e.emit("beforeInit"), e.params.breakpoints && e.setBreakpoint(), e.addClasses(), e.params.loop && e.loopCreate(), e.updateSize(), e.updateSlides(), e.params.watchOverflow && e.checkOverflow(), e.params.grabCursor && e.setGrabCursor(), e.params.preloadImages && e.preloadImages(), e.params.loop ? e.slideTo(e.params.initialSlide + e.loopedSlides, 0, e.params.runCallbacksOnInit) : e.slideTo(e.params.initialSlide, 0, e.params.runCallbacksOnInit), e.attachEvents(), e.initialized = !0, e.emit("init"))
                }, t.prototype.destroy = function (e, t) {
                    void 0 === e && (e = !0), void 0 === t && (t = !0);
                    var i = this,
                        n = i.params,
                        r = i.$el,
                        s = i.$wrapperEl,
                        a = i.slides;
                    return void 0 === i.params || i.destroyed || (i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), n.loop && i.loopDestroy(), t && (i.removeClasses(), r.removeAttr("style"), s.removeAttr("style"), a && a.length && a.removeClass([n.slideVisibleClass, n.slideActiveClass, n.slideNextClass, n.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach((function (e) {
                        i.off(e)
                    })), !1 !== e && (i.$el[0].swiper = null, i.$el.data("swiper", null), d.deleteProps(i)), i.destroyed = !0), null
                }, t.extendDefaults = function (e) {
                    d.extend(k, e)
                }, i.extendedDefaults.get = function () {
                    return k
                }, i.defaults.get = function () {
                    return S
                }, i.Class.get = function () {
                    return e
                }, i.$.get = function () {
                    return n
                }, Object.defineProperties(t, i), t
            }(u),
            I = {
                name: "device",
                proto: {
                    device: b
                },
                static: {
                    device: b
                }
            },
            D = {
                name: "support",
                proto: {
                    support: c
                },
                static: {
                    support: c
                }
            },
            A = {
                name: "browser",
                proto: {
                    browser: E
                },
                static: {
                    browser: E
                }
            },
            M = {
                name: "resize",
                create: function () {
                    var e = this;
                    d.extend(e, {
                        resize: {
                            resizeHandler: function () {
                                e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"))
                            },
                            orientationChangeHandler: function () {
                                e && !e.destroyed && e.initialized && e.emit("orientationchange")
                            }
                        }
                    })
                },
                on: {
                    init: function () {
                        t.addEventListener("resize", this.resize.resizeHandler), t.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                    },
                    destroy: function () {
                        t.removeEventListener("resize", this.resize.resizeHandler), t.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                    }
                }
            },
            P = {
                func: t.MutationObserver || t.WebkitMutationObserver,
                attach: function (e, i) {
                    void 0 === i && (i = {});
                    var n = this,
                        r = new P.func((function (e) {
                            if (1 !== e.length) {
                                var i = function () {
                                    n.emit("observerUpdate", e[0])
                                };
                                t.requestAnimationFrame ? t.requestAnimationFrame(i) : t.setTimeout(i, 0)
                            } else n.emit("observerUpdate", e[0])
                        }));
                    r.observe(e, {
                        attributes: void 0 === i.attributes || i.attributes,
                        childList: void 0 === i.childList || i.childList,
                        characterData: void 0 === i.characterData || i.characterData
                    }), n.observer.observers.push(r)
                },
                init: function () {
                    var e = this;
                    if (c.observer && e.params.observer) {
                        if (e.params.observeParents)
                            for (var t = e.$el.parents(), i = 0; i < t.length; i += 1) e.observer.attach(t[i]);
                        e.observer.attach(e.$el[0], {
                            childList: !1
                        }), e.observer.attach(e.$wrapperEl[0], {
                            attributes: !1
                        })
                    }
                },
                destroy: function () {
                    this.observer.observers.forEach((function (e) {
                        e.disconnect()
                    })), this.observer.observers = []
                }
            },
            O = {
                name: "observer",
                params: {
                    observer: !1,
                    observeParents: !1
                },
                create: function () {
                    d.extend(this, {
                        observer: {
                            init: P.init.bind(this),
                            attach: P.attach.bind(this),
                            destroy: P.destroy.bind(this),
                            observers: []
                        }
                    })
                },
                on: {
                    init: function () {
                        this.observer.init()
                    },
                    destroy: function () {
                        this.observer.destroy()
                    }
                }
            },
            N = {
                update: function (e) {
                    var t = this,
                        i = t.params,
                        n = i.slidesPerView,
                        r = i.slidesPerGroup,
                        s = i.centeredSlides,
                        a = t.params.virtual,
                        o = a.addSlidesBefore,
                        l = a.addSlidesAfter,
                        c = t.virtual,
                        u = c.from,
                        h = c.to,
                        p = c.slides,
                        f = c.slidesGrid,
                        g = c.renderSlide,
                        m = c.offset;
                    t.updateActiveIndex();
                    var v, y, b, w = t.activeIndex || 0;
                    v = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top", s ? (y = Math.floor(n / 2) + r + o, b = Math.floor(n / 2) + r + l) : (y = n + (r - 1) + o, b = r + l);
                    var x = Math.max((w || 0) - b, 0),
                        C = Math.min((w || 0) + y, p.length - 1),
                        T = (t.slidesGrid[x] || 0) - (t.slidesGrid[0] || 0);

                    function E() {
                        t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load()
                    }
                    if (d.extend(t.virtual, {
                            from: x,
                            to: C,
                            offset: T,
                            slidesGrid: t.slidesGrid
                        }), u === x && h === C && !e) return t.slidesGrid !== f && T !== m && t.slides.css(v, T + "px"), void t.updateProgress();
                    if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, {
                        offset: T,
                        from: x,
                        to: C,
                        slides: function () {
                            for (var e = [], t = x; t <= C; t += 1) e.push(p[t]);
                            return e
                        }()
                    }), void E();
                    var S = [],
                        _ = [];
                    if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();
                    else
                        for (var k = u; k <= h; k += 1)(k < x || C < k) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + k + '"]').remove();
                    for (var $ = 0; $ < p.length; $ += 1) x <= $ && $ <= C && (void 0 === h || e ? _.push($) : (h < $ && _.push($), $ < u && S.push($)));
                    _.forEach((function (e) {
                        t.$wrapperEl.append(g(p[e], e))
                    })), S.sort((function (e, t) {
                        return e < t
                    })).forEach((function (e) {
                        t.$wrapperEl.prepend(g(p[e], e))
                    })), t.$wrapperEl.children(".swiper-slide").css(v, T + "px"), E()
                },
                renderSlide: function (e, t) {
                    var i = this,
                        r = i.params.virtual;
                    if (r.cache && i.virtual.cache[t]) return i.virtual.cache[t];
                    var s = r.renderSlide ? n(r.renderSlide.call(i, e, t)) : n('<div class="' + i.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");
                    return s.attr("data-swiper-slide-index") || s.attr("data-swiper-slide-index", t), r.cache && (i.virtual.cache[t] = s), s
                },
                appendSlide: function (e) {
                    this.virtual.slides.push(e), this.virtual.update(!0)
                },
                prependSlide: function (e) {
                    var t = this;
                    if (t.virtual.slides.unshift(e), t.params.virtual.cache) {
                        var i = t.virtual.cache,
                            n = {};
                        Object.keys(i).forEach((function (e) {
                            n[e + 1] = i[e]
                        })), t.virtual.cache = n
                    }
                    t.virtual.update(!0), t.slideNext(0)
                }
            },
            L = {
                name: "virtual",
                params: {
                    virtual: {
                        enabled: !1,
                        slides: [],
                        cache: !0,
                        renderSlide: null,
                        renderExternal: null,
                        addSlidesBefore: 0,
                        addSlidesAfter: 0
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        virtual: {
                            update: N.update.bind(e),
                            appendSlide: N.appendSlide.bind(e),
                            prependSlide: N.prependSlide.bind(e),
                            renderSlide: N.renderSlide.bind(e),
                            slides: e.params.virtual.slides,
                            cache: {}
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if (e.params.virtual.enabled) {
                            e.classNames.push(e.params.containerModifierClass + "virtual");
                            var t = {
                                watchSlidesProgress: !0
                            };
                            d.extend(e.params, t), d.extend(e.originalParams, t), e.virtual.update()
                        }
                    },
                    setTranslate: function () {
                        this.params.virtual.enabled && this.virtual.update()
                    }
                }
            },
            z = {
                handle: function (i) {
                    var n = this,
                        r = n.rtlTranslate,
                        s = i;
                    s.originalEvent && (s = s.originalEvent);
                    var a = s.keyCode || s.charCode;
                    if (!n.allowSlideNext && (n.isHorizontal() && 39 === a || n.isVertical() && 40 === a)) return !1;
                    if (!n.allowSlidePrev && (n.isHorizontal() && 37 === a || n.isVertical() && 38 === a)) return !1;
                    if (!(s.shiftKey || s.altKey || s.ctrlKey || s.metaKey || e.activeElement && e.activeElement.nodeName && ("input" === e.activeElement.nodeName.toLowerCase() || "textarea" === e.activeElement.nodeName.toLowerCase()))) {
                        if (n.params.keyboard.onlyInViewport && (37 === a || 39 === a || 38 === a || 40 === a)) {
                            var o = !1;
                            if (0 < n.$el.parents("." + n.params.slideClass).length && 0 === n.$el.parents("." + n.params.slideActiveClass).length) return;
                            var l = t.innerWidth,
                                d = t.innerHeight,
                                c = n.$el.offset();
                            r && (c.left -= n.$el[0].scrollLeft);
                            for (var u = [
                                    [c.left, c.top],
                                    [c.left + n.width, c.top],
                                    [c.left, c.top + n.height],
                                    [c.left + n.width, c.top + n.height]
                                ], h = 0; h < u.length; h += 1) {
                                var p = u[h];
                                0 <= p[0] && p[0] <= l && 0 <= p[1] && p[1] <= d && (o = !0)
                            }
                            if (!o) return
                        }
                        n.isHorizontal() ? (37 !== a && 39 !== a || (s.preventDefault ? s.preventDefault() : s.returnValue = !1), (39 === a && !r || 37 === a && r) && n.slideNext(), (37 === a && !r || 39 === a && r) && n.slidePrev()) : (38 !== a && 40 !== a || (s.preventDefault ? s.preventDefault() : s.returnValue = !1), 40 === a && n.slideNext(), 38 === a && n.slidePrev()), n.emit("keyPress", a)
                    }
                },
                enable: function () {
                    this.keyboard.enabled || (n(e).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0)
                },
                disable: function () {
                    this.keyboard.enabled && (n(e).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1)
                }
            },
            j = {
                name: "keyboard",
                params: {
                    keyboard: {
                        enabled: !1,
                        onlyInViewport: !0
                    }
                },
                create: function () {
                    d.extend(this, {
                        keyboard: {
                            enabled: !1,
                            enable: z.enable.bind(this),
                            disable: z.disable.bind(this),
                            handle: z.handle.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.keyboard.enabled && this.keyboard.enable()
                    },
                    destroy: function () {
                        this.keyboard.enabled && this.keyboard.disable()
                    }
                }
            },
            H = {
                lastScrollTime: d.now(),
                event: -1 < t.navigator.userAgent.indexOf("firefox") ? "DOMMouseScroll" : function () {
                    var t = "onwheel",
                        i = t in e;
                    if (!i) {
                        var n = e.createElement("div");
                        n.setAttribute(t, "return;"), i = "function" == typeof n[t]
                    }
                    return !i && e.implementation && e.implementation.hasFeature && !0 !== e.implementation.hasFeature("", "") && (i = e.implementation.hasFeature("Events.wheel", "3.0")), i
                }() ? "wheel" : "mousewheel",
                normalize: function (e) {
                    var t = 0,
                        i = 0,
                        n = 0,
                        r = 0;
                    return "detail" in e && (i = e.detail), "wheelDelta" in e && (i = -e.wheelDelta / 120), "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i, i = 0), n = 10 * t, r = 10 * i, "deltaY" in e && (r = e.deltaY), "deltaX" in e && (n = e.deltaX), (n || r) && e.deltaMode && (1 === e.deltaMode ? (n *= 40, r *= 40) : (n *= 800, r *= 800)), n && !t && (t = n < 1 ? -1 : 1), r && !i && (i = r < 1 ? -1 : 1), {
                        spinX: t,
                        spinY: i,
                        pixelX: n,
                        pixelY: r
                    }
                },
                handleMouseEnter: function () {
                    this.mouseEntered = !0
                },
                handleMouseLeave: function () {
                    this.mouseEntered = !1
                },
                handle: function (e) {
                    var i = e,
                        n = this,
                        r = n.params.mousewheel;
                    if (!n.mouseEntered && !r.releaseOnEdges) return !0;
                    i.originalEvent && (i = i.originalEvent);
                    var s = 0,
                        a = n.rtlTranslate ? -1 : 1,
                        o = H.normalize(i);
                    if (r.forceToAxis)
                        if (n.isHorizontal()) {
                            if (!(Math.abs(o.pixelX) > Math.abs(o.pixelY))) return !0;
                            s = o.pixelX * a
                        } else {
                            if (!(Math.abs(o.pixelY) > Math.abs(o.pixelX))) return !0;
                            s = o.pixelY
                        }
                    else s = Math.abs(o.pixelX) > Math.abs(o.pixelY) ? -o.pixelX * a : -o.pixelY;
                    if (0 === s) return !0;
                    if (r.invert && (s = -s), n.params.freeMode) {
                        n.params.loop && n.loopFix();
                        var l = n.getTranslate() + s * r.sensitivity,
                            c = n.isBeginning,
                            u = n.isEnd;
                        if (l >= n.minTranslate() && (l = n.minTranslate()), l <= n.maxTranslate() && (l = n.maxTranslate()), n.setTransition(0), n.setTranslate(l), n.updateProgress(), n.updateActiveIndex(), n.updateSlidesClasses(), (!c && n.isBeginning || !u && n.isEnd) && n.updateSlidesClasses(), n.params.freeModeSticky && (clearTimeout(n.mousewheel.timeout), n.mousewheel.timeout = d.nextTick((function () {
                                n.slideToClosest()
                            }), 300)), n.emit("scroll", i), n.params.autoplay && n.params.autoplayDisableOnInteraction && n.autoplay.stop(), l === n.minTranslate() || l === n.maxTranslate()) return !0
                    } else {
                        if (60 < d.now() - n.mousewheel.lastScrollTime)
                            if (s < 0)
                                if (n.isEnd && !n.params.loop || n.animating) {
                                    if (r.releaseOnEdges) return !0
                                } else n.slideNext(), n.emit("scroll", i);
                        else if (n.isBeginning && !n.params.loop || n.animating) {
                            if (r.releaseOnEdges) return !0
                        } else n.slidePrev(), n.emit("scroll", i);
                        n.mousewheel.lastScrollTime = (new t.Date).getTime()
                    }
                    return i.preventDefault ? i.preventDefault() : i.returnValue = !1, !1
                },
                enable: function () {
                    var e = this;
                    if (!H.event) return !1;
                    if (e.mousewheel.enabled) return !1;
                    var t = e.$el;
                    return "container" !== e.params.mousewheel.eventsTarged && (t = n(e.params.mousewheel.eventsTarged)), t.on("mouseenter", e.mousewheel.handleMouseEnter), t.on("mouseleave", e.mousewheel.handleMouseLeave), t.on(H.event, e.mousewheel.handle), e.mousewheel.enabled = !0
                },
                disable: function () {
                    var e = this;
                    if (!H.event) return !1;
                    if (!e.mousewheel.enabled) return !1;
                    var t = e.$el;
                    return "container" !== e.params.mousewheel.eventsTarged && (t = n(e.params.mousewheel.eventsTarged)), t.off(H.event, e.mousewheel.handle), !(e.mousewheel.enabled = !1)
                }
            },
            q = {
                update: function () {
                    var e = this,
                        t = e.params.navigation;
                    if (!e.params.loop) {
                        var i = e.navigation,
                            n = i.$nextEl,
                            r = i.$prevEl;
                        r && 0 < r.length && (e.isBeginning ? r.addClass(t.disabledClass) : r.removeClass(t.disabledClass), r[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass)), n && 0 < n.length && (e.isEnd ? n.addClass(t.disabledClass) : n.removeClass(t.disabledClass), n[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass))
                    }
                },
                init: function () {
                    var e, t, i = this,
                        r = i.params.navigation;
                    (r.nextEl || r.prevEl) && (r.nextEl && (e = n(r.nextEl), i.params.uniqueNavElements && "string" == typeof r.nextEl && 1 < e.length && 1 === i.$el.find(r.nextEl).length && (e = i.$el.find(r.nextEl))), r.prevEl && (t = n(r.prevEl), i.params.uniqueNavElements && "string" == typeof r.prevEl && 1 < t.length && 1 === i.$el.find(r.prevEl).length && (t = i.$el.find(r.prevEl))), e && 0 < e.length && e.on("click", (function (e) {
                        e.preventDefault(), i.isEnd && !i.params.loop || i.slideNext()
                    })), t && 0 < t.length && t.on("click", (function (e) {
                        e.preventDefault(), i.isBeginning && !i.params.loop || i.slidePrev()
                    })), d.extend(i.navigation, {
                        $nextEl: e,
                        nextEl: e && e[0],
                        $prevEl: t,
                        prevEl: t && t[0]
                    }))
                },
                destroy: function () {
                    var e = this.navigation,
                        t = e.$nextEl,
                        i = e.$prevEl;
                    t && t.length && (t.off("click"), t.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click"), i.removeClass(this.params.navigation.disabledClass))
                }
            },
            R = {
                update: function () {
                    var e = this,
                        t = e.rtl,
                        i = e.params.pagination;
                    if (i.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var r, s = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                            a = e.pagination.$el,
                            o = e.params.loop ? Math.ceil((s - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length;
                        if (e.params.loop ? ((r = Math.ceil((e.activeIndex - e.loopedSlides) / e.params.slidesPerGroup)) > s - 1 - 2 * e.loopedSlides && (r -= s - 2 * e.loopedSlides), o - 1 < r && (r -= o), r < 0 && "bullets" !== e.params.paginationType && (r = o + r)) : r = void 0 !== e.snapIndex ? e.snapIndex : e.activeIndex || 0, "bullets" === i.type && e.pagination.bullets && 0 < e.pagination.bullets.length) {
                            var l, d, c, u = e.pagination.bullets;
                            if (i.dynamicBullets && (e.pagination.bulletSize = u.eq(0)[e.isHorizontal() ? "outerWidth" : "outerHeight"](!0), a.css(e.isHorizontal() ? "width" : "height", e.pagination.bulletSize * (i.dynamicMainBullets + 4) + "px"), 1 < i.dynamicMainBullets && void 0 !== e.previousIndex && (e.pagination.dynamicBulletIndex += r - e.previousIndex, e.pagination.dynamicBulletIndex > i.dynamicMainBullets - 1 ? e.pagination.dynamicBulletIndex = i.dynamicMainBullets - 1 : e.pagination.dynamicBulletIndex < 0 && (e.pagination.dynamicBulletIndex = 0)), l = r - e.pagination.dynamicBulletIndex, c = ((d = l + (Math.min(u.length, i.dynamicMainBullets) - 1)) + l) / 2), u.removeClass(i.bulletActiveClass + " " + i.bulletActiveClass + "-next " + i.bulletActiveClass + "-next-next " + i.bulletActiveClass + "-prev " + i.bulletActiveClass + "-prev-prev " + i.bulletActiveClass + "-main"), 1 < a.length) u.each((function (e, t) {
                                var s = n(t),
                                    a = s.index();
                                a === r && s.addClass(i.bulletActiveClass), i.dynamicBullets && (l <= a && a <= d && s.addClass(i.bulletActiveClass + "-main"), a === l && s.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), a === d && s.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next"))
                            }));
                            else if (u.eq(r).addClass(i.bulletActiveClass), i.dynamicBullets) {
                                for (var h = u.eq(l), p = u.eq(d), f = l; f <= d; f += 1) u.eq(f).addClass(i.bulletActiveClass + "-main");
                                h.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), p.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next")
                            }
                            if (i.dynamicBullets) {
                                var g = Math.min(u.length, i.dynamicMainBullets + 4),
                                    m = (e.pagination.bulletSize * g - e.pagination.bulletSize) / 2 - c * e.pagination.bulletSize,
                                    v = t ? "right" : "left";
                                u.css(e.isHorizontal() ? v : "top", m + "px")
                            }
                        }
                        if ("fraction" === i.type && (a.find("." + i.currentClass).text(i.formatFractionCurrent(r + 1)), a.find("." + i.totalClass).text(i.formatFractionTotal(o))), "progressbar" === i.type) {
                            var y;
                            y = i.progressbarOpposite ? e.isHorizontal() ? "vertical" : "horizontal" : e.isHorizontal() ? "horizontal" : "vertical";
                            var b = (r + 1) / o,
                                w = 1,
                                x = 1;
                            "horizontal" === y ? w = b : x = b, a.find("." + i.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + w + ") scaleY(" + x + ")").transition(e.params.speed)
                        }
                        "custom" === i.type && i.renderCustom ? (a.html(i.renderCustom(e, r + 1, o)), e.emit("paginationRender", e, a[0])) : e.emit("paginationUpdate", e, a[0]), a[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](i.lockClass)
                    }
                },
                render: function () {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var i = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                            n = e.pagination.$el,
                            r = "";
                        if ("bullets" === t.type) {
                            for (var s = e.params.loop ? Math.ceil((i - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length, a = 0; a < s; a += 1) t.renderBullet ? r += t.renderBullet.call(e, a, t.bulletClass) : r += "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
                            n.html(r), e.pagination.bullets = n.find("." + t.bulletClass)
                        }
                        "fraction" === t.type && (r = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>', n.html(r)), "progressbar" === t.type && (r = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>', n.html(r)), "custom" !== t.type && e.emit("paginationRender", e.pagination.$el[0])
                    }
                },
                init: function () {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el) {
                        var i = n(t.el);
                        0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && 1 < i.length && 1 === e.$el.find(t.el).length && (i = e.$el.find(t.el)), "bullets" === t.type && t.clickable && i.addClass(t.clickableClass), i.addClass(t.modifierClass + t.type), "bullets" === t.type && t.dynamicBullets && (i.addClass("" + t.modifierClass + t.type + "-dynamic"), e.pagination.dynamicBulletIndex = 0, t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)), "progressbar" === t.type && t.progressbarOpposite && i.addClass(t.progressbarOppositeClass), t.clickable && i.on("click", "." + t.bulletClass, (function (t) {
                            t.preventDefault();
                            var i = n(this).index() * e.params.slidesPerGroup;
                            e.params.loop && (i += e.loopedSlides), e.slideTo(i)
                        })), d.extend(e.pagination, {
                            $el: i,
                            el: i[0]
                        }))
                    }
                },
                destroy: function () {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var i = e.pagination.$el;
                        i.removeClass(t.hiddenClass), i.removeClass(t.modifierClass + t.type), e.pagination.bullets && e.pagination.bullets.removeClass(t.bulletActiveClass), t.clickable && i.off("click", "." + t.bulletClass)
                    }
                }
            },
            B = {
                setTranslate: function () {
                    var e = this;
                    if (e.params.scrollbar.el && e.scrollbar.el) {
                        var t = e.scrollbar,
                            i = e.rtlTranslate,
                            n = e.progress,
                            r = t.dragSize,
                            s = t.trackSize,
                            a = t.$dragEl,
                            o = t.$el,
                            l = e.params.scrollbar,
                            d = r,
                            u = (s - r) * n;
                        i ? 0 < (u = -u) ? (d = r - u, u = 0) : s < -u + r && (d = s + u) : u < 0 ? (d = r + u, u = 0) : s < u + r && (d = s - u), e.isHorizontal() ? (c.transforms3d ? a.transform("translate3d(" + u + "px, 0, 0)") : a.transform("translateX(" + u + "px)"), a[0].style.width = d + "px") : (c.transforms3d ? a.transform("translate3d(0px, " + u + "px, 0)") : a.transform("translateY(" + u + "px)"), a[0].style.height = d + "px"), l.hide && (clearTimeout(e.scrollbar.timeout), o[0].style.opacity = 1, e.scrollbar.timeout = setTimeout((function () {
                            o[0].style.opacity = 0, o.transition(400)
                        }), 1e3))
                    }
                },
                setTransition: function (e) {
                    this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e)
                },
                updateSize: function () {
                    var e = this;
                    if (e.params.scrollbar.el && e.scrollbar.el) {
                        var t = e.scrollbar,
                            i = t.$dragEl,
                            n = t.$el;
                        i[0].style.width = "", i[0].style.height = "";
                        var r, s = e.isHorizontal() ? n[0].offsetWidth : n[0].offsetHeight,
                            a = e.size / e.virtualSize,
                            o = a * (s / e.size);
                        r = "auto" === e.params.scrollbar.dragSize ? s * a : parseInt(e.params.scrollbar.dragSize, 10), e.isHorizontal() ? i[0].style.width = r + "px" : i[0].style.height = r + "px", n[0].style.display = 1 <= a ? "none" : "", e.params.scrollbarHide && (n[0].style.opacity = 0), d.extend(t, {
                            trackSize: s,
                            divider: a,
                            moveDivider: o,
                            dragSize: r
                        }), t.$el[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](e.params.scrollbar.lockClass)
                    }
                },
                setDragPosition: function (e) {
                    var t, i = this,
                        n = i.scrollbar,
                        r = i.rtlTranslate,
                        s = n.$el,
                        a = n.dragSize,
                        o = n.trackSize;
                    t = ((i.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - s.offset()[i.isHorizontal() ? "left" : "top"] - a / 2) / (o - a), t = Math.max(Math.min(t, 1), 0), r && (t = 1 - t);
                    var l = i.minTranslate() + (i.maxTranslate() - i.minTranslate()) * t;
                    i.updateProgress(l), i.setTranslate(l), i.updateActiveIndex(), i.updateSlidesClasses()
                },
                onDragStart: function (e) {
                    var t = this,
                        i = t.params.scrollbar,
                        n = t.scrollbar,
                        r = t.$wrapperEl,
                        s = n.$el,
                        a = n.$dragEl;
                    t.scrollbar.isTouched = !0, e.preventDefault(), e.stopPropagation(), r.transition(100), a.transition(100), n.setDragPosition(e), clearTimeout(t.scrollbar.dragTimeout), s.transition(0), i.hide && s.css("opacity", 1), t.emit("scrollbarDragStart", e)
                },
                onDragMove: function (e) {
                    var t = this.scrollbar,
                        i = this.$wrapperEl,
                        n = t.$el,
                        r = t.$dragEl;
                    this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), i.transition(0), n.transition(0), r.transition(0), this.emit("scrollbarDragMove", e))
                },
                onDragEnd: function (e) {
                    var t = this,
                        i = t.params.scrollbar,
                        n = t.scrollbar.$el;
                    t.scrollbar.isTouched && (t.scrollbar.isTouched = !1, i.hide && (clearTimeout(t.scrollbar.dragTimeout), t.scrollbar.dragTimeout = d.nextTick((function () {
                        n.css("opacity", 0), n.transition(400)
                    }), 1e3)), t.emit("scrollbarDragEnd", e), i.snapOnRelease && t.slideToClosest())
                },
                enableDraggable: function () {
                    var t = this;
                    if (t.params.scrollbar.el) {
                        var i = t.scrollbar,
                            n = t.touchEvents,
                            r = t.touchEventsDesktop,
                            s = t.params,
                            a = i.$el[0],
                            o = !(!c.passiveListener || !s.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            l = !(!c.passiveListener || !s.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        c.touch || !c.pointerEvents && !c.prefixedPointerEvents ? (c.touch && (a.addEventListener(n.start, t.scrollbar.onDragStart, o), a.addEventListener(n.move, t.scrollbar.onDragMove, o), a.addEventListener(n.end, t.scrollbar.onDragEnd, l)), (s.simulateTouch && !b.ios && !b.android || s.simulateTouch && !c.touch && b.ios) && (a.addEventListener("mousedown", t.scrollbar.onDragStart, o), e.addEventListener("mousemove", t.scrollbar.onDragMove, o), e.addEventListener("mouseup", t.scrollbar.onDragEnd, l))) : (a.addEventListener(r.start, t.scrollbar.onDragStart, o), e.addEventListener(r.move, t.scrollbar.onDragMove, o), e.addEventListener(r.end, t.scrollbar.onDragEnd, l))
                    }
                },
                disableDraggable: function () {
                    var t = this;
                    if (t.params.scrollbar.el) {
                        var i = t.scrollbar,
                            n = t.touchEvents,
                            r = t.touchEventsDesktop,
                            s = t.params,
                            a = i.$el[0],
                            o = !(!c.passiveListener || !s.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            l = !(!c.passiveListener || !s.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        c.touch || !c.pointerEvents && !c.prefixedPointerEvents ? (c.touch && (a.removeEventListener(n.start, t.scrollbar.onDragStart, o), a.removeEventListener(n.move, t.scrollbar.onDragMove, o), a.removeEventListener(n.end, t.scrollbar.onDragEnd, l)), (s.simulateTouch && !b.ios && !b.android || s.simulateTouch && !c.touch && b.ios) && (a.removeEventListener("mousedown", t.scrollbar.onDragStart, o), e.removeEventListener("mousemove", t.scrollbar.onDragMove, o), e.removeEventListener("mouseup", t.scrollbar.onDragEnd, l))) : (a.removeEventListener(r.start, t.scrollbar.onDragStart, o), e.removeEventListener(r.move, t.scrollbar.onDragMove, o), e.removeEventListener(r.end, t.scrollbar.onDragEnd, l))
                    }
                },
                init: function () {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var t = e.scrollbar,
                            i = e.$el,
                            r = e.params.scrollbar,
                            s = n(r.el);
                        e.params.uniqueNavElements && "string" == typeof r.el && 1 < s.length && 1 === i.find(r.el).length && (s = i.find(r.el));
                        var a = s.find("." + e.params.scrollbar.dragClass);
                        0 === a.length && (a = n('<div class="' + e.params.scrollbar.dragClass + '"></div>'), s.append(a)), d.extend(t, {
                            $el: s,
                            el: s[0],
                            $dragEl: a,
                            dragEl: a[0]
                        }), r.draggable && t.enableDraggable()
                    }
                },
                destroy: function () {
                    this.scrollbar.disableDraggable()
                }
            },
            W = {
                setTransform: function (e, t) {
                    var i = this.rtl,
                        r = n(e),
                        s = i ? -1 : 1,
                        a = r.attr("data-swiper-parallax") || "0",
                        o = r.attr("data-swiper-parallax-x"),
                        l = r.attr("data-swiper-parallax-y"),
                        d = r.attr("data-swiper-parallax-scale"),
                        c = r.attr("data-swiper-parallax-opacity");
                    if (o || l ? (o = o || "0", l = l || "0") : this.isHorizontal() ? (o = a, l = "0") : (l = a, o = "0"), o = 0 <= o.indexOf("%") ? parseInt(o, 10) * t * s + "%" : o * t * s + "px", l = 0 <= l.indexOf("%") ? parseInt(l, 10) * t + "%" : l * t + "px", null != c) {
                        var u = c - (c - 1) * (1 - Math.abs(t));
                        r[0].style.opacity = u
                    }
                    if (null == d) r.transform("translate3d(" + o + ", " + l + ", 0px)");
                    else {
                        var h = d - (d - 1) * (1 - Math.abs(t));
                        r.transform("translate3d(" + o + ", " + l + ", 0px) scale(" + h + ")")
                    }
                },
                setTranslate: function () {
                    var e = this,
                        t = e.$el,
                        i = e.slides,
                        r = e.progress,
                        s = e.snapGrid;
                    t.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each((function (t, i) {
                        e.parallax.setTransform(i, r)
                    })), i.each((function (t, i) {
                        var a = i.progress;
                        1 < e.params.slidesPerGroup && "auto" !== e.params.slidesPerView && (a += Math.ceil(t / 2) - r * (s.length - 1)), a = Math.min(Math.max(a, -1), 1), n(i).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each((function (t, i) {
                            e.parallax.setTransform(i, a)
                        }))
                    }))
                },
                setTransition: function (e) {
                    void 0 === e && (e = this.params.speed), this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each((function (t, i) {
                        var r = n(i),
                            s = parseInt(r.attr("data-swiper-parallax-duration"), 10) || e;
                        0 === e && (s = 0), r.transition(s)
                    }))
                }
            },
            F = {
                getDistanceBetweenTouches: function (e) {
                    if (e.targetTouches.length < 2) return 1;
                    var t = e.targetTouches[0].pageX,
                        i = e.targetTouches[0].pageY,
                        n = e.targetTouches[1].pageX,
                        r = e.targetTouches[1].pageY;
                    return Math.sqrt(Math.pow(n - t, 2) + Math.pow(r - i, 2))
                },
                onGestureStart: function (e) {
                    var t = this,
                        i = t.params.zoom,
                        r = t.zoom,
                        s = r.gesture;
                    if (r.fakeGestureTouched = !1, r.fakeGestureMoved = !1, !c.gestures) {
                        if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
                        r.fakeGestureTouched = !0, s.scaleStart = F.getDistanceBetweenTouches(e)
                    }
                    s.$slideEl && s.$slideEl.length || (s.$slideEl = n(e.target).closest(".swiper-slide"), 0 === s.$slideEl.length && (s.$slideEl = t.slides.eq(t.activeIndex)), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + i.containerClass), s.maxRatio = s.$imageWrapEl.attr("data-swiper-zoom") || i.maxRatio, 0 !== s.$imageWrapEl.length) ? (s.$imageEl.transition(0), t.zoom.isScaling = !0) : s.$imageEl = void 0
                },
                onGestureChange: function (e) {
                    var t = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!c.gestures) {
                        if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
                        i.fakeGestureMoved = !0, n.scaleMove = F.getDistanceBetweenTouches(e)
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (c.gestures ? this.zoom.scale = e.scale * i.currentScale : i.scale = n.scaleMove / n.scaleStart * i.currentScale, i.scale > n.maxRatio && (i.scale = n.maxRatio - 1 + Math.pow(i.scale - n.maxRatio + 1, .5)), i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)), n.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
                },
                onGestureEnd: function (e) {
                    var t = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!c.gestures) {
                        if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
                        if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !b.android) return;
                        i.fakeGestureTouched = !1, i.fakeGestureMoved = !1
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, n.maxRatio), t.minRatio), n.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (n.$slideEl = void 0))
                },
                onTouchStart: function (e) {
                    var t = this.zoom,
                        i = t.gesture,
                        n = t.image;
                    i.$imageEl && 0 !== i.$imageEl.length && (n.isTouched || (b.android && e.preventDefault(), n.isTouched = !0, n.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, n.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
                },
                onTouchMove: function (e) {
                    var t = this,
                        i = t.zoom,
                        n = i.gesture,
                        r = i.image,
                        s = i.velocity;
                    if (n.$imageEl && 0 !== n.$imageEl.length && (t.allowClick = !1, r.isTouched && n.$slideEl)) {
                        r.isMoved || (r.width = n.$imageEl[0].offsetWidth, r.height = n.$imageEl[0].offsetHeight, r.startX = d.getTranslate(n.$imageWrapEl[0], "x") || 0, r.startY = d.getTranslate(n.$imageWrapEl[0], "y") || 0, n.slideWidth = n.$slideEl[0].offsetWidth, n.slideHeight = n.$slideEl[0].offsetHeight, n.$imageWrapEl.transition(0), t.rtl && (r.startX = -r.startX, r.startY = -r.startY));
                        var a = r.width * i.scale,
                            o = r.height * i.scale;
                        if (!(a < n.slideWidth && o < n.slideHeight)) {
                            if (r.minX = Math.min(n.slideWidth / 2 - a / 2, 0), r.maxX = -r.minX, r.minY = Math.min(n.slideHeight / 2 - o / 2, 0), r.maxY = -r.minY, r.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, r.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !r.isMoved && !i.isScaling) {
                                if (t.isHorizontal() && (Math.floor(r.minX) === Math.floor(r.startX) && r.touchesCurrent.x < r.touchesStart.x || Math.floor(r.maxX) === Math.floor(r.startX) && r.touchesCurrent.x > r.touchesStart.x)) return void(r.isTouched = !1);
                                if (!t.isHorizontal() && (Math.floor(r.minY) === Math.floor(r.startY) && r.touchesCurrent.y < r.touchesStart.y || Math.floor(r.maxY) === Math.floor(r.startY) && r.touchesCurrent.y > r.touchesStart.y)) return void(r.isTouched = !1)
                            }
                            e.preventDefault(), e.stopPropagation(), r.isMoved = !0, r.currentX = r.touchesCurrent.x - r.touchesStart.x + r.startX, r.currentY = r.touchesCurrent.y - r.touchesStart.y + r.startY, r.currentX < r.minX && (r.currentX = r.minX + 1 - Math.pow(r.minX - r.currentX + 1, .8)), r.currentX > r.maxX && (r.currentX = r.maxX - 1 + Math.pow(r.currentX - r.maxX + 1, .8)), r.currentY < r.minY && (r.currentY = r.minY + 1 - Math.pow(r.minY - r.currentY + 1, .8)), r.currentY > r.maxY && (r.currentY = r.maxY - 1 + Math.pow(r.currentY - r.maxY + 1, .8)), s.prevPositionX || (s.prevPositionX = r.touchesCurrent.x), s.prevPositionY || (s.prevPositionY = r.touchesCurrent.y), s.prevTime || (s.prevTime = Date.now()), s.x = (r.touchesCurrent.x - s.prevPositionX) / (Date.now() - s.prevTime) / 2, s.y = (r.touchesCurrent.y - s.prevPositionY) / (Date.now() - s.prevTime) / 2, Math.abs(r.touchesCurrent.x - s.prevPositionX) < 2 && (s.x = 0), Math.abs(r.touchesCurrent.y - s.prevPositionY) < 2 && (s.y = 0), s.prevPositionX = r.touchesCurrent.x, s.prevPositionY = r.touchesCurrent.y, s.prevTime = Date.now(), n.$imageWrapEl.transform("translate3d(" + r.currentX + "px, " + r.currentY + "px,0)")
                        }
                    }
                },
                onTouchEnd: function () {
                    var e = this.zoom,
                        t = e.gesture,
                        i = e.image,
                        n = e.velocity;
                    if (t.$imageEl && 0 !== t.$imageEl.length) {
                        if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void(i.isMoved = !1);
                        i.isTouched = !1, i.isMoved = !1;
                        var r = 300,
                            s = 300,
                            a = n.x * r,
                            o = i.currentX + a,
                            l = n.y * s,
                            d = i.currentY + l;
                        0 !== n.x && (r = Math.abs((o - i.currentX) / n.x)), 0 !== n.y && (s = Math.abs((d - i.currentY) / n.y));
                        var c = Math.max(r, s);
                        i.currentX = o, i.currentY = d;
                        var u = i.width * e.scale,
                            h = i.height * e.scale;
                        i.minX = Math.min(t.slideWidth / 2 - u / 2, 0), i.maxX = -i.minX, i.minY = Math.min(t.slideHeight / 2 - h / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), t.$imageWrapEl.transition(c).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                    }
                },
                onTransitionEnd: function () {
                    var e = this.zoom,
                        t = e.gesture;
                    t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0, e.scale = 1, e.currentScale = 1)
                },
                toggle: function (e) {
                    var t = this.zoom;
                    t.scale && 1 !== t.scale ? t.out() : t.in(e)
                },
                in: function (e) {
                    var t, i, r, s, a, o, l, d, c, u, h, p, f, g, m, v, y = this,
                        b = y.zoom,
                        w = y.params.zoom,
                        x = b.gesture,
                        C = b.image;
                    x.$slideEl || (x.$slideEl = y.clickedSlide ? n(y.clickedSlide) : y.slides.eq(y.activeIndex), x.$imageEl = x.$slideEl.find("img, svg, canvas"), x.$imageWrapEl = x.$imageEl.parent("." + w.containerClass)), x.$imageEl && 0 !== x.$imageEl.length && (x.$slideEl.addClass("" + w.zoomedSlideClass), void 0 === C.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, i = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = C.touchesStart.x, i = C.touchesStart.y), b.scale = x.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, b.currentScale = x.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, e ? (m = x.$slideEl[0].offsetWidth, v = x.$slideEl[0].offsetHeight, r = x.$slideEl.offset().left + m / 2 - t, s = x.$slideEl.offset().top + v / 2 - i, l = x.$imageEl[0].offsetWidth, d = x.$imageEl[0].offsetHeight, c = l * b.scale, u = d * b.scale, f = -(h = Math.min(m / 2 - c / 2, 0)), g = -(p = Math.min(v / 2 - u / 2, 0)), (a = r * b.scale) < h && (a = h), f < a && (a = f), (o = s * b.scale) < p && (o = p), g < o && (o = g)) : o = a = 0, x.$imageWrapEl.transition(300).transform("translate3d(" + a + "px, " + o + "px,0)"), x.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + b.scale + ")"))
                },
                out: function () {
                    var e = this,
                        t = e.zoom,
                        i = e.params.zoom,
                        r = t.gesture;
                    r.$slideEl || (r.$slideEl = e.clickedSlide ? n(e.clickedSlide) : e.slides.eq(e.activeIndex), r.$imageEl = r.$slideEl.find("img, svg, canvas"), r.$imageWrapEl = r.$imageEl.parent("." + i.containerClass)), r.$imageEl && 0 !== r.$imageEl.length && (t.scale = 1, t.currentScale = 1, r.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), r.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), r.$slideEl.removeClass("" + i.zoomedSlideClass), r.$slideEl = void 0)
                },
                enable: function () {
                    var e = this,
                        t = e.zoom;
                    if (!t.enabled) {
                        t.enabled = !0;
                        var i = !("touchstart" !== e.touchEvents.start || !c.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        c.gestures ? (e.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.on(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.on(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.on(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)), e.$wrapperEl.on(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                },
                disable: function () {
                    var e = this,
                        t = e.zoom;
                    if (t.enabled) {
                        e.zoom.enabled = !1;
                        var i = !("touchstart" !== e.touchEvents.start || !c.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        c.gestures ? (e.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, i)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.off(e.touchEvents.start, ".swiper-slide", t.onGestureStart, i), e.$wrapperEl.off(e.touchEvents.move, ".swiper-slide", t.onGestureChange, i), e.$wrapperEl.off(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, i)), e.$wrapperEl.off(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                }
            },
            G = {
                loadInSlide: function (e, t) {
                    void 0 === t && (t = !0);
                    var i = this,
                        r = i.params.lazy;
                    if (void 0 !== e && 0 !== i.slides.length) {
                        var s = i.virtual && i.params.virtual.enabled ? i.$wrapperEl.children("." + i.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : i.slides.eq(e),
                            a = s.find("." + r.elementClass + ":not(." + r.loadedClass + "):not(." + r.loadingClass + ")");
                        !s.hasClass(r.elementClass) || s.hasClass(r.loadedClass) || s.hasClass(r.loadingClass) || (a = a.add(s[0])), 0 !== a.length && a.each((function (e, a) {
                            var o = n(a);
                            o.addClass(r.loadingClass);
                            var l = o.attr("data-background"),
                                d = o.attr("data-src"),
                                c = o.attr("data-srcset"),
                                u = o.attr("data-sizes");
                            i.loadImage(o[0], d || l, c, u, !1, (function () {
                                if (null != i && i && (!i || i.params) && !i.destroyed) {
                                    if (l ? (o.css("background-image", 'url("' + l + '")'), o.removeAttr("data-background")) : (c && (o.attr("srcset", c), o.removeAttr("data-srcset")), u && (o.attr("sizes", u), o.removeAttr("data-sizes")), d && (o.attr("src", d), o.removeAttr("data-src"))), o.addClass(r.loadedClass).removeClass(r.loadingClass), s.find("." + r.preloaderClass).remove(), i.params.loop && t) {
                                        var e = s.attr("data-swiper-slide-index");
                                        if (s.hasClass(i.params.slideDuplicateClass)) {
                                            var n = i.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + i.params.slideDuplicateClass + ")");
                                            i.lazy.loadInSlide(n.index(), !1)
                                        } else {
                                            var a = i.$wrapperEl.children("." + i.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                            i.lazy.loadInSlide(a.index(), !1)
                                        }
                                    }
                                    i.emit("lazyImageReady", s[0], o[0])
                                }
                            })), i.emit("lazyImageLoad", s[0], o[0])
                        }))
                    }
                },
                load: function () {
                    var e = this,
                        t = e.$wrapperEl,
                        i = e.params,
                        r = e.slides,
                        s = e.activeIndex,
                        a = e.virtual && i.virtual.enabled,
                        o = i.lazy,
                        l = i.slidesPerView;

                    function d(e) {
                        if (a) {
                            if (t.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0
                        } else if (r[e]) return !0;
                        return !1
                    }

                    function c(e) {
                        return a ? n(e).attr("data-swiper-slide-index") : n(e).index()
                    }
                    if ("auto" === l && (l = 0), e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0), e.params.watchSlidesVisibility) t.children("." + i.slideVisibleClass).each((function (t, i) {
                        var r = a ? n(i).attr("data-swiper-slide-index") : n(i).index();
                        e.lazy.loadInSlide(r)
                    }));
                    else if (1 < l)
                        for (var u = s; u < s + l; u += 1) d(u) && e.lazy.loadInSlide(u);
                    else e.lazy.loadInSlide(s);
                    if (o.loadPrevNext)
                        if (1 < l || o.loadPrevNextAmount && 1 < o.loadPrevNextAmount) {
                            for (var h = o.loadPrevNextAmount, p = l, f = Math.min(s + p + Math.max(h, p), r.length), g = Math.max(s - Math.max(p, h), 0), m = s + l; m < f; m += 1) d(m) && e.lazy.loadInSlide(m);
                            for (var v = g; v < s; v += 1) d(v) && e.lazy.loadInSlide(v)
                        } else {
                            var y = t.children("." + i.slideNextClass);
                            0 < y.length && e.lazy.loadInSlide(c(y));
                            var b = t.children("." + i.slidePrevClass);
                            0 < b.length && e.lazy.loadInSlide(c(b))
                        }
                }
            },
            X = {
                LinearSpline: function (e, t) {
                    var i, n, r, s, a;
                    return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function (e) {
                        return e ? (a = function (e, t) {
                            for (n = -1, i = e.length; 1 < i - n;) e[r = i + n >> 1] <= t ? n = r : i = r;
                            return i
                        }(this.x, e), s = a - 1, (e - this.x[s]) * (this.y[a] - this.y[s]) / (this.x[a] - this.x[s]) + this.y[s]) : 0
                    }, this
                },
                getInterpolateFunction: function (e) {
                    var t = this;
                    t.controller.spline || (t.controller.spline = t.params.loop ? new X.LinearSpline(t.slidesGrid, e.slidesGrid) : new X.LinearSpline(t.snapGrid, e.snapGrid))
                },
                setTranslate: function (e, t) {
                    var i, n, r = this,
                        s = r.controller.control;

                    function a(e) {
                        var t = r.rtlTranslate ? -r.translate : r.translate;
                        "slide" === r.params.controller.by && (r.controller.getInterpolateFunction(e), n = -r.controller.spline.interpolate(-t)), n && "container" !== r.params.controller.by || (i = (e.maxTranslate() - e.minTranslate()) / (r.maxTranslate() - r.minTranslate()), n = (t - r.minTranslate()) * i + e.minTranslate()), r.params.controller.inverse && (n = e.maxTranslate() - n), e.updateProgress(n), e.setTranslate(n, r), e.updateActiveIndex(), e.updateSlidesClasses()
                    }
                    if (Array.isArray(s))
                        for (var o = 0; o < s.length; o += 1) s[o] !== t && s[o] instanceof $ && a(s[o]);
                    else s instanceof $ && t !== s && a(s)
                },
                setTransition: function (e, t) {
                    var i, n = this,
                        r = n.controller.control;

                    function s(t) {
                        t.setTransition(e, n), 0 !== e && (t.transitionStart(), t.params.autoHeight && d.nextTick((function () {
                            t.updateAutoHeight()
                        })), t.$wrapperEl.transitionEnd((function () {
                            r && (t.params.loop && "slide" === n.params.controller.by && t.loopFix(), t.transitionEnd())
                        })))
                    }
                    if (Array.isArray(r))
                        for (i = 0; i < r.length; i += 1) r[i] !== t && r[i] instanceof $ && s(r[i]);
                    else r instanceof $ && t !== r && s(r)
                }
            },
            V = {
                makeElFocusable: function (e) {
                    return e.attr("tabIndex", "0"), e
                },
                addElRole: function (e, t) {
                    return e.attr("role", t), e
                },
                addElLabel: function (e, t) {
                    return e.attr("aria-label", t), e
                },
                disableEl: function (e) {
                    return e.attr("aria-disabled", !0), e
                },
                enableEl: function (e) {
                    return e.attr("aria-disabled", !1), e
                },
                onEnterKey: function (e) {
                    var t = this,
                        i = t.params.a11y;
                    if (13 === e.keyCode) {
                        var r = n(e.target);
                        t.navigation && t.navigation.$nextEl && r.is(t.navigation.$nextEl) && (t.isEnd && !t.params.loop || t.slideNext(), t.isEnd ? t.a11y.notify(i.lastSlideMessage) : t.a11y.notify(i.nextSlideMessage)), t.navigation && t.navigation.$prevEl && r.is(t.navigation.$prevEl) && (t.isBeginning && !t.params.loop || t.slidePrev(), t.isBeginning ? t.a11y.notify(i.firstSlideMessage) : t.a11y.notify(i.prevSlideMessage)), t.pagination && r.is("." + t.params.pagination.bulletClass) && r[0].click()
                    }
                },
                notify: function (e) {
                    var t = this.a11y.liveRegion;
                    0 !== t.length && (t.html(""), t.html(e))
                },
                updateNavigation: function () {
                    var e = this;
                    if (!e.params.loop) {
                        var t = e.navigation,
                            i = t.$nextEl,
                            n = t.$prevEl;
                        n && 0 < n.length && (e.isBeginning ? e.a11y.disableEl(n) : e.a11y.enableEl(n)), i && 0 < i.length && (e.isEnd ? e.a11y.disableEl(i) : e.a11y.enableEl(i))
                    }
                },
                updatePagination: function () {
                    var e = this,
                        t = e.params.a11y;
                    e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each((function (i, r) {
                        var s = n(r);
                        e.a11y.makeElFocusable(s), e.a11y.addElRole(s, "button"), e.a11y.addElLabel(s, t.paginationBulletMessage.replace(/{{index}}/, s.index() + 1))
                    }))
                },
                init: function () {
                    var e = this;
                    e.$el.append(e.a11y.liveRegion);
                    var t, i, n = e.params.a11y;
                    e.navigation && e.navigation.$nextEl && (t = e.navigation.$nextEl), e.navigation && e.navigation.$prevEl && (i = e.navigation.$prevEl), t && (e.a11y.makeElFocusable(t), e.a11y.addElRole(t, "button"), e.a11y.addElLabel(t, n.nextSlideMessage), t.on("keydown", e.a11y.onEnterKey)), i && (e.a11y.makeElFocusable(i), e.a11y.addElRole(i, "button"), e.a11y.addElLabel(i, n.prevSlideMessage), i.on("keydown", e.a11y.onEnterKey)), e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.$el.on("keydown", "." + e.params.pagination.bulletClass, e.a11y.onEnterKey)
                },
                destroy: function () {
                    var e, t, i = this;
                    i.a11y.liveRegion && 0 < i.a11y.liveRegion.length && i.a11y.liveRegion.remove(), i.navigation && i.navigation.$nextEl && (e = i.navigation.$nextEl), i.navigation && i.navigation.$prevEl && (t = i.navigation.$prevEl), e && e.off("keydown", i.a11y.onEnterKey), t && t.off("keydown", i.a11y.onEnterKey), i.pagination && i.params.pagination.clickable && i.pagination.bullets && i.pagination.bullets.length && i.pagination.$el.off("keydown", "." + i.params.pagination.bulletClass, i.a11y.onEnterKey)
                }
            },
            Y = {
                init: function () {
                    var e = this;
                    if (e.params.history) {
                        if (!t.history || !t.history.pushState) return e.params.history.enabled = !1, void(e.params.hashNavigation.enabled = !0);
                        var i = e.history;
                        i.initialized = !0, i.paths = Y.getPathValues(), (i.paths.key || i.paths.value) && (i.scrollToSlide(0, i.paths.value, e.params.runCallbacksOnInit), e.params.history.replaceState || t.addEventListener("popstate", e.history.setHistoryPopState))
                    }
                },
                destroy: function () {
                    this.params.history.replaceState || t.removeEventListener("popstate", this.history.setHistoryPopState)
                },
                setHistoryPopState: function () {
                    this.history.paths = Y.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
                },
                getPathValues: function () {
                    var e = t.location.pathname.slice(1).split("/").filter((function (e) {
                            return "" !== e
                        })),
                        i = e.length;
                    return {
                        key: e[i - 2],
                        value: e[i - 1]
                    }
                },
                setHistory: function (e, i) {
                    if (this.history.initialized && this.params.history.enabled) {
                        var n = this.slides.eq(i),
                            r = Y.slugify(n.attr("data-history"));
                        t.location.pathname.includes(e) || (r = e + "/" + r);
                        var s = t.history.state;
                        s && s.value === r || (this.params.history.replaceState ? t.history.replaceState({
                            value: r
                        }, null, r) : t.history.pushState({
                            value: r
                        }, null, r))
                    }
                },
                slugify: function (e) {
                    return e.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                },
                scrollToSlide: function (e, t, i) {
                    var n = this;
                    if (t)
                        for (var r = 0, s = n.slides.length; r < s; r += 1) {
                            var a = n.slides.eq(r);
                            if (Y.slugify(a.attr("data-history")) === t && !a.hasClass(n.params.slideDuplicateClass)) {
                                var o = a.index();
                                n.slideTo(o, e, i)
                            }
                        } else n.slideTo(0, e, i)
                }
            },
            U = {
                onHashCange: function () {
                    var t = this,
                        i = e.location.hash.replace("#", "");
                    if (i !== t.slides.eq(t.activeIndex).attr("data-hash")) {
                        var n = t.$wrapperEl.children("." + t.params.slideClass + '[data-hash="' + i + '"]').index();
                        if (void 0 === n) return;
                        t.slideTo(n)
                    }
                },
                setHash: function () {
                    var i = this;
                    if (i.hashNavigation.initialized && i.params.hashNavigation.enabled)
                        if (i.params.hashNavigation.replaceState && t.history && t.history.replaceState) t.history.replaceState(null, null, "#" + i.slides.eq(i.activeIndex).attr("data-hash") || "");
                        else {
                            var n = i.slides.eq(i.activeIndex),
                                r = n.attr("data-hash") || n.attr("data-history");
                            e.location.hash = r || ""
                        }
                },
                init: function () {
                    var i = this;
                    if (!(!i.params.hashNavigation.enabled || i.params.history && i.params.history.enabled)) {
                        i.hashNavigation.initialized = !0;
                        var r = e.location.hash.replace("#", "");
                        if (r)
                            for (var s = 0, a = i.slides.length; s < a; s += 1) {
                                var o = i.slides.eq(s);
                                if ((o.attr("data-hash") || o.attr("data-history")) === r && !o.hasClass(i.params.slideDuplicateClass)) {
                                    var l = o.index();
                                    i.slideTo(l, 0, i.params.runCallbacksOnInit, !0)
                                }
                            }
                        i.params.hashNavigation.watchState && n(t).on("hashchange", i.hashNavigation.onHashCange)
                    }
                },
                destroy: function () {
                    this.params.hashNavigation.watchState && n(t).off("hashchange", this.hashNavigation.onHashCange)
                }
            },
            K = {
                run: function () {
                    var e = this,
                        t = e.slides.eq(e.activeIndex),
                        i = e.params.autoplay.delay;
                    t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), e.autoplay.timeout = d.nextTick((function () {
                        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay"))
                    }), i)
                },
                start: function () {
                    var e = this;
                    return void 0 === e.autoplay.timeout && !e.autoplay.running && (e.autoplay.running = !0, e.emit("autoplayStart"), e.autoplay.run(), !0)
                },
                stop: function () {
                    var e = this;
                    return !!e.autoplay.running && void 0 !== e.autoplay.timeout && (e.autoplay.timeout && (clearTimeout(e.autoplay.timeout), e.autoplay.timeout = void 0), e.autoplay.running = !1, e.emit("autoplayStop"), !0)
                },
                pause: function (e) {
                    var t = this;
                    t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1, t.autoplay.run())))
                }
            },
            Q = {
                setTranslate: function () {
                    for (var e = this, t = e.slides, i = 0; i < t.length; i += 1) {
                        var n = e.slides.eq(i),
                            r = -n[0].swiperSlideOffset;
                        e.params.virtualTranslate || (r -= e.translate);
                        var s = 0;
                        e.isHorizontal() || (s = r, r = 0);
                        var a = e.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(n[0].progress), 0) : 1 + Math.min(Math.max(n[0].progress, -1), 0);
                        n.css({
                            opacity: a
                        }).transform("translate3d(" + r + "px, " + s + "px, 0px)")
                    }
                },
                setTransition: function (e) {
                    var t = this,
                        i = t.slides,
                        n = t.$wrapperEl;
                    if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
                        var r = !1;
                        i.transitionEnd((function () {
                            if (!r && t && !t.destroyed) {
                                r = !0, t.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) n.trigger(e[i])
                            }
                        }))
                    }
                }
            },
            Z = {
                setTranslate: function () {
                    var e, t = this,
                        i = t.$el,
                        r = t.$wrapperEl,
                        s = t.slides,
                        a = t.width,
                        o = t.height,
                        l = t.rtlTranslate,
                        d = t.size,
                        c = t.params.cubeEffect,
                        u = t.isHorizontal(),
                        h = t.virtual && t.params.virtual.enabled,
                        p = 0;
                    c.shadow && (u ? (0 === (e = r.find(".swiper-cube-shadow")).length && (e = n('<div class="swiper-cube-shadow"></div>'), r.append(e)), e.css({
                        height: a + "px"
                    })) : 0 === (e = i.find(".swiper-cube-shadow")).length && (e = n('<div class="swiper-cube-shadow"></div>'), i.append(e)));
                    for (var f = 0; f < s.length; f += 1) {
                        var g = s.eq(f),
                            m = f;
                        h && (m = parseInt(g.attr("data-swiper-slide-index"), 10));
                        var v = 90 * m,
                            y = Math.floor(v / 360);
                        l && (v = -v, y = Math.floor(-v / 360));
                        var b = Math.max(Math.min(g[0].progress, 1), -1),
                            w = 0,
                            x = 0,
                            C = 0;
                        m % 4 == 0 ? (w = 4 * -y * d, C = 0) : (m - 1) % 4 == 0 ? (w = 0, C = 4 * -y * d) : (m - 2) % 4 == 0 ? (w = d + 4 * y * d, C = d) : (m - 3) % 4 == 0 && (w = -d, C = 3 * d + 4 * d * y), l && (w = -w), u || (x = w, w = 0);
                        var T = "rotateX(" + (u ? 0 : -v) + "deg) rotateY(" + (u ? v : 0) + "deg) translate3d(" + w + "px, " + x + "px, " + C + "px)";
                        if (b <= 1 && -1 < b && (p = 90 * m + 90 * b, l && (p = 90 * -m - 90 * b)), g.transform(T), c.slideShadows) {
                            var S = u ? g.find(".swiper-slide-shadow-left") : g.find(".swiper-slide-shadow-top"),
                                _ = u ? g.find(".swiper-slide-shadow-right") : g.find(".swiper-slide-shadow-bottom");
                            0 === S.length && (S = n('<div class="swiper-slide-shadow-' + (u ? "left" : "top") + '"></div>'), g.append(S)), 0 === _.length && (_ = n('<div class="swiper-slide-shadow-' + (u ? "right" : "bottom") + '"></div>'), g.append(_)), S.length && (S[0].style.opacity = Math.max(-b, 0)), _.length && (_[0].style.opacity = Math.max(b, 0))
                        }
                    }
                    if (r.css({
                            "-webkit-transform-origin": "50% 50% -" + d / 2 + "px",
                            "-moz-transform-origin": "50% 50% -" + d / 2 + "px",
                            "-ms-transform-origin": "50% 50% -" + d / 2 + "px",
                            "transform-origin": "50% 50% -" + d / 2 + "px"
                        }), c.shadow)
                        if (u) e.transform("translate3d(0px, " + (a / 2 + c.shadowOffset) + "px, " + -a / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + c.shadowScale + ")");
                        else {
                            var k = Math.abs(p) - 90 * Math.floor(Math.abs(p) / 90),
                                $ = 1.5 - (Math.sin(2 * k * Math.PI / 360) / 2 + Math.cos(2 * k * Math.PI / 360) / 2),
                                I = c.shadowScale,
                                D = c.shadowScale / $,
                                A = c.shadowOffset;
                            e.transform("scale3d(" + I + ", 1, " + D + ") translate3d(0px, " + (o / 2 + A) + "px, " + -o / 2 / D + "px) rotateX(-90deg)")
                        } var M = E.isSafari || E.isUiWebView ? -d / 2 : 0;
                    r.transform("translate3d(0px,0," + M + "px) rotateX(" + (t.isHorizontal() ? 0 : p) + "deg) rotateY(" + (t.isHorizontal() ? -p : 0) + "deg)")
                },
                setTransition: function (e) {
                    var t = this.$el;
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
                }
            },
            J = {
                setTranslate: function () {
                    for (var e = this, t = e.slides, i = e.rtlTranslate, r = 0; r < t.length; r += 1) {
                        var s = t.eq(r),
                            a = s[0].progress;
                        e.params.flipEffect.limitRotation && (a = Math.max(Math.min(s[0].progress, 1), -1));
                        var o = -180 * a,
                            l = 0,
                            d = -s[0].swiperSlideOffset,
                            c = 0;
                        if (e.isHorizontal() ? i && (o = -o) : (c = d, l = -o, o = d = 0), s[0].style.zIndex = -Math.abs(Math.round(a)) + t.length, e.params.flipEffect.slideShadows) {
                            var u = e.isHorizontal() ? s.find(".swiper-slide-shadow-left") : s.find(".swiper-slide-shadow-top"),
                                h = e.isHorizontal() ? s.find(".swiper-slide-shadow-right") : s.find(".swiper-slide-shadow-bottom");
                            0 === u.length && (u = n('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "left" : "top") + '"></div>'), s.append(u)), 0 === h.length && (h = n('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "right" : "bottom") + '"></div>'), s.append(h)), u.length && (u[0].style.opacity = Math.max(-a, 0)), h.length && (h[0].style.opacity = Math.max(a, 0))
                        }
                        s.transform("translate3d(" + d + "px, " + c + "px, 0px) rotateX(" + l + "deg) rotateY(" + o + "deg)")
                    }
                },
                setTransition: function (e) {
                    var t = this,
                        i = t.slides,
                        n = t.activeIndex,
                        r = t.$wrapperEl;
                    if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), t.params.virtualTranslate && 0 !== e) {
                        var s = !1;
                        i.eq(n).transitionEnd((function () {
                            if (!s && t && !t.destroyed) {
                                s = !0, t.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) r.trigger(e[i])
                            }
                        }))
                    }
                }
            },
            ee = {
                setTranslate: function () {
                    for (var e = this, t = e.width, i = e.height, r = e.slides, s = e.$wrapperEl, a = e.slidesSizesGrid, o = e.params.coverflowEffect, l = e.isHorizontal(), d = e.translate, u = l ? t / 2 - d : i / 2 - d, h = l ? o.rotate : -o.rotate, p = o.depth, f = 0, g = r.length; f < g; f += 1) {
                        var m = r.eq(f),
                            v = a[f],
                            y = (u - m[0].swiperSlideOffset - v / 2) / v * o.modifier,
                            b = l ? h * y : 0,
                            w = l ? 0 : h * y,
                            x = -p * Math.abs(y),
                            C = l ? 0 : o.stretch * y,
                            T = l ? o.stretch * y : 0;
                        Math.abs(T) < .001 && (T = 0), Math.abs(C) < .001 && (C = 0), Math.abs(x) < .001 && (x = 0), Math.abs(b) < .001 && (b = 0), Math.abs(w) < .001 && (w = 0);
                        var E = "translate3d(" + T + "px," + C + "px," + x + "px)  rotateX(" + w + "deg) rotateY(" + b + "deg)";
                        if (m.transform(E), m[0].style.zIndex = 1 - Math.abs(Math.round(y)), o.slideShadows) {
                            var S = l ? m.find(".swiper-slide-shadow-left") : m.find(".swiper-slide-shadow-top"),
                                _ = l ? m.find(".swiper-slide-shadow-right") : m.find(".swiper-slide-shadow-bottom");
                            0 === S.length && (S = n('<div class="swiper-slide-shadow-' + (l ? "left" : "top") + '"></div>'), m.append(S)), 0 === _.length && (_ = n('<div class="swiper-slide-shadow-' + (l ? "right" : "bottom") + '"></div>'), m.append(_)), S.length && (S[0].style.opacity = 0 < y ? y : 0), _.length && (_[0].style.opacity = 0 < -y ? -y : 0)
                        }
                    }(c.pointerEvents || c.prefixedPointerEvents) && (s[0].style.perspectiveOrigin = u + "px 50%")
                },
                setTransition: function (e) {
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                }
            },
            te = {
                init: function () {
                    var e = this,
                        t = e.params.thumbs,
                        i = e.constructor;
                    t.swiper instanceof i ? (e.thumbs.swiper = t.swiper, d.extend(e.thumbs.swiper.originalParams, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    }), d.extend(e.thumbs.swiper.params, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })) : d.isObject(t.swiper) && (e.thumbs.swiper = new i(d.extend({}, t.swiper, {
                        watchSlidesVisibility: !0,
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })), e.thumbs.swiperCreated = !0), e.thumbs.swiper.$el.addClass(e.params.thumbs.thumbsContainerClass), e.thumbs.swiper.on("tap", e.thumbs.onThumbClick)
                },
                onThumbClick: function () {
                    var e = this,
                        t = e.thumbs.swiper;
                    if (t) {
                        var i = t.clickedIndex;
                        if (null != i) {
                            var r;
                            if (r = t.params.loop ? parseInt(n(t.clickedSlide).attr("data-swiper-slide-index"), 10) : i, e.params.loop) {
                                var s = e.activeIndex;
                                e.slides.eq(s).hasClass(e.params.slideDuplicateClass) && (e.loopFix(), e._clientLeft = e.$wrapperEl[0].clientLeft, s = e.activeIndex);
                                var a = e.slides.eq(s).prevAll('[data-swiper-slide-index="' + r + '"]').eq(0).index(),
                                    o = e.slides.eq(s).nextAll('[data-swiper-slide-index="' + r + '"]').eq(0).index();
                                r = void 0 === a ? o : void 0 === o ? a : o - s < s - a ? o : a
                            }
                            e.slideTo(r)
                        }
                    }
                },
                update: function (e) {
                    var t = this,
                        i = t.thumbs.swiper;
                    if (i) {
                        var n = "auto" === i.params.slidesPerView ? i.slidesPerViewDynamic() : i.params.slidesPerView;
                        if (t.realIndex !== i.realIndex) {
                            var r, s = i.activeIndex;
                            if (i.params.loop) {
                                i.slides.eq(s).hasClass(i.params.slideDuplicateClass) && (i.loopFix(), i._clientLeft = i.$wrapperEl[0].clientLeft, s = i.activeIndex);
                                var a = i.slides.eq(s).prevAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index(),
                                    o = i.slides.eq(s).nextAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index();
                                r = void 0 === a ? o : void 0 === o ? a : o - s < s - a ? o : a
                            } else r = t.realIndex;
                            i.visibleSlidesIndexes.indexOf(r) < 0 && (i.params.centeredSlides ? r = s < r ? r - Math.floor(n / 2) + 1 : r + Math.floor(n / 2) - 1 : s < r && (r = r - n + 1), i.slideTo(r, e ? 0 : void 0))
                        }
                        var l = 1,
                            d = t.params.thumbs.slideThumbActiveClass;
                        if (1 < t.params.slidesPerView && !t.params.centeredSlides && (l = t.params.slidesPerView), i.slides.removeClass(d), i.params.loop)
                            for (var c = 0; c < l; c += 1) i.$wrapperEl.children('[data-swiper-slide-index="' + (t.realIndex + c) + '"]').addClass(d);
                        else
                            for (var u = 0; u < l; u += 1) i.slides.eq(t.realIndex + u).addClass(d)
                    }
                }
            },
            ie = [I, D, A, M, O, L, j, {
                name: "mousewheel",
                params: {
                    mousewheel: {
                        enabled: !1,
                        releaseOnEdges: !1,
                        invert: !1,
                        forceToAxis: !1,
                        sensitivity: 1,
                        eventsTarged: "container"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        mousewheel: {
                            enabled: !1,
                            enable: H.enable.bind(e),
                            disable: H.disable.bind(e),
                            handle: H.handle.bind(e),
                            handleMouseEnter: H.handleMouseEnter.bind(e),
                            handleMouseLeave: H.handleMouseLeave.bind(e),
                            lastScrollTime: d.now()
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.mousewheel.enabled && this.mousewheel.enable()
                    },
                    destroy: function () {
                        this.mousewheel.enabled && this.mousewheel.disable()
                    }
                }
            }, {
                name: "navigation",
                params: {
                    navigation: {
                        nextEl: null,
                        prevEl: null,
                        hideOnClick: !1,
                        disabledClass: "swiper-button-disabled",
                        hiddenClass: "swiper-button-hidden",
                        lockClass: "swiper-button-lock"
                    }
                },
                create: function () {
                    d.extend(this, {
                        navigation: {
                            init: q.init.bind(this),
                            update: q.update.bind(this),
                            destroy: q.destroy.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.navigation.init(), this.navigation.update()
                    },
                    toEdge: function () {
                        this.navigation.update()
                    },
                    fromEdge: function () {
                        this.navigation.update()
                    },
                    destroy: function () {
                        this.navigation.destroy()
                    },
                    click: function (e) {
                        var t = this.navigation,
                            i = t.$nextEl,
                            r = t.$prevEl;
                        !this.params.navigation.hideOnClick || n(e.target).is(r) || n(e.target).is(i) || (i && i.toggleClass(this.params.navigation.hiddenClass), r && r.toggleClass(this.params.navigation.hiddenClass))
                    }
                }
            }, {
                name: "pagination",
                params: {
                    pagination: {
                        el: null,
                        bulletElement: "span",
                        clickable: !1,
                        hideOnClick: !1,
                        renderBullet: null,
                        renderProgressbar: null,
                        renderFraction: null,
                        renderCustom: null,
                        progressbarOpposite: !1,
                        type: "bullets",
                        dynamicBullets: !1,
                        dynamicMainBullets: 1,
                        formatFractionCurrent: function (e) {
                            return e
                        },
                        formatFractionTotal: function (e) {
                            return e
                        },
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        modifierClass: "swiper-pagination-",
                        currentClass: "swiper-pagination-current",
                        totalClass: "swiper-pagination-total",
                        hiddenClass: "swiper-pagination-hidden",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                        clickableClass: "swiper-pagination-clickable",
                        lockClass: "swiper-pagination-lock"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        pagination: {
                            init: R.init.bind(e),
                            render: R.render.bind(e),
                            update: R.update.bind(e),
                            destroy: R.destroy.bind(e),
                            dynamicBulletIndex: 0
                        }
                    })
                },
                on: {
                    init: function () {
                        this.pagination.init(), this.pagination.render(), this.pagination.update()
                    },
                    activeIndexChange: function () {
                        (this.params.loop || void 0 === this.snapIndex) && this.pagination.update()
                    },
                    snapIndexChange: function () {
                        this.params.loop || this.pagination.update()
                    },
                    slidesLengthChange: function () {
                        this.params.loop && (this.pagination.render(), this.pagination.update())
                    },
                    snapGridLengthChange: function () {
                        this.params.loop || (this.pagination.render(), this.pagination.update())
                    },
                    destroy: function () {
                        this.pagination.destroy()
                    },
                    click: function (e) {
                        var t = this;
                        t.params.pagination.el && t.params.pagination.hideOnClick && 0 < t.pagination.$el.length && !n(e.target).hasClass(t.params.pagination.bulletClass) && t.pagination.$el.toggleClass(t.params.pagination.hiddenClass)
                    }
                }
            }, {
                name: "scrollbar",
                params: {
                    scrollbar: {
                        el: null,
                        dragSize: "auto",
                        hide: !1,
                        draggable: !1,
                        snapOnRelease: !0,
                        lockClass: "swiper-scrollbar-lock",
                        dragClass: "swiper-scrollbar-drag"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        scrollbar: {
                            init: B.init.bind(e),
                            destroy: B.destroy.bind(e),
                            updateSize: B.updateSize.bind(e),
                            setTranslate: B.setTranslate.bind(e),
                            setTransition: B.setTransition.bind(e),
                            enableDraggable: B.enableDraggable.bind(e),
                            disableDraggable: B.disableDraggable.bind(e),
                            setDragPosition: B.setDragPosition.bind(e),
                            onDragStart: B.onDragStart.bind(e),
                            onDragMove: B.onDragMove.bind(e),
                            onDragEnd: B.onDragEnd.bind(e),
                            isTouched: !1,
                            timeout: null,
                            dragTimeout: null
                        }
                    })
                },
                on: {
                    init: function () {
                        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate()
                    },
                    update: function () {
                        this.scrollbar.updateSize()
                    },
                    resize: function () {
                        this.scrollbar.updateSize()
                    },
                    observerUpdate: function () {
                        this.scrollbar.updateSize()
                    },
                    setTranslate: function () {
                        this.scrollbar.setTranslate()
                    },
                    setTransition: function (e) {
                        this.scrollbar.setTransition(e)
                    },
                    destroy: function () {
                        this.scrollbar.destroy()
                    }
                }
            }, {
                name: "parallax",
                params: {
                    parallax: {
                        enabled: !1
                    }
                },
                create: function () {
                    d.extend(this, {
                        parallax: {
                            setTransform: W.setTransform.bind(this),
                            setTranslate: W.setTranslate.bind(this),
                            setTransition: W.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.parallax.enabled && (this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                    },
                    init: function () {
                        this.params.parallax && this.parallax.setTranslate()
                    },
                    setTranslate: function () {
                        this.params.parallax && this.parallax.setTranslate()
                    },
                    setTransition: function (e) {
                        this.params.parallax && this.parallax.setTransition(e)
                    }
                }
            }, {
                name: "zoom",
                params: {
                    zoom: {
                        enabled: !1,
                        maxRatio: 3,
                        minRatio: 1,
                        toggle: !0,
                        containerClass: "swiper-zoom-container",
                        zoomedSlideClass: "swiper-slide-zoomed"
                    }
                },
                create: function () {
                    var e = this,
                        t = {
                            enabled: !1,
                            scale: 1,
                            currentScale: 1,
                            isScaling: !1,
                            gesture: {
                                $slideEl: void 0,
                                slideWidth: void 0,
                                slideHeight: void 0,
                                $imageEl: void 0,
                                $imageWrapEl: void 0,
                                maxRatio: 3
                            },
                            image: {
                                isTouched: void 0,
                                isMoved: void 0,
                                currentX: void 0,
                                currentY: void 0,
                                minX: void 0,
                                minY: void 0,
                                maxX: void 0,
                                maxY: void 0,
                                width: void 0,
                                height: void 0,
                                startX: void 0,
                                startY: void 0,
                                touchesStart: {},
                                touchesCurrent: {}
                            },
                            velocity: {
                                x: void 0,
                                y: void 0,
                                prevPositionX: void 0,
                                prevPositionY: void 0,
                                prevTime: void 0
                            }
                        };
                    "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach((function (i) {
                        t[i] = F[i].bind(e)
                    })), d.extend(e, {
                        zoom: t
                    })
                },
                on: {
                    init: function () {
                        this.params.zoom.enabled && this.zoom.enable()
                    },
                    destroy: function () {
                        this.zoom.disable()
                    },
                    touchStart: function (e) {
                        this.zoom.enabled && this.zoom.onTouchStart(e)
                    },
                    touchEnd: function (e) {
                        this.zoom.enabled && this.zoom.onTouchEnd(e)
                    },
                    doubleTap: function (e) {
                        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e)
                    },
                    transitionEnd: function () {
                        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                    }
                }
            }, {
                name: "lazy",
                params: {
                    lazy: {
                        enabled: !1,
                        loadPrevNext: !1,
                        loadPrevNextAmount: 1,
                        loadOnTransitionStart: !1,
                        elementClass: "swiper-lazy",
                        loadingClass: "swiper-lazy-loading",
                        loadedClass: "swiper-lazy-loaded",
                        preloaderClass: "swiper-lazy-preloader"
                    }
                },
                create: function () {
                    d.extend(this, {
                        lazy: {
                            initialImageLoaded: !1,
                            load: G.load.bind(this),
                            loadInSlide: G.loadInSlide.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                    },
                    init: function () {
                        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                    },
                    scroll: function () {
                        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                    },
                    resize: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    scrollbarDragMove: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    transitionStart: function () {
                        var e = this;
                        e.params.lazy.enabled && (e.params.lazy.loadOnTransitionStart || !e.params.lazy.loadOnTransitionStart && !e.lazy.initialImageLoaded) && e.lazy.load()
                    },
                    transitionEnd: function () {
                        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                    }
                }
            }, {
                name: "controller",
                params: {
                    controller: {
                        control: void 0,
                        inverse: !1,
                        by: "slide"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        controller: {
                            control: e.params.controller.control,
                            getInterpolateFunction: X.getInterpolateFunction.bind(e),
                            setTranslate: X.setTranslate.bind(e),
                            setTransition: X.setTransition.bind(e)
                        }
                    })
                },
                on: {
                    update: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    resize: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    observerUpdate: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    setTranslate: function (e, t) {
                        this.controller.control && this.controller.setTranslate(e, t)
                    },
                    setTransition: function (e, t) {
                        this.controller.control && this.controller.setTransition(e, t)
                    }
                }
            }, {
                name: "a11y",
                params: {
                    a11y: {
                        enabled: !0,
                        notificationClass: "swiper-notification",
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide {{index}}"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        a11y: {
                            liveRegion: n('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                        }
                    }), Object.keys(V).forEach((function (t) {
                        e.a11y[t] = V[t].bind(e)
                    }))
                },
                on: {
                    init: function () {
                        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation())
                    },
                    toEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    fromEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    paginationUpdate: function () {
                        this.params.a11y.enabled && this.a11y.updatePagination()
                    },
                    destroy: function () {
                        this.params.a11y.enabled && this.a11y.destroy()
                    }
                }
            }, {
                name: "history",
                params: {
                    history: {
                        enabled: !1,
                        replaceState: !1,
                        key: "slides"
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        history: {
                            init: Y.init.bind(e),
                            setHistory: Y.setHistory.bind(e),
                            setHistoryPopState: Y.setHistoryPopState.bind(e),
                            scrollToSlide: Y.scrollToSlide.bind(e),
                            destroy: Y.destroy.bind(e)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.history.enabled && this.history.init()
                    },
                    destroy: function () {
                        this.params.history.enabled && this.history.destroy()
                    },
                    transitionEnd: function () {
                        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                    }
                }
            }, {
                name: "hash-navigation",
                params: {
                    hashNavigation: {
                        enabled: !1,
                        replaceState: !1,
                        watchState: !1
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        hashNavigation: {
                            initialized: !1,
                            init: U.init.bind(e),
                            destroy: U.destroy.bind(e),
                            setHash: U.setHash.bind(e),
                            onHashCange: U.onHashCange.bind(e)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.init()
                    },
                    destroy: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                    },
                    transitionEnd: function () {
                        this.hashNavigation.initialized && this.hashNavigation.setHash()
                    }
                }
            }, {
                name: "autoplay",
                params: {
                    autoplay: {
                        enabled: !1,
                        delay: 3e3,
                        waitForTransition: !0,
                        disableOnInteraction: !0,
                        stopOnLastSlide: !1,
                        reverseDirection: !1
                    }
                },
                create: function () {
                    var e = this;
                    d.extend(e, {
                        autoplay: {
                            running: !1,
                            paused: !1,
                            run: K.run.bind(e),
                            start: K.start.bind(e),
                            stop: K.stop.bind(e),
                            pause: K.pause.bind(e),
                            onTransitionEnd: function (t) {
                                e && !e.destroyed && e.$wrapperEl && t.target === this && (e.$wrapperEl[0].removeEventListener("transitionend", e.autoplay.onTransitionEnd), e.$wrapperEl[0].removeEventListener("webkitTransitionEnd", e.autoplay.onTransitionEnd), e.autoplay.paused = !1, e.autoplay.running ? e.autoplay.run() : e.autoplay.stop())
                            }
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.autoplay.enabled && this.autoplay.start()
                    },
                    beforeTransitionStart: function (e, t) {
                        this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop())
                    },
                    sliderFirstMove: function () {
                        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                    },
                    destroy: function () {
                        this.autoplay.running && this.autoplay.stop()
                    }
                }
            }, {
                name: "effect-fade",
                params: {
                    fadeEffect: {
                        crossFade: !1
                    }
                },
                create: function () {
                    d.extend(this, {
                        fadeEffect: {
                            setTranslate: Q.setTranslate.bind(this),
                            setTransition: Q.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("fade" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "fade");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            d.extend(e.params, t), d.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "fade" === this.params.effect && this.fadeEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "fade" === this.params.effect && this.fadeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-cube",
                params: {
                    cubeEffect: {
                        slideShadows: !0,
                        shadow: !0,
                        shadowOffset: 20,
                        shadowScale: .94
                    }
                },
                create: function () {
                    d.extend(this, {
                        cubeEffect: {
                            setTranslate: Z.setTranslate.bind(this),
                            setTransition: Z.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("cube" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "cube"), e.classNames.push(e.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                resistanceRatio: 0,
                                spaceBetween: 0,
                                centeredSlides: !1,
                                virtualTranslate: !0
                            };
                            d.extend(e.params, t), d.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "cube" === this.params.effect && this.cubeEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "cube" === this.params.effect && this.cubeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-flip",
                params: {
                    flipEffect: {
                        slideShadows: !0,
                        limitRotation: !0
                    }
                },
                create: function () {
                    d.extend(this, {
                        flipEffect: {
                            setTranslate: J.setTranslate.bind(this),
                            setTransition: J.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        if ("flip" === e.params.effect) {
                            e.classNames.push(e.params.containerModifierClass + "flip"), e.classNames.push(e.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            d.extend(e.params, t), d.extend(e.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "flip" === this.params.effect && this.flipEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "flip" === this.params.effect && this.flipEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-coverflow",
                params: {
                    coverflowEffect: {
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: !0
                    }
                },
                create: function () {
                    d.extend(this, {
                        coverflowEffect: {
                            setTranslate: ee.setTranslate.bind(this),
                            setTransition: ee.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this;
                        "coverflow" === e.params.effect && (e.classNames.push(e.params.containerModifierClass + "coverflow"), e.classNames.push(e.params.containerModifierClass + "3d"), e.params.watchSlidesProgress = !0, e.originalParams.watchSlidesProgress = !0)
                    },
                    setTranslate: function () {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                    },
                    setTransition: function (e) {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e)
                    }
                }
            }, {
                name: "thumbs",
                params: {
                    thumbs: {
                        swiper: null,
                        slideThumbActiveClass: "swiper-slide-thumb-active",
                        thumbsContainerClass: "swiper-container-thumbs"
                    }
                },
                create: function () {
                    d.extend(this, {
                        thumbs: {
                            swiper: null,
                            init: te.init.bind(this),
                            update: te.update.bind(this),
                            onThumbClick: te.onThumbClick.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        var e = this.params.thumbs;
                        e && e.swiper && (this.thumbs.init(), this.thumbs.update(!0))
                    },
                    slideChange: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    update: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    resize: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    observerUpdate: function () {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    setTransition: function (e) {
                        var t = this.thumbs.swiper;
                        t && t.setTransition(e)
                    },
                    beforeDestroy: function () {
                        var e = this.thumbs.swiper;
                        e && this.thumbs.swiperCreated && e && e.destroy()
                    }
                }
            }];
        return void 0 === $.use && ($.use = $.Class.use, $.installModule = $.Class.installModule), $.use(ie), $
    })),
    function (e) {
        var t, i, n = ((i = document.createElement("input")).setAttribute("onpaste", ""), ("function" == typeof i.onpaste ? "paste" : "input") + ".mask"),
            r = navigator.userAgent,
            s = /iphone/i.test(r),
            a = /android/i.test(r);
        e.mask = {
            definitions: {
                9: "[0-9]",
                a: "[A-Za-z]",
                "*": "[A-Za-z0-9]"
            },
            dataName: "rawMaskFn",
            placeholder: "_"
        }, e.fn.extend({
            caret: function (e, t) {
                var i;
                if (0 !== this.length && !this.is(":hidden")) return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each((function () {
                    this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && ((i = this.createTextRange()).collapse(!0), i.moveEnd("character", t), i.moveStart("character", e), i.select())
                }))) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (i = document.selection.createRange(), e = 0 - i.duplicate().moveStart("character", -1e5), t = e + i.text.length), {
                    begin: e,
                    end: t
                })
            },
            unmask: function () {
                return this.trigger("unmask")
            },
            mask: function (i, r) {
                var o, l, d, c, u;
                return !i && this.length > 0 ? e(this[0]).data(e.mask.dataName)() : (r = e.extend({
                    placeholder: e.mask.placeholder,
                    completed: null
                }, r), o = e.mask.definitions, l = [], d = u = i.length, c = null, e.each(i.split(""), (function (e, t) {
                    "?" == t ? (u--, d = e) : o[t] ? (l.push(new RegExp(o[t])), null === c && (c = l.length - 1)) : l.push(null)
                })), this.trigger("unmask").each((function () {
                    var h = e(this),
                        p = e.map(i.split(""), (function (e, t) {
                            if ("?" != e) return o[e] ? r.placeholder : e
                        })),
                        f = h.val();

                    function g(e) {
                        for (; ++e < u && !l[e];);
                        return e
                    }

                    function m(e, t) {
                        var i, n;
                        if (!(e < 0)) {
                            for (i = e, n = g(t); i < u; i++)
                                if (l[i]) {
                                    if (!(n < u && l[i].test(p[n]))) break;
                                    p[i] = p[n], p[n] = r.placeholder, n = g(n)
                                } y(), h.caret(Math.max(c, e))
                        }
                    }

                    function v(e, t) {
                        var i;
                        for (i = e; i < t && i < u; i++) l[i] && (p[i] = r.placeholder)
                    }

                    function y() {
                        h.val(p.join(""))
                    }

                    function b(e) {
                        var t, i, n = h.val(),
                            s = -1;
                        for (t = 0, pos = 0; t < u; t++)
                            if (l[t]) {
                                for (p[t] = r.placeholder; pos++ < n.length;)
                                    if (i = n.charAt(pos - 1), l[t].test(i)) {
                                        p[t] = i, s = t;
                                        break
                                    } if (pos > n.length) break
                            } else p[t] === n.charAt(pos) && t !== d && (pos++, s = t);
                        return e ? y() : s + 1 < d ? (h.val(""), v(0, u)) : (y(), h.val(h.val().substring(0, s + 1))), d ? t : c
                    }
                    h.data(e.mask.dataName, (function () {
                        return e.map(p, (function (e, t) {
                            return l[t] && e != r.placeholder ? e : null
                        })).join("")
                    })), h.attr("readonly") || h.one("unmask", (function () {
                        h.unbind(".mask").removeData(e.mask.dataName)
                    })).bind("focus.mask", (function () {
                        var e;
                        clearTimeout(t), f = h.val(), e = b(), t = setTimeout((function () {
                            y(), e == i.length ? h.caret(0, e) : h.caret(e)
                        }), 10)
                    })).bind("blur.mask", (function () {
                        b(), h.val() != f && h.change()
                    })).bind("keydown.mask", (function (e) {
                        var t, i, n, r = e.which;
                        8 === r || 46 === r || s && 127 === r ? (i = (t = h.caret()).begin, (n = t.end) - i == 0 && (i = 46 !== r ? function (e) {
                            for (; --e >= 0 && !l[e];);
                            return e
                        }(i) : n = g(i - 1), n = 46 === r ? g(n) : n), v(i, n), m(i, n - 1), e.preventDefault()) : 27 == r && (h.val(f), h.caret(0, b()), e.preventDefault())
                    })).bind("keypress.mask", (function (t) {
                        var i, n, s, o = t.which,
                            d = h.caret();
                        t.ctrlKey || t.altKey || t.metaKey || o < 32 || o && (d.end - d.begin != 0 && (v(d.begin, d.end), m(d.begin, d.end - 1)), (i = g(d.begin - 1)) < u && (n = String.fromCharCode(o), l[i].test(n) && (! function (e) {
                            var t, i, n, s;
                            for (t = e, i = r.placeholder; t < u; t++)
                                if (l[t]) {
                                    if (n = g(t), s = p[t], p[t] = i, !(n < u && l[n].test(s))) break;
                                    i = s
                                }
                        }(i), p[i] = n, y(), s = g(i), a ? setTimeout(e.proxy(e.fn.caret, h, s), 0) : h.caret(s), r.completed && s >= u && r.completed.call(h))), t.preventDefault())
                    })).bind(n, (function () {
                        setTimeout((function () {
                            var e = b(!0);
                            h.caret(e), r.completed && e == h.val().length && r.completed.call(h)
                        }), 0)
                    })), b()
                })))
            }
        })
    }(jQuery),
    function (e, t) {
        "object" == typeof exports && "undefined" != typeof module ? t(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], t) : t((e = e || self).bootstrap = {}, e.jQuery, e.Popper)
    }(this, (function (e, t, i) {
        "use strict";

        function n(e, t) {
            for (var i = 0; i < t.length; i++) {
                var n = t[i];
                n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
            }
        }

        function r(e, t, i) {
            return t && n(e.prototype, t), i && n(e, i), e
        }

        function s(e) {
            for (var t = 1; t < arguments.length; t++) {
                var i = null != arguments[t] ? arguments[t] : {},
                    n = Object.keys(i);
                "function" == typeof Object.getOwnPropertySymbols && (n = n.concat(Object.getOwnPropertySymbols(i).filter((function (e) {
                    return Object.getOwnPropertyDescriptor(i, e).enumerable
                })))), n.forEach((function (t) {
                    var n, r, s;
                    n = e, s = i[r = t], r in n ? Object.defineProperty(n, r, {
                        value: s,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : n[r] = s
                }))
            }
            return e
        }
        t = t && t.hasOwnProperty("default") ? t.default : t, i = i && i.hasOwnProperty("default") ? i.default : i;
        var a = "transitionend";
        var o = {
            TRANSITION_END: "bsTransitionEnd",
            getUID: function (e) {
                for (; e += ~~(1e6 * Math.random()), document.getElementById(e););
                return e
            },
            getSelectorFromElement: function (e) {
                var t = e.getAttribute("data-target");
                if (!t || "#" === t) {
                    var i = e.getAttribute("href");
                    t = i && "#" !== i ? i.trim() : ""
                }
                try {
                    return document.querySelector(t) ? t : null
                } catch (e) {
                    return null
                }
            },
            getTransitionDurationFromElement: function (e) {
                if (!e) return 0;
                var i = t(e).css("transition-duration"),
                    n = t(e).css("transition-delay"),
                    r = parseFloat(i),
                    s = parseFloat(n);
                return r || s ? (i = i.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(i) + parseFloat(n))) : 0
            },
            reflow: function (e) {
                return e.offsetHeight
            },
            triggerTransitionEnd: function (e) {
                t(e).trigger(a)
            },
            supportsTransitionEnd: function () {
                return Boolean(a)
            },
            isElement: function (e) {
                return (e[0] || e).nodeType
            },
            typeCheckConfig: function (e, t, i) {
                for (var n in i)
                    if (Object.prototype.hasOwnProperty.call(i, n)) {
                        var r = i[n],
                            s = t[n],
                            a = s && o.isElement(s) ? "element" : (l = s, {}.toString.call(l).match(/\s([a-z]+)/i)[1].toLowerCase());
                        if (!new RegExp(r).test(a)) throw new Error(e.toUpperCase() + ': Option "' + n + '" provided type "' + a + '" but expected type "' + r + '".')
                    } var l
            },
            findShadowRoot: function (e) {
                if (!document.documentElement.attachShadow) return null;
                if ("function" != typeof e.getRootNode) return e instanceof ShadowRoot ? e : e.parentNode ? o.findShadowRoot(e.parentNode) : null;
                var t = e.getRootNode();
                return t instanceof ShadowRoot ? t : null
            }
        };
        t.fn.emulateTransitionEnd = function (e) {
            var i = this,
                n = !1;
            return t(this).one(o.TRANSITION_END, (function () {
                n = !0
            })), setTimeout((function () {
                n || o.triggerTransitionEnd(i)
            }), e), this
        }, t.event.special[o.TRANSITION_END] = {
            bindType: a,
            delegateType: a,
            handle: function (e) {
                if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
            }
        };
        var l = "alert",
            d = "bs.alert",
            c = "." + d,
            u = t.fn[l],
            h = {
                CLOSE: "close" + c,
                CLOSED: "closed" + c,
                CLICK_DATA_API: "click" + c + ".data-api"
            },
            p = function () {
                function e(e) {
                    this._element = e
                }
                var i = e.prototype;
                return i.close = function (e) {
                    var t = this._element;
                    e && (t = this._getRootElement(e)), this._triggerCloseEvent(t).isDefaultPrevented() || this._removeElement(t)
                }, i.dispose = function () {
                    t.removeData(this._element, d), this._element = null
                }, i._getRootElement = function (e) {
                    var i = o.getSelectorFromElement(e),
                        n = !1;
                    return i && (n = document.querySelector(i)), n || (n = t(e).closest(".alert")[0]), n
                }, i._triggerCloseEvent = function (e) {
                    var i = t.Event(h.CLOSE);
                    return t(e).trigger(i), i
                }, i._removeElement = function (e) {
                    var i = this;
                    if (t(e).removeClass("show"), t(e).hasClass("fade")) {
                        var n = o.getTransitionDurationFromElement(e);
                        t(e).one(o.TRANSITION_END, (function (t) {
                            return i._destroyElement(e, t)
                        })).emulateTransitionEnd(n)
                    } else this._destroyElement(e)
                }, i._destroyElement = function (e) {
                    t(e).detach().trigger(h.CLOSED).remove()
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this),
                            r = n.data(d);
                        r || (r = new e(this), n.data(d, r)), "close" === i && r[i](this)
                    }))
                }, e._handleDismiss = function (e) {
                    return function (t) {
                        t && t.preventDefault(), e.close(this)
                    }
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }]), e
            }();
        t(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', p._handleDismiss(new p)), t.fn[l] = p._jQueryInterface, t.fn[l].Constructor = p, t.fn[l].noConflict = function () {
            return t.fn[l] = u, p._jQueryInterface
        };
        var f = "button",
            g = "bs.button",
            m = "." + g,
            v = ".data-api",
            y = t.fn[f],
            b = "active",
            w = '[data-toggle^="button"]',
            x = ".btn",
            C = {
                CLICK_DATA_API: "click" + m + v,
                FOCUS_BLUR_DATA_API: "focus" + m + v + " blur" + m + v
            },
            T = function () {
                function e(e) {
                    this._element = e
                }
                var i = e.prototype;
                return i.toggle = function () {
                    var e = !0,
                        i = !0,
                        n = t(this._element).closest('[data-toggle="buttons"]')[0];
                    if (n) {
                        var r = this._element.querySelector('input:not([type="hidden"])');
                        if (r) {
                            if ("radio" === r.type)
                                if (r.checked && this._element.classList.contains(b)) e = !1;
                                else {
                                    var s = n.querySelector(".active");
                                    s && t(s).removeClass(b)
                                } if (e) {
                                if (r.hasAttribute("disabled") || n.hasAttribute("disabled") || r.classList.contains("disabled") || n.classList.contains("disabled")) return;
                                r.checked = !this._element.classList.contains(b), t(r).trigger("change")
                            }
                            r.focus(), i = !1
                        }
                    }
                    i && this._element.setAttribute("aria-pressed", !this._element.classList.contains(b)), e && t(this._element).toggleClass(b)
                }, i.dispose = function () {
                    t.removeData(this._element, g), this._element = null
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this).data(g);
                        n || (n = new e(this), t(this).data(g, n)), "toggle" === i && n[i]()
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }]), e
            }();
        t(document).on(C.CLICK_DATA_API, w, (function (e) {
            e.preventDefault();
            var i = e.target;
            t(i).hasClass("btn") || (i = t(i).closest(x)), T._jQueryInterface.call(t(i), "toggle")
        })).on(C.FOCUS_BLUR_DATA_API, w, (function (e) {
            var i = t(e.target).closest(x)[0];
            t(i).toggleClass("focus", /^focus(in)?$/.test(e.type))
        })), t.fn[f] = T._jQueryInterface, t.fn[f].Constructor = T, t.fn[f].noConflict = function () {
            return t.fn[f] = y, T._jQueryInterface
        };
        var E = "carousel",
            S = "bs.carousel",
            _ = "." + S,
            k = ".data-api",
            $ = t.fn[E],
            I = {
                interval: 5e3,
                keyboard: !0,
                slide: !1,
                pause: "hover",
                wrap: !0,
                touch: !0
            },
            D = {
                interval: "(number|boolean)",
                keyboard: "boolean",
                slide: "(boolean|string)",
                pause: "(string|boolean)",
                wrap: "boolean",
                touch: "boolean"
            },
            A = "next",
            M = "prev",
            P = {
                SLIDE: "slide" + _,
                SLID: "slid" + _,
                KEYDOWN: "keydown" + _,
                MOUSEENTER: "mouseenter" + _,
                MOUSELEAVE: "mouseleave" + _,
                TOUCHSTART: "touchstart" + _,
                TOUCHMOVE: "touchmove" + _,
                TOUCHEND: "touchend" + _,
                POINTERDOWN: "pointerdown" + _,
                POINTERUP: "pointerup" + _,
                DRAG_START: "dragstart" + _,
                LOAD_DATA_API: "load" + _ + k,
                CLICK_DATA_API: "click" + _ + k
            },
            O = "active",
            N = ".active.carousel-item",
            L = {
                TOUCH: "touch",
                PEN: "pen"
            },
            z = function () {
                function e(e, t) {
                    this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(t), this._element = e, this._indicatorsElement = this._element.querySelector(".carousel-indicators"), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners()
                }
                var i = e.prototype;
                return i.next = function () {
                    this._isSliding || this._slide(A)
                }, i.nextWhenVisible = function () {
                    !document.hidden && t(this._element).is(":visible") && "hidden" !== t(this._element).css("visibility") && this.next()
                }, i.prev = function () {
                    this._isSliding || this._slide(M)
                }, i.pause = function (e) {
                    e || (this._isPaused = !0), this._element.querySelector(".carousel-item-next, .carousel-item-prev") && (o.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
                }, i.cycle = function (e) {
                    e || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
                }, i.to = function (e) {
                    var i = this;
                    this._activeElement = this._element.querySelector(N);
                    var n = this._getItemIndex(this._activeElement);
                    if (!(e > this._items.length - 1 || e < 0))
                        if (this._isSliding) t(this._element).one(P.SLID, (function () {
                            return i.to(e)
                        }));
                        else {
                            if (n === e) return this.pause(), void this.cycle();
                            var r = n < e ? A : M;
                            this._slide(r, this._items[e])
                        }
                }, i.dispose = function () {
                    t(this._element).off(_), t.removeData(this._element, S), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
                }, i._getConfig = function (e) {
                    return e = s({}, I, e), o.typeCheckConfig(E, e, D), e
                }, i._handleSwipe = function () {
                    var e = Math.abs(this.touchDeltaX);
                    if (!(e <= 40)) {
                        var t = e / this.touchDeltaX;
                        0 < t && this.prev(), t < 0 && this.next()
                    }
                }, i._addEventListeners = function () {
                    var e = this;
                    this._config.keyboard && t(this._element).on(P.KEYDOWN, (function (t) {
                        return e._keydown(t)
                    })), "hover" === this._config.pause && t(this._element).on(P.MOUSEENTER, (function (t) {
                        return e.pause(t)
                    })).on(P.MOUSELEAVE, (function (t) {
                        return e.cycle(t)
                    })), this._config.touch && this._addTouchEventListeners()
                }, i._addTouchEventListeners = function () {
                    var e = this;
                    if (this._touchSupported) {
                        var i = function (t) {
                                e._pointerEvent && L[t.originalEvent.pointerType.toUpperCase()] ? e.touchStartX = t.originalEvent.clientX : e._pointerEvent || (e.touchStartX = t.originalEvent.touches[0].clientX)
                            },
                            n = function (t) {
                                e._pointerEvent && L[t.originalEvent.pointerType.toUpperCase()] && (e.touchDeltaX = t.originalEvent.clientX - e.touchStartX), e._handleSwipe(), "hover" === e._config.pause && (e.pause(), e.touchTimeout && clearTimeout(e.touchTimeout), e.touchTimeout = setTimeout((function (t) {
                                    return e.cycle(t)
                                }), 500 + e._config.interval))
                            };
                        t(this._element.querySelectorAll(".carousel-item img")).on(P.DRAG_START, (function (e) {
                            return e.preventDefault()
                        })), this._pointerEvent ? (t(this._element).on(P.POINTERDOWN, (function (e) {
                            return i(e)
                        })), t(this._element).on(P.POINTERUP, (function (e) {
                            return n(e)
                        })), this._element.classList.add("pointer-event")) : (t(this._element).on(P.TOUCHSTART, (function (e) {
                            return i(e)
                        })), t(this._element).on(P.TOUCHMOVE, (function (t) {
                            var i;
                            (i = t).originalEvent.touches && 1 < i.originalEvent.touches.length ? e.touchDeltaX = 0 : e.touchDeltaX = i.originalEvent.touches[0].clientX - e.touchStartX
                        })), t(this._element).on(P.TOUCHEND, (function (e) {
                            return n(e)
                        })))
                    }
                }, i._keydown = function (e) {
                    if (!/input|textarea/i.test(e.target.tagName)) switch (e.which) {
                        case 37:
                            e.preventDefault(), this.prev();
                            break;
                        case 39:
                            e.preventDefault(), this.next()
                    }
                }, i._getItemIndex = function (e) {
                    return this._items = e && e.parentNode ? [].slice.call(e.parentNode.querySelectorAll(".carousel-item")) : [], this._items.indexOf(e)
                }, i._getItemByDirection = function (e, t) {
                    var i = e === A,
                        n = e === M,
                        r = this._getItemIndex(t),
                        s = this._items.length - 1;
                    if ((n && 0 === r || i && r === s) && !this._config.wrap) return t;
                    var a = (r + (e === M ? -1 : 1)) % this._items.length;
                    return -1 === a ? this._items[this._items.length - 1] : this._items[a]
                }, i._triggerSlideEvent = function (e, i) {
                    var n = this._getItemIndex(e),
                        r = this._getItemIndex(this._element.querySelector(N)),
                        s = t.Event(P.SLIDE, {
                            relatedTarget: e,
                            direction: i,
                            from: r,
                            to: n
                        });
                    return t(this._element).trigger(s), s
                }, i._setActiveIndicatorElement = function (e) {
                    if (this._indicatorsElement) {
                        var i = [].slice.call(this._indicatorsElement.querySelectorAll(".active"));
                        t(i).removeClass(O);
                        var n = this._indicatorsElement.children[this._getItemIndex(e)];
                        n && t(n).addClass(O)
                    }
                }, i._slide = function (e, i) {
                    var n, r, s, a = this,
                        l = this._element.querySelector(N),
                        d = this._getItemIndex(l),
                        c = i || l && this._getItemByDirection(e, l),
                        u = this._getItemIndex(c),
                        h = Boolean(this._interval);
                    if (s = e === A ? (n = "carousel-item-left", r = "carousel-item-next", "left") : (n = "carousel-item-right", r = "carousel-item-prev", "right"), c && t(c).hasClass(O)) this._isSliding = !1;
                    else if (!this._triggerSlideEvent(c, s).isDefaultPrevented() && l && c) {
                        this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(c);
                        var p = t.Event(P.SLID, {
                            relatedTarget: c,
                            direction: s,
                            from: d,
                            to: u
                        });
                        if (t(this._element).hasClass("slide")) {
                            t(c).addClass(r), o.reflow(c), t(l).addClass(n), t(c).addClass(n);
                            var f = parseInt(c.getAttribute("data-interval"), 10);
                            this._config.interval = f ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, f) : this._config.defaultInterval || this._config.interval;
                            var g = o.getTransitionDurationFromElement(l);
                            t(l).one(o.TRANSITION_END, (function () {
                                t(c).removeClass(n + " " + r).addClass(O), t(l).removeClass(O + " " + r + " " + n), a._isSliding = !1, setTimeout((function () {
                                    return t(a._element).trigger(p)
                                }), 0)
                            })).emulateTransitionEnd(g)
                        } else t(l).removeClass(O), t(c).addClass(O), this._isSliding = !1, t(this._element).trigger(p);
                        h && this.cycle()
                    }
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this).data(S),
                            r = s({}, I, t(this).data());
                        "object" == typeof i && (r = s({}, r, i));
                        var a = "string" == typeof i ? i : r.slide;
                        if (n || (n = new e(this, r), t(this).data(S, n)), "number" == typeof i) n.to(i);
                        else if ("string" == typeof a) {
                            if (void 0 === n[a]) throw new TypeError('No method named "' + a + '"');
                            n[a]()
                        } else r.interval && r.ride && (n.pause(), n.cycle())
                    }))
                }, e._dataApiClickHandler = function (i) {
                    var n = o.getSelectorFromElement(this);
                    if (n) {
                        var r = t(n)[0];
                        if (r && t(r).hasClass("carousel")) {
                            var a = s({}, t(r).data(), t(this).data()),
                                l = this.getAttribute("data-slide-to");
                            l && (a.interval = !1), e._jQueryInterface.call(t(r), a), l && t(r).data(S).to(l), i.preventDefault()
                        }
                    }
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return I
                    }
                }]), e
            }();
        t(document).on(P.CLICK_DATA_API, "[data-slide], [data-slide-to]", z._dataApiClickHandler), t(window).on(P.LOAD_DATA_API, (function () {
            for (var e = [].slice.call(document.querySelectorAll('[data-ride="carousel"]')), i = 0, n = e.length; i < n; i++) {
                var r = t(e[i]);
                z._jQueryInterface.call(r, r.data())
            }
        })), t.fn[E] = z._jQueryInterface, t.fn[E].Constructor = z, t.fn[E].noConflict = function () {
            return t.fn[E] = $, z._jQueryInterface
        };
        var j = "collapse",
            H = "bs.collapse",
            q = "." + H,
            R = t.fn[j],
            B = {
                toggle: !0,
                parent: ""
            },
            W = {
                toggle: "boolean",
                parent: "(string|element)"
            },
            F = {
                SHOW: "show" + q,
                SHOWN: "shown" + q,
                HIDE: "hide" + q,
                HIDDEN: "hidden" + q,
                CLICK_DATA_API: "click" + q + ".data-api"
            },
            G = "show",
            X = "collapse",
            V = "collapsing",
            Y = "collapsed",
            U = '[data-toggle="collapse"]',
            K = function () {
                function e(e, t) {
                    this._isTransitioning = !1, this._element = e, this._config = this._getConfig(t), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));
                    for (var i = [].slice.call(document.querySelectorAll(U)), n = 0, r = i.length; n < r; n++) {
                        var s = i[n],
                            a = o.getSelectorFromElement(s),
                            l = [].slice.call(document.querySelectorAll(a)).filter((function (t) {
                                return t === e
                            }));
                        null !== a && 0 < l.length && (this._selector = a, this._triggerArray.push(s))
                    }
                    this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
                }
                var i = e.prototype;
                return i.toggle = function () {
                    t(this._element).hasClass(G) ? this.hide() : this.show()
                }, i.show = function () {
                    var i, n, r = this;
                    if (!(this._isTransitioning || t(this._element).hasClass(G) || (this._parent && 0 === (i = [].slice.call(this._parent.querySelectorAll(".show, .collapsing")).filter((function (e) {
                            return "string" == typeof r._config.parent ? e.getAttribute("data-parent") === r._config.parent : e.classList.contains(X)
                        }))).length && (i = null), i && (n = t(i).not(this._selector).data(H)) && n._isTransitioning))) {
                        var s = t.Event(F.SHOW);
                        if (t(this._element).trigger(s), !s.isDefaultPrevented()) {
                            i && (e._jQueryInterface.call(t(i).not(this._selector), "hide"), n || t(i).data(H, null));
                            var a = this._getDimension();
                            t(this._element).removeClass(X).addClass(V), this._element.style[a] = 0, this._triggerArray.length && t(this._triggerArray).removeClass(Y).attr("aria-expanded", !0), this.setTransitioning(!0);
                            var l = "scroll" + (a[0].toUpperCase() + a.slice(1)),
                                d = o.getTransitionDurationFromElement(this._element);
                            t(this._element).one(o.TRANSITION_END, (function () {
                                t(r._element).removeClass(V).addClass(X).addClass(G), r._element.style[a] = "", r.setTransitioning(!1), t(r._element).trigger(F.SHOWN)
                            })).emulateTransitionEnd(d), this._element.style[a] = this._element[l] + "px"
                        }
                    }
                }, i.hide = function () {
                    var e = this;
                    if (!this._isTransitioning && t(this._element).hasClass(G)) {
                        var i = t.Event(F.HIDE);
                        if (t(this._element).trigger(i), !i.isDefaultPrevented()) {
                            var n = this._getDimension();
                            this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", o.reflow(this._element), t(this._element).addClass(V).removeClass(X).removeClass(G);
                            var r = this._triggerArray.length;
                            if (0 < r)
                                for (var s = 0; s < r; s++) {
                                    var a = this._triggerArray[s],
                                        l = o.getSelectorFromElement(a);
                                    null !== l && (t([].slice.call(document.querySelectorAll(l))).hasClass(G) || t(a).addClass(Y).attr("aria-expanded", !1))
                                }
                            this.setTransitioning(!0), this._element.style[n] = "";
                            var d = o.getTransitionDurationFromElement(this._element);
                            t(this._element).one(o.TRANSITION_END, (function () {
                                e.setTransitioning(!1), t(e._element).removeClass(V).addClass(X).trigger(F.HIDDEN)
                            })).emulateTransitionEnd(d)
                        }
                    }
                }, i.setTransitioning = function (e) {
                    this._isTransitioning = e
                }, i.dispose = function () {
                    t.removeData(this._element, H), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
                }, i._getConfig = function (e) {
                    return (e = s({}, B, e)).toggle = Boolean(e.toggle), o.typeCheckConfig(j, e, W), e
                }, i._getDimension = function () {
                    return t(this._element).hasClass("width") ? "width" : "height"
                }, i._getParent = function () {
                    var i, n = this;
                    o.isElement(this._config.parent) ? (i = this._config.parent, void 0 !== this._config.parent.jquery && (i = this._config.parent[0])) : i = document.querySelector(this._config.parent);
                    var r = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
                        s = [].slice.call(i.querySelectorAll(r));
                    return t(s).each((function (t, i) {
                        n._addAriaAndCollapsedClass(e._getTargetFromElement(i), [i])
                    })), i
                }, i._addAriaAndCollapsedClass = function (e, i) {
                    var n = t(e).hasClass(G);
                    i.length && t(i).toggleClass(Y, !n).attr("aria-expanded", n)
                }, e._getTargetFromElement = function (e) {
                    var t = o.getSelectorFromElement(e);
                    return t ? document.querySelector(t) : null
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this),
                            r = n.data(H),
                            a = s({}, B, n.data(), "object" == typeof i && i ? i : {});
                        if (!r && a.toggle && /show|hide/.test(i) && (a.toggle = !1), r || (r = new e(this, a), n.data(H, r)), "string" == typeof i) {
                            if (void 0 === r[i]) throw new TypeError('No method named "' + i + '"');
                            r[i]()
                        }
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return B
                    }
                }]), e
            }();
        t(document).on(F.CLICK_DATA_API, U, (function (e) {
            "A" === e.currentTarget.tagName && e.preventDefault();
            var i = t(this),
                n = o.getSelectorFromElement(this),
                r = [].slice.call(document.querySelectorAll(n));
            t(r).each((function () {
                var e = t(this),
                    n = e.data(H) ? "toggle" : i.data();
                K._jQueryInterface.call(e, n)
            }))
        })), t.fn[j] = K._jQueryInterface, t.fn[j].Constructor = K, t.fn[j].noConflict = function () {
            return t.fn[j] = R, K._jQueryInterface
        };
        var Q = "dropdown",
            Z = "bs.dropdown",
            J = "." + Z,
            ee = ".data-api",
            te = t.fn[Q],
            ie = new RegExp("38|40|27"),
            ne = {
                HIDE: "hide" + J,
                HIDDEN: "hidden" + J,
                SHOW: "show" + J,
                SHOWN: "shown" + J,
                CLICK: "click" + J,
                CLICK_DATA_API: "click" + J + ee,
                KEYDOWN_DATA_API: "keydown" + J + ee,
                KEYUP_DATA_API: "keyup" + J + ee
            },
            re = "disabled",
            se = "show",
            ae = "dropdown-menu-right",
            oe = '[data-toggle="dropdown"]',
            le = ".dropdown-menu",
            de = {
                offset: 0,
                flip: !0,
                boundary: "scrollParent",
                reference: "toggle",
                display: "dynamic"
            },
            ce = {
                offset: "(number|string|function)",
                flip: "boolean",
                boundary: "(string|element)",
                reference: "(string|element)",
                display: "string"
            },
            ue = function () {
                function e(e, t) {
                    this._element = e, this._popper = null, this._config = this._getConfig(t), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
                }
                var n = e.prototype;
                return n.toggle = function () {
                    if (!this._element.disabled && !t(this._element).hasClass(re)) {
                        var n = e._getParentFromElement(this._element),
                            r = t(this._menu).hasClass(se);
                        if (e._clearMenus(), !r) {
                            var s = {
                                    relatedTarget: this._element
                                },
                                a = t.Event(ne.SHOW, s);
                            if (t(n).trigger(a), !a.isDefaultPrevented()) {
                                if (!this._inNavbar) {
                                    if (void 0 === i) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
                                    var l = this._element;
                                    "parent" === this._config.reference ? l = n : o.isElement(this._config.reference) && (l = this._config.reference, void 0 !== this._config.reference.jquery && (l = this._config.reference[0])), "scrollParent" !== this._config.boundary && t(n).addClass("position-static"), this._popper = new i(l, this._menu, this._getPopperConfig())
                                }
                                "ontouchstart" in document.documentElement && 0 === t(n).closest(".navbar-nav").length && t(document.body).children().on("mouseover", null, t.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), t(this._menu).toggleClass(se), t(n).toggleClass(se).trigger(t.Event(ne.SHOWN, s))
                            }
                        }
                    }
                }, n.show = function () {
                    if (!(this._element.disabled || t(this._element).hasClass(re) || t(this._menu).hasClass(se))) {
                        var i = {
                                relatedTarget: this._element
                            },
                            n = t.Event(ne.SHOW, i),
                            r = e._getParentFromElement(this._element);
                        t(r).trigger(n), n.isDefaultPrevented() || (t(this._menu).toggleClass(se), t(r).toggleClass(se).trigger(t.Event(ne.SHOWN, i)))
                    }
                }, n.hide = function () {
                    if (!this._element.disabled && !t(this._element).hasClass(re) && t(this._menu).hasClass(se)) {
                        var i = {
                                relatedTarget: this._element
                            },
                            n = t.Event(ne.HIDE, i),
                            r = e._getParentFromElement(this._element);
                        t(r).trigger(n), n.isDefaultPrevented() || (t(this._menu).toggleClass(se), t(r).toggleClass(se).trigger(t.Event(ne.HIDDEN, i)))
                    }
                }, n.dispose = function () {
                    t.removeData(this._element, Z), t(this._element).off(J), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
                }, n.update = function () {
                    this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
                }, n._addEventListeners = function () {
                    var e = this;
                    t(this._element).on(ne.CLICK, (function (t) {
                        t.preventDefault(), t.stopPropagation(), e.toggle()
                    }))
                }, n._getConfig = function (e) {
                    return e = s({}, this.constructor.Default, t(this._element).data(), e), o.typeCheckConfig(Q, e, this.constructor.DefaultType), e
                }, n._getMenuElement = function () {
                    if (!this._menu) {
                        var t = e._getParentFromElement(this._element);
                        t && (this._menu = t.querySelector(le))
                    }
                    return this._menu
                }, n._getPlacement = function () {
                    var e = t(this._element.parentNode),
                        i = "bottom-start";
                    return e.hasClass("dropup") ? (i = "top-start", t(this._menu).hasClass(ae) && (i = "top-end")) : e.hasClass("dropright") ? i = "right-start" : e.hasClass("dropleft") ? i = "left-start" : t(this._menu).hasClass(ae) && (i = "bottom-end"), i
                }, n._detectNavbar = function () {
                    return 0 < t(this._element).closest(".navbar").length
                }, n._getOffset = function () {
                    var e = this,
                        t = {};
                    return "function" == typeof this._config.offset ? t.fn = function (t) {
                        return t.offsets = s({}, t.offsets, e._config.offset(t.offsets, e._element) || {}), t
                    } : t.offset = this._config.offset, t
                }, n._getPopperConfig = function () {
                    var e = {
                        placement: this._getPlacement(),
                        modifiers: {
                            offset: this._getOffset(),
                            flip: {
                                enabled: this._config.flip
                            },
                            preventOverflow: {
                                boundariesElement: this._config.boundary
                            }
                        }
                    };
                    return "static" === this._config.display && (e.modifiers.applyStyle = {
                        enabled: !1
                    }), e
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this).data(Z);
                        if (n || (n = new e(this, "object" == typeof i ? i : null), t(this).data(Z, n)), "string" == typeof i) {
                            if (void 0 === n[i]) throw new TypeError('No method named "' + i + '"');
                            n[i]()
                        }
                    }))
                }, e._clearMenus = function (i) {
                    if (!i || 3 !== i.which && ("keyup" !== i.type || 9 === i.which))
                        for (var n = [].slice.call(document.querySelectorAll(oe)), r = 0, s = n.length; r < s; r++) {
                            var a = e._getParentFromElement(n[r]),
                                o = t(n[r]).data(Z),
                                l = {
                                    relatedTarget: n[r]
                                };
                            if (i && "click" === i.type && (l.clickEvent = i), o) {
                                var d = o._menu;
                                if (t(a).hasClass(se) && !(i && ("click" === i.type && /input|textarea/i.test(i.target.tagName) || "keyup" === i.type && 9 === i.which) && t.contains(a, i.target))) {
                                    var c = t.Event(ne.HIDE, l);
                                    t(a).trigger(c), c.isDefaultPrevented() || ("ontouchstart" in document.documentElement && t(document.body).children().off("mouseover", null, t.noop), n[r].setAttribute("aria-expanded", "false"), t(d).removeClass(se), t(a).removeClass(se).trigger(t.Event(ne.HIDDEN, l)))
                                }
                            }
                        }
                }, e._getParentFromElement = function (e) {
                    var t, i = o.getSelectorFromElement(e);
                    return i && (t = document.querySelector(i)), t || e.parentNode
                }, e._dataApiKeydownHandler = function (i) {
                    if ((/input|textarea/i.test(i.target.tagName) ? !(32 === i.which || 27 !== i.which && (40 !== i.which && 38 !== i.which || t(i.target).closest(le).length)) : ie.test(i.which)) && (i.preventDefault(), i.stopPropagation(), !this.disabled && !t(this).hasClass(re))) {
                        var n = e._getParentFromElement(this),
                            r = t(n).hasClass(se);
                        if (r && (!r || 27 !== i.which && 32 !== i.which)) {
                            var s = [].slice.call(n.querySelectorAll(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)"));
                            if (0 !== s.length) {
                                var a = s.indexOf(i.target);
                                38 === i.which && 0 < a && a--, 40 === i.which && a < s.length - 1 && a++, a < 0 && (a = 0), s[a].focus()
                            }
                        } else {
                            if (27 === i.which) {
                                var o = n.querySelector(oe);
                                t(o).trigger("focus")
                            }
                            t(this).trigger("click")
                        }
                    }
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return de
                    }
                }, {
                    key: "DefaultType",
                    get: function () {
                        return ce
                    }
                }]), e
            }();
        t(document).on(ne.KEYDOWN_DATA_API, oe, ue._dataApiKeydownHandler).on(ne.KEYDOWN_DATA_API, le, ue._dataApiKeydownHandler).on(ne.CLICK_DATA_API + " " + ne.KEYUP_DATA_API, ue._clearMenus).on(ne.CLICK_DATA_API, oe, (function (e) {
            e.preventDefault(), e.stopPropagation(), ue._jQueryInterface.call(t(this), "toggle")
        })).on(ne.CLICK_DATA_API, ".dropdown form", (function (e) {
            e.stopPropagation()
        })), t.fn[Q] = ue._jQueryInterface, t.fn[Q].Constructor = ue, t.fn[Q].noConflict = function () {
            return t.fn[Q] = te, ue._jQueryInterface
        };
        var he = "modal",
            pe = "bs.modal",
            fe = "." + pe,
            ge = t.fn[he],
            me = {
                backdrop: !0,
                keyboard: !0,
                focus: !0,
                show: !0
            },
            ve = {
                backdrop: "(boolean|string)",
                keyboard: "boolean",
                focus: "boolean",
                show: "boolean"
            },
            ye = {
                HIDE: "hide" + fe,
                HIDDEN: "hidden" + fe,
                SHOW: "show" + fe,
                SHOWN: "shown" + fe,
                FOCUSIN: "focusin" + fe,
                RESIZE: "resize" + fe,
                CLICK_DISMISS: "click.dismiss" + fe,
                KEYDOWN_DISMISS: "keydown.dismiss" + fe,
                MOUSEUP_DISMISS: "mouseup.dismiss" + fe,
                MOUSEDOWN_DISMISS: "mousedown.dismiss" + fe,
                CLICK_DATA_API: "click" + fe + ".data-api"
            },
            be = "modal-open",
            we = "fade",
            xe = "show",
            Ce = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
            Te = ".sticky-top",
            Ee = function () {
                function e(e, t) {
                    this._config = this._getConfig(t), this._element = e, this._dialog = e.querySelector(".modal-dialog"), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0
                }
                var i = e.prototype;
                return i.toggle = function (e) {
                    return this._isShown ? this.hide() : this.show(e)
                }, i.show = function (e) {
                    var i = this;
                    if (!this._isShown && !this._isTransitioning) {
                        t(this._element).hasClass(we) && (this._isTransitioning = !0);
                        var n = t.Event(ye.SHOW, {
                            relatedTarget: e
                        });
                        t(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), t(this._element).on(ye.CLICK_DISMISS, '[data-dismiss="modal"]', (function (e) {
                            return i.hide(e)
                        })), t(this._dialog).on(ye.MOUSEDOWN_DISMISS, (function () {
                            t(i._element).one(ye.MOUSEUP_DISMISS, (function (e) {
                                t(e.target).is(i._element) && (i._ignoreBackdropClick = !0)
                            }))
                        })), this._showBackdrop((function () {
                            return i._showElement(e)
                        })))
                    }
                }, i.hide = function (e) {
                    var i = this;
                    if (e && e.preventDefault(), this._isShown && !this._isTransitioning) {
                        var n = t.Event(ye.HIDE);
                        if (t(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
                            this._isShown = !1;
                            var r = t(this._element).hasClass(we);
                            if (r && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), t(document).off(ye.FOCUSIN), t(this._element).removeClass(xe), t(this._element).off(ye.CLICK_DISMISS), t(this._dialog).off(ye.MOUSEDOWN_DISMISS), r) {
                                var s = o.getTransitionDurationFromElement(this._element);
                                t(this._element).one(o.TRANSITION_END, (function (e) {
                                    return i._hideModal(e)
                                })).emulateTransitionEnd(s)
                            } else this._hideModal()
                        }
                    }
                }, i.dispose = function () {
                    [window, this._element, this._dialog].forEach((function (e) {
                        return t(e).off(fe)
                    })), t(document).off(ye.FOCUSIN), t.removeData(this._element, pe), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null
                }, i.handleUpdate = function () {
                    this._adjustDialog()
                }, i._getConfig = function (e) {
                    return e = s({}, me, e), o.typeCheckConfig(he, e, ve), e
                }, i._showElement = function (e) {
                    var i = this,
                        n = t(this._element).hasClass(we);
                    this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), t(this._dialog).hasClass("modal-dialog-scrollable") ? this._dialog.querySelector(".modal-body").scrollTop = 0 : this._element.scrollTop = 0, n && o.reflow(this._element), t(this._element).addClass(xe), this._config.focus && this._enforceFocus();
                    var r = t.Event(ye.SHOWN, {
                            relatedTarget: e
                        }),
                        s = function () {
                            i._config.focus && i._element.focus(), i._isTransitioning = !1, t(i._element).trigger(r)
                        };
                    if (n) {
                        var a = o.getTransitionDurationFromElement(this._dialog);
                        t(this._dialog).one(o.TRANSITION_END, s).emulateTransitionEnd(a)
                    } else s()
                }, i._enforceFocus = function () {
                    var e = this;
                    t(document).off(ye.FOCUSIN).on(ye.FOCUSIN, (function (i) {
                        document !== i.target && e._element !== i.target && 0 === t(e._element).has(i.target).length && e._element.focus()
                    }))
                }, i._setEscapeEvent = function () {
                    var e = this;
                    this._isShown && this._config.keyboard ? t(this._element).on(ye.KEYDOWN_DISMISS, (function (t) {
                        27 === t.which && (t.preventDefault(), e.hide())
                    })) : this._isShown || t(this._element).off(ye.KEYDOWN_DISMISS)
                }, i._setResizeEvent = function () {
                    var e = this;
                    this._isShown ? t(window).on(ye.RESIZE, (function (t) {
                        return e.handleUpdate(t)
                    })) : t(window).off(ye.RESIZE)
                }, i._hideModal = function () {
                    var e = this;
                    this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop((function () {
                        t(document.body).removeClass(be), e._resetAdjustments(), e._resetScrollbar(), t(e._element).trigger(ye.HIDDEN)
                    }))
                }, i._removeBackdrop = function () {
                    this._backdrop && (t(this._backdrop).remove(), this._backdrop = null)
                }, i._showBackdrop = function (e) {
                    var i = this,
                        n = t(this._element).hasClass(we) ? we : "";
                    if (this._isShown && this._config.backdrop) {
                        if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", n && this._backdrop.classList.add(n), t(this._backdrop).appendTo(document.body), t(this._element).on(ye.CLICK_DISMISS, (function (e) {
                                i._ignoreBackdropClick ? i._ignoreBackdropClick = !1 : e.target === e.currentTarget && ("static" === i._config.backdrop ? i._element.focus() : i.hide())
                            })), n && o.reflow(this._backdrop), t(this._backdrop).addClass(xe), !e) return;
                        if (!n) return void e();
                        var r = o.getTransitionDurationFromElement(this._backdrop);
                        t(this._backdrop).one(o.TRANSITION_END, e).emulateTransitionEnd(r)
                    } else if (!this._isShown && this._backdrop) {
                        t(this._backdrop).removeClass(xe);
                        var s = function () {
                            i._removeBackdrop(), e && e()
                        };
                        if (t(this._element).hasClass(we)) {
                            var a = o.getTransitionDurationFromElement(this._backdrop);
                            t(this._backdrop).one(o.TRANSITION_END, s).emulateTransitionEnd(a)
                        } else s()
                    } else e && e()
                }, i._adjustDialog = function () {
                    var e = this._element.scrollHeight > document.documentElement.clientHeight;
                    !this._isBodyOverflowing && e && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !e && (this._element.style.paddingRight = this._scrollbarWidth + "px")
                }, i._resetAdjustments = function () {
                    this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
                }, i._checkScrollbar = function () {
                    var e = document.body.getBoundingClientRect();
                    this._isBodyOverflowing = e.left + e.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
                }, i._setScrollbar = function () {
                    var e = this;
                    if (this._isBodyOverflowing) {
                        var i = [].slice.call(document.querySelectorAll(Ce)),
                            n = [].slice.call(document.querySelectorAll(Te));
                        t(i).each((function (i, n) {
                            var r = n.style.paddingRight,
                                s = t(n).css("padding-right");
                            t(n).data("padding-right", r).css("padding-right", parseFloat(s) + e._scrollbarWidth + "px")
                        })), t(n).each((function (i, n) {
                            var r = n.style.marginRight,
                                s = t(n).css("margin-right");
                            t(n).data("margin-right", r).css("margin-right", parseFloat(s) - e._scrollbarWidth + "px")
                        }));
                        var r = document.body.style.paddingRight,
                            s = t(document.body).css("padding-right");
                        t(document.body).data("padding-right", r).css("padding-right", parseFloat(s) + this._scrollbarWidth + "px")
                    }
                    t(document.body).addClass(be)
                }, i._resetScrollbar = function () {
                    var e = [].slice.call(document.querySelectorAll(Ce));
                    t(e).each((function (e, i) {
                        var n = t(i).data("padding-right");
                        t(i).removeData("padding-right"), i.style.paddingRight = n || ""
                    }));
                    var i = [].slice.call(document.querySelectorAll("" + Te));
                    t(i).each((function (e, i) {
                        var n = t(i).data("margin-right");
                        void 0 !== n && t(i).css("margin-right", n).removeData("margin-right")
                    }));
                    var n = t(document.body).data("padding-right");
                    t(document.body).removeData("padding-right"), document.body.style.paddingRight = n || ""
                }, i._getScrollbarWidth = function () {
                    var e = document.createElement("div");
                    e.className = "modal-scrollbar-measure", document.body.appendChild(e);
                    var t = e.getBoundingClientRect().width - e.clientWidth;
                    return document.body.removeChild(e), t
                }, e._jQueryInterface = function (i, n) {
                    return this.each((function () {
                        var r = t(this).data(pe),
                            a = s({}, me, t(this).data(), "object" == typeof i && i ? i : {});
                        if (r || (r = new e(this, a), t(this).data(pe, r)), "string" == typeof i) {
                            if (void 0 === r[i]) throw new TypeError('No method named "' + i + '"');
                            r[i](n)
                        } else a.show && r.show(n)
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return me
                    }
                }]), e
            }();
        t(document).on(ye.CLICK_DATA_API, '[data-toggle="modal"]', (function (e) {
            var i, n = this,
                r = o.getSelectorFromElement(this);
            r && (i = document.querySelector(r));
            var a = t(i).data(pe) ? "toggle" : s({}, t(i).data(), t(this).data());
            "A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();
            var l = t(i).one(ye.SHOW, (function (e) {
                e.isDefaultPrevented() || l.one(ye.HIDDEN, (function () {
                    t(n).is(":visible") && n.focus()
                }))
            }));
            Ee._jQueryInterface.call(t(i), a, this)
        })), t.fn[he] = Ee._jQueryInterface, t.fn[he].Constructor = Ee, t.fn[he].noConflict = function () {
            return t.fn[he] = ge, Ee._jQueryInterface
        };
        var Se = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
            _e = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
            ke = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

        function $e(e, t, i) {
            if (0 === e.length) return e;
            if (i && "function" == typeof i) return i(e);
            for (var n = (new window.DOMParser).parseFromString(e, "text/html"), r = Object.keys(t), s = [].slice.call(n.body.querySelectorAll("*")), a = function (e, i) {
                    var n = s[e],
                        a = n.nodeName.toLowerCase();
                    if (-1 === r.indexOf(n.nodeName.toLowerCase())) return n.parentNode.removeChild(n), "continue";
                    var o = [].slice.call(n.attributes),
                        l = [].concat(t["*"] || [], t[a] || []);
                    o.forEach((function (e) {
                        (function (e, t) {
                            var i = e.nodeName.toLowerCase();
                            if (-1 !== t.indexOf(i)) return -1 === Se.indexOf(i) || Boolean(e.nodeValue.match(_e) || e.nodeValue.match(ke));
                            for (var n = t.filter((function (e) {
                                    return e instanceof RegExp
                                })), r = 0, s = n.length; r < s; r++)
                                if (i.match(n[r])) return !0;
                            return !1
                        })(e, l) || n.removeAttribute(e.nodeName)
                    }))
                }, o = 0, l = s.length; o < l; o++) a(o);
            return n.body.innerHTML
        }
        var Ie = "tooltip",
            De = "bs.tooltip",
            Ae = "." + De,
            Me = t.fn[Ie],
            Pe = "bs-tooltip",
            Oe = new RegExp("(^|\\s)" + Pe + "\\S+", "g"),
            Ne = ["sanitize", "whiteList", "sanitizeFn"],
            Le = {
                animation: "boolean",
                template: "string",
                title: "(string|element|function)",
                trigger: "string",
                delay: "(number|object)",
                html: "boolean",
                selector: "(string|boolean)",
                placement: "(string|function)",
                offset: "(number|string|function)",
                container: "(string|element|boolean)",
                fallbackPlacement: "(string|array)",
                boundary: "(string|element)",
                sanitize: "boolean",
                sanitizeFn: "(null|function)",
                whiteList: "object"
            },
            ze = {
                AUTO: "auto",
                TOP: "top",
                RIGHT: "right",
                BOTTOM: "bottom",
                LEFT: "left"
            },
            je = {
                animation: !0,
                template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
                trigger: "hover focus",
                title: "",
                delay: 0,
                html: !1,
                selector: !1,
                placement: "top",
                offset: 0,
                container: !1,
                fallbackPlacement: "flip",
                boundary: "scrollParent",
                sanitize: !0,
                sanitizeFn: null,
                whiteList: {
                    "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
                    a: ["target", "href", "title", "rel"],
                    area: [],
                    b: [],
                    br: [],
                    col: [],
                    code: [],
                    div: [],
                    em: [],
                    hr: [],
                    h1: [],
                    h2: [],
                    h3: [],
                    h4: [],
                    h5: [],
                    h6: [],
                    i: [],
                    img: ["src", "alt", "title", "width", "height"],
                    li: [],
                    ol: [],
                    p: [],
                    pre: [],
                    s: [],
                    small: [],
                    span: [],
                    sub: [],
                    sup: [],
                    strong: [],
                    u: [],
                    ul: []
                }
            },
            He = "show",
            qe = {
                HIDE: "hide" + Ae,
                HIDDEN: "hidden" + Ae,
                SHOW: "show" + Ae,
                SHOWN: "shown" + Ae,
                INSERTED: "inserted" + Ae,
                CLICK: "click" + Ae,
                FOCUSIN: "focusin" + Ae,
                FOCUSOUT: "focusout" + Ae,
                MOUSEENTER: "mouseenter" + Ae,
                MOUSELEAVE: "mouseleave" + Ae
            },
            Re = "fade",
            Be = "show",
            We = "hover",
            Fe = "focus",
            Ge = function () {
                function e(e, t) {
                    if (void 0 === i) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
                    this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = e, this.config = this._getConfig(t), this.tip = null, this._setListeners()
                }
                var n = e.prototype;
                return n.enable = function () {
                    this._isEnabled = !0
                }, n.disable = function () {
                    this._isEnabled = !1
                }, n.toggleEnabled = function () {
                    this._isEnabled = !this._isEnabled
                }, n.toggle = function (e) {
                    if (this._isEnabled)
                        if (e) {
                            var i = this.constructor.DATA_KEY,
                                n = t(e.currentTarget).data(i);
                            n || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
                        } else {
                            if (t(this.getTipElement()).hasClass(Be)) return void this._leave(null, this);
                            this._enter(null, this)
                        }
                }, n.dispose = function () {
                    clearTimeout(this._timeout), t.removeData(this.element, this.constructor.DATA_KEY), t(this.element).off(this.constructor.EVENT_KEY), t(this.element).closest(".modal").off("hide.bs.modal"), this.tip && t(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
                }, n.show = function () {
                    var e = this;
                    if ("none" === t(this.element).css("display")) throw new Error("Please use show on visible elements");
                    var n = t.Event(this.constructor.Event.SHOW);
                    if (this.isWithContent() && this._isEnabled) {
                        t(this.element).trigger(n);
                        var r = o.findShadowRoot(this.element),
                            s = t.contains(null !== r ? r : this.element.ownerDocument.documentElement, this.element);
                        if (n.isDefaultPrevented() || !s) return;
                        var a = this.getTipElement(),
                            l = o.getUID(this.constructor.NAME);
                        a.setAttribute("id", l), this.element.setAttribute("aria-describedby", l), this.setContent(), this.config.animation && t(a).addClass(Re);
                        var d = "function" == typeof this.config.placement ? this.config.placement.call(this, a, this.element) : this.config.placement,
                            c = this._getAttachment(d);
                        this.addAttachmentClass(c);
                        var u = this._getContainer();
                        t(a).data(this.constructor.DATA_KEY, this), t.contains(this.element.ownerDocument.documentElement, this.tip) || t(a).appendTo(u), t(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new i(this.element, a, {
                            placement: c,
                            modifiers: {
                                offset: this._getOffset(),
                                flip: {
                                    behavior: this.config.fallbackPlacement
                                },
                                arrow: {
                                    element: ".arrow"
                                },
                                preventOverflow: {
                                    boundariesElement: this.config.boundary
                                }
                            },
                            onCreate: function (t) {
                                t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t)
                            },
                            onUpdate: function (t) {
                                return e._handlePopperPlacementChange(t)
                            }
                        }), t(a).addClass(Be), "ontouchstart" in document.documentElement && t(document.body).children().on("mouseover", null, t.noop);
                        var h = function () {
                            e.config.animation && e._fixTransition();
                            var i = e._hoverState;
                            e._hoverState = null, t(e.element).trigger(e.constructor.Event.SHOWN), "out" === i && e._leave(null, e)
                        };
                        if (t(this.tip).hasClass(Re)) {
                            var p = o.getTransitionDurationFromElement(this.tip);
                            t(this.tip).one(o.TRANSITION_END, h).emulateTransitionEnd(p)
                        } else h()
                    }
                }, n.hide = function (e) {
                    var i = this,
                        n = this.getTipElement(),
                        r = t.Event(this.constructor.Event.HIDE),
                        s = function () {
                            i._hoverState !== He && n.parentNode && n.parentNode.removeChild(n), i._cleanTipClass(), i.element.removeAttribute("aria-describedby"), t(i.element).trigger(i.constructor.Event.HIDDEN), null !== i._popper && i._popper.destroy(), e && e()
                        };
                    if (t(this.element).trigger(r), !r.isDefaultPrevented()) {
                        if (t(n).removeClass(Be), "ontouchstart" in document.documentElement && t(document.body).children().off("mouseover", null, t.noop), this._activeTrigger.click = !1, this._activeTrigger[Fe] = !1, this._activeTrigger[We] = !1, t(this.tip).hasClass(Re)) {
                            var a = o.getTransitionDurationFromElement(n);
                            t(n).one(o.TRANSITION_END, s).emulateTransitionEnd(a)
                        } else s();
                        this._hoverState = ""
                    }
                }, n.update = function () {
                    null !== this._popper && this._popper.scheduleUpdate()
                }, n.isWithContent = function () {
                    return Boolean(this.getTitle())
                }, n.addAttachmentClass = function (e) {
                    t(this.getTipElement()).addClass(Pe + "-" + e)
                }, n.getTipElement = function () {
                    return this.tip = this.tip || t(this.config.template)[0], this.tip
                }, n.setContent = function () {
                    var e = this.getTipElement();
                    this.setElementContent(t(e.querySelectorAll(".tooltip-inner")), this.getTitle()), t(e).removeClass(Re + " " + Be)
                }, n.setElementContent = function (e, i) {
                    "object" != typeof i || !i.nodeType && !i.jquery ? this.config.html ? (this.config.sanitize && (i = $e(i, this.config.whiteList, this.config.sanitizeFn)), e.html(i)) : e.text(i) : this.config.html ? t(i).parent().is(e) || e.empty().append(i) : e.text(t(i).text())
                }, n.getTitle = function () {
                    var e = this.element.getAttribute("data-original-title");
                    return e || (e = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), e
                }, n._getOffset = function () {
                    var e = this,
                        t = {};
                    return "function" == typeof this.config.offset ? t.fn = function (t) {
                        return t.offsets = s({}, t.offsets, e.config.offset(t.offsets, e.element) || {}), t
                    } : t.offset = this.config.offset, t
                }, n._getContainer = function () {
                    return !1 === this.config.container ? document.body : o.isElement(this.config.container) ? t(this.config.container) : t(document).find(this.config.container)
                }, n._getAttachment = function (e) {
                    return ze[e.toUpperCase()]
                }, n._setListeners = function () {
                    var e = this;
                    this.config.trigger.split(" ").forEach((function (i) {
                        if ("click" === i) t(e.element).on(e.constructor.Event.CLICK, e.config.selector, (function (t) {
                            return e.toggle(t)
                        }));
                        else if ("manual" !== i) {
                            var n = i === We ? e.constructor.Event.MOUSEENTER : e.constructor.Event.FOCUSIN,
                                r = i === We ? e.constructor.Event.MOUSELEAVE : e.constructor.Event.FOCUSOUT;
                            t(e.element).on(n, e.config.selector, (function (t) {
                                return e._enter(t)
                            })).on(r, e.config.selector, (function (t) {
                                return e._leave(t)
                            }))
                        }
                    })), t(this.element).closest(".modal").on("hide.bs.modal", (function () {
                        e.element && e.hide()
                    })), this.config.selector ? this.config = s({}, this.config, {
                        trigger: "manual",
                        selector: ""
                    }) : this._fixTitle()
                }, n._fixTitle = function () {
                    var e = typeof this.element.getAttribute("data-original-title");
                    (this.element.getAttribute("title") || "string" !== e) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
                }, n._enter = function (e, i) {
                    var n = this.constructor.DATA_KEY;
                    (i = i || t(e.currentTarget).data(n)) || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), e && (i._activeTrigger["focusin" === e.type ? Fe : We] = !0), t(i.getTipElement()).hasClass(Be) || i._hoverState === He ? i._hoverState = He : (clearTimeout(i._timeout), i._hoverState = He, i.config.delay && i.config.delay.show ? i._timeout = setTimeout((function () {
                        i._hoverState === He && i.show()
                    }), i.config.delay.show) : i.show())
                }, n._leave = function (e, i) {
                    var n = this.constructor.DATA_KEY;
                    (i = i || t(e.currentTarget).data(n)) || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), e && (i._activeTrigger["focusout" === e.type ? Fe : We] = !1), i._isWithActiveTrigger() || (clearTimeout(i._timeout), i._hoverState = "out", i.config.delay && i.config.delay.hide ? i._timeout = setTimeout((function () {
                        "out" === i._hoverState && i.hide()
                    }), i.config.delay.hide) : i.hide())
                }, n._isWithActiveTrigger = function () {
                    for (var e in this._activeTrigger)
                        if (this._activeTrigger[e]) return !0;
                    return !1
                }, n._getConfig = function (e) {
                    var i = t(this.element).data();
                    return Object.keys(i).forEach((function (e) {
                        -1 !== Ne.indexOf(e) && delete i[e]
                    })), "number" == typeof (e = s({}, this.constructor.Default, i, "object" == typeof e && e ? e : {})).delay && (e.delay = {
                        show: e.delay,
                        hide: e.delay
                    }), "number" == typeof e.title && (e.title = e.title.toString()), "number" == typeof e.content && (e.content = e.content.toString()), o.typeCheckConfig(Ie, e, this.constructor.DefaultType), e.sanitize && (e.template = $e(e.template, e.whiteList, e.sanitizeFn)), e
                }, n._getDelegateConfig = function () {
                    var e = {};
                    if (this.config)
                        for (var t in this.config) this.constructor.Default[t] !== this.config[t] && (e[t] = this.config[t]);
                    return e
                }, n._cleanTipClass = function () {
                    var e = t(this.getTipElement()),
                        i = e.attr("class").match(Oe);
                    null !== i && i.length && e.removeClass(i.join(""))
                }, n._handlePopperPlacementChange = function (e) {
                    var t = e.instance;
                    this.tip = t.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(e.placement))
                }, n._fixTransition = function () {
                    var e = this.getTipElement(),
                        i = this.config.animation;
                    null === e.getAttribute("x-placement") && (t(e).removeClass(Re), this.config.animation = !1, this.hide(), this.show(), this.config.animation = i)
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this).data(De),
                            r = "object" == typeof i && i;
                        if ((n || !/dispose|hide/.test(i)) && (n || (n = new e(this, r), t(this).data(De, n)), "string" == typeof i)) {
                            if (void 0 === n[i]) throw new TypeError('No method named "' + i + '"');
                            n[i]()
                        }
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return je
                    }
                }, {
                    key: "NAME",
                    get: function () {
                        return Ie
                    }
                }, {
                    key: "DATA_KEY",
                    get: function () {
                        return De
                    }
                }, {
                    key: "Event",
                    get: function () {
                        return qe
                    }
                }, {
                    key: "EVENT_KEY",
                    get: function () {
                        return Ae
                    }
                }, {
                    key: "DefaultType",
                    get: function () {
                        return Le
                    }
                }]), e
            }();
        t.fn[Ie] = Ge._jQueryInterface, t.fn[Ie].Constructor = Ge, t.fn[Ie].noConflict = function () {
            return t.fn[Ie] = Me, Ge._jQueryInterface
        };
        var Xe = "popover",
            Ve = "bs.popover",
            Ye = "." + Ve,
            Ue = t.fn[Xe],
            Ke = "bs-popover",
            Qe = new RegExp("(^|\\s)" + Ke + "\\S+", "g"),
            Ze = s({}, Ge.Default, {
                placement: "right",
                trigger: "click",
                content: "",
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
            }),
            Je = s({}, Ge.DefaultType, {
                content: "(string|element|function)"
            }),
            et = {
                HIDE: "hide" + Ye,
                HIDDEN: "hidden" + Ye,
                SHOW: "show" + Ye,
                SHOWN: "shown" + Ye,
                INSERTED: "inserted" + Ye,
                CLICK: "click" + Ye,
                FOCUSIN: "focusin" + Ye,
                FOCUSOUT: "focusout" + Ye,
                MOUSEENTER: "mouseenter" + Ye,
                MOUSELEAVE: "mouseleave" + Ye
            },
            tt = function (e) {
                var i, n;

                function s() {
                    return e.apply(this, arguments) || this
                }
                n = e, (i = s).prototype = Object.create(n.prototype), (i.prototype.constructor = i).__proto__ = n;
                var a = s.prototype;
                return a.isWithContent = function () {
                    return this.getTitle() || this._getContent()
                }, a.addAttachmentClass = function (e) {
                    t(this.getTipElement()).addClass(Ke + "-" + e)
                }, a.getTipElement = function () {
                    return this.tip = this.tip || t(this.config.template)[0], this.tip
                }, a.setContent = function () {
                    var e = t(this.getTipElement());
                    this.setElementContent(e.find(".popover-header"), this.getTitle());
                    var i = this._getContent();
                    "function" == typeof i && (i = i.call(this.element)), this.setElementContent(e.find(".popover-body"), i), e.removeClass("fade show")
                }, a._getContent = function () {
                    return this.element.getAttribute("data-content") || this.config.content
                }, a._cleanTipClass = function () {
                    var e = t(this.getTipElement()),
                        i = e.attr("class").match(Qe);
                    null !== i && 0 < i.length && e.removeClass(i.join(""))
                }, s._jQueryInterface = function (e) {
                    return this.each((function () {
                        var i = t(this).data(Ve),
                            n = "object" == typeof e ? e : null;
                        if ((i || !/dispose|hide/.test(e)) && (i || (i = new s(this, n), t(this).data(Ve, i)), "string" == typeof e)) {
                            if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                            i[e]()
                        }
                    }))
                }, r(s, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return Ze
                    }
                }, {
                    key: "NAME",
                    get: function () {
                        return Xe
                    }
                }, {
                    key: "DATA_KEY",
                    get: function () {
                        return Ve
                    }
                }, {
                    key: "Event",
                    get: function () {
                        return et
                    }
                }, {
                    key: "EVENT_KEY",
                    get: function () {
                        return Ye
                    }
                }, {
                    key: "DefaultType",
                    get: function () {
                        return Je
                    }
                }]), s
            }(Ge);
        t.fn[Xe] = tt._jQueryInterface, t.fn[Xe].Constructor = tt, t.fn[Xe].noConflict = function () {
            return t.fn[Xe] = Ue, tt._jQueryInterface
        };
        var it = "scrollspy",
            nt = "bs.scrollspy",
            rt = "." + nt,
            st = t.fn[it],
            at = {
                offset: 10,
                method: "auto",
                target: ""
            },
            ot = {
                offset: "number",
                method: "string",
                target: "(string|element)"
            },
            lt = {
                ACTIVATE: "activate" + rt,
                SCROLL: "scroll" + rt,
                LOAD_DATA_API: "load" + rt + ".data-api"
            },
            dt = "active",
            ct = ".nav, .list-group",
            ut = ".nav-link",
            ht = ".list-group-item",
            pt = "position",
            ft = function () {
                function e(e, i) {
                    var n = this;
                    this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(i), this._selector = this._config.target + " " + ut + "," + this._config.target + " " + ht + "," + this._config.target + " .dropdown-item", this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, t(this._scrollElement).on(lt.SCROLL, (function (e) {
                        return n._process(e)
                    })), this.refresh(), this._process()
                }
                var i = e.prototype;
                return i.refresh = function () {
                    var e = this,
                        i = this._scrollElement === this._scrollElement.window ? "offset" : pt,
                        n = "auto" === this._config.method ? i : this._config.method,
                        r = n === pt ? this._getScrollTop() : 0;
                    this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map((function (e) {
                        var i, s = o.getSelectorFromElement(e);
                        if (s && (i = document.querySelector(s)), i) {
                            var a = i.getBoundingClientRect();
                            if (a.width || a.height) return [t(i)[n]().top + r, s]
                        }
                        return null
                    })).filter((function (e) {
                        return e
                    })).sort((function (e, t) {
                        return e[0] - t[0]
                    })).forEach((function (t) {
                        e._offsets.push(t[0]), e._targets.push(t[1])
                    }))
                }, i.dispose = function () {
                    t.removeData(this._element, nt), t(this._scrollElement).off(rt), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
                }, i._getConfig = function (e) {
                    if ("string" != typeof (e = s({}, at, "object" == typeof e && e ? e : {})).target) {
                        var i = t(e.target).attr("id");
                        i || (i = o.getUID(it), t(e.target).attr("id", i)), e.target = "#" + i
                    }
                    return o.typeCheckConfig(it, e, ot), e
                }, i._getScrollTop = function () {
                    return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
                }, i._getScrollHeight = function () {
                    return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                }, i._getOffsetHeight = function () {
                    return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
                }, i._process = function () {
                    var e = this._getScrollTop() + this._config.offset,
                        t = this._getScrollHeight(),
                        i = this._config.offset + t - this._getOffsetHeight();
                    if (this._scrollHeight !== t && this.refresh(), i <= e) {
                        var n = this._targets[this._targets.length - 1];
                        this._activeTarget !== n && this._activate(n)
                    } else {
                        if (this._activeTarget && e < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
                        for (var r = this._offsets.length; r--;) this._activeTarget !== this._targets[r] && e >= this._offsets[r] && (void 0 === this._offsets[r + 1] || e < this._offsets[r + 1]) && this._activate(this._targets[r])
                    }
                }, i._activate = function (e) {
                    this._activeTarget = e, this._clear();
                    var i = this._selector.split(",").map((function (t) {
                            return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
                        })),
                        n = t([].slice.call(document.querySelectorAll(i.join(","))));
                    n.hasClass("dropdown-item") ? (n.closest(".dropdown").find(".dropdown-toggle").addClass(dt), n.addClass(dt)) : (n.addClass(dt), n.parents(ct).prev(ut + ", " + ht).addClass(dt), n.parents(ct).prev(".nav-item").children(ut).addClass(dt)), t(this._scrollElement).trigger(lt.ACTIVATE, {
                        relatedTarget: e
                    })
                }, i._clear = function () {
                    [].slice.call(document.querySelectorAll(this._selector)).filter((function (e) {
                        return e.classList.contains(dt)
                    })).forEach((function (e) {
                        return e.classList.remove(dt)
                    }))
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this).data(nt);
                        if (n || (n = new e(this, "object" == typeof i && i), t(this).data(nt, n)), "string" == typeof i) {
                            if (void 0 === n[i]) throw new TypeError('No method named "' + i + '"');
                            n[i]()
                        }
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return at
                    }
                }]), e
            }();
        t(window).on(lt.LOAD_DATA_API, (function () {
            for (var e = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')), i = e.length; i--;) {
                var n = t(e[i]);
                ft._jQueryInterface.call(n, n.data())
            }
        })), t.fn[it] = ft._jQueryInterface, t.fn[it].Constructor = ft, t.fn[it].noConflict = function () {
            return t.fn[it] = st, ft._jQueryInterface
        };
        var gt = "bs.tab",
            mt = "." + gt,
            vt = t.fn.tab,
            yt = {
                HIDE: "hide" + mt,
                HIDDEN: "hidden" + mt,
                SHOW: "show" + mt,
                SHOWN: "shown" + mt,
                CLICK_DATA_API: "click" + mt + ".data-api"
            },
            bt = "active",
            wt = ".active",
            xt = "> li > .active",
            Ct = function () {
                function e(e) {
                    this._element = e
                }
                var i = e.prototype;
                return i.show = function () {
                    var e = this;
                    if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && t(this._element).hasClass(bt) || t(this._element).hasClass("disabled"))) {
                        var i, n, r = t(this._element).closest(".nav, .list-group")[0],
                            s = o.getSelectorFromElement(this._element);
                        if (r) {
                            var a = "UL" === r.nodeName || "OL" === r.nodeName ? xt : wt;
                            n = (n = t.makeArray(t(r).find(a)))[n.length - 1]
                        }
                        var l = t.Event(yt.HIDE, {
                                relatedTarget: this._element
                            }),
                            d = t.Event(yt.SHOW, {
                                relatedTarget: n
                            });
                        if (n && t(n).trigger(l), t(this._element).trigger(d), !d.isDefaultPrevented() && !l.isDefaultPrevented()) {
                            s && (i = document.querySelector(s)), this._activate(this._element, r);
                            var c = function () {
                                var i = t.Event(yt.HIDDEN, {
                                        relatedTarget: e._element
                                    }),
                                    r = t.Event(yt.SHOWN, {
                                        relatedTarget: n
                                    });
                                t(n).trigger(i), t(e._element).trigger(r)
                            };
                            i ? this._activate(i, i.parentNode, c) : c()
                        }
                    }
                }, i.dispose = function () {
                    t.removeData(this._element, gt), this._element = null
                }, i._activate = function (e, i, n) {
                    var r = this,
                        s = (!i || "UL" !== i.nodeName && "OL" !== i.nodeName ? t(i).children(wt) : t(i).find(xt))[0],
                        a = n && s && t(s).hasClass("fade"),
                        l = function () {
                            return r._transitionComplete(e, s, n)
                        };
                    if (s && a) {
                        var d = o.getTransitionDurationFromElement(s);
                        t(s).removeClass("show").one(o.TRANSITION_END, l).emulateTransitionEnd(d)
                    } else l()
                }, i._transitionComplete = function (e, i, n) {
                    if (i) {
                        t(i).removeClass(bt);
                        var r = t(i.parentNode).find("> .dropdown-menu .active")[0];
                        r && t(r).removeClass(bt), "tab" === i.getAttribute("role") && i.setAttribute("aria-selected", !1)
                    }
                    if (t(e).addClass(bt), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), o.reflow(e), e.classList.contains("fade") && e.classList.add("show"), e.parentNode && t(e.parentNode).hasClass("dropdown-menu")) {
                        var s = t(e).closest(".dropdown")[0];
                        if (s) {
                            var a = [].slice.call(s.querySelectorAll(".dropdown-toggle"));
                            t(a).addClass(bt)
                        }
                        e.setAttribute("aria-expanded", !0)
                    }
                    n && n()
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this),
                            r = n.data(gt);
                        if (r || (r = new e(this), n.data(gt, r)), "string" == typeof i) {
                            if (void 0 === r[i]) throw new TypeError('No method named "' + i + '"');
                            r[i]()
                        }
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }]), e
            }();
        t(document).on(yt.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', (function (e) {
            e.preventDefault(), Ct._jQueryInterface.call(t(this), "show")
        })), t.fn.tab = Ct._jQueryInterface, t.fn.tab.Constructor = Ct, t.fn.tab.noConflict = function () {
            return t.fn.tab = vt, Ct._jQueryInterface
        };
        var Tt = "toast",
            Et = "bs.toast",
            St = "." + Et,
            _t = t.fn[Tt],
            kt = {
                CLICK_DISMISS: "click.dismiss" + St,
                HIDE: "hide" + St,
                HIDDEN: "hidden" + St,
                SHOW: "show" + St,
                SHOWN: "shown" + St
            },
            $t = "show",
            It = "showing",
            Dt = {
                animation: "boolean",
                autohide: "boolean",
                delay: "number"
            },
            At = {
                animation: !0,
                autohide: !0,
                delay: 500
            },
            Mt = function () {
                function e(e, t) {
                    this._element = e, this._config = this._getConfig(t), this._timeout = null, this._setListeners()
                }
                var i = e.prototype;
                return i.show = function () {
                    var e = this;
                    t(this._element).trigger(kt.SHOW), this._config.animation && this._element.classList.add("fade");
                    var i = function () {
                        e._element.classList.remove(It), e._element.classList.add($t), t(e._element).trigger(kt.SHOWN), e._config.autohide && e.hide()
                    };
                    if (this._element.classList.remove("hide"), this._element.classList.add(It), this._config.animation) {
                        var n = o.getTransitionDurationFromElement(this._element);
                        t(this._element).one(o.TRANSITION_END, i).emulateTransitionEnd(n)
                    } else i()
                }, i.hide = function (e) {
                    var i = this;
                    this._element.classList.contains($t) && (t(this._element).trigger(kt.HIDE), e ? this._close() : this._timeout = setTimeout((function () {
                        i._close()
                    }), this._config.delay))
                }, i.dispose = function () {
                    clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains($t) && this._element.classList.remove($t), t(this._element).off(kt.CLICK_DISMISS), t.removeData(this._element, Et), this._element = null, this._config = null
                }, i._getConfig = function (e) {
                    return e = s({}, At, t(this._element).data(), "object" == typeof e && e ? e : {}), o.typeCheckConfig(Tt, e, this.constructor.DefaultType), e
                }, i._setListeners = function () {
                    var e = this;
                    t(this._element).on(kt.CLICK_DISMISS, '[data-dismiss="toast"]', (function () {
                        return e.hide(!0)
                    }))
                }, i._close = function () {
                    var e = this,
                        i = function () {
                            e._element.classList.add("hide"), t(e._element).trigger(kt.HIDDEN)
                        };
                    if (this._element.classList.remove($t), this._config.animation) {
                        var n = o.getTransitionDurationFromElement(this._element);
                        t(this._element).one(o.TRANSITION_END, i).emulateTransitionEnd(n)
                    } else i()
                }, e._jQueryInterface = function (i) {
                    return this.each((function () {
                        var n = t(this),
                            r = n.data(Et);
                        if (r || (r = new e(this, "object" == typeof i && i), n.data(Et, r)), "string" == typeof i) {
                            if (void 0 === r[i]) throw new TypeError('No method named "' + i + '"');
                            r[i](this)
                        }
                    }))
                }, r(e, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.3.1"
                    }
                }, {
                    key: "DefaultType",
                    get: function () {
                        return Dt
                    }
                }, {
                    key: "Default",
                    get: function () {
                        return At
                    }
                }]), e
            }();
        t.fn[Tt] = Mt._jQueryInterface, t.fn[Tt].Constructor = Mt, t.fn[Tt].noConflict = function () {
                return t.fn[Tt] = _t, Mt._jQueryInterface
            },
            function () {
                if (void 0 === t) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
                var e = t.fn.jquery.split(" ")[0].split(".");
                if (e[0] < 2 && e[1] < 9 || 1 === e[0] && 9 === e[1] && e[2] < 1 || 4 <= e[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
            }(), e.Util = o, e.Alert = p, e.Button = T, e.Carousel = z, e.Collapse = K, e.Dropdown = ue, e.Modal = Ee, e.Popover = tt, e.Scrollspy = ft, e.Tab = Ct, e.Toast = Mt, e.Tooltip = Ge, Object.defineProperty(e, "__esModule", {
                value: !0
            })
    })), $("#before-load").find("img").fadeOut().end().delay(400).fadeOut("slow"), $(document).ready((function () {
        $(".section").lightGallery({
            selector: ".an",
            mode: "lg-slide",
            cssEasing: "cubic-bezier(0.785, 0.135, 0.15, 0.86)",
            speed: 400,
            pause: 5e3,
            hash: !1,
            share: !1,
            counter: !1,
            pullCaptionUp: !0,
            autoplayControls: !1,
            download: !1,
            fullScreen: !0,
            zoom: !0,
            mousewheel: !0,
            enableDrag: !0,
            enableSwipe: !0,
            thumbnail: !0,
            animateThumb: !0,
            thumbWidth: 150,
            thumbContHeight: 150,
            currentPagerPosition: "middle",
            showThumbByDefault: !0
        });
        var e = $("#menu-btn"),
            t = $(".naviagation__item a"),
            i = $(".sidebarBtn");
        $(".sidebarBtn").on("click", (function () {
            $(".logo").toggleClass("logo2")
        })), e.on("click", (function (t) {
            t.preventDefault(), e.hasClass("open") ? (e.removeClass("open"), e.addClass("close")) : (e.removeClass("close"), e.addClass("open"))
        })), t.on("click", (function () {
            window.innerWidth < 732 && ($(".naviagation__item").toggle(500), i.toggleClass("toggle"))
        })), $(".sidebarBtn").click((function () {
            $(".sidebarBtn").toggleClass("toggle")
        })), $(".wrap__button_mob").on("click", (function () {
            $(".naviagation__item").stop(!0).slideToggle(500, (function () {
                "none" === $(this).css("display") && $(this).removeAttr("style")
            }))
        }));
        var n = $(".navigation");
        $(window).scroll((function () {
            var e = $(this).scrollTop();
            e >= 10 ? window.innerWidth > 768 && n.addClass("fixed") : e <= 200 && window.innerWidth > 768 && n.removeClass("fixed")
        })), $(window).scroll((function () {
            $(this).scrollTop() >= 10 && window.innerWidth < 732 && ($(".naviagation__item").css("display", "none"), $(".sidebarBtn").removeClass("toggle"))
        }));
        new Swiper(".swiper-container-offers", {
            slidesPerView: 1,
            spaceBetween: 0,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            loop: !0,
            pagination: {
                el: ".swiper-pagination-offers",
                clickable: !0
            }
        }), new Swiper(".swiper-container-transport", {
            slidesPerView: 2,
            spaceBetween: 30,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            loop: !0,
            pagination: {
                el: ".swiper-pagination",
                type: "fraction",
                clickable: !1
            },
            navigation: {
                nextEl: ".btn_next2",
                prevEl: ".btn_prev2"
            },
            breakpoints: {
                992: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    slidesPerGroup: 1
                }
            }
        }), new Swiper(".swiper-container-otziv", {
            slidesPerView: 3,
            spaceBetween: 0,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            loop: !1,
            effect: "slide",
            pagination: {
                el: ".swiper-pagination",
                clickable: !0
            },
            navigation: {
                nextEl: ".btn_next2",
                prevEl: ".btn_prev2"
            },
            breakpoints: {
                1200: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                    slidesPerGroup: 1
                },
                768: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    slidesPerGroup: 1
                }
            }
        });
        $('a[data-target^="anchor"]').click((function () {
            var e = $(this).attr("href");
            return $("html, body").animate({
                scrollTop: $(e).offset().top - 85
            }, 1500), !1
        })), $(".popup").magnificPopup({}), $('a[href^="#form"]').click((function () {
            var e = $(this).data("form"),
                t = $(this).data("text");
            $(".popup__form h3").text(t), $(".popup__form [name=admin-data").val(e)
        })), $("#form_order").click((function () {
            var e = $(this).data("form"),
                t = $(this).data("text");
            $(".popup__form h3").text(t), $(".popup__form [name=admin-data").val(e)
        })), $("#button").click((function () {
            var e = $(this).data("form");
            $(".formcalc [name=admin-data").val(e)
        })), $('a[href="#form1"]').click((function () {
            var e = $(this).data("form"),
                t = $(this).data("text");
            $(".popup__form h3").text(t), $(".popup__form [name=admin-data").val(e)
        })), $('input[type="tel"]').mask("+375 (99) 999-99-99", {
            placeholder: "X"
        }), $("form").submit((function () {
            gtag("event", "submit", {
                event_category: "form",
                event_label: "submit_form"
            });
            var e = $(this);
            return $.ajax({
                type: "POST",
                url: "/mail.php",
                data: e.serialize()
            }).done((function () {
                $(this).find("input").val(""), $(this).find("textarea").val(""), $(this).find("input:radio:cheked").val(""), $(this).find("select option:selected").val(""), $(this).find("input:checkbox:checked").val(""), $(".success").addClass("active"), setTimeout((function () {
                    $(".success").removeClass("active"), $(".mfp-close").click()
                }), 4e3)
            })), !1
        })), $(".ord__num").on("click", (function () {
            gtag("event", "click", {
                event_category: "tel",
                event_label: "click_tel"
            })
        })), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(".cre-animate").css({
            transform: "none",
            "-webkit-transform": "none",
            "-moz-transform": "none",
            "-ms-transform": "none",
            "-o-transform": "none",
            transition: "none",
            "-webkit-transition": "none",
            opacity: 1
        }).removeClass("cre-animate"), $(".btn-review").on("click", (function () {
            $(".success__box").html("<p>Ваша отзыв принят!</p>")
        })), $(".contacts__map").on("click", (function () {
            $(this).append('<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa8ec5bc47b2cab7dfb5419f6817947fd8ebfe9a9a86da050a7f57c9ef80243a0&amp;source=constructor" width="100%" height="380" frameborder="0"></iframe>')
        }))
    }));